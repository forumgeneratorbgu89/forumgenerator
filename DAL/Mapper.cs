﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using DomainEntity.Models.ForumManagement;

namespace DAL
{
    public class Mapper
    {
        
        public string connectionString { get; set; }
        readonly string Db = "db";
        readonly string EmptyDB = "emptyDB";

        public Mapper()
        {
            string startupPath1 = AppDomain.CurrentDomain.BaseDirectory;
            startupPath1 = Path.GetFullPath(Path.Combine(startupPath1, @"..\DAL\DataBases\")) ;
            connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + startupPath1 + $"{EmptyDB}.mdf;Integrated Security=True";
        }

        public void ExecuteQuery(string sql)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlCommand myCommand = new SqlCommand(sql, com);
            myCommand.ExecuteNonQuery();
            
            com.Close();
        }

        // TODO: this seems wrong. check out comments in body
        public bool ExistIDQuery(int id,string table)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader myReader = null;
            int num = -1;

            // TODO: is this right? shouldn't there be a "WHERE `ID` = id"?
            string cmd = "SELECT count(DISTINCT " + id + ") as sum FROM " + table;
            SqlCommand myCommand = new SqlCommand(cmd, com);
            myReader = myCommand.ExecuteReader();
            if (myReader.Read())
            {
                num = Int32.Parse(myReader["sum"].ToString());
            }
            
            com.Close();
            // TODO: shouldnt 0 return false as well? true should be for values greater then 0
            return num != -1 ? true : false;
        }

        public bool isSuperAdmin(string username)
        {
            return ExistsQuery(username, DbConsts.SuperAdmins_Username, DbConsts.Table_SuperAdmins);
            //return username.Equals("admin") ? true : false;
        }

        // TODO: this is the modified (fixed?) version i (rani) made:
        public bool ExistsQuery(string value, string field, string table)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            int num = -1;
            SqlDataReader reader = null;
            string cmd = "SELECT count(*) as sum FROM " + table + " WHERE " + field + " = '" + value + "'";
            SqlCommand command = new SqlCommand(cmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                num = (int)reader["sum"];
            }
            com.Close();

            if (num > 0) { return true; }
            else { return false; }
        }

        /* receives a string of sql command to execute and a name */
        public string getFieldFromQuery(string commandString, string field)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string output = null;
            SqlDataReader reader = null;
            SqlCommand command = new SqlCommand(commandString, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                output = reader[field].ToString();
            }

            com.Close();
            return output;
        }

        protected string encrypt(string password)
        {
            return EncryptDecrypt.Encrypt(password);
            //SHA256 mySHA256 = SHA256Managed.Create();
            //byte[] hashValue = System.Text.Encoding.UTF8.GetBytes(password);
            //byte[] encHashValue = mySHA256.ComputeHash(hashValue);
            //string encPW = "";
            //foreach (byte theByte in encHashValue)
            //{
            //    encPW += theByte.ToString();
            //}
            ////string encPW = System.Text.Encoding.UTF8.GetString(encHashValue);
            //int minLeng = Math.Min(50, encPW.Length) - 1;
            //encPW = encPW.Substring(0, minLeng);
            //return encPW;
        }

        protected string decrypt(string encPass)
        {
            return EncryptDecrypt.Decrypt(encPass);
        }
    }
}
