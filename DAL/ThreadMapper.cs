﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ThreadMapper : Mapper
    {
        public void DeleteThread(int threadId, int subforumId)
        {
            string sql = "DELETE FROM Threads WHERE " + DbConsts.Threads_ID + "=" + threadId + " AND " + DbConsts.Threads_SubForumID + "=" + subforumId;
            ExecuteQuery(sql);
        } 

        public void EditPost(string forumname, string subforumname, int threadid, string postpath, string poster, string newContent)
        {
            string sql = "UPDATE Posts SET " + DbConsts.Posts_Content + " = '" + newContent + "' WHERE " + 
                DbConsts.Posts_ForumName + " = '" + forumname + "' AND " + DbConsts.Posts_SubForumName + 
                " = '" + subforumname + "' AND " + DbConsts.Posts_ThreadID + " = " + threadid + " AND " + 
                DbConsts.Posts_Path + " = '" + postpath +"'";
            ExecuteQuery(sql);
        }

        public List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> GetThreadPosts(int forumid, int subforumid, int threadid)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            
            List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> allposts = new List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>>();
            string sql = "SELECT P.* FROM Posts as P LEFT JOIN Forums as F ON(P.ForumName=F.Name) LEFT JOIN SubForums as S ON(P.SubForumName=S.Name) WHERE F.ID= " + forumid + " AND S.ID= " + subforumid + " AND [ThreadID] = " + threadid;
            SqlCommand command = new SqlCommand(sql, com);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                string fname = (string)reader[DbConsts.Posts_ForumName];
                string sfname = (string)reader[DbConsts.Posts_SubForumName];
                int tid = int.Parse(reader[DbConsts.Posts_ThreadID].ToString());
                bool isinit = (int.Parse(reader[DbConsts.Posts_isInitialPost].ToString()))==1 ? true : false;
                string path = (string)reader[DbConsts.Posts_Path];
                string fathpath = (string)reader[DbConsts.Posts_FatherPath];
                string content = (string)reader[DbConsts.Posts_Content];
                string writer = (string)reader[DbConsts.Posts_WriterUsername];
                DateTime date = DateTime.Parse(reader[DbConsts.Posts_DateCreated].ToString());

                Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> post = new Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>(
                    fname,
                    sfname,
                    tid,
                    isinit,
                    path,
                    fathpath,
                    content,
                    new Tuple<string, DateTime>(
                    writer,
                    date));
                allposts.Add(post);
            }

            com.Close();
            return allposts;
        }

        public void DeletePost(string forumname, string subforumname, int threadid, string path)
        {
            // delete post
            string sql = "DELETE FROM "+DbConsts.Table_Posts +" WHERE "+DbConsts.Posts_ForumName+ " = '" + forumname + "' AND " + DbConsts.Posts_SubForumName + " = '" + subforumname + "' AND " + DbConsts.Posts_ThreadID + " = '" + threadid + "' AND " + DbConsts.Posts_Path + " = '" + path + "'";
            ExecuteQuery(sql);
            
            // delete all comments
            sql = "DELETE FROM " + DbConsts.Table_Posts + " WHERE " + DbConsts.Posts_ForumName + " = '" + forumname + "' AND " + DbConsts.Posts_SubForumName + " = '" + subforumname + "' AND " + DbConsts.Posts_ThreadID + " = '" + threadid + "' AND " + DbConsts.Posts_FatherPath + " = '" + path + "'";
            ExecuteQuery(sql);
        }

        public Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> GetPostByPath(string forumname, string subforumname, int tid, string postpath)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> post = null;

            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM Posts WHERE " + DbConsts.Posts_ForumName + " = '" + forumname + "' AND  " + DbConsts.Posts_SubForumName + "= '" + subforumname + "' AND  " + DbConsts.Posts_ThreadID + " = " + tid + " AND  " + DbConsts.Posts_Path + " = '" + postpath + "'";

            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                post = new Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>(
                    (string)reader[DbConsts.Posts_ForumName],
                    (string)reader[DbConsts.Posts_SubForumName],
                    int.Parse(reader[DbConsts.Posts_ThreadID].ToString()),
                    (int.Parse(reader[DbConsts.Posts_isInitialPost].ToString())) == 1 ? true : false,
                    (string)reader[DbConsts.Posts_Path],
                    (string)reader[DbConsts.Posts_FatherPath],
                    (string)reader[DbConsts.Posts_Content],
                    new Tuple<string, DateTime>(
                    (string)reader[DbConsts.Posts_WriterUsername],
                    DateTime.Parse(reader[DbConsts.Posts_DateCreated].ToString())));
            }

            com.Close();
            return post;
        }

        public List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> GetAllComments(string forumname, string subforumname,int threadid, string path)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> comments = new List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>>();

            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM Posts WHERE " + DbConsts.Posts_ForumName + " = '" + forumname + "' AND  " + DbConsts.Posts_SubForumName + "= '" + subforumname + "' AND  " + DbConsts.Posts_ThreadID + " = " + threadid + " AND  " + DbConsts.Posts_FatherPath + " = '" + path + "'";
            
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> post = new Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>(
                    (string)reader[DbConsts.Posts_ForumName],
                    (string)reader[DbConsts.Posts_SubForumName],
                    int.Parse(reader[DbConsts.Posts_ThreadID].ToString()),
                    (int.Parse(reader[DbConsts.Posts_isInitialPost].ToString())) == 1 ? true : false,
                    (string)reader[DbConsts.Posts_Path],
                    (string)reader[DbConsts.Posts_FatherPath],
                    (string)reader[DbConsts.Posts_Content],
                    new Tuple<string, DateTime>(
                    (string)reader[DbConsts.Posts_WriterUsername],
                    DateTime.Parse(reader[DbConsts.Posts_DateCreated].ToString())));
                comments.Add(post);
            }

            com.Close();
            return comments;
        }

        public Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> GetOpenningPost(string forumname, string subforumname, int threadid)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> post = null;
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM "+DbConsts.Table_Posts+" WHERE "+DbConsts.Posts_ForumName+"= '" + forumname + "' AND " + DbConsts.Posts_SubForumName + "= '" + subforumname + "' AND " + DbConsts.Posts_ThreadID + " = " + threadid + " AND " + DbConsts.Posts_isInitialPost + " = 1";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                post = new Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>(
                    (string)reader[DbConsts.Posts_ForumName],
                    (string)reader[DbConsts.Posts_SubForumName],
                    int.Parse(reader[DbConsts.Posts_ThreadID].ToString()),
                    (int.Parse(reader[DbConsts.Posts_isInitialPost].ToString())) == 1 ? true : false,
                    (string)reader[DbConsts.Posts_Path],
                    (string)reader[DbConsts.Posts_FatherPath],
                    (string)reader[DbConsts.Posts_Content],
                    new Tuple<string, DateTime>(
                    (string)reader[DbConsts.Posts_WriterUsername],
                    DateTime.Parse(reader[DbConsts.Posts_DateCreated].ToString())));
            }
            com.Close();
            return post;
        }

        public string ReplyToPost(string forumname, string subforumname, int threadid, string path, string creator, string text)
        {
            string newPostPath = GetNextPostID(forumname, subforumname, threadid, path);
            //string newPostPath = path + "," + nextPostCommentID;
            if (ExistsPost(path)) // moderator exists
            {
                string sql = "INSERT INTO Posts ("+ DbConsts.Posts_ForumName+ ", "+ DbConsts.Posts_SubForumName+ ", "+ 
                    DbConsts.Posts_ThreadID + ", "+ DbConsts.Posts_isInitialPost + ", "+ DbConsts.Posts_Path + ", "+ 
                    DbConsts.Posts_FatherPath + ", "+ DbConsts.Posts_Content + ", "+ DbConsts.Posts_WriterUsername + 
                    ", "+ DbConsts.Posts_DateCreated + ") VALUES ('" + forumname + "','" + subforumname + "' , " + 
                    threadid + " , 0 , '" + newPostPath + "' , '" + path + "' , '" + text + "' , '" + creator +
                    "' , '" + DateTime.Now.ToString() + "')";
                ExecuteQuery(sql);
                return newPostPath;
            }
            else throw new Exception("Post doesn't exists");
        }

        private string GetNextPostID(string forumname, string subforumname, int threadid, string path)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            string id = "";
            SqlDataReader reader = null;
            string selectCmd = "SELECT top 1 " + DbConsts.Posts_Path + " FROM " + DbConsts.Table_Posts + 
                " WHERE " + DbConsts.Posts_ForumName + "= '" + forumname + "' AND " + 
                DbConsts.Posts_SubForumName + "= '" + subforumname + "' AND " + 
                DbConsts.Posts_ThreadID + " = " + threadid + " AND " + 
                DbConsts.Posts_FatherPath + " = '" + path + "' ORDER BY " + DbConsts.Posts_Path + " DESC";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (string)reader[DbConsts.Posts_Path];
            }
            if(id.Equals(string.Empty)) // it means no brother comments so he is the first one
            {
                return path + ",0";
            }
            // 0,0
            string[] allNums = id.Split(',');
            int lastchar = Int32.Parse(allNums[allNums.Length - 1]);
            //int lastchar = (int)id.ElementAt(id.Count() - 1);
            id = id.Substring(0, id.Count()-1);
            id += ++lastchar;
            com.Close();
            return id;
        }

        private bool ExistsPost(string postStr)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int num = -1;
            SqlDataReader reader = null;
            string cmd = "SELECT count(*) as cnt FROM Posts WHERE "+DbConsts.Posts_Path+" = '" + postStr + "'";
            SqlCommand command = new SqlCommand(cmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                num = (int)reader["cnt"];
            }
            com.Close();

            return num > 0 ? true : false;
        }
        
    }
}
