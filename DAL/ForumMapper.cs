﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DAL
{
    public class ForumMapper:Mapper
    {

        public void AddForum(string name)
        {
            int id = -1;
            id = GetForumIdByName(name);
            if (id == -1)
            {
                string sql = "INSERT INTO " + DbConsts.Table_Forums + " (" + DbConsts.Forums_Name + ") VALUES ('" + name + "')";
                ExecuteQuery(sql);
            }
            else throw new Exception("Forum name " + name + " already exists");
        }

        public List<Tuple<int, string>> getAllForums() // done together
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader myReader = null;
            List<Tuple<int, string>> forums = new List<Tuple<int, string>>();

            string cmd = "SELECT * FROM " + DbConsts.Table_Forums + "";
            SqlCommand myCommand = new SqlCommand(cmd, com);
            myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                forums.Add(new Tuple<int, string>(Int32.Parse(myReader[DbConsts.Forums_ID].ToString()),
                    myReader[DbConsts.Forums_Name].ToString()));
            }
            
            com.Close();
            return forums;
        }

        public List<Tuple<string, string, string, string, DateTime?, string>> getAllSuperAdmins()
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader myReader = null;
            List<Tuple<string, string, string, string, DateTime?, string>> saList = new List<Tuple<string, string, string, string, DateTime?, string>>();

            string cmd = "SELECT * FROM "+DbConsts.Table_SuperAdmins;
            SqlCommand myCommand = new SqlCommand(cmd, com);
            myReader = myCommand.ExecuteReader();
            while (myReader.Read())
            {
                saList.Add(new Tuple<string, string, string, string, DateTime?, string>(
                    myReader[DbConsts.SuperAdmins_Username].ToString(),
                    myReader[DbConsts.SuperAdmins_Password].ToString(),
                    myReader[DbConsts.SuperAdmins_FirstName].ToString(),
                    myReader[DbConsts.SuperAdmins_LastName].ToString(),
                    DateTime.Parse(myReader[DbConsts.SuperAdmins_BirthDate].ToString()),
                    myReader[DbConsts.SuperAdmins_Email].ToString()));
            }

            com.Close();
            return saList;
        }

        public void SendFriendRequest(string sender, string reciver, string forumname)
        {
            int senderid = GetMemberIdByName(sender, forumname);
            int recieverid = GetMemberIdByName(reciver, forumname);

            if (senderid == -1)
                throw new Exception("Username " + sender + " doesn't exists");
            if (recieverid == -1)
                throw new Exception("Username " + reciver + " doesn't exists");
            else
            {
                string sql = "INSERT INTO " + DbConsts.Table_Friends + " (" + DbConsts.Friends_Username
                    + ", " + DbConsts.Friends_FriendUsername + " ,  " + DbConsts.Friends_ForumName + " ,  " + DbConsts.Friends_IsFriendRequest
                    + ") VALUES ('" + sender + "','" + reciver + "','" + forumname + "', 1)";
                ExecuteQuery(sql);
            }
        }

        public void ConfirmFriendRequest(string sender, string reciver, string forumname)
        {
            int senderid = GetMemberIdByName(sender, forumname);
            int recieverid = GetMemberIdByName(reciver, forumname);

            if (senderid == -1)
                throw new Exception("Username " + sender + " doesn't exists");
            if (recieverid == -1)
                throw new Exception("Username " + reciver + " doesn't exists");
            else
            {
                string sql = "UPDATE " + DbConsts.Table_Friends + " SET " + DbConsts.Friends_IsFriendRequest + " = 0 WHERE "
                    + DbConsts.Friends_Username + " = '" + sender + "' AND " + DbConsts.Friends_FriendUsername + " = '" + reciver + "'"
                    + " AND " + DbConsts.Friends_ForumName + " = '" + forumname + "'";
                ExecuteQuery(sql);
            }
        }

        public void AddFriend(string sender, string reciver, string forumname)
        {
            int senderid = GetMemberIdByName(sender, forumname);
            int recieverid = GetMemberIdByName(reciver, forumname);

            if (senderid == -1)
                throw new Exception("Username " + sender + " doesn't exists");
            if (recieverid == -1)
                throw new Exception("Username " + reciver + " doesn't exists");
            else
            {
                string sql = "INSERT INTO " + DbConsts.Table_Friends + " (" + DbConsts.Friends_Username
                    + ", " + DbConsts.Friends_FriendUsername + " ,  " + DbConsts.Friends_ForumName
                    + ") VALUES ('" + sender + "','" + reciver + "','" + forumname + "')";
                ExecuteQuery(sql);
            }
        }

        public List<List<string>> GetIncomingMessagesOfUser(int forumID, string usernameTarget, string userNameSender)
        {
            List<List<string>> output = new List<List<string>>();
            List<String> message;

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader reader = null;

            string forumName = null;
            string selectCmd = "SELECT  " + DbConsts.Forums_Name + " FROM  " + DbConsts.Table_Forums
                + " WHERE  " + DbConsts.Forums_ID + " = " + forumID;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                forumName = (string)reader[DbConsts.Forums_Name];
            }

            selectCmd = "SELECT " + DbConsts.Messages_Content + ", " + DbConsts.Messages_SentDate + " FROM " + DbConsts.Table_Messages
                + " WHERE " + DbConsts.Messages_ForumName + " = " + forumName + " AND " + DbConsts.Messages_SenderUsername
                + " = '" + userNameSender + "' AND " + DbConsts.Messages_RecieverUsername + " = '" + usernameTarget + "'";
            command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                message = new List<string>();
                message.Add((string)reader[DbConsts.Messages_Content]);
                message.Add((string)reader[DbConsts.Messages_SentDate]);
                output.Add(message);
            }

            com.Close();
            return output;
        }

        public List<List<string>> GetIncomingMessages(string forumName, string usernameTarget)
        {
            List<List<string>> output = new List<List<string>>();
            List<String> message;

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader reader = null;
            
            string selectCmd = "SELECT " + DbConsts.Messages_SenderUsername + ", " + DbConsts.Messages_Content + ", " + DbConsts.Messages_SentDate
                + " FROM " + DbConsts.Table_Messages 
                + " WHERE " + DbConsts.Messages_ForumName + " = '" + forumName
                + "' AND " + DbConsts.Messages_RecieverUsername + " = '" + usernameTarget + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                message = new List<string>();
                message.Add((string)reader[DbConsts.Messages_SenderUsername]);
                message.Add((string)reader[DbConsts.Messages_Content]);
                message.Add((string)reader[DbConsts.Messages_SentDate]);
                output.Add(message);
            }

            com.Close();
            return output;
        }
        public bool isUserVerified(string name, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int isVer = 0;

            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Members_isVerified + " FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_Username
                + " = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                isVer = (int)reader[DbConsts.Members_isVerified];
            }

            com.Close();
            return isVer == 1 ? true : false;
        }

        public void AddMember(int forumID, string username, string password, string firstName, string lastName, string email, string code, DateTime? birthDate, DateTime suspendExpire, bool isVerified, int offlineInteractive)
        {
            if (ExistsQuery(username, DbConsts.SuperAdmins_Username, DbConsts.Table_SuperAdmins) == false)
                password = encrypt(password);
            int id = -1;
            string forumName = GetForumNameByID(forumID);

            id = GetMemberIdByName(username, forumName);
            if (id == -1)
            {
                string sql = "INSERT INTO "+DbConsts.Table_Members+" ("+DbConsts.Members_Username+","+
                    DbConsts.Members_Password+","+ DbConsts.Members_FirstName+","+ DbConsts.Members_LastName+
                    ","+ DbConsts.Members_Email+","+ DbConsts.Members_Code+","+ DbConsts.Members_BirthDate+","+
                    DbConsts.Members_SuspendExpireDate+","+ DbConsts.Members_isVerified+","+ DbConsts.Members_ForumName +
                    "," + DbConsts.Members_OfflineInteractivness + ") VALUES ('" + username + "','" + password + "','" + firstName + "','" + lastName + 
                    "','" + email + "','" + code + "','" + (birthDate==null? "": birthDate.Value.ToString("yyyy-MM-dd")) + "','" + 
                    suspendExpire.ToString("yyyy-MM-dd HH:mm:ss") + "'," + (isVerified ? 1 : 0) + 
                    ", '" + forumName + "', " + offlineInteractive + ")";
                ExecuteQuery(sql);
            }
            else throw new Exception("Username " + username + " already exists");
        }

        public void RemoveFriend(string username1, string username2, string forumID)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            
            string sql = "DELETE FROM " + DbConsts.Table_Friends + " WHERE " + DbConsts.Friends_ForumName + " = '"
                + forumID + "' AND " + DbConsts.Friends_Username + " = '" + username1 + "' AND "
                + DbConsts.Friends_FriendUsername + " = '" + username2 + "'";
            ExecuteQuery(sql);

            sql = "DELETE FROM " + DbConsts.Table_Friends + " WHERE " + DbConsts.Friends_ForumName + " = '"
                + forumID + "' AND " + DbConsts.Friends_Username + " = '" + username2 + "' AND "
                + DbConsts.Friends_FriendUsername + " = '" + username1 + "'";
            ExecuteQuery(sql);

            com.Close();
        }

        public void AddSuperAdminAsMember(string superName, int forumId)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            string selectCmd = "SELECT * FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_Username + "='" + superName + "' AND "+
                DbConsts.Members_ForumName + "='" + GetForumNameByID(forumId) + "'";
            SqlCommand myCommand = new SqlCommand(selectCmd, com);
            SqlDataReader myReader = myCommand.ExecuteReader();

            if (myReader.Read()) { return; }
            myReader.Close();

            string password = null;
            string firstName = null;
            string lastName = null;
            string email = null;
            string code = "-1111";
            DateTime birthDate = new DateTime();
            DateTime suspendExpire = DateTime.Now;
            bool isVerified = true;
            
            selectCmd = "SELECT * FROM "+DbConsts.Table_SuperAdmins+" WHERE "+DbConsts.SuperAdmins_Username+"='" +superName+ "'";
            myCommand = new SqlCommand(selectCmd, com);
            myReader = myCommand.ExecuteReader();

            if (myReader.Read())
            {
                password = myReader[DbConsts.SuperAdmins_Password].ToString();
                firstName = myReader[DbConsts.SuperAdmins_FirstName].ToString();
                lastName = myReader[DbConsts.SuperAdmins_LastName].ToString();
                email = myReader[DbConsts.SuperAdmins_Email].ToString();
                birthDate = DateTime.Parse(myReader[DbConsts.SuperAdmins_BirthDate].ToString());
            }

            com.Close();
            
            AddMember(forumId, superName, password, firstName, lastName, email, code, birthDate, suspendExpire, isVerified, 1);
        }

        public void AddPolicy(string forumname, int maxModeratorsNumber, int minPasswordLetters, int minUserNameLetters, bool passwordRequiredUpperLowerCase, bool passwordReuqiredNumbers, int suspendModeratorMinTime, int suspendAdminMinTime, int suspendMemberMinTime, bool guestCanPost, bool guestCanStartThread, int minSeniorityForMediatorToEditAPost, int minSeniorityForMediatorToRemoveAPost, int interactive)
        {
            if(ExistsQuery(forumname, DbConsts.Policies_ForumName, DbConsts.Table_Policies))
                throw new Exception("Forum policy already exists");
            else
            {
                string sql = "insert into "+DbConsts.Table_Policies+" ("+ DbConsts.Policies_ForumName+
                    ","+ DbConsts.Policies_MaxModeratorsNumber+","+ DbConsts.Policies_MinPasswordLetters+
                    ","+ DbConsts.Policies_MinUserNameLetters+","+ DbConsts.Policies_PasswordRequiredUpperLowerCase+
                    ","+ DbConsts.Policies_PasswordReuqiredNumbers+","+ DbConsts.Policies_SuspendModeratorMinTime+
                    ","+ DbConsts.Policies_SuspendAdminMinTime+","+ DbConsts.Policies_SuspendMemberMinTime+
                    ","+ DbConsts.Policies_GuestCanPost+","+ DbConsts.Policies_GuestCanStartThread+","+
                    DbConsts.Policies_MinSeniorityForMediatorToEditAPost+","+ DbConsts.Policies_MinSeniorityForMediatorToRemoveAPost+
                    "," + DbConsts.Policies_Interactive +
                    ") VALUES ('" + forumname + "' , " + maxModeratorsNumber + " , " + minPasswordLetters +
                    " , " + minUserNameLetters + " , '" + (passwordRequiredUpperLowerCase? 1:0) + "' , '" +
                    (passwordReuqiredNumbers ? 1 : 0) + "' , " + suspendModeratorMinTime + " ,  " +
                    suspendAdminMinTime + " ,  " + suspendMemberMinTime + " ,  '" + (guestCanPost ? 1 : 0) + 
                    "' ,  '" + (guestCanStartThread ? 1 : 0) + "' ,  " + minSeniorityForMediatorToEditAPost + 
                    " ,  " + minSeniorityForMediatorToRemoveAPost + ", " + interactive + ")";
                ExecuteQuery(sql);

                int forumid = GetForumIdByName(forumname);
                int polid = GetPolicyIDByForumName(forumname);
                sql = "UPDATE " + DbConsts.Table_Forums + " SET " + DbConsts.Forums_PolicyID + " = " + polid +
                    " WHERE " + DbConsts.Forums_ID + " = " + forumid;
                ExecuteQuery(sql);
            }
            
        }

        public void RemoveAdmin(string username, string forumName)
        {
            try
            {
                string sql = "DELETE FROM " + DbConsts.Table_ForumAdmins + " WHERE " + DbConsts.ForumAdmins_Username + "='" + username + "' AND " +
                    DbConsts.ForumAdmins_ForumName + "='" + forumName + "'";
                ExecuteQuery(sql);
            }
            catch { throw new Exception("cant find this admin!"); }
        }

        public List<string> getSubForumByName(string subforumName)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> sf = new List<string>();
            SqlDataReader myReader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_SubForums + " WHERE " + DbConsts.SubForums_Name
                + " = '" + subforumName + "'";
            SqlCommand myCommand = new SqlCommand(selectCmd, com);
            myReader = myCommand.ExecuteReader();

            if (myReader.Read())
            {
                sf.Add(myReader[DbConsts.SubForums_ID].ToString());
                sf.Add(myReader[DbConsts.SubForums_Name].ToString());
                sf.Add(myReader[DbConsts.SubForums_Subject].ToString());
                sf.Add(myReader[DbConsts.SubForums_ForumID].ToString());
            }

            com.Close();
            return sf;
        }

        public void AddForumAdmin(string forumName, string username)
        {
            int usernameId = -1;
            int forumId = -1;
            usernameId = GetMemberIdByName(username, forumName);
            forumId = GetForumIdByName(forumName);
            if (usernameId == -1)
            {
                throw new Exception("Username " + username + " not exists");
            }
            if(forumId == -1)
            {
                throw new Exception("Forum name " + forumName + " doesn't exists");
            }
            else
            {
                string sql = "INSERT INTO "+DbConsts.Table_ForumAdmins+ " ("+DbConsts.ForumAdmins_Username+", "+DbConsts.ForumAdmins_ForumName+") VALUES ('" + username + "','" + forumName + "')";
                ExecuteQuery(sql);
            }
        }


        public bool isUserAdmin(string forumName, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) AS cnt FROM " + DbConsts.Table_ForumAdmins + " WHERE " + DbConsts.ForumAdmins_ForumName
                + " = '" + forumName + "' AND " + DbConsts.ForumAdmins_Username
                + "='" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["cnt"];
            }

            com.Close();
            return id != -1 ? true : false;
        }

        public bool isModerator(string subForumName, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) AS cnt FROM " + DbConsts.Table_Moderators + " WHERE " + DbConsts.Moderators_Username
                + "='" + username + "' AND " + DbConsts.Moderators_SubForumName + " = '" + subForumName + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["cnt"];
            }

            com.Close();
            return id != -1 ? true : false;
        }

        public int GetPolicyIDByForumName(string forumname)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Policies_ID + " FROM " + DbConsts.Table_Policies + " WHERE " + DbConsts.Policies_ForumName 
                + " = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read() && !reader.IsDBNull(0))
            {
                id = (int)reader[DbConsts.Policies_ID];
            }

            com.Close();
            return id;
        }

        public void ChangeForumName(string oldName,string newName)
        {
            int id = -1;
            id = GetForumIdByName(oldName);
            if (id != -1)
            {
                string sql = "UPDATE " + DbConsts.Table_Forums + " SET " + DbConsts.Forums_Name + " = '" + newName 
                    + "' WHERE " + DbConsts.Forums_Name + " = '" + oldName + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Forum name " + oldName + " doesn't exists");
        }

        public List<string> GetSubForumModerators(string subforumName)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> moderators = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_Moderators + " WHERE " + DbConsts.Moderators_SubForumName 
                + " = '" + subforumName + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                moderators.Add((string)reader[DbConsts.Moderators_Username]);
            }

            com.Close();
            return moderators;
        }

        public void AddSubForum(string forumName, string subforumName, string subForumSubject, List<string> moderators)
        {
            int id = -1;
            id = GetSubForumIdByName(subforumName, forumName);
            if (id == -1)
            {
                string sql = "INSERT INTO " + DbConsts.Table_SubForums + " (" + DbConsts.SubForums_Name + ", " + DbConsts.SubForums_Subject +
                    ", " + DbConsts.SubForums_ForumID + ") VALUES ('" + subforumName + "','" + subForumSubject + "','" + 
                    GetForumIdByName(forumName) + "')";
                ExecuteQuery(sql);
                
                foreach (string moderator in moderators)
                {
                    AddModeratorToSubForum(moderator,subforumName,forumName);
                }
            }
            else throw new Exception("Sub Forum name " + subforumName + " already exists");
        }

        public void AddModeratorToSubForum(string moderator,string subforumName, string forumName)
        {
            int id = -1;
            id = GetMemberIdByName(moderator, forumName);
            if (id != -1) // moderator exists
            {
                string sql = "INSERT INTO " + DbConsts.Table_Moderators + " (" + DbConsts.Moderators_Username +
                    ", " + DbConsts.Moderators_SubForumName + ", " + DbConsts.Moderators_PromotedDate + ", " + DbConsts.Moderators_ForumName
                    + ") VALUES ('" + moderator + "','" + subforumName + "','" + DateTime.Now.Date.ToString() + "','" + forumName
                    + "')";
                ExecuteQuery(sql);
            }
            else throw new Exception("Moderator " + moderator + " doesn't exists in members");
        }

        public void signUpSuperAdmin(string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            password = encrypt(password);
            string sql = "INSERT INTO " + DbConsts.Table_SuperAdmins +
                " (" + DbConsts.SuperAdmins_Username + ", " + DbConsts.SuperAdmins_Password + ", " + 
                DbConsts.SuperAdmins_FirstName+ ", " + DbConsts.SuperAdmins_LastName+", " + DbConsts.SuperAdmins_Email+ 
                ", " + DbConsts.SuperAdmins_BirthDate+ ") VALUES ('" + username + "','" + password + "','"+firstName+
                "', '"+lastName+"', '"+eMail+"','"+birthDate.ToString()+"')";
            ExecuteQuery(sql);
        }

        public void AddModeratorToSubForum(string moderator, int subforumid, string forumName)
        {
            string subforumName = GetSubForumNameById(subforumid);
            if(subforumName.Equals(""))
                throw new Exception("Sub forum id " + subforumid + " doesn't exists");
            int id = -1;
            id = GetMemberIdByName(moderator, forumName);
            if (id != -1) // moderator exists
            {
                string sql = "INSERT INTO " + DbConsts.Table_Moderators + " (" + DbConsts.Moderators_Username 
                    + ", " + DbConsts.Moderators_SubForumName + ") VALUES ('" + moderator + "','" + subforumName + "')";
                ExecuteQuery(sql);
            }
            else throw new Exception("Moderator " + moderator + " doesn't exists in members");
        }

        private int GetSubForumIdByName(string subforumName, string forumName)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.SubForums_ID + " FROM " + DbConsts.Table_SubForums + " where " 
                + DbConsts.SubForums_Name + " = '" + subforumName + "'" + " AND " + DbConsts.SubForums_ForumID + " = "
                + GetForumIdByName(forumName);
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader[DbConsts.SubForums_ID];
            }

            com.Close();
            return id;
        }

        public List<string> GetAllSubForumsModeratedBy(string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> subforums = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Moderators_SubForumName + " FROM " + DbConsts.Table_Moderators + " WHERE " 
                + DbConsts.Moderators_Username + " = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                subforums.Add((string)reader[DbConsts.Moderators_SubForumName]);
            }

            com.Close();
            return subforums;
        }

        public bool LegalMemberSuspensionTime(int policyid, int days)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int num = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Policies_SuspendMemberMinTime + " FROM " + DbConsts.Table_Policies
                + " WHERE " + DbConsts.Policies_ID + " = " + policyid;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                num = (int)reader[DbConsts.Policies_SuspendMemberMinTime];
            }

            com.Close();
            return days < num ? false : true;
        }

        public List<string> GetPolicy(int id)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> policy = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_Policies + " WHERE " + DbConsts.Policies_ID + " = " + id;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                policy.Add(reader[DbConsts.Policies_ID].ToString());
                policy.Add(reader[DbConsts.Policies_MaxModeratorsNumber].ToString());
                policy.Add(reader[DbConsts.Policies_MinPasswordLetters].ToString());
                policy.Add(reader[DbConsts.Policies_MinUserNameLetters].ToString());
                policy.Add(reader[DbConsts.Policies_PasswordRequiredUpperLowerCase].ToString());
                policy.Add(reader[DbConsts.Policies_PasswordReuqiredNumbers].ToString());
                policy.Add(reader[DbConsts.Policies_SuspendModeratorMinTime].ToString());
                policy.Add(reader[DbConsts.Policies_SuspendAdminMinTime].ToString());
                policy.Add(reader[DbConsts.Policies_SuspendMemberMinTime].ToString());
                policy.Add(reader[DbConsts.Policies_GuestCanPost].ToString());
                policy.Add(reader[DbConsts.Policies_GuestCanStartThread].ToString());
                policy.Add(reader[DbConsts.Policies_MinSeniorityForMediatorToEditAPost].ToString());
                policy.Add(reader[DbConsts.Policies_MinSeniorityForMediatorToRemoveAPost].ToString());
                policy.Add(reader[DbConsts.Policies_Interactive].ToString());
            }

            com.Close();
            return policy;
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<string[]> notifications = new List<string[]>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_Notifications + " WHERE " + DbConsts.Notifications_ForumName
                + " = '" + forumName + "' AND " + DbConsts.Notifications_Username + " = '" + userName + "'";
            if (getUnreadNotifications)
            {
                selectCmd += " AND " + DbConsts.Notifications_IsRead + " = 0";
            }
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                string[] notification = new string[2];
                notification[0] = ((string)reader[DbConsts.Notifications_Title]);
                notification[1] = ((string)reader[DbConsts.Notifications_Message]);
                notifications.Add(notification);
            }

            com.Close();

            com = new SqlConnection(connectionString);
            com.Open();

            selectCmd = "UPDATE " + DbConsts.Table_Notifications + " SET " + DbConsts.Notifications_IsRead
                + " = 1 WHERE " + DbConsts.Notifications_ForumName + " = '" + forumName + "' AND " 
                + DbConsts.Notifications_Username + " = '" + userName + "'";


            /*selectCmd = "DELETE FROM " + DbConsts.Table_Notifications + " WHERE " + DbConsts.Notifications_ForumName
                + " = '" + forumName + "' AND " + DbConsts.Notifications_Username + " = '" + userName + "'";*/

            command = new SqlCommand(selectCmd, com);
            ExecuteQuery(selectCmd);

            return notifications;
        }

        public string GetLastForumName()
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string forum = "";
            SqlDataReader reader = null;
            string selectCmd = "select top 1 " + DbConsts.Forums_Name + " from " + DbConsts.Table_Forums + " order by " 
                + DbConsts.Forums_ID + " desc";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                forum = (string)reader[DbConsts.Forums_Name];
            }

            com.Close();
            return forum;
        }

        public string getSecondLastForumName()
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string forum = "";
            string forumSecond = "";
            SqlDataReader reader = null;
            string selectCmd = "select top 2 " + DbConsts.Forums_Name + " from " + DbConsts.Table_Forums + " order by "
                + DbConsts.Forums_ID + " desc";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                forum = (string)reader[DbConsts.Forums_Name];
            }
            //second forum is:
            if (reader.Read())
            {
                forumSecond = (string)reader[DbConsts.Forums_Name];
            }

            com.Close();
            return forumSecond;
        }

        public List<List<string>> GetNotifications(string forumname, string userName)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<List<string>> notifications = new List<List<string>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_Notifications + " WHERE " + DbConsts.Notifications_ForumName
                + " = '" + forumname + "' AND " + DbConsts.Notifications_Username + " = '" + userName + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                List<string> notification = new List<string>();
                notification.Add((string)reader[DbConsts.Notifications_ID]);
                notification.Add((string)reader[DbConsts.Notifications_ForumName]);
                notification.Add((string)reader[DbConsts.Notifications_Username]);
                notification.Add((string)reader[DbConsts.Notifications_Title]);
                notification.Add((string)reader[DbConsts.Notifications_Message]);
                notifications.Add(notification);
            }

            com.Close();
            return notifications;
        }

        public bool IsUserSuperAdmin(string username)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            int num = 0;
            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) as num FROM " + DbConsts.Table_SuperAdmins + " WHERE " + DbConsts.SuperAdmins_Username
                + " = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();
            if (reader.Read())
            {
                num = (int)reader["num"];
            }

            return num != 0;
        }

        public string getLastSubForumName(string forumname)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string subforum = "";
            SqlDataReader reader = null;
            string selectCmd = "select top 1 " + DbConsts.SubForums_Name + " from " + DbConsts.Table_SubForums
                + " WHERE " + DbConsts.SubForums_ForumID + " = '" + GetForumIdByName(forumname) + "' order by " + DbConsts.SubForums_ID 
                + " desc";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                subforum = (string)reader[DbConsts.SubForums_Name];
            }

            com.Close();
            return subforum;
        }

        public List<List<string>> GetAllPostsOfUser(string forumname, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<List<string>> posts = new List<List<string>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM "+DbConsts.Table_Posts+ " WHERE "+DbConsts.Posts_ForumName+" = '" + 
                forumname + "' AND "+DbConsts.Posts_WriterUsername+" = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                List<string> post = new List<string>();
                post.Add((string)reader[DbConsts.Posts_Path]);
                post.Add((string)reader[DbConsts.Posts_Content]);
                post.Add((string)reader[DbConsts.Posts_WriterUsername]);
                post.Add((string)reader[DbConsts.Posts_DateCreated]);
                posts.Add(post);
            }

            com.Close();
            return posts;
        }

        public List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> GetForumAdmins(string forumname)
        {
            List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> output =
                new List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>>();

            string username = null;
            string password = null;
            string firstName = null;
            string lastName = null;
            string email = null;
            string code = "-1111";
            DateTime birthDate = new DateTime();
            DateTime suspendExpire = DateTime.Now;
            bool isVerified = false;
            int offlineInteractive = 1;

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            
            SqlDataReader reader = null;

            string selectCmd = "SELECT m.* FROM "+DbConsts.Table_Members+" AS m JOIN "+DbConsts.Table_ForumAdmins +
                " AS fa ON m." + DbConsts.Members_Username + "= fa." + DbConsts.ForumAdmins_Username +" WHERE m."+
                DbConsts.Members_ForumName+" = '"+forumname+"' AND fa."+DbConsts.ForumAdmins_ForumName+"= '"+forumname+"'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                username = reader[DbConsts.Members_Username].ToString();
                password = reader[DbConsts.Members_Password].ToString();
                firstName = reader[DbConsts.Members_FirstName].ToString();
                lastName = reader[DbConsts.Members_LastName].ToString();
                email = reader[DbConsts.Members_Email].ToString();
                code = reader[DbConsts.Members_Code].ToString();
                birthDate = DateTime.Parse(reader[DbConsts.Members_BirthDate].ToString());
                suspendExpire = DateTime.Parse(reader[DbConsts.Members_SuspendExpireDate].ToString());
                isVerified = (int.Parse(reader[DbConsts.Members_isVerified].ToString()) == 0) ? false : true;
                offlineInteractive = (int.Parse(reader[DbConsts.Members_OfflineInteractivness].ToString()));
                output.Add(new Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>(username,
                    password, firstName, lastName, email, code, birthDate, new Tuple<DateTime, bool, int>(suspendExpire, isVerified, offlineInteractive)));
            }

            com.Close();
            return output;
        }

        public string GetUserEmail(string forumname, string username)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string email = "";
            SqlDataReader reader = null;
            string selectCmd = "select " + DbConsts.Members_Email + " from " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_Username
                + " = '" + username + "' AND " + DbConsts.Members_ForumName + " = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                email = (string)reader[DbConsts.Members_Email];
            }

            com.Close();
            return email;
        }

        public string GetLastUsername()
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string user = "";
            SqlDataReader reader = null;
            string selectCmd = "select top 1 " + DbConsts.Members_Username + " from " + DbConsts.Table_Members + " order by " + DbConsts.Members_ID
                + " desc";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                user = (string)reader[DbConsts.Members_Username];
            }

            com.Close();
            return user;
        }

        public void DeleteForumByName(string name)
        {
            int id = -1;
            id = GetForumIdByName(name);
            if (id != -1)
            {
                string sql = "DELETE FROM "+DbConsts.Table_Forums+" WHERE "+DbConsts.Forums_Name+"='" + name + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Forum name " + name + " not exists");
        }

        public bool isUserRegisteredToForum(string forumName, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) AS cnt FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_ForumName
                + " = " + forumName + "' AND " + DbConsts.Members_Username + " = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["cnt"];
            }

            com.Close();
            return id != -1 ? true : false;
        }

        public List<string> GetAllModerators(string forumname)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<string> mods = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT "+DbConsts.Moderators_Username+ " FROM "+DbConsts.Table_Moderators+
                " WHERE "+DbConsts.Moderators_ForumName+"= '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                mods.Add((string)reader[DbConsts.Moderators_Username]);
            }

            com.Close();
            return mods;
        }

        public List<string> GetAllFriendRequests(string username)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<string> friends = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Friends_Username + " FROM " + DbConsts.Table_Friends
                + " WHERE " + DbConsts.Friends_FriendUsername + " = '" + username + "' AND " + DbConsts.Friends_IsFriendRequest + " = 1";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                friends.Add((string)reader[DbConsts.Friends_Username]);
            }

            com.Close();
            return friends;
        }

        public List<string> GetAllFriends(string username)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<string> friends = new List<string>();
            SqlDataReader reader = null;
            string selectCmd1 = "SELECT " + DbConsts.Friends_FriendUsername + " FROM " + DbConsts.Table_Friends
                + " WHERE " + DbConsts.Friends_Username + " = '" + username + "' AND " + DbConsts.Friends_IsFriendRequest + " = 0";

            string selectCmd2 = "SELECT " + DbConsts.Friends_Username + " FROM " + DbConsts.Table_Friends
                + " WHERE " + DbConsts.Friends_FriendUsername + " = '" + username + "' AND " + DbConsts.Friends_IsFriendRequest + " = 0";
            SqlCommand command = new SqlCommand(selectCmd1, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                friends.Add((string)reader[DbConsts.Friends_FriendUsername]);
            }

            com.Close();
            com.Open();

            SqlCommand command2 = new SqlCommand(selectCmd2, com);

            SqlDataReader reader2 = null;

            reader2 = command2.ExecuteReader();

            while (reader2.Read())
            {
                friends.Add((string)reader2[DbConsts.Friends_Username]);
            }

            com.Close();
            return friends;
        }

        public List<Tuple<int, string, string, int>> GetAllSubForumsByForumName(string forumName)
        {
            List<Tuple<int, string, string, int>> output = new List<Tuple<int, string, string, int>>();

            int id = -1;
            string name = null;
            string subject = null;
            int forumId = GetForumIdByName(forumName);

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_SubForums +" WHERE " + DbConsts.SubForums_ForumID + " = " + forumId;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                id = int.Parse(reader[DbConsts.SubForums_ID].ToString());
                name = reader[DbConsts.SubForums_Name].ToString();
                subject = reader[DbConsts.SubForums_Subject].ToString();
                forumId = int.Parse(reader[DbConsts.SubForums_ForumID].ToString());
                output.Add(new Tuple<int, string, string, int>(id, name, subject, forumId));
            }

            com.Close();
            return output;
        }

        public void DeleteSubForumByName(string subForumName, string forumName)
        {
            int id = -1;
            id = GetSubForumIdByName(subForumName, forumName);
            if (id != -1)
            {
                string sql = "DELETE FROM " + DbConsts.Table_SubForums + " WHERE "
                    + DbConsts.SubForums_Name + " = '" + subForumName + "' AND " + DbConsts.SubForums_ForumID
                    + " = " + GetForumIdByName(forumName);
                ExecuteQuery(sql);
            }
            else throw new Exception("Forum name " + subForumName + " not exists");
        }

        public void setPolicy(int policyid, int maxModeratorsNumber, int minPasswordLetters, int minUserNameLetters, bool passwordRequiredUpperLowerCase, bool passwordReuqiredNumbers, int suspendModeratorMinTime, int suspendAdminMinTime, int suspendMemberMinTime, bool guestCanPost, bool guestCanStartThread, int minSeniorityForMediatorToEditAPost, int minSeniorityForMediatorToRemoveAPost, int interactive)
        {
            string sql = "UPDATE "+DbConsts.Table_Policies+" SET "+ DbConsts.Policies_MaxModeratorsNumber + " = " + maxModeratorsNumber + " , " 
                + DbConsts.Policies_MinPasswordLetters + " = " + minPasswordLetters + " , " + DbConsts.Policies_MinUserNameLetters + "= " + minUserNameLetters + " , " + 
                DbConsts.Policies_PasswordRequiredUpperLowerCase + " = '" + ((passwordRequiredUpperLowerCase == true) ? 1 : 0) + "' , " + DbConsts.Policies_PasswordReuqiredNumbers + 
                " = '" + ((passwordReuqiredNumbers == true) ? 1 : 0) + "' , " + DbConsts.Policies_SuspendModeratorMinTime + " = " + suspendModeratorMinTime + " , " + 
                DbConsts.Policies_SuspendAdminMinTime + " = " + suspendAdminMinTime + " , " + DbConsts.Policies_SuspendMemberMinTime + " = " + suspendMemberMinTime +
                " , " + DbConsts.Policies_GuestCanPost + " = '" + ((guestCanPost == true) ? 1 : 0) + "' , " + DbConsts.Policies_GuestCanStartThread + " = '" + ((guestCanStartThread == true) ? 1 : 0) +
                "' , " + DbConsts.Policies_MinSeniorityForMediatorToEditAPost + " = " + minSeniorityForMediatorToEditAPost + " , " + 
                DbConsts.Policies_MinSeniorityForMediatorToRemoveAPost + " = " + minSeniorityForMediatorToRemoveAPost + ", " +
                DbConsts.Policies_Interactive + " = " + interactive +
                " WHERE " + DbConsts.Policies_ID + " = " + policyid;
            ExecuteQuery(sql);
        }

        private string GetSubForumNameById(int subforumid)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            string name="";

            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.SubForums_Name + " FROM " + DbConsts.Table_SubForums 
                + " WHERE " + DbConsts.SubForums_ID + " = " + subforumid;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                name = (string)reader[DbConsts.SubForums_Name];
            }

            com.Close();
            return name;
        }

        public List<string> getSubForumByID(int subForumId)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> sf = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_SubForums + " WHERE " + DbConsts.SubForums_ID + " = " + subForumId;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                sf.Add((string)reader[DbConsts.SubForums_ID]);
                sf.Add((string)reader[DbConsts.SubForums_Name]);
                sf.Add((string)reader[DbConsts.SubForums_Subject]);
                sf.Add((string)reader[DbConsts.SubForums_ForumID]);
            }

            com.Close();
            return sf;
        }

        public List<string> GetMemberByName(string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            List<string> member = new List<string>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_Username + "  = '" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                member.Add(((int)reader[DbConsts.Members_ID]).ToString());
                member.Add((string)reader[DbConsts.Members_Username]);
                member.Add((string)reader[DbConsts.Members_Password]);
                member.Add((string)reader[DbConsts.Members_FirstName]);
                member.Add((string)reader[DbConsts.Members_LastName]);
                member.Add((string)reader[DbConsts.Members_Email]);
                member.Add((string)reader[DbConsts.Members_Code]);
                member.Add((string)reader[DbConsts.Members_BirthDate]);
                member.Add((string)reader[DbConsts.Members_SuspendExpireDate].ToString());
                member.Add((string)reader[DbConsts.Members_isVerified].ToString());
                member.Add((string)reader[DbConsts.Members_ForumName]);
                member.Add((string)reader[DbConsts.Members_OfflineInteractivness]);
            }

            com.Close();
            return member;
        }

        public void RemoveMember(string username, string forumName)
        {
            int id = -1;
            id = GetMemberIdByName(username, forumName);
            if (id != -1)
            {
                string sql = "DELETE FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_Username + "='" + username + "' AND " +
                    DbConsts.Members_ForumName + "='" + forumName + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Username " + username + " not exists");
        }

        public int GetMemberIdByName(string username, string forumName)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT "+DbConsts.Members_ID+" FROM "+DbConsts.Table_Members+" WHERE "+
                DbConsts.Members_Username+"='" + username + "' AND "+DbConsts.Members_ForumName+"='"+
                forumName+"'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader[DbConsts.Members_ID];
            }

            com.Close();
            return id;
        }

        public string GetForumNameByID(int id)
        {

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            string name = "";
            SqlDataReader reader = null;
            string selectCmd = "SELECT " + DbConsts.Forums_Name + " FROM " + DbConsts.Table_Forums + " where " + DbConsts.Forums_ID 
                + " = " + id;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                name = (string)reader[DbConsts.Forums_Name];
            }

            com.Close();
            return name;
        }

        public int GetForumIdByName(string name)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT "+DbConsts.Forums_ID+" FROM "+DbConsts.Table_Forums+" WHERE Name = '" + name + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader[DbConsts.Forums_ID];
            }

            com.Close();
            return id;
        }

        public List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> GetAllMembers(string forumName)
        {
            List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> output = 
                new List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>>();
            
            string username = null;
            string password = null;
            string firstName = null;
            string lastName = null;
            string email = null;
            string code = "-1111";
            DateTime birthDate = new DateTime();
            DateTime suspendExpire = DateTime.Now;
            bool isVerified = false;
            int offlineInteractive = 1;

            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            string selectCmd = "SELECT * FROM " + DbConsts.Table_Members + " WHERE " + DbConsts.Members_ForumName + "='" + forumName + "'";
            SqlCommand myCommand = new SqlCommand(selectCmd, com);
            SqlDataReader myReader = myCommand.ExecuteReader();

            while (myReader.Read())
            {
                username = myReader[DbConsts.Members_Username].ToString();
                password = myReader[DbConsts.Members_Password].ToString();
                firstName = myReader[DbConsts.Members_FirstName].ToString();
                lastName = myReader[DbConsts.Members_LastName].ToString();
                email = myReader[DbConsts.Members_Email].ToString();
                code = myReader[DbConsts.Members_Code].ToString();
                birthDate = DateTime.Parse(myReader[DbConsts.Members_BirthDate].ToString());
                suspendExpire = DateTime.Parse(myReader[DbConsts.Members_SuspendExpireDate].ToString());
                isVerified = (int.Parse(myReader[DbConsts.Members_isVerified].ToString()) == 0) ? false : true;
                offlineInteractive = (int.Parse(myReader[DbConsts.Members_OfflineInteractivness].ToString()));
                output.Add(new Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>(username,
                    password, firstName, lastName, email, code, birthDate, new Tuple<DateTime, bool, int>(suspendExpire, isVerified, offlineInteractive)));
            }

            com.Close();
            return output;
        }

        public int GetNumOfPosts(string forumname)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int num = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) as num FROM " + DbConsts.Table_Posts + " WHERE " + DbConsts.Posts_ForumName + " = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                num = (int)reader["num"];
            }

            com.Close();
            return num;
        }
    }
}
