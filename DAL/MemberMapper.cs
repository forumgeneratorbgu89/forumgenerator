﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;

namespace DAL
{
    public class MemberMapper:Mapper
    {
        public void Verify(string username)
        {
            int id = -1;
            id = GetMemberIdByName(username);
            if (id != -1)
            {
                //string sql = "UPDATE `Members` SET `isVerified` = \"1\" WHERE `ID` = \"" + id + "\"";
                string sql = @"UPDATE " + DbConsts.Table_Members + " SET " + DbConsts.Members_isVerified +
                    " = 1 WHERE " + DbConsts.Members_Username + " = '" + username + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Username " + username + " not exists");
        }

        private int GetMemberIdByName(string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT ID FROM Members where Username='" + username + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["ID"];
            }

            com.Close();
            return id;
        }

        public void UpdateSuspendDate(string username, DateTime suspendExpire)
        {
            int id = -1;
            id = GetMemberIdByName(username);
            if (id != -1)
            {
                string sql = "UPDATE " + DbConsts.Table_Members + " SET " + DbConsts.Members_SuspendExpireDate + " = '"
                    + suspendExpire.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE " + DbConsts.Members_Username + " = '" + username + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Username " + username + " not exists");
        }

        public Nullable<DateTime> GetSuspendDate(string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            string susDate;
            Nullable<DateTime> suspendDate = null;
            SqlDataReader reader = null;
            string selectCmd = "SELECT `Suspend Expire Date` FROM `Members` WHERE `Username`=\"" + username + "\"";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                susDate = (string)reader["Suspend Expire Date"];
                suspendDate = Convert.ToDateTime(susDate);
            }

            com.Close();
            return suspendDate;
        }

        public void AddMessage(string forumName, string senderUserName, string recieverUserName, string text, bool isFriend)
        {
            int isFriendsInt = isFriend ? 1 : 0;
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            string insertCmd = @"INSERT INTO " + DbConsts.Table_Messages 
                + " (" + DbConsts.Messages_ForumName + ", " + DbConsts.Messages_SenderUsername +
                ", " + DbConsts.Messages_RecieverUsername + ", " + DbConsts.Messages_Content
                + ", " + DbConsts.Messages_SentDate + ", " + DbConsts.Messages_ForOthers + ") VALUES ('" + forumName + "', '"
                + senderUserName + "', '" + recieverUserName + "', '" + text + "', '" + DateTime.Now.ToString() + "', " + isFriendsInt + ");";

            SqlCommand command = new SqlCommand(insertCmd, com);
            command.ExecuteNonQuery();
            com.Close();
        }

        public void saveNotification(string forumName, string username, string title, string s, int isRead)
        {
            string sql = "INSERT INTO " + DbConsts.Table_Notifications
                        + " (" + DbConsts.Notifications_ForumName + ", "
                        + DbConsts.Notifications_Username + ", " + DbConsts.Notifications_Title + ", "
                        + DbConsts.Notifications_Message + ", " + DbConsts.Notifications_IsRead + ") VALUES('" + forumName
                        + "', '" + username + "', '" + title + "', '" + s + "', " + isRead + ")";
            ExecuteQuery(sql);
        }

        public bool changePassword(string forumId, string username, string newPass)
        {
            string sql = "UPDATE " + DbConsts.Table_Members + " SET " + DbConsts.Members_Password + " = '" 
                + EncryptDecrypt.Encrypt(newPass) + "' WHERE " +
                DbConsts.Members_ForumName + " = '" + forumId + "' AND " + DbConsts.Members_Username + " = '" + username + "'";
            ExecuteQuery(sql);
            return true;
        }
    }
}
