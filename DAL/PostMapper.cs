﻿using System;
using System.Data.SqlClient;

namespace DAL
{
    public class PostMapper : Mapper
    {
        public void deletePost(int postID, int threadID)
        {
            string sql = "DELETE FROM `Posts` WHERE (`ID` = \"" + postID + "\" AND `Thread ID` = \"" + threadID + "\")";
            ExecuteQuery(sql);
        }

        // TODO: this is a duplicate from SubForumMapper
        public int GetThreadID(string threadheader)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT `ID` FROM `Threads` WHERE `Header`=\"" + threadheader + "\"";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["ID"];
            }

            com.Close();
            return id;
        }

        public void addPost(int postId, Func<string> username, string postContent,
            int fatherPostId, int threadId)
        {
            string ID = postId.ToString();
            string Content = postContent;
            string Writer_Username = username();
            string is_Initial_Post = "0";
            string Thread_ID = threadId.ToString();
            string Father_Post_ID = fatherPostId.ToString();
            string Sub_Forum_Name = getSubforumFromThreadId(threadId);
            string Forum_Name = getForumFromThreadId(threadId);
            string fieldNames = "`ID`, `Content`, `Writer Username`, `is Initial Post`, `Thread ID`, `Father Post ID`, `Sub Forum Name`, `Forum Name`";
            string values = "\"" + ID + "\", \"" + Content + "\", \"" + Writer_Username + "\", \"" + is_Initial_Post + "\", \"" + Thread_ID + "\", \"" + Father_Post_ID + "\", \"" + Sub_Forum_Name + "\", \"" + Forum_Name + "\", \"";

            string sql = "INSERT INTO `Posts` ("+fieldNames+") VALUES ("+values+")";

            ExecuteQuery(sql);
        }

        private string getSubforumFromThreadId(int threadId)
        {
            string sql = "SELECT `Sub Forum Name` FROM `Threads` WHERE `ID`=\""+threadId+"\"";
            return "";// getFieldFromQuery(sql, "Sub Forum Name");
        }

        private string getForumFromThreadId(int threadId)
        {
            string sql = "SELECT `Forum Name` FROM `Threads` WHERE `ID`=\"" + threadId + "\"";
            return "";// getFieldFromQuery(sql, "Forum Name");
        }
    }
}
