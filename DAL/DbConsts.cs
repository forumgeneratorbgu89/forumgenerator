﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class DbConsts
    {
        public const string Table_ForumAdmins = "ForumAdmins";
        public const string ForumAdmins_ID = "ID";
        public const string ForumAdmins_Username = "Username";
        public const string ForumAdmins_ForumName = "ForumName";

        public const string Table_Forums = "Forums";
        public const string Forums_ID = "ID";
        public const string Forums_Name = "Name";
        public const string Forums_PolicyID = "PolicyID";

        public const string Table_Friends = "Friends";
        public const string Friends_Username = "Username";
        public const string Friends_FriendUsername = "FriendUsername";
        public const string Friends_ForumName = "ForumName";
        public const string Friends_IsFriendRequest = "IsFriendRequest";

        public const string Table_Members = "Members";
        public const string Members_ID = "ID";
        public const string Members_Username = "Username";
        public const string Members_Password = "Password";
        public const string Members_FirstName = "FirstName";
        public const string Members_LastName = "LastName";
        public const string Members_Email = "Email";
        public const string Members_Code = "Code";
        public const string Members_BirthDate = "BirthDate";
        public const string Members_SuspendExpireDate = "SuspendExpireDate";
        public const string Members_isVerified = "isVerified";
        public const string Members_ForumName = "ForumName";
        public const string Members_OfflineInteractivness = "OfflineInteractivness";

        public const string Table_Messages = "Messages";
        public const string Messages_ForumName = "ForumName";
        public const string Messages_SenderUsername = "SenderUsername";
        public const string Messages_RecieverUsername = "RecieverUsername";
        public const string Messages_Content = "Content";
        public const string Messages_SentDate = "SentDate";
        public const string Messages_ForOthers = "ForOthers";

        public const string Table_Moderators = "Moderators";
        public const string Moderators_ID = "ID";
        public const string Moderators_Username = "Username";
        public const string Moderators_SubForumName = "SubForumName";
        public const string Moderators_ForumName = "ForumName";
        public const string Moderators_PromotedDate = "PromotedDate";
        public const string Moderators_TimeLimit = "TimeLimit";

        public const string Table_Notifications = "Notifications";
        public const string Notifications_ID = "ID";
        public const string Notifications_ForumName = "ForumName";
        public const string Notifications_Username = "Username";
        public const string Notifications_Title = "Title";
        public const string Notifications_Message = "Message";
        public const string Notifications_IsRead = "IsRead"; 

        public const string Table_Policies = "Policies";
        public const string Policies_ID = "ID";
        public const string Policies_ForumName = "ForumName";
        public const string Policies_MaxModeratorsNumber = "MaxModeratorsNumber";
        public const string Policies_MinPasswordLetters = "MinPasswordLetters";
        public const string Policies_MinUserNameLetters = "MinUserNameLetters";
        public const string Policies_PasswordRequiredUpperLowerCase = "PasswordRequiredUpperLowerCase";
        public const string Policies_PasswordReuqiredNumbers = "PasswordReuqiredNumbers";
        public const string Policies_SuspendModeratorMinTime = "SuspendModeratorMinTime";
        public const string Policies_SuspendAdminMinTime = "SuspendAdminMinTime";
        public const string Policies_SuspendMemberMinTime = "SuspendMemberMinTime";
        public const string Policies_GuestCanPost = "GuestCanPost";
        public const string Policies_GuestCanStartThread = "GuestCanStartThread";
        public const string Policies_MinSeniorityForMediatorToEditAPost = "MinSeniorityForMediatorToEditAPost";
        public const string Policies_MinSeniorityForMediatorToRemoveAPost = "MinSeniorityForMediatorToRemoveAPost";
        public const string Policies_Interactive = "Interactive";

        public const string Table_Posts = "Posts";
        public const string Posts_ForumName = "ForumName";
        public const string Posts_SubForumName = "SubForumName";
        public const string Posts_ThreadID = "ThreadID";
        public const string Posts_isInitialPost = "isInitialPost";
        public const string Posts_Path = "Path";
        public const string Posts_FatherPath = "FatherPath";
        public const string Posts_Content = "Content";
        public const string Posts_WriterUsername = "WriterUsername";
        public const string Posts_DateCreated = "DateCreated";

        public const string Table_SubForums = "SubForums";
        public const string SubForums_ID = "ID";
        public const string SubForums_Name = "Name";
        public const string SubForums_Subject = "Subject";
        public const string SubForums_ForumID = "ForumID";

        public const string Table_SuperAdmins = "SuperAdmins";
        public const string SuperAdmins_Username = "Username";
        public const string SuperAdmins_Password = "Password";
        public const string SuperAdmins_FirstName = "FirstName";
        public const string SuperAdmins_LastName = "LastName";
        public const string SuperAdmins_Email = "Email";
        public const string SuperAdmins_BirthDate = "BirthDate";

        public const string Table_Threads = "Threads";
        public const string Threads_ID = "ID";
        public const string Threads_Header = "Header";
        public const string Threads_SubForumID = "SubForumID";
        public const string Threads_ForumID = "ForumID";
        public const string Threads_WriterUsername = "WriterUsername";
        public const string Threads_CreatedDate = "CreatedDate";
    }
}
