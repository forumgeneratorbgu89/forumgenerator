﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DAL
{
    public class SubForumMapper:Mapper
    {
        public int AddThreadWithInitialPost(int forumID, int subForumID, string threadheader, string postContent, string username)
        {
            // Add new Thread
            string time = DateTime.Now.ToString();
            string sql = "INSERT INTO Threads (Header,[SubForumID], [ForumID], [WriterUsername], [CreatedDate]) VALUES ('" + threadheader + "'," + subForumID + "," + forumID + ",'" + username + "','" + time + "')";
            ExecuteQuery(sql);

            // Get thread id
            int threadid = GetThreadID(threadheader);

            // Get Forum Name
            sql = "SELECT Name FROM Forums WHERE ID = " + forumID;
            string fname = getFieldFromQuery(sql, "Name");

            // Get Sub Forum Name
            sql = "SELECT Name FROM SubForums WHERE ID = " + subForumID;
            string sfname = getFieldFromQuery(sql, "Name");

            // Add initial post to the thread
            sql = "INSERT INTO Posts (ForumName, SubForumName, [ThreadID] ,[isInitialPost], Path, FatherPath, Content, [WriterUsername], DateCreated) VALUES ('" + fname + "','" + sfname + "'," + threadid + ", 1 , 0 , -1 ,'" + postContent + "','" + username + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            ExecuteQuery(sql);

            return threadid;
        }

        public List<Tuple<int, string, string, string, string, string, string, Tuple<DateTime, DateTime, bool, string, int>>> GetAllModerators(string forumName,string subforumName)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<Tuple<int, string, string, string, string, string, string, Tuple<DateTime, DateTime, bool, string, int>>> mods = new List<Tuple<int, string, string, string, string, string, string, Tuple<DateTime, DateTime, bool, string, int>>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT B.* FROM "+DbConsts.Table_Moderators+ " as A LEFT JOIN "+
                DbConsts.Table_Members+ " as B ON (A."+DbConsts.Moderators_Username+ " = B."+
                DbConsts.Members_Username+ ") WHERE A."+DbConsts.Moderators_ForumName+"= '" + forumName + "' AND A."+DbConsts.Moderators_SubForumName+" = '" + subforumName + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                mods.Add(new Tuple<int, string, string, string, string, string, string,Tuple<DateTime, DateTime, bool, string, int>>(
                    Int32.Parse(reader[DbConsts.Members_ID].ToString()),
                    (string)reader[DbConsts.Members_Username],
                    (string)reader[DbConsts.Members_Password],
                    (string)reader[DbConsts.Members_FirstName],
                    (string)reader[DbConsts.Members_LastName],
                    (string)reader[DbConsts.Members_Email],
                    (string)reader[DbConsts.Members_Code],
                    new Tuple<DateTime, DateTime, bool, string, int>(
                    //Convert.ToDateTime(reader[DbConsts.Members_BirthDate]),
                    DateTime.Now,
                    //Convert.ToDateTime(reader[DbConsts.Members_SuspendExpireDate]),
                    DateTime.Now,
                    //(bool)reader[DbConsts.Members_isVerified],
                    true,
                    (string)reader[DbConsts.Members_ForumName],
                    int.Parse(reader[DbConsts.Members_OfflineInteractivness].ToString()))));
            }

            com.Close();
            return mods;
        }

        public int GetForumIDByName(string forumname)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT ID FROM Forums where Name = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["ID"];
            }

            com.Close();
            return id;
        }

        public int GetThreadID(string threadheader)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT ID FROM Threads WHERE Header ='" + threadheader + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["ID"];
            }

            com.Close();
            return id;
        }

        public void DeleteThread(int threadID)
        {
            if(ExistIDQuery(threadID, DbConsts.Table_Threads))
            {
                // remove thread
                string sql = "DELETE FROM "+DbConsts.Table_Threads+" WHERE "+DbConsts.Threads_ID+"='" + threadID + "'";
                ExecuteQuery(sql);

                // remove posts
                sql = "DELETE FROM " + DbConsts.Table_Posts + " WHERE " + DbConsts.Posts_ThreadID + "='" + threadID + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Thread ID " + threadID + " not exists");
        }

        public void AddModerator(string subForumName, string username,DateTime timelimit, string forumName)
        {
            string sql = "INSERT INTO "+DbConsts.Table_Moderators+ " ("+DbConsts.Moderators_Username+
                ","+DbConsts.Moderators_SubForumName+ ","+DbConsts.Moderators_PromotedDate+","+ 
                DbConsts.Moderators_TimeLimit +","+DbConsts.Moderators_ForumName+") VALUES ('" + username + 
                "','" + subForumName + "','" +DateTime.Now.ToString() + "','"
                +timelimit.ToString("yyyy-MM-dd HH:mm:ss")+"','"+forumName+"')";
            ExecuteQuery(sql);
        }

        public int GetModeratorsCount(string forumName, string subforumname)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int id = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT count(*) as num FROM Moderators WHERE " + DbConsts.Moderators_ForumName + "='" + forumName + "' AND " + DbConsts.Moderators_SubForumName + "='" + subforumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["num"];
            }
            
            com.Close();
            return id;
        }

        public void RemoveModerator(string forumName, string subForumName, string username)
        {
            if (GetModeratorsCount(forumName, subForumName) > 1)
            {
                string sql = "DELETE FROM Moderators WHERE "+DbConsts.Moderators_Username+"='" + username + "' AND " + DbConsts.Moderators_SubForumName + "='" + subForumName + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Cannot Remove Moderator " + username + " ,sub forum has only 1 moderator");
        }

        public bool CheckNumModerators(int policyid, int count)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();

            int num = -1;
            SqlDataReader reader = null;
            string selectCmd = "SELECT MaxModeratorsNumber FROM Policies WHERE ID=" + policyid;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                num = (int)reader["MaxModeratorsNumber"];
            }
            
            com.Close();
            return count > num ? false : true;
        }

        public List<Tuple<string, DateTime?>> GetModeratorsForTimeLimit(string forumname, string subforumname)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<Tuple<string, DateTime?>> mods = new List<Tuple<string, DateTime?>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT Username, TimeLimit FROM Moderators WHERE ForumName = '" + forumname + "' AND SubForumName = '" + subforumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                if (reader["TimeLimit"] == null)
                    mods.Add(new Tuple<string, DateTime?>(
                        (string) reader["Username"],
                        null));
                else
                    mods.Add(new Tuple<string, DateTime?>(
                        (string) reader["Username"],
                        Convert.ToDateTime(reader["TimeLimit"])));
            }

            com.Close();
            return mods;
        }

        public void UpdateModeratorTimeLimit(string forumname, string subforumname, string username, DateTime newUntilWhen)
        {
            if (ExistsQuery(username,username,"Moderators"))
            {
                string sql = "UPDATE Moderators SET TimeLimit = '" + newUntilWhen.ToString() + "' WHERE ForumName = '" + forumname + "' AND SubForumName = '" + subforumname + "' AND Username = '" + username + "'";
                ExecuteQuery(sql);
            }
            else throw new Exception("Moderator " + username + " doesn't exists");
        }

        public string getLastThreadTitle(string forumname, string name)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            string title = "";
            SqlDataReader reader = null;
            string selectCmd = "select top 1 Header from Threads order by ID desc";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                title = (string)reader["Header"];
            }

            com.Close();
            return title;
        }

        public List<Tuple<int, string, int, int, string, DateTime>> GetAllThreads(int forumid, int subforumid)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<Tuple<int, string, int, int, string, DateTime>> threads = new List<Tuple<int, string, int, int, string, DateTime>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM Threads WHERE ForumID = " + forumid + " AND SubForumID = " + subforumid;
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                threads.Add(new Tuple<int, string, int, int, string, DateTime>(
                    Int32.Parse(reader["ID"].ToString()), 
                    (string)reader["Header"], 
                    Int32.Parse(reader["SubForumID"].ToString()),
                    Int32.Parse(reader["ForumID"].ToString()), 
                    (string)reader["WriterUsername"],
                    Convert.ToDateTime(reader["CreatedDate"])));
            }

            com.Close();
            return threads;
        }

        public List<Tuple<string, DateTime?>> GetSubForumModerators(string forumname, string subforumname)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<Tuple<string, DateTime?>> mods = new List<Tuple<string, DateTime?>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT * FROM Moderators WHERE " + DbConsts.Moderators_ForumName + "= '" + forumname + "' AND " + DbConsts.Moderators_SubForumName + "= '" + subforumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                Tuple<string, DateTime?> mod = new Tuple<string, DateTime?>((string)reader["Username"], Convert.ToDateTime(reader[DbConsts.Moderators_PromotedDate]));
                mods.Add(mod);
            }

            com.Close();
            return mods;
        }

        public void UnpromoteModerator(string forumname, string subforumname, string adminUnpromoting, string moderatorToUnpromote)
        {
            string sql = "DELETE FROM Moderators WHERE Username='" + moderatorToUnpromote + "' AND " + DbConsts.Moderators_ForumName + "='" + forumname + "' AND " + DbConsts.Moderators_SubForumName + "= '" + subforumname + "'";
            ExecuteQuery(sql);
        }

        public List<Tuple<string, string, string, string, DateTime, string>> GetAllMembers(string forumname)
        {
            SqlConnection com = new SqlConnection(connectionString);
            com.Open();

            List<Tuple<string, string, string, string, DateTime, string>> members = new List<Tuple<string, string, string, string, DateTime, string>>();
            SqlDataReader reader = null;
            string selectCmd = "SELECT Username FROM Members WHERE " + DbConsts.Members_ForumName + "= '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                //members.Add((string)reader["Username"]);
            }

            com.Close();
            return members;
        }

        public bool isModerator(string forumname,string subforumname, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int count = 0;

            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) AS cnt FROM Moderators WHERE Username = '" + username + "' AND " + DbConsts.Moderators_SubForumName + " = '" + subforumname + "' AND " + DbConsts.Moderators_ForumName + " = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                count = (int)reader["cnt"];
            }

            com.Close();
            return count != 0 ? true : false;
        }

        public void RemoveAllModerators(string forumname, string subforumname)
        {
            string sql = "DELETE FROM Moderators WHERE "+DbConsts.Moderators_ForumName+"='" + forumname + "' AND "+DbConsts.Moderators_SubForumName+"='" + subforumname + "'";
            ExecuteQuery(sql);
        }

        public bool isForumAdmin(string forumname, string subforumname, string username)
        {
            SqlConnection com = new SqlConnection();
            com.ConnectionString = connectionString;
            com.Open();
            int id = -1;

            SqlDataReader reader = null;
            string selectCmd = "SELECT COUNT(*) AS cnt FROM " + DbConsts.Table_ForumAdmins + " WHERE Username = '" + username + "' AND " + DbConsts.ForumAdmins_ForumName + " = '" + forumname + "'";
            SqlCommand command = new SqlCommand(selectCmd, com);
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                id = (int)reader["cnt"];
            }

            com.Close();
            return id != -1 ? true : false;
        }
    }
}
