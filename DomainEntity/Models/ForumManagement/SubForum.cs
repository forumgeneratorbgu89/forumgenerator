﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainEntity.Models.ForumManagement;
using DAL;
using DomainEntity.Models.UserManagement;
using DomainEntity.MinimizedModels;

namespace DomainEntity.Models.ForumManagement
{
    public class SubForum
    {
        //fields
        private SubForumMapper db;
        public Forum Forum;
        public int ID { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        private List<Member> _moderators;
        public List<Member> Moderators
        {
            set { _moderators = value; }
            get
            {
                if (_moderators.Count == 0)
                {
                    List<Tuple<int, string, string, string, string, string, string, Tuple<DateTime, DateTime, bool, string, int>>> lst = db.GetAllModerators(this.Forum.Name, this.Name);
                    foreach (var x in lst)
                    {
                        int mID = x.Item1;
                        string mUsername = x.Item2;
                        string mPass = x.Item3;
                        string mFirstName = x.Item4;
                        string mLastName = x.Item5;
                        string mEmail = x.Item6;
                        string mCode = x.Item7;
                        DateTime mBdate = x.Rest.Item1;
                        DateTime mSusdate = x.Rest.Item2;
                        bool mIsVer = x.Rest.Item3;
                        //int mForumID = db.GetForumIDByName(x.Rest.Item4);
                        _moderators.Add(new Member(mID, mUsername, mPass, mFirstName, mLastName, mEmail, mCode, mBdate, mSusdate, mIsVer, x.Rest.Item4, x.Rest.Item5));
                    }
                }
                return _moderators;
            }
        }
        private Dictionary<int, Thread> _threads;
        public Dictionary<int, Thread> Threads
        {
            set { _threads = value; }
            get
            {
                if (_threads.Count == 0)
                {
                    // 1. thread id, 2. thread header, 3. thread sub forum id, 4. ???, 5. post writer, 6. date?
                    List<Tuple<int, string, int, int, string, DateTime>> lst = db.GetAllThreads(this.Forum.ID, this.ID);
                    foreach (var x in lst)
                    {
                        int tID = x.Item1;
                        string tHeader = x.Item2;
                        User tPostWriter = this.Forum.getMemberByName(x.Item5);
                        string tPostContent = null;
                        int tSubForumID = x.Item3;
                        DateTime date = x.Item6;
                        _threads.Add(x.Item1, new Thread(tID, tHeader, tPostWriter, tPostContent, Forum, this, date));
                    }
                }
                return _threads;
            }
        }
        private Dictionary<Member, DateTime?> _modsTimeLimit;
        public Dictionary<Member, DateTime?> ModsTimeLimit
        {
            set { _modsTimeLimit = value; }
            get
            {
                if (_modsTimeLimit.Count == 0)
                {
                    List<Tuple<string, DateTime?>> lst = db.GetModeratorsForTimeLimit(Forum.Name, Name);
                    foreach (var x in lst)
                    {
                        ModsTimeLimit.Add(Forum.Members[x.Item1], x.Item2);
                    }
                }
                return _modsTimeLimit;
            }
        }

        //constructor
        public SubForum(string name, string subject, int id, Forum forum, List<Member> moderators)
        {
            Name = name;
            Subject = subject;
            ID = id;
            Moderators = moderators;
            Threads = new Dictionary<int, Thread>();
            Forum = forum;
            ModsTimeLimit = new Dictionary<Member, DateTime?>();
            db = new SubForumMapper();
            //ThreadIndexID++;
        }

        public SubForum(int id, string name, string subject, Forum forum)
        {
            Name = name;
            Subject = subject;
            ID = id;
            Moderators = new List<Member>();
            Threads = new Dictionary<int, Thread>();
            Forum = forum;
            ModsTimeLimit = new Dictionary<Member, DateTime?>();
            db = new SubForumMapper();
            //ThreadIndexID++;
        }

        //methods
        internal int GetNumOfThreads()
        {
            return Threads.Count;
        }

        public int AddThread(string Threadheader, User Postwriter, string postContent)
        {
            int threadid = -1;
            try {
                threadid = db.AddThreadWithInitialPost(this.Forum.ID, ID, Threadheader, postContent, Postwriter.getUsername());
                Threads[threadid] = new Thread(threadid, Threadheader, Postwriter, postContent, Forum, this, DateTime.Now);
            } catch { }
            return threadid;
        }

        internal void DeleteThread(int threadID)
        {
            db.DeleteThread(threadID);
            Threads.Remove(threadID);
        }

        // not in use
        //internal bool DeleteThread(int threadid, User deleter)
        //{
        //    if (db.isModerator(Forum.Name, Name, deleter.getUsername()) || 
        //        db.isForumAdmin(Forum.Name, Name, deleter.getUsername()) || 
        //        db.isSuperAdmin(Forum.Name, Name, deleter.getUsername())){
        //        db.DeleteThread(threadid);
        //        Threads.Remove(threadid);
        //        return true;
        //    }
        //    return false;
        //}

        public bool AddModerator(Member m)
        {
            if (this.Forum.Policy.MaxModeratorsNumber > Moderators.Count)
            {
                db.AddModerator(Name, m.getUsername(), DateTime.MaxValue, Forum.Name);
                Moderators.Add(Forum.Members[m.username]);
                return true;
            }
            throw new Exception("Num of moderators acceeds the limit");
        }

        public void RemoveModerator(string username)
        {
            db.RemoveModerator(Forum.Name, Name, username);
            Moderators.Remove(Forum.Members[username]);
        }

        internal bool CanRemoveModerator()
        {
            return this.Moderators.Count() > 0;
        }

        public Thread GetThreadByTitle(string title)
        {
            try
            {
                foreach (KeyValuePair<int, Thread> entry in Threads)
                {
                    if (entry.Value.Header.Equals(title))
                        return Threads[entry.Key];
                }
            }
            catch (Exception)
            {
                //throw new Exception("Thread doesn't exists");
                return null;
            }
            return null;
        }

        public Thread GetThreadByID(int threadID)
        {
            foreach (KeyValuePair<int, Thread> entry in Threads)
            {
                if (entry.Value.id == threadID)
                    return Threads[entry.Key];
            }
            throw new Exception("Thread doesn't exists");
        }

        internal string getLastThreadTitle(string forumname)
        {
            return db.getLastThreadTitle(forumname, Name);
        }

        internal List<MinimizedThread> GetAllThreads(string forumname)
        {
            List<MinimizedThread> mthreads = new List<MinimizedThread>();
            foreach (Thread thread in Threads.Values)
            {
                MinimizedThread mt = new MinimizedThread();
                mt.ID = thread.id;
                mt.Date = thread.OpenningPost.Time;
                mt.Title = thread.Header;
                if (thread.OpenningPost.Writer != null)
                {
                    mt.UserNameWriter = thread.OpenningPost.Writer.getUsername();
                }
                else
                {
                    mt.UserNameWriter = "guest";
                }
                mthreads.Add(mt);
            }
            return mthreads;
        }

        // not in use
        //public DateTime getModTimeLimit(Member m)
        //{
        //    return ModsTimeLimit[m];
        //}

        //// not in use
        //public bool editModTimeLimit(Member m, DateTime newUntilWhen)
        //{
        //    db.UpdateModeratorTimeLimit(Forum.Name, Name, m.username, newUntilWhen);
        //    this.ModsTimeLimit[m] = newUntilWhen;
        //    return true;
        //}

        //// not in use
        //public bool findThread(int threadid)
        //{
        //    if (Threads.Keys.Contains(threadid))
        //        return true;
        //    return false;
        //}

        //// not in use
        //public bool findPost(int threadid, int postid)
        //{
        //    if (Threads.Keys.Contains(threadid))
        //        return true;
        //    return false;
        //}

        public bool isModerator(User user)
        {
            try
            {
                return db.isModerator(Forum.Name, Name, user.getUsername());
            }
            catch
            {
                return false;
            }
        }

        //internal void DestructPost(int threadID, List<int> postID)
        //{
        //    List<MinimizedPost> posts = Threads[threadID].GetThreadPosts(Forum.Name, this.Name);
        //    // check if the postID is the only post, if so -> delete thread
        //    if (posts.Count == 1)
        //    {
        //        string[] ids = posts[0].PostPath.Split(',');
        //        bool isLastPost = true;
        //        for (int i = 0; i < ids.Count(); i++)
        //        {
        //            if (Int32.Parse(ids[i]) != postID[i])
        //            {
        //                isLastPost = false;
        //                break; // the only post is not equal to the given post
        //            }
        //        }
        //        if (isLastPost)
        //            DeleteThread(threadID);
        //    }
        //    else Threads[threadID].DeletePost(postID);
        //}

        internal bool EditPost(string forumname, int threadId, List<int> postId, string poster, string newContent)
        {
            // TODO: edit post in RAM and DB. return true for success; false otherwise
            try
            {
                // edit in DB
                string oldContent = Threads[threadId].findPostObject(postId).Content;
                Threads[threadId].EditPost(forumname, this.Name, postId, poster, newContent);
                try
                {
                    // edit RAM
                    this.Threads[threadId].OpenningPost.Content = newContent;
                }
                catch
                {
                    // ram edit failed, restore DB to previous
                    this.Threads[threadId].EditPost(forumname, this.Name, postId, poster, oldContent);
                    return false;
                }
            }
            catch { return false; } // db edit failed
            return true;

            /*try { this.Threads[threadId].EditPost(forumname, this.Name, postId, poster, newContent); }
            catch (Exception) { return false; }
            return true;*/
        }

        internal List<Tuple<string, DateTime?>> GetSubForumModerators(string forumname)
        {
            //TODO: DateTime means birthdate?
            List<Tuple<string, DateTime?>> moderators = new List<Tuple<string, DateTime?>>();
            foreach (Member mod in Moderators)
                moderators.Add(new Tuple<string, DateTime?>(mod.username, mod.birthDate));

            return moderators;
        }

        internal bool UnpromoteModerator(string forumname, string adminUnpromoting, string moderatorToUnpromote)
        {
            //TODO: unpromote means remove or do i miss something ?
            RemoveModerator(moderatorToUnpromote);
            return true;
        }

        internal List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(string forumname)
        {
            //TODO: how to define if it is guest?
            List<MinimizedUserInSubForum> min = new List<MinimizedUserInSubForum>();
            foreach (KeyValuePair<string,Member> member in this.Forum.Members)
            {
                MinimizedUserInSubForum minsub = new MinimizedUserInSubForum();
                minsub.UserName = member.Key;
                if (db.isSuperAdmin(member.Key))
                    minsub.State = UserTypeInSubForum.SuperAdmin;
                else if (this.Forum.GetAdmins().Contains(member.Key))
                    minsub.State = UserTypeInSubForum.Admin;
                else if (isModerator(member.Value))
                    minsub.State = UserTypeInSubForum.Moderator;
                else minsub.State = UserTypeInSubForum.Member;
                min.Add(minsub);
            }
            return min;
        }

        internal List<MinimizedPost> GetAllComments(string forumname, int threadID, List<int> postID)
        {
            return this.Threads[threadID].GetAllComments(forumname, this.Name, postID);
        }

        internal MinimizedPost GetOpenningPost(string forumname, int threadID)
        {
            return this.Threads[threadID].GetOpenningPost();
        }

        internal MinimizedUserInSubForum GetUserType(string forumname, string username)
        {
            //TODO: how to define if it is guest?
            Member m = this.Forum.Members[username];
            MinimizedUserInSubForum minMem = new MinimizedUserInSubForum();
            minMem.UserName = m.username;
            if (Moderators.Contains(m))
                minMem.State = UserTypeInSubForum.Moderator;
            else if (this.Forum.GetAdmins().Contains(m.username))
                minMem.State = UserTypeInSubForum.Admin;
            else if (db.isSuperAdmin(m.username))
                minMem.State = UserTypeInSubForum.SuperAdmin;
            else if (m.username.Equals("guest") || m.username.Equals("Guest"))
                minMem.State = UserTypeInSubForum.Guest;
            else minMem.State = UserTypeInSubForum.Member;
            return minMem;
        }

        internal bool editModTimeLimit(string modName, DateTime newUntilWhen)
        {
            //edit mod time limit in RAM and DB and return true if success; false otherwise
            db.UpdateModeratorTimeLimit(this.Forum.Name, this.Name, modName, newUntilWhen);
            this.ModsTimeLimit[this.Forum.getMemberByName(modName)] = newUntilWhen;
            return true;
        }

        internal void Destroy()
        {
            // TODO: this should remove from RAM and DB all sub-objects (objects and collections, all non-primitives)
            // if the subobjects have sub-objects, they should call their "destroy()" functions etc'
            
            // remove thread posts
            foreach(KeyValuePair<int,Thread> t in Threads)
            {
                t.Value.Destructor();
                db.DeleteThread(t.Key);
            }
            this.Threads.Clear();

            // remove moderators
            this.Moderators.Clear();
            db.RemoveAllModerators(this.Forum.Name, this.Name);

            // remove modetimelimit
            this.ModsTimeLimit.Clear();
        }

        public DateTime? getModTimeLimit(string username)
        {
            return ModsTimeLimit[this.Forum.getMemberByName(username)];
        }
    }
}