﻿using System;

namespace DomainEntity.Models.ForumManagement
{
    internal class Session
    {
        private string forumName = null;
        private string username = null;
        private string password = null;
        private int connectedDevices = 0;

        public Session(string forumName, string username)
        {
            this.forumName = forumName;
            this.username = username;
            this.password = generateSessionPassword();
            connectedDevices = 1;
        }

        public void removeDevice() { connectedDevices--; }

        public void addDevice() { connectedDevices++; }

        private string generateSessionPassword()
        {
            return Math.Abs(DateTime.Now.GetHashCode() + forumName.GetHashCode() + username.GetHashCode()).ToString();
        }

        public int getConnectedDevices() { return connectedDevices; }

        public string getPassword() { return password; }

        public static string GenerateSessionName(string forumName, string username) { return forumName + "#" + username; }
    }
}