﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainEntity.Models.UserManagement;
using DomainEntity.MinimizedModels;
using DAL;

namespace DomainEntity.Models.ForumManagement
{
    public class Thread
    {
        private ThreadMapper db;
        public int id { get; set; }
        public Forum forum { get; set; }
        public SubForum subforum { get; set; }
        public string Header { get; set; }
        private Post _openningPost;
        public Post OpenningPost {
            set { _openningPost = value; }
            get
            {
                // id,content,writer,date
                Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> open = db.GetOpenningPost(forum.Name, subforum.Name, this.id);
                //string forumname = open.Item1;
                //string subforumname = open.Item2;
                //int threadid = open.Item3;
                //bool isInit = open.Item4;
                //string path = open.Item5;
                int pid = Int32.Parse(open.Item5.Substring(open.Item5.Count() - 1));
                //string fatherPath = open.Item6;
                string content = open.Item7;
                string writer = open.Rest.Item1;
                DateTime datecreated = open.Rest.Item2;

                _openningPost = new Post(pid, forum.getMemberByName(writer), content, null, this, datecreated);
                return _openningPost;
            }
        }
        private Dictionary<string, Post> _posts;
        public Dictionary<string, Post> Posts
        {
            set { _posts = value; }
            get
            {
                if (_posts.Count == 0)
                {
                    List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> lst = db.GetThreadPosts(forum.ID,subforum.ID,id);
                    foreach (var x in lst)
                    {
                        string forumname = x.Item1;
                        string subforumname = x.Item2;
                        int threadid = x.Item3;
                        bool isInit = x.Item4;
                        string path = x.Item5;
                        int pid = Int32.Parse(x.Item5.Substring(x.Item5.Count() - 1));
                        string fatherPath = x.Item6;
                        string content = x.Item7;
                        string writer = x.Rest.Item1;
                        DateTime datecreated = x.Rest.Item2;
                        if (isInit)
                            OpenningPost = new Post(pid, forum.getMemberByName(writer),content,null,this, datecreated);
                        try { _posts.Add(path, new Post(pid, forum.getMemberByName(writer), content, null, this, datecreated)); }
                        catch { };
                    }
                }
                return _posts;
            }
        }

        public Thread(int threadID, string threadheader, User postwriter, string postContent, Forum forum, SubForum subForum,DateTime date)
        {
            this.id = threadID;
            this.Header = threadheader;
            this.OpenningPost = new Post(0,postwriter, postContent, null, this,date);
            this.forum = forum;
            this.subforum = subForum;
            db = new ThreadMapper();
            Posts = new Dictionary<string, Post>();
        }

        internal bool DeletePost(List<int> postID)
        {
            string path = ConvertListPostToPath(postID);
            db.DeletePost(this.forum.Name, this.subforum.Name, this.id, path);
            Posts.Remove(path);
            return true;
        }

        internal List<Post> GetPostReplies(List<int> postid)
        {
            string path = ConvertListPostToPath(postid);
            List<Post> replies = new List<Post>();
            List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> lst = db.GetAllComments(this.forum.Name, this.subforum.Name, this.id, path);
            foreach (var x in lst)
            {
                string forumname = x.Item1;
                string subforumname = x.Item2;
                int threadid = x.Item3;
                bool isInit = x.Item4;
                int pid = Int32.Parse(x.Item5.Substring(x.Item5.Count() - 1));
                string fatherPath = x.Item6;
                string content = x.Item7;
                string writer = x.Rest.Item1;
                DateTime datecreated = x.Rest.Item2;
                replies.Add(new Post(pid, this.forum.getMemberByName(writer), content, null, this, datecreated));
            }
            return replies;
        }

        // need to think if we need this function in order to initialize the Post father object
        private Post GetPostByPath(string fatherPath)
        {
            Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>> post = db.GetPostByPath(this.forum.Name,this.subforum.Name,this.id,fatherPath);
            if (post != null)
            {
                string forumname = post.Item1;
                string subforumname = post.Item2;
                int threadid = post.Item3;
                bool isInit = post.Item4;
                int pid = Int32.Parse(post.Item5.Substring(post.Item5.Count() - 1));
                string fPath = post.Item6;
                string content = post.Item7;
                string writer = post.Rest.Item1;
                DateTime datecreated = post.Rest.Item2;
                return new Post(pid, this.forum.getMemberByName(writer), content, null, this, datecreated);
            }
            return null;
        }

        internal List<int> ReplyToPost(string forumname, string subforumname, List<int> post, string creator, string text)
        {
            string path = ConvertListPostToPath(post);
            string newpostpath = db.ReplyToPost(forumname, subforumname, this.id, path, creator, text);
            List<int> postids = convertPathToListPost(newpostpath);
            Member mem = this.subforum.Forum.getMemberByName(creator);
            Post p = new Post(postids[postids.Count - 1], mem, text, null, this, DateTime.Now);
            try { Posts.Add(newpostpath, p); }
            catch { }; // already added
            return postids;
        }

        private List<int> convertPathToListPost(string path)
        {
            string[] ids = path.Split(',');
            List<int> post = new List<int>();
            for (int i = 0; i < ids.Length; i++)
            {
                post.Add(Int32.Parse(ids[i]));
            }
            return post;
        }

        private string ConvertListPostToPath(List<int> post)
        {
            string path = "";
            for (int i = 0; i < post.Count(); i++)
            {
                if (i == post.Count - 1) // the last i with no comma
                    path += post[i].ToString();
                else
                    path += post[i].ToString() + ",";
            }
            return path;
        }

        internal MinimizedThread minimize()
        {
            MinimizedThread output = new MinimizedThread();
            output.Date = this.OpenningPost.Time;
            output.ID = this.id;
            output.Title = this.Header;
            string writerUserName = this.OpenningPost.Writer != null ? this.OpenningPost.Writer.getUsername() : "guest";
            output.UserNameWriter = writerUserName;
            return output;
        }

        // not in use
        public void EditHeader(string header)
        {
            Header = header;
        }
        
        public bool Destructor()
        {
            // remove posts
            foreach(KeyValuePair<string,Post> p in Posts)
            {
                db.DeletePost(this.forum.Name, this.subforum.Name, this.id, p.Key);
            }
            this.Posts.Clear();
            return true;
        }

        // not in use
        internal bool findPost(int postid)
        {
            //if (OpenningPost.id == (postid))
            //{
            //    return true;
            //}
            //else
            //{
            //    foreach (Post p in OpenningPost.getComtents())
            //    {
            //        if (p.id == postid)
            //        {
            //            return true;
            //        }
            //    }
            //}
            return false;
        }

        internal Post findPostObject(List<int> postid)
        {
            return GetPostByPath(ConvertListPostToPath(postid));
        }

        internal void EditPost(string forumname,string subforumname, List<int> postId, string poster, string newContent)
        {
            string path = ConvertListPostToPath(postId);
            db.EditPost(forumname, subforumname, this.id, path, poster, newContent);
        }
        
        internal List<MinimizedPost> GetAllComments(string forumname, string subforumname, List<int> postID)
        {
            List<MinimizedPost> comments = new List<MinimizedPost>();
            List<Tuple<string, string, int, bool, string, string, string, Tuple<string, DateTime>>> lst = db.GetAllComments(forumname, subforumname, this.id, ConvertListPostToPath(postID));
            foreach (var x in lst)
            {
                string fname = x.Item1;
                string sfname = x.Item2;
                int threadid = x.Item3;
                bool isInit = x.Item4;
                int pid = Int32.Parse(x.Item5.Substring(x.Item5.Count() - 1));
                string fatherPath = x.Item6;
                string content = x.Item7;
                string writer = x.Rest.Item1;
                DateTime datecreated = x.Rest.Item2;
                comments.Add(new MinimizedPost(pid, content, writer, datecreated));
            }

            // recursive call to get all comments of the comments found
            //if (comments.Count > 0)
            //{
            //    List<MinimizedPost> subcomments = new List<MinimizedPost>();
            //    foreach (MinimizedPost post in comments)
            //    {
            //        List<int> postPath = new List<int>(postID);
            //        postPath.Add(post.PostId);
            //        subcomments = subcomments.Concat(GetAllComments(forumname, subforumname, postPath)).ToList<MinimizedPost>();
            //    }
            //    comments = comments.Concat(subcomments).ToList<MinimizedPost>();
            //}
            return comments;
        }

        // not in use
        internal MinimizedPost GetOpenningPost()
        {
            string writerName = this.OpenningPost.Writer != null ? this.OpenningPost.Writer.getUsername() : "guest";
            return new MinimizedPost(this.OpenningPost.ID, this.OpenningPost.Content, writerName, this.OpenningPost.Time);
        }
    }
}