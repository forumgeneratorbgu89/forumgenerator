﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using DomainEntity.Models.UserManagement;
using DomainEntity.MinimizedModels;

namespace DomainEntity.Models.ForumManagement
{
    public class Forum
    {

        public Forum(int id, string Name, ForumPolicy p)
        {
            this.ID = id;
            this.Name = Name;
            Members = new Dictionary<string, Member>();
            ForumAdmins = new Dictionary<string, Member>();
            SubForums = new Dictionary<int, SubForum>();
            Policy = p;
            db = new ForumMapper();
        }

        //fields
        public int ID { get; set; }
        public string Name { get; set; }
        private Dictionary<string, Member> _members;
        public Dictionary<string, Member> Members
        {
            set { _members = value; }
            get
            {
                if (_members.Count == 0)
                {
                    // username, password, firstName, lastName, email, code, birthDate, suspendExpire, isVerified, OfflineInteractive
                    List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> mList =
                        db.GetAllMembers(Name);
                    foreach (var t in mList)
                    {
                        _members.Add(t.Item1, new Member(t.Item1, EncryptDecrypt.Decrypt(t.Item2), t.Item3, t.Item4, t.Item5,
 t.Item6, t.Item7, t.Rest.Item1, t.Rest.Item2, Name, t.Rest.Item3));
                    }
                }
                return _members;
            }
        }
        private Dictionary<string, Member> _forumAdmins;
        public Dictionary<string, Member> ForumAdmins
        {
            set { _forumAdmins = value; }
            get
            {
                if (_forumAdmins.Count == 0)
                {
                    // username, password, firstName, lastName, email, code, birthDate, suspendExpire, isVerified
                    List<Tuple<string, string, string, string, string, string, DateTime, Tuple<DateTime, bool, int>>> faList = db.GetForumAdmins(Name);
                    foreach (var t in faList)
                    {
                        _forumAdmins.Add(t.Item1, new Member(t.Item1, EncryptDecrypt.Decrypt(t.Item2), t.Item3, t.Item4, t.Item5,
t.Item6, t.Item7, t.Rest.Item1, t.Rest.Item2, Name, t.Rest.Item3));
                    }
                }
                return _forumAdmins;
            }
        }
        private Dictionary<int, SubForum> _subForums;
        public Dictionary<int, SubForum> SubForums
        {
            set { _subForums = value; }
            get
            {
                if (_subForums.Count == 0)
                {
                    // id, name, subject, forumId
                    List<Tuple<int, string, string, int>> sfList = db.GetAllSubForumsByForumName(Name);
                    foreach (var t in sfList) { _subForums.Add(t.Item1, new SubForum(t.Item1, t.Item2, t.Item3, this)); }
                }
                return _subForums;
            }
        }





        private ForumPolicy _policy;
        public ForumPolicy Policy
        {
            set { _policy = value; }
            get
            {
                if (_policy == null) { GetPolicy(); }
                return _policy;
            }
        }

        public ForumMapper db;

        //methods

        public bool AddMember(Member member)
        {
            bool isAdded = false;
            try
            {
                // add to ram
                Members.Add(member.username, member);
                member.ForumID = Name;
                // add to db
                db.AddMember(ID, member.username, member.password, member.firstName, member.lastName, member.Email, member.Code, member.birthDate, member.suspendExpire, member.IsVerified(), member.OfflineInteractive);
                isAdded = true;
            }
            catch (Exception)
            {
                isAdded = false;
            }
            return isAdded;
        }

        public void RemoveMember(string username)
        {
            try
            {
                // from db
                db.RemoveMember(username, Name);
                // form ram
                Members.Remove(username);
            }
            catch (Exception)
            {
                throw new Exception("Could'nt remove");
            }
        }

        public Member getMemberByName(string username) // done together :)
        {
            Member mem = null;
            try
            {
                mem = Members[username];
            }
            catch (Exception)
            {
                mem = null;
            }


            return mem;
        }

        public Member searchMember(string username)
        {
            return getMemberByName(username);
        }

        public SubForum GetSubForumByName(string subforumname)
        {
            SubForum sf = null;
            foreach (SubForum subForum in SubForums.Values)
            {
                if (subForum.Name.Equals(subforumname))
                {
                    sf = subForum;
                    break;
                }
            }

            return sf;
        }
        public SubForum AddSubForum(string SubforumName, string SubForumSubject, List<string> moderators)
        {
            SubForum sf = null;
            int subforumId = -1;
            try
            {
                db.AddSubForum(Name, SubforumName, SubForumSubject, moderators);
                try
                {
                    List<string> details = db.getSubForumByName(SubforumName);
                    // to ram
                    subforumId = Convert.ToInt32(details[0]);
                    SubForums[subforumId] = new SubForum(details[1], details[2], subforumId, this, getSubForumModerators(SubforumName));
                    sf = SubForums[subforumId];
                }
                catch
                {
                    sf = null;
                    db.DeleteSubForumByName(SubforumName, Name);
                }
            }
            catch (Exception) { sf = null; }
            return sf;
        }

        private List<Member> getSubForumModerators(string SubforumName)
        {
            List<Member> moderators = new List<Member>();
            List<string> mods = db.GetSubForumModerators(SubforumName);
            foreach (string m in mods)
            {
                moderators.Add(searchMember(m));
            }
            return moderators;
        }

        internal void Destroy()
        {
            foreach (SubForum subForum in SubForums.Values)
            {
                db.DeleteSubForumByName(subForum.Name, Name);
            }
            SubForums.Clear();
            foreach (Member admin in ForumAdmins.Values)
            {
                db.RemoveAdmin(admin.username, Name);
            }
            ForumAdmins.Clear();
            foreach (Member member in Members.Values)
            {
                db.RemoveMember(member.username, Name);
            }
            Members.Clear();
            db.ExecuteQuery("DELETE FROM " + DbConsts.Table_Policies + " WHERE " +
                            DbConsts.Policies_ForumName + " = '" + this.Name + "'");
            Policy = null;
        }

        public bool BanModerator(SubForum sf, Member m, int days)
        {
            bool isBanned = false;
            if (sf.isModerator(m) && sf.CanRemoveModerator() && Policy.LegalModeratorsSuspensionTime(days))
            {
                m.suspend(days);
                isBanned = true;
            }
            return isBanned;
        }

        public bool BanMember(SubForum sf, Member m, int days)
        {
            bool isBanned = false;
            if (Policy.LegalModeratorsSuspensionTime(days))
            {
                m.suspend(days);
                isBanned = true;
            }
            return isBanned;
        }

        internal bool VerifyUser(string username, string code)
        {
            Member member = getMemberByName(username);
            if (member != null)
            {
                if (member.Code.Equals(code))
                {
                    member.verify();
                    return true;
                }
                else throw new Exception("Wrong verification code!");
            }
            else throw new Exception("Member doesn't exists");
        }

        public SubForum getSubForumByID(int subForumId)
        {
            SubForum sf = null;
            try
            {
                sf = SubForums[subForumId];
            }
            catch (Exception)
            {
                sf = null;
            }
            return sf;
        }

        internal void SetPolicy(ForumPolicy Policy)
        {
            // to ram
            this.Policy = Policy;
            // to db
            db.setPolicy(db.GetPolicyIDByForumName(Name), Policy.MaxModeratorsNumber, Policy.MinPasswordLetters, Policy.MinUserNameLetters, Policy.PasswordRequiredUpperLowerCase, Policy.PasswordReuqiredNumbers, Policy.SuspendModeratorMinTime, Policy.SuspendAdminMinTime, Policy.SuspendMemberMinTime, Policy.GuestCanPost, Policy.GuestCanStartThread, Policy.MinSeniorityForMediatorToEditAPost, Policy.MinSeniorityForMediatorToRemoveAPost, Policy.Interactive);
        }

        internal bool DestructThread(int subforumid, int threadID)
        {
            bool isDeleted = false;
            SubForum sf = getSubForumByID(subforumid);
            if (sf != null)
            {
                sf.DeleteThread(threadID);
                isDeleted = true;
            }
            return isDeleted;
        }

        internal bool DestructPost(int subforumid, int threadID, List<int> postID)
        {
            bool isDeleted = false;
            SubForum sf = getSubForumByID(subforumid);
            if (sf != null)
            {
                Thread thread = sf.GetThreadByID(threadID);
                isDeleted = thread.DeletePost(postID);
            }
            return isDeleted;
        }

        internal bool editModTimeLimit(int subforum, string modName, DateTime newUntilWhen)
        {
            return SubForums[subforum].editModTimeLimit(modName, newUntilWhen);
        }

        internal bool isCanGuestReplyToPost()
        {
            return Policy.GuestCanPost;
        }

        internal bool isCanGuestStartThread()
        {
            return Policy.GuestCanStartThread;
        }

        public List<SubForum> AllSubForumsModeratedBy(Member m)
        {
            List<SubForum> subForums = new List<SubForum>();
            try
            {
                foreach (SubForum subForum in SubForums.Values)
                {
                    if (subForum.isModerator(m))
                    {
                        subForums.Add(subForum);
                    }
                }
            }
            catch (Exception)
            {

            }
            return subForums;
        }

        internal List<int> ReplyToPost(int subForum, int thread, List<int> post, string creator, string text)
        {
            try
            {
                SubForum sf = getSubForumByID(subForum);
                Thread t = sf.GetThreadByID(thread);
                return t.ReplyToPost(Name, sf.Name, post, creator, text);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool checkValidation(string username, string password)
        {
            if (Members.ContainsKey(username))
                throw new Exception("username already exists!");
            return Policy.CheckUserName(username) && Policy.CheckPassword(password);
        }

        public bool IsUserRegistered(string username)
        {
            return Members.ContainsKey(username);
        }

        internal List<MinimizedSubForum> GetAllSubForums()
        {
            List<MinimizedSubForum> subForums = new List<MinimizedSubForum>();
            foreach (SubForum subForum in SubForums.Values)
            {
                MinimizedSubForum msf = new MinimizedSubForum();
                msf.ID = subForum.ID;
                msf.Name = subForum.Name;
                msf.Subject = subForum.Subject;
                subForums.Add(msf);
            }
            return subForums;
        }

        internal string GetUserEmail(string username)
        {
            string email = string.Empty;
            if (Members.ContainsKey(username))
            {
                email = Members[username].Email;
            }
            return email;
        }

        internal List<string> GetAdmins()
        {
            List<string> admins = new List<string>();
            foreach (Member admin in ForumAdmins.Values)
            {
                admins.Add(admin.username);
            }
            return admins;
        }

        internal string GetLastUsername()
        {
            return db.GetLastUsername();
        }

        internal string getLastThreadTitle(string subForum)
        {
            return GetSubForumByName(subForum).getLastThreadTitle(Name);
        }

        internal ForumPolicy GetPolicy()
        {
            if (_policy == null)
            {
                int policyid = db.GetPolicyIDByForumName(Name);
                List<string> pol = db.GetPolicy(policyid);
                Policy = new ForumPolicy(Int32.Parse(pol[1]), Int32.Parse(pol[2]), Int32.Parse(pol[3]), ((pol[4]).Equals("1")) ? true : false,
                    ((pol[5]).Equals("1")) ? true : false, Int32.Parse(pol[6]), Int32.Parse(pol[7]), Int32.Parse(pol[8]),
                    ((pol[9]).Equals("1")) ? true : false, ((pol[10]).Equals("1")) ? true : false, Int32.Parse(pol[11]),
                    Int32.Parse(pol[12]), Int32.Parse(pol[13]));
            }
            return Policy;
        }

        internal List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int subforumid)
        {
            return getSubForumByID(subforumid).getAllMinimizedUserInSubForum(Name);
        }

        internal List<MinimizedPost> GetCommentsForUser(string username)
        {
            List<List<string>> posts = db.GetAllPostsOfUser(Name, username);
            List<MinimizedPost> minposts = new List<MinimizedPost>();
            foreach (List<string> post in posts)
            {
                MinimizedPost min = new MinimizedPost();
                min.PostId = Int32.Parse(post[0].Substring(post[0].Length - 1)); // post path
                min.Text = post[1];
                min.userNameWriter = post[2];
                min.Date = Convert.ToDateTime(post[3]);
                minposts.Add(min);
            }
            return minposts;
        }
        internal string getLastSubForumName()
        {
            return db.getLastSubForumName(Name);
        }

        internal List<string> GetAllModerators()
        {
            return db.GetAllModerators(Name);
        }

        internal List<string> GetAllFriends(string username)
        {
            return getMemberByName(username).GetAllFriends();
        }

        internal bool EditPost(string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            return GetSubForumByName(_subForum).EditPost(Name, _threadId, postId, poster, newContent);
        }

        internal Post findPostObject(int subforumid, int threadid, List<int> postid)
        {
            SubForum sf = getSubForumByID(subforumid);
            if (sf.Threads.ContainsKey(threadid))
            {
                return sf.Threads[threadid].findPostObject(postid);
            }
            else
            {
                return null;
            }
        }

        internal bool isSuspended(string username)
        {
            try
            {
                return Members[username].suspendExpire > DateTime.Now;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        internal string GetVerificationCode(string username)
        {
            return getMemberByName(username).Code;
        }

        public bool Login(string username, string password, out bool isUserExistButNotVerify)
        {
            bool isVerified = Members.ContainsKey(username) ? Members[username].isVerified : false;
            isUserExistButNotVerify = Members.ContainsKey(username) && !Members[username].isVerified;
            return (Members.ContainsKey(username) && Members[username].password.Equals(password) && isVerified);
        }

        public bool Promote(string usernamePromoted, string usernamePromoter)
        {
            if (!ForumAdmins.ContainsKey(usernamePromoter))
                throw new Exception("Promoter usename " + usernamePromoter + " is not admin");
            if (ForumAdmins.ContainsKey(usernamePromoted))
                throw new Exception("Promoted usename " + usernamePromoted + " is admin already");
            // to db
            db.AddForumAdmin(Name, usernamePromoted);
            // to ram
            ForumAdmins.Add(usernamePromoted, getMemberByName(usernamePromoted));
            return true;
        }

        internal void ResetToDefaultPolicy()
        {
            Policy = new ForumPolicy();
            db.setPolicy(db.GetPolicyIDByForumName(Name), Policy.MaxModeratorsNumber, Policy.MinPasswordLetters, Policy.MinUserNameLetters, Policy.PasswordRequiredUpperLowerCase, Policy.PasswordReuqiredNumbers, Policy.SuspendModeratorMinTime, Policy.SuspendAdminMinTime, Policy.SuspendMemberMinTime, Policy.GuestCanPost, Policy.GuestCanStartThread, Policy.MinSeniorityForMediatorToEditAPost, Policy.MinSeniorityForMediatorToRemoveAPost, 1);
        }

        public DateTime? getModTimeLimit(int subforum, string username)
        {
            return SubForums[subforum].getModTimeLimit(username);
        }

        internal void SaveNotification(string user, string title, string s, int isRead)
        {
            Members[user].SaveNotification(title, s, isRead);
        }

        public List<string> GetAllRequestFriends(string userName)
        {
            List<string> usernames = new List<string>();
            var requsts = Members[userName].GetFriendRequests();
            foreach (KeyValuePair<string, Member> requst in requsts)
            {
                usernames.Add(requst.Key);
            }
            return usernames;
        }

        public void Unfriend(string username, string usernameToUnfriend)
        {
            Members[username].removeFriend(Members[usernameToUnfriend]);
        }

        public int GetNumOfPosts()
        {
            return db.GetNumOfPosts(this.Name);
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum()
        {
            //TODO: how to define if it is guest?
            List<MinimizedUserInForum> min = new List<MinimizedUserInForum>();
            foreach (KeyValuePair<string, Member> member in this.Members)
            {
                MinimizedUserInForum miniuser = new MinimizedUserInForum();
                miniuser.UserName = member.Key;
                if (db.isSuperAdmin(member.Key))
                    miniuser.State = UserTypeInForum.SuperAdmin;
                else if (ForumAdmins.Keys.Contains(member.Key))
                    miniuser.State = UserTypeInForum.Admin;
                else miniuser.State = UserTypeInForum.Member;
                min.Add(miniuser);
            }
            return min;
        }

        public string GetsubForumNameByID(int subForumid)
        {
            return SubForums[subForumid].Name;
        }

        public bool changePassword(string username, string newPass)
        {
            if(!Policy.CheckPassword(newPass))
                return false;
            if (Members[username].changePassword(newPass))
            {
                Members[username].password = newPass;
                return true;
            }
            return false;
        }

        internal bool UnpromoteAdmin(string adminToUnpromote)
        {
            //TODO: unpromote means remove or do i miss something ?
            db.RemoveAdmin(adminToUnpromote, this.Name);
            ForumAdmins.Remove(adminToUnpromote);
            return true;
        }

        internal void suspend(string friendUserName)
        {
            Members[friendUserName].suspend(10);
        }

        internal void unSuspend(string friendUserName)
        {
            Members[friendUserName].unSuspend();
        }
    }
}
