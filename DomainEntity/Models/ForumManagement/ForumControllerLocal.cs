﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Reflection;
using ObserverPattern;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting;
using System.Threading;
using Logger;
using System.Diagnostics;

namespace DomainEntity.Models.ForumManagement
{
    public class ForumControllerLocal : ObserverPattern.Subject, IforumController
    {
        public ForumMapper db { get; }
        private static ForumControllerLocal instance = null;
        private Dictionary<string, Session> sessions;
        private Dictionary<int, Forum> _forums;
        private Dictionary<int, Forum> Forums
        {
            set { _forums = value; }
            get {
               if (_forums.Count == 0) {
                    // forumid, forumname
                    List<Tuple<int, string>> fList = db.getAllForums();
                    foreach (var t in fList) { _forums.Add(t.Item1, new Forum(t.Item1, t.Item2, null)); }
                }
                return _forums;
            }
        }
        private Dictionary<string, SuperAdmin> _superAdmins;
        private Dictionary<string, SuperAdmin> superAdmins
        {
            set { _superAdmins = value; }
            get
            {
                if (_superAdmins.Count == 0)
                {
                    //id, username, password, firstName, lastName, birthDate, eMail
                    List<Tuple<string, string, string, string, DateTime?, string>> saList = db.getAllSuperAdmins();
                    foreach (var t in saList) { _superAdmins.Add(t.Item1, new SuperAdmin(t.Item1, EncryptDecrypt.Decrypt(t.Item2), t.Item3, t.Item4, ((t.Item5 == null)? new DateTime() : t.Item5.Value), t.Item6)); }
                }
                return _superAdmins;
            }
        }
        private string lastSession;

        public ForumControllerLocal()
        {
            Forums = new Dictionary<int, Forum>();
            superAdmins = new Dictionary<string, SuperAdmin>();
            sessions = new Dictionary<string, Session>();
            db = new ForumMapper();
            ForumGeneratorLogger.init(string.Empty, ForumGeneratorLogger.Level.i);
        }

        private Observer ConnectToObserver()
        {
            Observer observer = (Observer)(Activator.GetObject(typeof(Observer), "tcp://localhost:9999/Observer"));
            if (observer == null)
                throw new Exception();
            return observer;
        }

        public static ForumControllerLocal GetInstance()
        {
            if (instance == null)
            {
                instance = new ForumControllerLocal();
            }
            return instance;
        }

        public static void ClearInstance()
        {
            // this function is needed for testing purposes
            instance = null;            
        }

        // TODO: add "viewFriendRequests" method to EVERYWHERE, and add "approved" field to db freinds table

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            // this is an admin-only function. make sure this is admin:
            if (Forums[forumID].ForumAdmins.ContainsKey(adminUserName) == false) { throw new Exception("YOU ("+adminUserName+") ARE NOT AN ADMIN! FUCK OFF!!"); }

            List<MinimizedPost> output = new List<MinimizedPost>();
            foreach (var post in Forums[forumID].GetCommentsForUser(username))
            {
                output.Add(post);
            }
            return output;
        }

        public List<MinimizedPost> GetAllComments(int forumID, int subForumId, int threadID, List<int> PostID)
        {
            List<MinimizedPost> output = new List<MinimizedPost>();
            Forum forum = Forums[forumID];
            SubForum subforum = forum.getSubForumByID(subForumId);
            foreach (var post in subforum.GetThreadByID(threadID).GetAllComments(forum.Name, subforum.Name, PostID))
            {
                output.Add(post);
            }
            output = output.OrderBy(o => o.Date).ToList();
            return output;
        
        }

        public MinimizedPost GetOpenningPost(int forumID, int subForumId, int ThreadID)
        {
            return Forums[forumID].getSubForumByID(subForumId).GetThreadByID(ThreadID).GetOpenningPost();
            //if(p.Content == null)
            //{
            //    //string content = GetAllComments(forumID, subForumId, ThreadID, new List<int> { 0 })[0].Text;
            //    //p.Content = content;
            //}
            //return (new MinimizedPost(p.ID, p.Content, p.Writer.getUsername(), p.Time));
        }

        public List<string> GetAllModerators(int forumID)
        {
            return Forums[forumID].GetAllModerators();
        }

        public void deleteForum(string name)
        {
            Forum f = GetForum(name);
            f.Destroy();
            Forums.Remove(f.ID);
            db.DeleteForumByName(name);
        }

        public void deleteMember(int forumID, string username)
        {
            Forums[forumID].RemoveMember(username);
        }

        public void deleteSubForum(string forumName, string subForumName)
        {
            Forum f = GetForum(forumName);
            SubForum sf = f.GetSubForumByName(subForumName);
            sf.Destroy();
            f.SubForums.Remove(sf.ID);
            db.DeleteSubForumByName(subForumName, forumName);
        }

        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            return Forums[forumId].GetAllSubForums();
        }

        public int NumOfForums()
        {
            return Forums.Count;
        }

        public int AddForum(string name, string admin)
        {
            return AddForum(name, admin, new ForumPolicy());
        }
        public int AddForum(string name, string admin, ForumPolicy p)
        {
            int output = -1;
            // check that forum name is provided, isn't taken, and that admin is superAdmin
            if (name.Length <= 0) { return output; }
            if (db.ExistsQuery(name, DbConsts.Forums_Name, DbConsts.Table_Forums) == true) { return output; }
            if (db.ExistsQuery(admin, DbConsts.SuperAdmins_Username, DbConsts.Table_SuperAdmins) == false) { return output; }

            // add forum, admin and policy to DB
            db.AddForum(name);
            int forumId = db.GetForumIdByName(name);
            db.AddSuperAdminAsMember(admin, forumId);
            db.AddForumAdmin(name, admin);
            db.AddPolicy(name, p.MaxModeratorsNumber, p.MinPasswordLetters, p.MinUserNameLetters, p.PasswordRequiredUpperLowerCase,
                p.PasswordReuqiredNumbers, p.SuspendModeratorMinTime, p.SuspendAdminMinTime, p.SuspendMemberMinTime, p.GuestCanPost,
                p.GuestCanStartThread, p.MinSeniorityForMediatorToEditAPost, p.MinSeniorityForMediatorToRemoveAPost, p.Interactive);
            
            // add forum to RAM
            Forums[forumId] = new Forum(forumId, name, p);

            output = forumId;
            return output;
        }

        public int AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            if(name.Equals(string.Empty)) { return -1; }

            bool isFound = false;
            List<string> modsWithoutAdminsAndSupers = new List<string>();
            foreach(string moderatorName in moderators)
            {
                if (IsUserAdminOfForum(forumID, moderatorName) || superAdmins.ContainsKey(moderatorName) ||
                    Forums[forumID].getMemberByName(moderatorName) != null)
                {
                    isFound = true;
                }
                else { return -1; }

                if (!IsUserAdminOfForum(forumID, moderatorName) && !!superAdmins.ContainsKey(moderatorName) &&
                    Forums[forumID].getMemberByName(moderatorName) != null)
                {
                    modsWithoutAdminsAndSupers.Add(moderatorName);
                }
            }
            if (!isFound) { return -1; }

            // rani we decided to return the id instead of boolean, -1 in case did'nt succeed
            SubForum subforum = Forums[forumID].AddSubForum(name, subject, moderators);
            int output = (subforum == null) ? -1 : subforum.ID;
            return output;
        }

        public int AddThread(int forumID, string SubForumName, string header, string Postcontent, User Postwriter)
        {
            // rani we decided to return the id instead of boolean, -1 in case did'nt succeed
            if (Postwriter.getUsername().Equals("Guest"))
            {
                Forum forum = Forums[forumID];
                if (forum == null)
                    return -1;
                if (!forum.isCanGuestStartThread())
                    return -1;
            }
            int threadId = -1;
            try { threadId = Forums[forumID].GetSubForumByName(SubForumName).AddThread(header, Postwriter, Postcontent); }
            catch(Exception e) { ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message); }
            return threadId;
        }

        public Forum GetForum(int id)
        {
            if (Forums.ContainsKey(id)) { return Forums[id]; }
            return null;
        }

        public Forum GetForum(string name)
        {
            foreach (var forum in Forums)
            {
                if (forum.Value.Name.Equals(name))
                {
                    return forum.Value;
                }
            }
            return null;
        }

        public bool SendMail(string subject, string text, string eMail)
        {
            try
            {
                MailMessage msg = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                msg.From = new MailAddress("forumcompany2016@gmail.com");
                msg.To.Add(eMail);
                msg.Subject = subject;
                msg.Body = text;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential("forumcompany2016", "revivo123");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                Console.WriteLine(e.Data);
                return false;
            }
        }

        public bool Register(int forumID, string username, string password, string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive)
        {
            bool isRegistered = false;
            Forum forum;
            try
            {
                forum = Forums[forumID];
            }
            catch (Exception e )
            {
                forum = null;
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                
            }

            DateTime birthDate = new DateTime(birthYear, birthMonth, birthDay);

            // validation checking            
            if (forum != null && forum.checkValidation(username, password) && !CheckNames(firstName, lastName) 
                && CheckMail(eMail) && birthDate.CompareTo(DateTime.Now) <= 0)
            {
                Member member = new Member(username, password, firstName, lastName, birthDate, eMail, offlineInteractive);
                db.AddMember(forumID, username, password, firstName, lastName, eMail, member.Code, birthDate, DateTime.Now, false, offlineInteractive);
                forum.AddMember(member);
#if DEBUG
                Debug.WriteLine($"activation code is : {member.Code}");
#endif
#if !DEBUG
                    SendMail("User Verification", "Your verification code is: " + member.Code, eMail);
#endif
                if (Forums[forumID].Members.ContainsKey(username) == false) { isRegistered = false; }
                isRegistered = true;
            }
            return isRegistered;
        }

        private bool checkBirthDate(DateTime birthDate)
        {
            if (!(birthDate.CompareTo(DateTime.Now) <= 0))
                throw new Exception("Illegal birth date");
            return true;
        }

        private bool CheckMail(string eMail)
        {
            if (!eMail.Contains("@"))
                throw new Exception("Illegal mail");
            return true;
        }

        private bool CheckNames(string firstName, string lastName)
        {
            bool ret = firstName.Equals(string.Empty) || lastName.Equals(string.Empty);
            if (ret == true) throw new Exception("Invalid name");
            return false;
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            try
            {
                Forums[forumid].VerifyUser(username, code);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
            return true;
        }

        public Member GetMember(string memberUserName, int forumID)
        {
            return Forums[forumID].getMemberByName(memberUserName);
        }

        public SuperAdmin signUpSuperAdmin(string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            try
            {
                db.signUpSuperAdmin(username, password, firstName, lastName, birthDate, eMail);
                superAdmins[username] = new SuperAdmin(username, password, firstName, lastName, birthDate, eMail);
                return superAdmins[username];
            }
            catch(Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
            
        }

        public bool signInSuperAdmin(string username, string password)
        {
            return (superAdmins.ContainsKey(username) && superAdmins[username].password.Equals(password));
        }

        public SubForum GetSubForum(int iD, string subForumName)
        {
            return Forums[iD].GetSubForumByName(subForumName);
        }

        public Thread GetThreadByTitle(int forumid, int subforumid, string title)
        {
            return Forums[forumid].SubForums[subforumid].GetThreadByTitle(title);
        }

        public ForumPolicy CreatePolicy()
        {
            return new ForumPolicy();
        }

        public bool DestructPost(int forumid, int subforumid, int threadID, List<int> postIDs)
        {
            return Forums[forumid].DestructPost(subforumid, threadID, postIDs);
        }

        public bool DestructThread(int forumid, int subforumid, int threadID)
        {
            return Forums[forumid].DestructThread(subforumid, threadID);
        }

        public List<Post> GetPostReplies(int forumid, int subforumid, int threadid, List<int> postid)
        {
            return Forums[forumid].SubForums[subforumid].GetThreadByID(threadid).GetPostReplies(postid);
        }

        public bool IsUserRegisteredToForum(int forum, string user)
        {
            return Forums[forum].IsUserRegistered(user);
        }

        public PrimitiveResult Login(int iD, string username, string password)
        {
            PrimitiveResult output = new PrimitiveResult();
            output.Result = null;

            if (GetForum(iD).isSuspended(username))
            {
                output = new PrimitiveResult();
                output.Result = null;
                output.ErrorMessageg = "You are susspended from this forum. For any problem call 052-646-4356!";
                return output;
            }

            string sessionId = Session.GenerateSessionName(Forums[iD].Name, username);
            bool isUserExistButNotVerify=false;
            if (username == "guest" && password == "guest")
            {
                output.Result = new MinimizedUserInForum();
                output.Result.UserName = username;
                output.Result.State = UserTypeInForum.Guest;
            }
            
            // error when trying to login to session without session password
            else if (sessions.ContainsKey(sessionId) && !password.Equals(sessions[sessionId].getPassword())) { output.ErrorMessageg = "correct info, but we require SESSION PASSWORD instead of the regular password!"; }

            // login and start session
            else if ((sessions.ContainsKey(sessionId) == false) && GetForum(iD).Login(username, password,out isUserExistButNotVerify))
            {
                Session newSession = new Session(Forums[iD].Name, username);
                sessions[sessionId] = newSession;
                lastSession = sessions[sessionId].getPassword();

                output.Result = new MinimizedUserInForum();
                output.Result.UserName = username;
                output.Result.State = getUserType(username, iD);
                output.Result.SessionPassword = sessions[sessionId].getPassword();
                output.Result.OfflineInteractive = GetMember(username, iD).OfflineInteractive;
            }
            else if (isUserExistButNotVerify)
            {
                output.Result = new MinimizedUserInForum();
                output.Result.UserName = username;
                output.Result.State = UserTypeInForum.NotApproved;
            }
            // login to existing session
            else if (sessions.ContainsKey(sessionId) && password.Equals(sessions[sessionId].getPassword()))
            {
                sessions[sessionId].addDevice();
                output.Result = new MinimizedUserInForum();
                output.Result.UserName = username;
                output.Result.State = getUserType(username, iD);
                output.Result.SessionPassword = sessions[sessionId].getPassword();
            }
            else { output.ErrorMessageg = "can't find matching username/password"; }
            return output;
        }

        private UserTypeInForum getUserType(string username, int iD)
        {
            if (superAdmins.ContainsKey(username))
            {
                return UserTypeInForum.SuperAdmin;
            }
            else if (IsUserAdminOfForum(iD, username))
            {
                return UserTypeInForum.Admin;
            }
            else if (IsUserRegisteredToForum(iD, username))
            {
                return UserTypeInForum.Member;
            }
            else { return UserTypeInForum.Guest; }
        }

        private UserTypeInSubForum getUserTypeInSubforum(string username, int iD, int subForrumID)
        {
            if (superAdmins.ContainsKey(username))
            {
                return UserTypeInSubForum.SuperAdmin;
            }
            else if (IsUserAdminOfForum(iD, username))
            {
                return UserTypeInSubForum.Admin;
            }
            else if (isUserModeratorOfSubForum(iD, subForrumID, username))
            {
                return UserTypeInSubForum.Moderator;
            }
            else if (IsUserRegisteredToForum(iD, username))
            {
                return UserTypeInSubForum.Member;
            }
            else { return UserTypeInSubForum.Guest; }
        }
        

        public bool SetForumPolicy(int iD, ForumPolicy policy)
        {
            Forums[iD].SetPolicy(policy);
            return true;
        }

        public bool PromoteToAdmin(int forumId, string usernamePromoted, string usernamePromoter)
        {
            return Forums[forumId].Promote(usernamePromoted, usernamePromoter);
        }

        public SuperAdmin initSuperAdmin(string username, string password)
        {
            return signUpSuperAdmin(username, password, username, username, DateTime.Now, "nomail@admin.com");            
        }

        public MinimizedThread findThread(int forumid, int subforumid, int threadid)
        {
            try
            {
                return Forums[forumid].SubForums[subforumid].GetThreadByID(threadid).minimize();
            }
            catch(Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            MinimizedPost min = null;
            try
            {
                min = Forums[forumid].SubForums[subforumid].Threads[threadid].findPostObject(postid).minimize();
            }
            catch (Exception e) { ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message); }
            return min;
            //if (p != null)
            //    return Forums[forumid].findPostObject(subforumid, threadid, postid).minimize();
            //else return null;
        }
        
        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            if (isAllowedToDeleteThread(forumid, subforumid, threadid, deleter)) { return DestructThread(forumid, subforumid, threadid); }
            else { return false; }
        }

        private bool isAllowedToDeleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            SubForum sf = Forums[forumid].SubForums[subforumid];
            if(deleter.Equals("Guest") || deleter.Equals("guest"))
            {
                //throw new Exception("Guest cannot delete thread");
                return false;
            }
            return (sf.Threads[threadid].OpenningPost.Writer.getUsername().Equals(deleter)
                || IsUserAdminOfForum(forumid, deleter)
                || isUserModeratorOfSubForum(forumid, subforumid, deleter));
        }

        public string GetVerificationCode(int forumID, string username)
        {
            return Forums[forumID].GetVerificationCode(username);
        }

        public int GetForumIdByName(string forumName)
        {
            foreach (var forum in Forums) { if (forum.Value.Name.Equals(forumName)) { return forum.Value.ID; } }
            return -1;
        }

        public bool IsUserAdminOfForum(int forumId, string username)
        {
            return Forums[forumId].ForumAdmins.ContainsKey(username);
        }

        public int GetSubForumIdByName(string subForumName, int forumId)
        {
            return Forums[forumId].GetSubForumByName(subForumName).ID;
        }

        public bool AddModerator(string username, int forumID, int subForumID)
        {
            Member user = Forums[forumID].Members[username];
            return Forums[forumID].SubForums[subForumID].AddModerator(user);
        }

        public void ResetToDefaultPolicy(int iD)
        {
            Forums[iD].ResetToDefaultPolicy();
        }

        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            return Forums[forumid].SubForums[subforumid].GetThreadByTitle(title).minimize();
        }

        public bool CanGuestStartThread(int iD)
        {
            return Forums[iD].isCanGuestStartThread();
        }

        public bool CanGuestReplyToPost(int iD)
        {
            return Forums[iD].isCanGuestReplyToPost();
        }

        public List<int> replyToPost(int forum, int subForum, int thread, List<int> post, string creator, string text)
        {
            if ((creator.Equals("guest")|| creator.Equals("Guest")) && Forums[forum].Policy.GuestCanPost == false)
                throw new Exception("Guest can't reply to post");
            return Forums[forum].ReplyToPost(subForum, thread, post, creator, text);
        }

        public int findThreadId(int forum, int subForum, string title)
        {
            Thread thread = Forums[forum].SubForums[subForum].GetThreadByTitle(title);
            int output = (thread == null) ? -1 : thread.id;
            return output;
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
            return GetMember(senderUserName, forumID).sendMessage(GetMember(targetUsername, forumID), text);
        }

        public bool isUserModeratorOfSubForum(int forum, int subforum, string username)
        {
            return Forums[forum].SubForums[subforum].isModerator(Forums[forum].getMemberByName(username));
        }

        public DateTime? getModTimeLimit(int forum, int subforum, string username)
        {
            return Forums[forum].getModTimeLimit(subforum, username);
            //SubForum sf = Forums[forum].SubForums[subforum];
            //Member m = Forums[forum].getMemberByName(username);
            //return sf.ModsTimeLimit[m];
        }

        public bool IsSystemIniaitlized()
        {
            return (superAdmins.Count > 0);
        }

        public Dictionary<int, string> GetAllForums() // done together :)
        {
            Dictionary <int, string> minimizedDictionary = new Dictionary<int, string>();

            foreach (var forum in Forums)
            {
                minimizedDictionary.Add(forum.Key, forum.Value.Name);
            }

            return minimizedDictionary;
        }

        public bool editModTimeLimit(int forum, int subforum, string modName, DateTime newUntilWhen)
        {
            return Forums[forum].editModTimeLimit(subforum, modName, newUntilWhen);
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            return GetForum(_forum).EditPost(_subForum, _threadId, postId, poster, newContent);
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            return Forums[forumID].Members[username].GetAllFriends();
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            return Forums[forumId].SubForums[subForumID].GetAllThreads(Forums[forumId].Name);
        }

        public ForumPolicy GetForumPolicy(int forumId)
        {
            return Forums[forumId].GetPolicy();
        }

        public string getLastSubForumName(int forumId)
        {
            return Forums[forumId].getLastSubForumName();
        }

        public List<Notification> GetNotifications(string forumname, string userName)
        {
            return GetForum(forumname).Members[userName].GetNotifications();
        }

        public List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID)
        {
            return Forums[forumID].SubForums[subForumID].GetSubForumModerators(Forums[forumID].Name);
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            return Forums[forumId].GetSubForumByName(subForumName).UnpromoteModerator(Forums[forumId].Name, adminUnpromoting, moderatorToUnpromote);
        }

        public bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote)
        {
            if (superAdmins.ContainsKey(superAdminUnpromoting) == false) { return false; }
            return Forums[forumId].UnpromoteAdmin(adminToUnpromote);
        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            // this returns a user, which is used to know if he is a mod.
            MinimizedUserInSubForum output = new MinimizedUserInSubForum();
            output.State = getUserTypeInSubforum(userName, forumID, subForrumID);
            output.UserName = userName;
            return output;
        }

        public string getLastUserName(int forumId)
        {
            return Forums[forumId].GetLastUsername();
        }

        public string getUsersEmail(int forumId, string lastUserName)
        {
            return Forums[forumId].GetUserEmail(lastUserName);
        }

        public string getLastForumName()
        {
            return db.GetLastForumName();
        }

        public List<string> GetForumAdmins(int forumId)
        {
            return Forums[forumId].GetAdmins();
        }

        public string getSecondLastForumName()
        {
            return db.getSecondLastForumName();
        }

        public string getLastThreadTitle(int forum1Id, string subForum)
        {
            return Forums[forum1Id].getLastThreadTitle(subForum);
        }

        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            return Forums[forumid].getAllMinimizedUserInSubForum(subforumid);
        }

        public void ClearDatabase()
        {
            string sql = "DELETE FROM " + DbConsts.Table_ForumAdmins;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Forums;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Friends;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Members;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Messages;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Moderators;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Notifications;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Policies;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Posts;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_SubForums;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_SuperAdmins;
            db.ExecuteQuery(sql);
            sql = "DELETE FROM " + DbConsts.Table_Threads;
            db.ExecuteQuery(sql);
            superAdmins = new Dictionary<string, SuperAdmin>();
            Forums = new Dictionary<int, Forum>();
        }

        public List<string> getAllUserNames(int forumID)
        {
            List<string> ans = new List<string>();
            foreach(string s in Forums[forumID].Members.Keys)
            {
                ans.Add(s);
            }
            return ans;
        }

        public void clearForumsFromRAM()
        {
            Forums = new Dictionary<int, Forum>();
        }

        public void AttachObserver(Observer growlNotifiactions, int forum, string user)
        {
            HttpChannel channel;
            string path = $"http://localhost:8080/{forum}{user}";
            try
            {
                channel = new HttpChannel();
                ChannelServices.RegisterChannel(channel, false);
                
                // Registers the remote class. (This could be done with a
                // configuration file instead of a direct call.)


            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
            }
            
                RemotingConfiguration.RegisterWellKnownClientType(
                        typeof(ObserverPattern.Observer),
                        path);
            Observer observer = new ObserverPattern.Observer();
            Attach(observer, forum, user);

        }

        public void DetachObserver(Observer growlNotifiactions, int forum, string user)
        {
            Detach(growlNotifiactions, forum, user);
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void SendNotification(string title, string message, List<string> toSend)
        {
            int forumid = Int32.Parse(toSend[0].Split(' ')[0]);
            string forumname = GetForum(forumid).Name;

            System.Threading.Thread t = new System.Threading.Thread(() =>
            {
                System.Threading.Thread.CurrentThread.IsBackground = true;
                Notify(title, message, toSend, db, forumname);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            return db.GetNotificationsString(forumName, userName, getUnreadNotifications);
        }

        public void LogOut(string forumname, string username)
        {
            string sessionId = Session.GenerateSessionName(forumname, username);
            sessions[sessionId].removeDevice();
            if (sessions[sessionId].getConnectedDevices() == 0) { sessions.Remove(sessionId); }
        }

        public void SaveNotification(int forumid, string user, string title, string s, int isRead)
        {
            GetForum(forumid).SaveNotification(user, title, s, isRead);
        }

        public List<string> GetAllRequestFriends(int forumId, string userName)
        {
            return Forums[forumId].GetAllRequestFriends(userName);
        }

        public void Unfriend(int forumid, string username, string usernameToUnfriend)
        {
            Forums[forumid].Unfriend(username, usernameToUnfriend);
        }

        public int GetNumOfPosts(int forumid)
        {
            return Forums[forumid].GetNumOfPosts();
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            return Forums[forumid].getAllMinimizedUserInForum();
        }

        public string GetsubForumNameByID(int forumid, int subForumid)
        {
            return Forums[forumid].GetsubForumNameByID(subForumid);
        }

        public bool SendNewCodeToMail(int forumID, string username)
        {
            string code = GetVerificationCode(forumID, username);
            Member m = GetMember(username, forumID);
#if DEBUG
            Debug.WriteLine($"activation code is : {code}");
#endif
#if !DEBUG
                    SendMail("User Verification", "Your verification code is: " + code, m.Email);
#endif
            return true;
        }

        public string getLastSessionNum()
        {
            return lastSession;
        }

        public bool changePassword(int forumId, string username, string newPass)
        {
            return Forums[forumId].changePassword(username, newPass);
        }

        public bool IsUserSuperAdmin(string username)
        {
            return db.IsUserSuperAdmin(username);
        }

        public bool isUserSuspended(int forumID, string friendUserName)
        {
            return Forums[forumID].isSuspended(friendUserName);
        }

        public void suspend(int forumID, string friendUserName)
        {
            Forums[forumID].suspend(friendUserName);
        }

        public void unSuspend(int forumID, string friendUserName)
        {
            Forums[forumID].unSuspend(friendUserName);
        }

        public ForumControllerLocal(List<Forum> forums)
        {
        }
    }
}