﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.Models.ForumManagement
{
    public class ForumPolicy
    {
        public int MaxModeratorsNumber { get; set; }
        public int MinPasswordLetters { get; set; }
        public int MinUserNameLetters { get; set; }
        public bool PasswordRequiredUpperLowerCase { get; set; }
        public bool PasswordReuqiredNumbers { get; set; }
        public int SuspendModeratorMinTime { get; set; }
        public int SuspendAdminMinTime { get; set; }
        public int SuspendMemberMinTime { get; set; }
        public bool GuestCanPost { get; set; }
        public bool GuestCanStartThread { get; set; }
        public int MinSeniorityForMediatorToEditAPost { get; set; }
        public int MinSeniorityForMediatorToRemoveAPost { get; set; }
        public int Interactive { get; set; }

        public const int ONLINE_NOTIFICATION = 0;
        public const int OFFLINE_NOTIFICATION = 1;
        public const int USER_CHOISE_NOTIFICATIONS = 2;

        public ForumPolicy()
        {
            MaxModeratorsNumber = 10;
            MinPasswordLetters = 5;
            MinUserNameLetters = 5;
            PasswordRequiredUpperLowerCase = false;
            PasswordReuqiredNumbers = false;
            SuspendModeratorMinTime = 0;
            SuspendAdminMinTime = 0;
            SuspendMemberMinTime = 0;
            GuestCanPost = true;
            GuestCanStartThread = true;
            MinSeniorityForMediatorToEditAPost = 0;
            MinSeniorityForMediatorToRemoveAPost = 0;
            Interactive = OFFLINE_NOTIFICATION;
        }

        public ForumPolicy(int maxModeratorsNumber, int minPasswordLetters, int minUserNameLetters,
            bool passwordRequiredUpperLowerCase, bool passwordReuqiredNumbers, 
            int suspendModeratorMinTime, int suspendAdminMinTime,
            int suspendMemberMinTime, bool guestCanPost, bool guestCanStartThread,
            int minSeniorityForMediatorToEditAPost, int minSeniorityForMediatorToRemoveAPost, int interactive)
        {
            MaxModeratorsNumber = maxModeratorsNumber;
            MinPasswordLetters = minPasswordLetters;
            MinUserNameLetters = minPasswordLetters;
            PasswordRequiredUpperLowerCase = passwordRequiredUpperLowerCase;
            PasswordReuqiredNumbers = passwordReuqiredNumbers;
            SuspendModeratorMinTime = suspendModeratorMinTime;
            SuspendAdminMinTime = suspendAdminMinTime;
            SuspendMemberMinTime = suspendMemberMinTime;
            GuestCanPost = guestCanPost;
            GuestCanStartThread = guestCanStartThread;
            MinSeniorityForMediatorToEditAPost = minSeniorityForMediatorToEditAPost;
            MinSeniorityForMediatorToRemoveAPost = minSeniorityForMediatorToRemoveAPost;
            Interactive = interactive;
        }

        internal bool CheckNumModerators(int count)
        {
            return count < MaxModeratorsNumber;
        }
        // this is a stupid comment only for merge [=
        public bool CheckPassword(string password)
        {
            //bool legal = true;
            if (PasswordRequiredUpperLowerCase && !(password.Any(char.IsUpper) && password.Any(char.IsLower)))
            {
                //legal = false;
                throw new Exception("Password must contain upper and lower letter");
            }
            if (PasswordReuqiredNumbers && !password.Any(char.IsDigit))
            {
                //legal = false;
                throw new Exception("Password must contain number");
            }
            if (password.Length < MinPasswordLetters)
            {
                //legal = false;
                throw new Exception("Password to short");
            }
            //return legal;
            return true;
        }

        internal bool LegalMemberSuspensionTime(int days)
        {
            return days >= SuspendMemberMinTime;
        }

        internal bool LegalModeratorsSuspensionTime(int days)
        {
            return days >= SuspendModeratorMinTime;
        }

        public bool CheckUserName(string userName)
        {
            //return userName.Length >= MinUserNameLetters && !userName.Contains("Guest");
            if (userName.Length < MinUserNameLetters)
                throw new Exception("'" + userName + "' contains less than " + MinUserNameLetters);
            if (userName.IndexOf("Guest", StringComparison.OrdinalIgnoreCase) >= 0)
                throw new Exception("Invalid Username. Contains 'Guest'.");
            return true;
        }
    }
}
