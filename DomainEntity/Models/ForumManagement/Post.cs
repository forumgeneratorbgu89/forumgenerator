﻿//using Domain.Models.UserManagement;
using DomainEntity.Models.UserManagement;
using System;
using System.Collections.Generic;
using DomainEntity.MinimizedModels;

namespace DomainEntity.Models.ForumManagement
{
    public class Post
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public User Writer { get; set; }
        public Post Father { get; set; }
        private Dictionary<int, Post> Comments;
        public Thread Thread { get; set; }
        public DateTime Time { get; set; }

        public Post(int ID, User writer, string content, Post father, Thread thread, DateTime time)
        {
            this.ID = ID;
            Content = content;
            Writer = writer;
            Father = father;
            Thread = thread;
            Comments = new Dictionary<int, Post>();
            Time = time;
        }

        internal MinimizedPost minimize()
        {
            MinimizedPost output = new MinimizedPost();
            output.Date = this.Time;
            output.PostId = this.ID;
            output.Text = this.Content;
            string writerUserName = Writer != null ? Writer.getUsername() : "guest";
            output.userNameWriter = writerUserName;
            return output;
        }

        //public bool Destructor()
        //{
        //    bool isDestruct = false;
        //    if (Father == null)
        //    {
        //        isDestruct = Thread.Destructor();
        //    }
        //    else
        //    {
        //        isDestruct = Father.DeleteComment(ID);
        //    }
        //    return isDestruct;
        //}

        // not in use
        private bool DeleteComment(int postID)
        {
            throw new NotImplementedException();
        }

        // not in use
        public int PostComment(User postWriter,string postContent)
        {
            throw new NotImplementedException();
        }

        // not in use
        private int increasePostID()
        {
            return ID++;
        }

        // not in use
        // returns true if the user is the owner of message
        public bool CanEditContent(User m)
        {
            return Writer.Equals(m);
        }

        // not in use
        public void EditPost(string content)
        {
            Content = content;
        }

        // not in use
        public void AddThread(Thread t)
        {
            Thread = t;
        }

        // not in use
        internal bool ContainPost(int post)
        {
            throw new NotImplementedException();
        }

        // not in use
        public List<Post> getComtents()
        {
            throw new NotImplementedException();
        }

        // not in use
        internal bool removeComment(int postID)
        {
            throw new NotImplementedException();
        }

        
    }
}