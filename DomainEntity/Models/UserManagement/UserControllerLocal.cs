﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using DomainEntity.Models.ForumManagement;
using DomainEntity.MinimizedModels;

namespace DomainEntity.Models.UserManagement
{
    public class UserControllerLocal : IuserController
    {
        private static UserControllerLocal instance = null;

        public IforumController ForumController;

        private UserControllerLocal()
        {
            ForumController = ForumControllerLocal.GetInstance();
        }

        private UserControllerLocal(List<Forum> forums){}

        public static UserControllerLocal GetInstance()
        {
            if(instance == null)
            {
                instance = new UserControllerLocal();
            }
            return instance;
        }

        // should return an empty list if problem
        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            try
            {
                Member memberTarget = ForumController.GetMember(usernameTarget, forumID);
                Member memberSender = ForumController.GetMember(userNameSender, forumID);
                List<MinimizedMessage> lst1 = memberTarget.GetIncomingMessagesOfUser(memberSender);
                List<MinimizedMessage> lst2 = memberSender.GetIncomingMessagesOfUser(memberTarget);
                lst1.AddRange(lst2);
                lst1.Sort(new DateComparer());
                int leng = lst1.Count;
                for(int i = 0; i < leng-1; i++)
                {
                    if((lst1.ElementAt(i).date.Subtract(lst1.ElementAt(i+1).date)).Seconds <= 2
                        && ((lst1.ElementAt(i).date.Subtract(lst1.ElementAt(i + 1).date)).Seconds >= -2))
                    {
                        lst1.RemoveAt(i);
                        break;
                    }
                }
                return lst1;
            }
            catch(Exception e)
            {
                // it means forumID not good
                return null;
            }
        }

        // name, last, user, email
        public Tuple<string, string, string, string> GetUserDetails(int forumID, string userName)
        {
            try
            {
                Member member = ForumController.GetMember(userName, forumID);
                return new Tuple<string, string, string, string>(member.firstName, member.lastName, userName, member.Email);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool AddFreind(string sender, string reciver, int forumid)
        {
            try
            {
                Member memberReciever = ForumController.GetMember(reciver, forumid);
                Member memberSender = ForumController.GetMember(sender, forumid);
                return memberSender.sendFriendRequest(memberReciever);
            }
            catch (Exception)
            {
                // it means forumID not good
                return false;
            }
        }

        public bool ConfirmFriendRequest(string sender, string reciver, int forumid)
        {
            try
            {
            Member memberReciever = ForumController.GetMember(reciver, forumid);
            Member memberSender = ForumController.GetMember(sender, forumid);
            return memberReciever.confirmFriendRequest(memberSender);
            }
            catch(Exception)
            {
                // it means forumID not good
                return false;
            }
        }

        public bool IsUserApproved(string member, int forumid)
        {
            return ForumController.GetMember(member, forumid).IsVerified();
        }

        private class DateComparer : IComparer<MinimizedMessage>
        {
            public int Compare(MinimizedMessage x, MinimizedMessage y)
            {
                return x.date.CompareTo(y.date);
            }
        }
    }
}