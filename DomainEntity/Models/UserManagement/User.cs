﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.Models.UserManagement
{
    public interface User
    {
        string getUsername();
    }
}
