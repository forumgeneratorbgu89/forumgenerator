﻿using System;
using System.Collections.Generic;
using DAL;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;
using DomainEntity.Models.ForumManagement;
using System.Threading;

namespace DomainEntity.Models.UserManagement
{
    public class Member:User
    {
        public int ID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public DateTime? birthDate { get; set; }
        public DateTime suspendExpire { get; set; }
        public string ForumID { get; set; } // this is forumname
        public bool isVerified { get; set; }
        private MemberMapper db = new MemberMapper();
        private ForumMapper dbForum = new ForumMapper();
        public int OfflineInteractive { get; set; }

        public const int OfflineInteractiveOn = 1;
        public const int OfflineInteractiveOff = 0;

        private List<Notification> _notifications;
        public List<Notification> Notifications
        {
            set { _notifications = value; }
            get
            {
                if (_notifications.Count == 0)
                {
                    List<List<string>> notifications = dbForum.GetNotifications(ForumID, username);
                    foreach (List<string> notif in notifications)
                    {
                        /*Notification n = new Notification();
                        n.ID = Int32.Parse(notif[0]);
                        n. = notif[1];
                        n.ID = notif[2];
                        n.ID = notif[3];
                        [DbConsts.Notifications_ID]);
            notification.Add((string)reader[DbConsts.Notifications_ForumName]);
                notification.Add((string)reader[DbConsts.Notifications_Username]);
                notification.Add((string)reader[DbConsts.Notifications_Content*/
                    }
                }
                return _notifications;
            }
        }

        public Dictionary<string, Member> _friendrRequests;
        public Dictionary<string, Member> friendrRequests
        {
            set { _friendrRequests = value; }
            get
            {
                if (_friendrRequests.Count == 0)
                {
                    List<string> allFriends = dbForum.GetAllFriendRequests(username);
                    foreach (string friend in allFriends)
                    {
                        Member m = ForumControllerLocal.GetInstance().GetMember(friend, dbForum.GetForumIdByName(ForumID));
                        _friendrRequests.Add(friend, m);
                    }
                }
                return _friendrRequests;
            }
        }

        private Dictionary<string, Member> _friends;
        public Dictionary<string, Member> friends
        {
            set { _friends = value; }
            get
            {
                if (_friends.Count == 0)
                {
                    List<string> allFriends = dbForum.GetAllFriends(username);
                    foreach(string friend in allFriends)
                    {
                        Member m = ForumControllerLocal.GetInstance().GetMember(friend, dbForum.GetForumIdByName(ForumID));
                        _friends.Add(friend, m);
                    }
                }
                return _friends;
            }
        }

        private Dictionary<string, List<Message>> _friendMessages;
        public Dictionary<string, List<Message>> friendMessages
        {
            set { _friendMessages = value; }
            get
            {
                if (_friendMessages.Count == 0)
                {
                    List<List<string>> incoming = dbForum.GetIncomingMessages(ForumID, username);
                    foreach (List<string> message in incoming)
                    {
                        if (!_friendMessages.ContainsKey(message[0]))
                        {
                            _friendMessages.Add(message[0], new List<Message>());
                        }
                        _friendMessages[message[0]].Add(new Message(message[1],
                            ForumControllerLocal.GetInstance().GetMember(message[0], dbForum.GetForumIdByName(ForumID)), this));
                        _friendMessages[message[0]][_friendMessages[message[0]].Count - 1].Time = DateTime.Parse(message[2]);
                        /*if (friends.ContainsKey(message[0]))
                        {
                            if (!_friendMessages.ContainsKey(message[0]))
                            {
                                _friendMessages.Add(message[0], new List<Message>());
                            }
                            _friendMessages[message[0]].Add(new Message(message[1],
                                ForumControllerLocal.GetInstance().GetMember(message[0], dbForum.GetForumIdByName(ForumID)), this));
                            _friendMessages[message[0]][_friendMessages[message[0]].Count - 1].Time = DateTime.Parse(message[2]);
                        }*/
                    }
                }
                return _friendMessages;
            }
        }

        /*private Dictionary<string, List<Message>> _othersMessages;
        public Dictionary<string, List<Message>> othersMessages
        {
            set { _othersMessages = value; }
            get
            {
                if (_othersMessages.Count == 0)
                {
                    List<List<string>> incoming = dbForum.GetIncomingMessages(ForumID, username);
                    foreach(List<string> message in incoming)
                    {
                        if (!friends.ContainsKey(message[0]))
                        {
                            if(!_othersMessages.ContainsKey(message[0]))
                            {
                                _othersMessages.Add(message[0], new List<Message>());
                            }
                            _othersMessages[message[0]].Add(new Message(message[1],
                                ForumControllerLocal.GetInstance().GetMember(message[0], dbForum.GetForumIdByName(ForumID)), this));
                            _othersMessages[message[0]][_othersMessages[message[0]].Count - 1].Time = DateTime.Parse(message[2]);
                        }
                    }
                }
                return _othersMessages;
            }
        }*/

        public Member(string username, string password,
            string firstName, string lastName, DateTime? birthDate, string eMail, int offlineIteractive)
        {
            this.username = username;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            Code = this.GetHashCode().ToString();
            suspendExpire = DateTime.Today;
            this.Email = eMail;
            friends = new Dictionary<string, Member>();
            friendrRequests = new Dictionary<string, Member>();
            friendMessages = new Dictionary<string, List<Message>>();
            //othersMessages = new Dictionary<string, List<Message>>();
            isVerified = false;
            OfflineInteractive = offlineIteractive;
        }

        public Member(string username, string password,
            string firstName, string lastName, string eMail, string code, DateTime birthDate, 
            DateTime suspendExpire, bool isVerified, string forumName, int offlineIteractive)
        {
            this.username = username;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.Email = eMail;
            this.Code = code;
            this.birthDate = birthDate;
            this.suspendExpire = suspendExpire;
            this.isVerified = isVerified;
            this.ForumID = forumName;
            friends = new Dictionary<string, Member>();
            friendrRequests = new Dictionary<string, Member>();
            friendMessages = new Dictionary<string, List<Message>>();
            //othersMessages = new Dictionary<string, List<Message>>();
            OfflineInteractive = offlineIteractive;
        }

        public Member(int ID, string Username, string Pass, string FirstName, string LastName, string Email, string Code, DateTime Bdate, DateTime Susdate, bool IsVer, string forumName, int offlineIteractive)
        {
            this.ID = ID;
            this.username = Username;
            this.password = Pass;
            this.firstName = FirstName;
            this.lastName = LastName;
            this.Email = Email;
            this.Code = Code;
            this.birthDate = Bdate;
            this.suspendExpire = Susdate;
            this.isVerified = IsVer;
            this.ForumID = forumName;
            OfflineInteractive = offlineIteractive;
        }

        public string getUsername()
        {
            return username;
        }

        public void verify() // is registration completed by answer the mail
        {
            db.Verify(username);
            isVerified = true;
        }

        public bool IsVerified()
        {
            return isVerified;
        }

        public bool isFriend(Member member)
        {
            if(member == null)
            {
                return false;
            }
            return friends.ContainsKey(member.username);
        }

        public bool confirmFriendRequest(Member member)
        {
            if (friendrRequests.ContainsKey(member.username))
            {
                try
                {
                    friendMessages.Add(member.username, new List<Message>());
                }
                catch (Exception)
                {
                }
                /*if (othersMessages.ContainsKey(member.username))
                {
                    MoveFromOthersToFriends(member);
                }
                else
                {
                    try
                    {
                        friendMessages.Add(member.username, new List<Message>());
                    }
                    catch (Exception)
                    {
                    }
                }*/
                _friends.Add(member.username, friendrRequests[member.username]);
                _friendrRequests.Remove(member.username);
                member.friendRequestApprooved(this);
            }
            dbForum.ConfirmFriendRequest(member.username, username, ForumID);
            return friends.ContainsKey(member.username);
        }

        private void recieveFriendRequests (Member member)
        {
            // add to ram
            friendrRequests.Add(member.username, member);
            // add to db
            dbForum.SendFriendRequest(member.username, username, ForumID);
            ForumControllerLocal.GetInstance().SendNotification("Got friend request", member.firstName + "sent you a friend request", new List<string> { ForumControllerLocal.GetInstance().GetForumIdByName(ForumID) + " " + username });
        }

        public void removeFriend(Member member)
        {
            _friends.Remove(member.username);
            //MoveFromFriendsToOthers(member);
            member.removedBy(this);
        }

        /*private void MoveFromFriendsToOthers(Member member)
        {
            _othersMessages.Add(member.username, friendMessages[member.username]);
            _friendMessages.Remove(member.username);
        }*/

        private void removedBy(Member member)
        {
            // from ram
            _friends.Remove(member.username);
            //MoveFromFriendsToOthers(member);
            // from db
            dbForum.RemoveFriend(username, member.username, ForumID);
        }

        private void friendRequestApprooved (Member member)
        {
            /*if (othersMessages.ContainsKey(member.username))
            {
                MoveFromOthersToFriends(member);
            }
            else
            {
                friendMessages.Add(member.username, new List<Message>());
            }*/
            try
            {
                friendMessages.Add(member.username, new List<Message>());
            }
            catch (Exception)
            {
                
            }
            _friends.Add(member.username, member);
            _friendrRequests.Remove(member.username);
        }

        /*private void MoveFromOthersToFriends(Member member)
        {
            _friendMessages.Add(member.username, othersMessages[member.username]);
            _othersMessages.Remove(member.username);
        }*/

        public bool sendFriendRequest(Member member)
        {
            if(member == null)
            {
                return false;
            }
            member.recieveFriendRequests(this);
            return true;
        }

        public void suspend (int days)
        {
            // on ram
            this.suspendExpire = DateTime.Today.AddDays(days);
            // on db
            db.UpdateSuspendDate(username, this.suspendExpire);
        }

        internal void unSuspend()
        {
            // on ram
            this.suspendExpire = DateTime.Now;
            // on db
            db.UpdateSuspendDate(username, this.suspendExpire);
        }

        // Return true if the user is in a suspention right now
        public bool isSuspend()
        {
            return suspendExpire.CompareTo(DateTime.Today) < 0;
        }

        public bool sendMessage(Member reciever, string text)
        {
            Message m = new Message(text, this, reciever);
            bool isFriend = false;
            if (friendMessages.ContainsKey(reciever.username))
            {
                isFriend = true;
                // add to ram
                //friendMessages[reciever.username].Add(m);
            }
            else
            {
                isFriend = false;
                if (!friendMessages.ContainsKey(reciever.username))
                {
                    friendMessages.Add(reciever.username, new List<Message>());
                }
                // to ram
                //othersMessages[reciever.username].Add(m);
            }
            // add to db
            db.AddMessage(ForumID, username, reciever.username, text, isFriend);
            return reciever.recieveMessage(this, text);
            //return true;
        }

        private bool recieveMessage(Member member, string text)
        {
            string name = member.username;
            Message message = new Message(text, member, this);
            if (friendMessages.ContainsKey(name))
            {
                friendMessages[name].Add(message);
            }
            else
            {
                if (!friendMessages.ContainsKey(name))
                {
                    friendMessages.Add(name, new List<Message>());
                }
                friendMessages[name].Add(message);
            }
            return true;
        }

        public List<MinimizedMessage> GetIncomingMessagesOfUser(Member senderMember)
        {
            if(senderMember == null)
            {
                return new List<MinimizedMessage>();
            }
            // this function checks if senderMember is a friend if true returns the list of Minimized messages from senderMember
            // from FriendsMessages dictionary, otherwise return it from the 
            List<MinimizedMessage> allMinimizedMessages = new List<MinimizedMessage>();
            List<Message> messages = friendMessages[senderMember.username];
            foreach(Message message in messages)
            {
                allMinimizedMessages.Add(message.minimize());
            }
            return allMinimizedMessages;
        }

        internal List<Notification> GetNotifications()
        {
            return Notifications;
        }

        internal List<string> GetAllFriends()
        {
            List<string> allStringFriends = new List<string>();
            foreach(Member member in friends.Values)
            {
                allStringFriends.Add(member.username);
            }
            return allStringFriends;
        }

        public void SaveNotification(string title, string s, int isRead)
        {
            db.saveNotification(ForumID, username, title, s, isRead);
        }

        public IEnumerable<KeyValuePair<string, Member>> GetFriendRequests()
        {
            return friendrRequests;
        }

        internal bool changePassword(string newPass)
        {
            return db.changePassword(ForumID, username, newPass);
        }
    }
}
