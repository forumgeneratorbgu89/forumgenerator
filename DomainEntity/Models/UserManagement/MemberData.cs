﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.Models.UserManagement
{
    public class MemberData
    {
        public String ID { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String eMail { get; set; }
        public DateTime birthDate { get; set; }

        public MemberData(String id, String username, String password,
            String firstName, String lastName, DateTime birthDate, String eMail)
        {
            this.ID = id;
            this.username = username;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
        }
    }
}
