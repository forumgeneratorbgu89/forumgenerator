﻿//using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.Models.UserManagement
{
    public class SuperAdmin:Member
    {
        public SuperAdmin (String username, String password,
            string firstName, String lastName, DateTime? birthDate, String eMail): 
            base (username, password, firstName, lastName, birthDate, eMail, Member.OfflineInteractiveOn)
        {
            isVerified = true;
        }

        
    }
}
