﻿using System.Collections.Generic;
//using Domain.Models.ForumManagement;

namespace DomainEntity.Models.UserManagement
{
    public class Notification
    {
        public int ID { get; set; }
        public int SubForum { get; set; }
        public int Thread { get; set; }
        public List<int> Post { get; set; }
    }
}