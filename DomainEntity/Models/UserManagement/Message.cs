﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.MinimizedModels;
//using Domain.Models.UserManagement;

namespace DomainEntity.Models.UserManagement
{
    public class Message
    {
        public string message { get; set; }
        public Member from { get; set; }
        public Member to { get; set; }
        public DateTime Time { get; set; }

        public Message (string message, Member from, Member to)
        {
            this.message = message;
            this.from = from;
            this.to = to;
            this.Time = DateTime.Now;
        }

        internal MinimizedMessage minimize()
        {
            MinimizedMessage mm = new MinimizedMessage();
            mm.content = message;
            mm.date = Time;
            mm.from = from.username;
            mm.to = to.username;
            return mm;
        }
    }
}
