﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.MinimizedModels
{
    public class MinimizedUserInForum
    {
        public string SessionPassword;

        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private int offlineInteractive;

        public int OfflineInteractive
        {
            get { return offlineInteractive; }
            set { offlineInteractive = value; }
        }

        private UserTypeInForum state;


        public MinimizedUserInForum()
        {

        }

        public MinimizedUserInForum(MinimizedUserInForum result)
        {
            this.State = result.State;
            this.OfflineInteractive = result.OfflineInteractive;
            this.UserName = result.UserName;
        }

        public UserTypeInForum State
        {
            get { return state; }
            set { state = value; }
        }


    }
    public enum UserTypeInForum
    {
        Guest ,
        NotApproved ,
        Admin ,
        Member,
        SuperAdmin,
        Susspend
    }
}
