﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.MinimizedModels
{
    public class MinimizedSubForum
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
    }
}
