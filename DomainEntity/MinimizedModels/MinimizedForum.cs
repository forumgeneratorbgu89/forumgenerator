﻿namespace DomainEntity.ReturnedValues
{
    public class MinimizedForum
    {
        private string foroumName;

        public string ForumName
        {
            get { return foroumName; }
            set { foroumName = value; }
        }

        private int forumID;

        public MinimizedForum(MinimizedForum selectedItem)
        {
            this.ForumID = selectedItem.ForumID;
            this.ForumName = selectedItem.foroumName;
        }

        public MinimizedForum()
        {
        }

        public int ForumID
        {
            get { return forumID; }
            set { forumID = value; }
        }


    }
}
