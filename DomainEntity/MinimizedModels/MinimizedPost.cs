﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.MinimizedModels
{
    public class MinimizedPost
    {
        public int PostId { get; set; }

        public string Text { get; set; }

        public string userNameWriter { get; set; }

        public DateTime Date { get; set; }

        public MinimizedPost()
        { }

        public MinimizedPost(int PostId, string Text, string userNameWriter, DateTime Date)
        {
            this.PostId = PostId;
            this.Text = Text;
            this.userNameWriter = userNameWriter;
            this.Date = Date;
        }
    }
}
