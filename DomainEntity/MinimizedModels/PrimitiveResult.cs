﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainEntity.MinimizedModels
{
    public class PrimitiveResult
    {
        public MinimizedUserInForum Result { get; set; }
        
        public string ErrorMessageg { get; set; }

        public string SessionPassword { get; set; }
    }
}
