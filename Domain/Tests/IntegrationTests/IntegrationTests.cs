﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ForumGenerator.Controllers;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;

namespace AcceptenceTests.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        ForumDB db;
        Member revivo;

        public IntegrationTests()
        {
            db = new ForumDB();
            revivo = new Member("1", "liranre1", "revivo1", "liran", "revivo", new DateTime(), "rev@gmail.com");
            db.Forums[1].AddMember(revivo);
        }


        [TestMethod]
        public void IntegrationTest2()
        {
            //db.Forums[1].AddSubForum("nSf", "some subject", new List<string>());
            //bool added = db.Forums[1].AddModerator(db.Forums[1].SubForums[1], revivo);

            //bool containsLiran = db.Forums[1].SubForums[1].Moderators.Contains(revivo);
            //Assert.AreEqual(added, false);
        }

        [TestMethod]
        public void IntegrationTest3()
        {
            //db.Forums[1].AddSubForum("nSf", "some subject", new List<string>());
            //db.Forums[1].SubForums[1].AddModerator(revivo);
            //db.Forums[1].BanModerator(db.Forums[1].SubForums[1],revivo, 30);
            //var containsLiran = db.Forums[1].SubForums[1].Moderators.Contains(revivo);
            //Assert.AreEqual(containsLiran, true);
        }

        [TestMethod]
        public void IntegrationTest4()
        {
            var member = db.Forums[1].searchMember("liranre1");
            Assert.AreEqual(member.username, revivo.username);
        }

        [TestMethod]
        public void IntegrationTest5()
        {
            var prevName = db.Forums[1].Name;
            db.Forums[1].ChangeForumName("myForum");
            Assert.AreNotEqual(db.Forums[1].Name, prevName);
        }
    }
}
