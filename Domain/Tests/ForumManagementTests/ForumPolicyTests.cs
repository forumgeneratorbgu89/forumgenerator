﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;

namespace AcceptenceTests.Tests.DomainTests
{
    [TestClass]
    public class ForumPolicyTests
    {
        ForumPolicy policy;
        
        public ForumPolicyTests()
        {
            policy = new ForumPolicy(4, 8, 4, true, true, 30, 10, 5, false, false, 10, 10);
        }

        [TestMethod]
        public void checkPassword()
        {
           var a = policy.CheckPassword("liranRevivoTheUltimateTester1");
            Assert.AreEqual(a, true);
        }

        [TestMethod]
        public void checkUsername()
        {
            var b = policy.CheckUserName("LiranReviv's");
            Assert.AreEqual(b, true);
        }
    }
}
