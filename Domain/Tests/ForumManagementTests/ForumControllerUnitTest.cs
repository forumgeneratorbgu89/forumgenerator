﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using NUnit.Framework;
using System.Collections.Generic;
using Domain.Models.UserManagement;
using Domain.Models.ForumManagement;
using ForumGenerator.Controllers;
//using MySql.Data.MySqlClient;
using System.Data;

namespace Domain.Tests.ForumMangementTests
{
    [TestClass]
    public class ForumControllerUnitTest
    {
        private IforumController controller;
        private Forum walla;
        private Forum tapuz;
        private Member avichay;
        private Member noah;
        private Member revivo;
        private Member navara;
        private Member rani;
        private Member benayoun;
        private Member hemed;
        private Member maor;
        private SubForum wallaSport;
#pragma warning disable CS0169 // The field 'ForumControllerUnitTest.tapuzFun' is never used
        private SubForum tapuzFun;
#pragma warning restore CS0169 // The field 'ForumControllerUnitTest.tapuzFun' is never used

        public ForumControllerUnitTest()
        {
            var Forums = new List<Forum>();

            walla = new Forum(1, "Walla");
            revivo = new Member("444444444", "revivo", "1234", "liran", "revivo", new DateTime(1999, 12, 31),"mail@gmail.com");
            walla.AddForumAdmin(revivo); //although he is not deserve it :)
            navara = new Member("555555555", "navara", "1234", "ofir", "navara", new DateTime(1989, 01, 19), "mail@gmail.com");
            avichay = new Member("111111111", "avichay13", "1234", "avichay", "attlan", new DateTime(1989, 01, 08), "mail@gmail.com");
            noah= new Member("333333333", "noah", "1234", "tom", "noah", new DateTime(1995, 01, 21), "mail@gmail.com");
            walla.AddMember(avichay);
            walla.AddMember(noah);
            walla.AddMember(navara);
            wallaSport = walla.AddSubForum("Sport", "Sport...", new List<string>());
            wallaSport.AddModerator(navara);
            Forums.Add(walla);

            tapuz = new Forum(2, "Tapuz");
            benayoun = new Member("888888888", "benayun", "1234", "yosi", "benayun", new DateTime(1989, 01, 08), "mail@gmail.com");
            tapuz.AddForumAdmin(benayoun);
            rani = new Member("666666666", "rani", "1234", "rani", "zinger", new DateTime(1989, 01, 01), "mail@gmail.com");
            hemed = new Member("777777777", "hemed", "1234", "tomer", "hemed", new DateTime(1989, 01, 08), "mail@gmail.com");
            maor = new Member("999999999", "maor", "1234", "maor", "buzaglo", new DateTime(1989, 01, 08), "mail@gmail.com");
            tapuz.AddMember(rani);
            tapuz.AddMember(hemed);
            tapuz.AddMember(maor);
            SubForum tapuzFun = tapuz.AddSubForum("Fun", "Talk about everything:)", new List<string>());
            tapuzFun.AddModerator(maor);
            Forums.Add(tapuz);
            controller = new ForumControllerLocal(Forums);
        }
        #region addForum
        [TestMethod]
        public void AddForumUnitTest()
        {

            //SqlConnection com = new SqlConnection();
            //com.ConnectionString = connectionString;
            //com.Open();

            //SqlCommand cmd = new SqlCommand("CREATE TABLE MyGuests (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,firstname VARCHAR(30) NOT NULL,lastname VARCHAR(30) NOT NULL,email VARCHAR(50),reg_date TIMESTAMP)", com);
            //MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //adp.Fill(ds);


            int before = controller.NumOfForums();
            //@todo: add policy
            //Assert.IsTrue(controller.AddForum("newForum", controller.getSuperAdmin().username));// new SuperAdmin("111", "admin", "1234543324234", "super", "admin", new DateTime(1989, 5, 8), "admin@mail.com")));
            int after = controller.NumOfForums();
            Assert.AreEqual(before + 1, after);
        }

        //ForumIsAlreadyExist
        [TestMethod]
        public void AddForumFailedForumIsAlreadyExistUnitTest()
        {
            //@todo: add policy
            //Assert.IsFalse(controller.AddForum("Tapuz", controller.getSuperAdmin().username));// new SuperAdmin("111", "admin", "1234543324234", "super", "admin", new DateTime(1989, 5, 8), "admin@mail.com")));
        }
        #endregion

        #region 
        [TestMethod]
        public void AddSubForum()
        {
            var modetors = new List<string>();
            modetors.Add(rani.username);
            Assert.IsTrue(controller.AddSubForum(tapuz.ID, "new SubForum",modetors, "subject"));
        }
        [TestMethod]

        public void AddSubForumModeatorIsNotAmemberUnitTest()
        {
            var modetors = new List<string>();
            modetors.Add(navara.username);
            Assert.IsFalse(controller.AddSubForum(tapuz.ID, "new SubForum", modetors, "subject"));
        }
        [TestMethod]

        public void AddSubForumWithSameNameUnitTest()
        {
            Assert.IsFalse(controller.AddSubForum(walla.ID, "Sport", new List<string>(), "subject"));
        }
        [TestMethod]

        public void SendMail()
        {
            Assert.IsTrue(controller.SendMail("Mail for test", "send mail fuction is good", "forumcompany2016@gmail.com"));
        }
        [TestMethod]

        public void Register()
        {
            //@todo: add policy
            //bool isRegisterd = controller.Register(walla.ID, "111222333", "shukumuku", "revivo123", "shuku", "muku", 1990, 1, 5, "forumcompany2016@gmail.com");
            //Assert.IsTrue(isRegisterd);
            string verificationCode = walla.getMemberByName("shukumuku").Code;
            bool isVerified = controller.VerifyUser(walla.ID, "shukumuku", verificationCode);
            Assert.IsTrue(isVerified);
        }
        [TestMethod]

        public void RegisterWithIllegalName()
        {
            //@todo: add policy
            //bool isRegisterd = controller.Register(walla.ID, "111222333", "shukumuku", "revivo123", "", "muku", 1990, 1, 5, "forumcompany2016@gmail.com");
            //Assert.IsFalse(isRegisterd);
        }
        [TestMethod]

        public void RegisterWithIllegalPassword()
        {
            //@todo: add policy
            //bool isRegisterd = controller.Register(walla.ID, "111222333", "shukumuku", "125", "shuku", "muku", 1990, 1, 5, "forumcompany2016@gmail.com");
            //Assert.IsFalse(isRegisterd);
        }

        #endregion
    }
}
