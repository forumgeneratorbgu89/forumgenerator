﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using System.Collections.Generic;

namespace AcceptenceTests.Tests.DomainTests
{
    [TestClass]
    public class ForumTests
    {
        private Forum walla;
#pragma warning disable CS0169 // The field 'ForumTests.tapuz' is never used
        private Forum tapuz;
#pragma warning restore CS0169 // The field 'ForumTests.tapuz' is never used
        private Member avichay;
        private Member noah;
        private Member revivo;
        private Member navara;
#pragma warning disable CS0169 // The field 'ForumTests.rani' is never used
        private Member rani;
#pragma warning restore CS0169 // The field 'ForumTests.rani' is never used
#pragma warning disable CS0169 // The field 'ForumTests.benayoun' is never used
        private Member benayoun;
#pragma warning restore CS0169 // The field 'ForumTests.benayoun' is never used

#pragma warning disable CS0169 // The field 'ForumTests.hemed' is never used
        private Member hemed;
#pragma warning restore CS0169 // The field 'ForumTests.hemed' is never used

#pragma warning disable CS0169 // The field 'ForumTests.maor' is never used
        private Member maor;
#pragma warning restore CS0169 // The field 'ForumTests.maor' is never used
        private SubForum wallaSport;
#pragma warning disable CS0169 // The field 'ForumTests.tapuzFun' is never used
        private SubForum tapuzFun;
#pragma warning restore CS0169 // The field 'ForumTests.tapuzFun' is never used

        public ForumTests()
        {
            walla = new Forum(1, "Walla");
            revivo = new Member("444444444", "revivo", "1234", "liran", "revivo", new DateTime(1999, 12, 31), "mail@gmail.com");
            walla.AddForumAdmin(revivo); //although he is not deserve it :)
            navara = new Member("555555555", "navara", "1234", "ofir", "navara", new DateTime(1989, 01, 19), "mail@gmail.com");
            avichay = new Member("111111111", "avichay13", "1234", "avichay", "attlan", new DateTime(1989, 01, 08), "mail@gmail.com");
            noah = new Member("333333333", "noah", "1234", "tom", "noah", new DateTime(1995, 01, 21), "mail@gmail.com");
            walla.AddMember(avichay);
            walla.AddMember(noah);
            wallaSport = walla.AddSubForum("Sport", "Sport...", new List<string>());
            wallaSport.AddModerator(navara);
        }

        [TestMethod]
        public void AddMember()
        {
            //var numBeforeAddition = walla.Members.Count;
            //walla.AddMember(navara);
            //Assert.AreEqual(numBeforeAddition + 1, walla.Members.Count);*/
         }

        [TestMethod]
        public void removeMember()
        {
            //var numBeforeAddition = walla.Members.Count;
            //walla.RemoveMember(avichay);
            //Assert.AreEqual(numBeforeAddition - 1, walla.Members.Count);
        } 

        [TestMethod]
        public void addSubForumUnitTestUnitTest()
        {
            var res = walla.AddSubForum("Sport", "Sport...", new List<string>());
            Assert.AreEqual(null, res);
        }
        [TestMethod]
        public void banModerator ()
        {
            walla.BanModerator(wallaSport, navara, 30);
            Assert.AreEqual(navara.isSuspend(), false);
        }

        [TestMethod]
        public void addSubForumModerator()
        {
            var list = new System.Collections.Generic.List<Member>();
            list.Add(navara);
            list.Add(noah);
            walla.addModeratorsToSubForum(1, list);
            Assert.AreEqual(1, wallaSport.Moderators.Count);
        }
    }
}
