﻿using Domain.MinimizedModels;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using Domain.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForumGenerator.Controllers
{
    public interface IforumController
    {
        // functions for forum statistics
        List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username);

        List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID);

        MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID);

        List<string> GetAllModerators(int forum);

        void deleteForum(string name);

        void deleteMember(int forumID, string username);

        void deleteSubForum(string forumID, string subForumName);

        List<MinimizedSubForum> getAllSubForum(int forumId);

        int NumOfForums();

        bool AddForum(string name, string admin, ForumPolicy p);//does admin argument nessecery?

        bool AddSubForum(int forumID, string name, List<string> moderators, string subject);

        bool AddThread(int forumID, string SubForumName, string header, string Postcontent, User Postwriter);//not tested yet

        Forum GetForum(int id);// not tested yet

        bool SendMail(string subject, string text, string eMail);

        bool Register(int forumID, string username, string password,
                string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail);

        bool VerifyUser(int forumid, string username, string code);

        Member GetMember(string memberUserName, int forumID);

        bool signUpSuperAdmin(String id, String username, String password,
            string firstName, String lastName, DateTime birthDate, String eMail);

        bool signInSuperAdmin(String username, String password);

        Forum GetForum(string name);// avichay

        SubForum GetSubForum(int iD, string subForumName);
        
        Thread GetThreadByTitle(int forumid, int subforumid, string title);

        ForumPolicy CreatePolicy();

        bool DestructPost(int forumid, int subforumid, int threadID, List<int> postIDs);

        bool DestructThread(int forumid, int subforumid, int threadID);

        bool SetGuestCanPostReplies(int forumid, bool setValue);

        bool SetGuestCanStartThread(int forumid, bool setValue);

        bool SetMinUsernameLength(int forumid, int newLength);

        List<Post> GetPostReplies(int forumid, int subforumid, int threadid, int postid);

        bool IsUserRegisteredToForum(int forum, string user);

        PrimitiveResult Login(int iD, string username, string password);

        bool SetForumPolicy(int iD, ForumPolicy policy);

        bool PromoteToAdmin(int iD, string usernamePromoted, string usernamePromoter);

        SuperAdmin initSuperAdmin(string username, string password);

        MinimizedThread findThread(int forumid, int subforumid, int threadid);

        MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid);

        bool deleteThread(int forumid, int subforumid, int threadid, string deleter);

        string GetVerificationCode(int forumID, string username);

        int GetForumIdByName(string forumName);
        
        bool IsUserAdminOfForum(int iD, string username);

        int GetForumIdByName(string subForumName, int iD);

        bool AddModerator(string username, int forumID, int subForumID);

        void Destruct();

        void ResetToDefaultPolicy(int iD);

        void SetMinPasswordLength(int iD, int newLength);

        MinimizedThread findThreadByTitle(int forumid, int subforumid, string title);
        
        bool CanGuestStartThread(int iD);

        bool CanGuestReplyToPost(int iD);

        int replyToPost(int forum, int subForum, int thread, List<int> post, string creator, string text);

        int findThreadId(int forum, int subForum, string title);

        bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName);

        bool isUserModeratorOfSubForum(int forum, int subforum, string username);

        DateTime? getModTimeLimit(int forum, int subforum, string username);

        bool IsSystemIniaitlized();

        Dictionary<int, string> GetAllForums();

        string GetForumByID(int id);

        bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen);

        bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent);

        List<string> GetAllFriends(int forumID, string username);

        List<MinimizedThread> GetAllThreads(int forumId, int subForumID);

        ForumPolicy GetForumPolicy(int forumId);
        
        string getLastSubForumName(int forumId);

        List<Notification> GetNotifications(string forumname, string userName);

        List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID);
       
        bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote);

        MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName);

        /* for tests */
        string getLastUserName(int forumId);

        string getUsersEmail(int forumId, string lastUserName);

        // returns the last forum name - if forums return string empty
        string getLastForumName();

        // returns a list of usernames of the admins, no way it would be an empty list - minimum 1 admin
        List<string> GetForumAdmins(int forumId);

        string getSecondLastForumName();

        string getLastThreadTitle(int forum1Id, string subForum);

        List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid);
    }
}