﻿using System;
using System.Collections.Generic;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using System.Net;
using System.Net.Mail;
using Domain.MinimizedModels;

namespace ForumGenerator.Controllers
{
    public class ForumControllerLocal : IforumController
    {
        private static ForumControllerLocal instance;
        private DAL.ForumMapper db;

        #region constuctor
        private ForumControllerLocal()
        {
            db = new DAL.ForumMapper();
        }

        public static ForumControllerLocal GetInstance()
        {
            if (instance == null)
            {
                instance = new ForumControllerLocal();
            }
            return instance;
        }

        public ForumControllerLocal(List<Forum> forums)
        {
            db = new DAL.ForumMapper();
            foreach (Forum f in forums)
            {
                db.AddForum(f.Name);
            }
        }

        #endregion

        #region geters

        private List<Forum> GetAllForums()
        {
            //Dictionary<int, string> forums = db.getAllForums();
            //List<Forum> forumsObj = new List<Forum>();
            //foreach (KeyValuePair<int, string> entry in forums)
            //{
            //    forumsObj.Add(new Forum(entry.Key, entry.Value));
            //}
            //return forumsObj;
            return null;
        }

        public Forum GetForum(int id)
        {
            return new Forum(id, db.GetForumNameByID(id));
        }



        #endregion

        //does admin argument nessecery?
        public bool AddForum(string name, string admin, ForumPolicy forumPolicy)
        {
            if (!String.IsNullOrEmpty(name) && admin.Equals("admin"))
            {
                db.AddForum(name);
                db.AddForumAdmin(name, admin);
                Forum f = GetForum(db.GetForumIdByName(name));
                f.AddPolicy(forumPolicy);
                return true;
            }
            else
            {
                throw new Exception("You must enter a forum name");
            }
        }

        public bool SendMail(string subject, string text, string eMail)
        {

            try
            {
                MailMessage msg = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                msg.From = new MailAddress("forumcompany2016@gmail.com");
                msg.To.Add(eMail);
                msg.Subject = subject;
                msg.Body = text;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential("forumcompany2016", "revivo123");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Data);
                return false;
            }
        }


        public bool Register(int forumID, string username, string password,
            string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail)
        {
            bool isRegisterd = false;
            try
            {
                DateTime birthDate = new DateTime(birthYear, birthMonth, birthDay);
                Forum forum = GetForum(forumID);
                if (!(forum == null || !forum.checkValidation(username, password) || CheckNames(firstName, lastName) || !CheckMail(eMail) || birthDate.CompareTo(DateTime.Now) > 0))
                {
                    //@TODO: change member constructor: remove id
                    Member member = new Member("1", username, password, firstName, lastName, birthDate, eMail);
                    forum.AddMember(member);
                    member.verify();
#if !DEBUG
                    SendMail("User Verification", "Your verification code is: " + member.Code, eMail);
#endif
                    isRegisterd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Data);
            }
            return isRegisterd;
        }

        private bool CheckMail(string eMail)
        {
            return eMail.Contains("@");
        }

        private bool CheckNames(string firstName, string lastName)
        {
            bool ret = firstName.Equals(string.Empty) || lastName.Equals(string.Empty);
            if (ret == true) throw new Exception("Invalid name");
            return false;
        }

        public List<string> GetAllModerators(int forum)
        {
            return GetForum(forum).GetAllModerators();
        }

        public void deleteForum(string name)
        {
            db.DeleteForumByName(name);
        }

        public void deleteMember(int forumID, string username)
        {
            GetForum(forumID).RemoveMember(username);
        }

        public void deleteSubForum(string forumName, string subForumName)
        {
            GetForum(forumName).DeleteSubForum(subForumName);
        }

        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            return GetForum(forumId).GetAllSubForums();
        }

        public int NumOfForums()
        {
            return 1;
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            return GetForum(forumid).VerifyUser(username, code);
        }

        public bool AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            Forum f = GetForum(forumID);
            return f.AddSubForum(name, subject, moderators) != null;
        }

        public bool AddThread(int forumID, string SubForumName, string header, string Postcontent, User writer)
        {
            Forum f = GetForum(forumID);
            return f.AddThread(SubForumName, header, Postcontent, writer);
        }

        public bool signUpSuperAdmin(string id, string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            //db.signUpSuperAdmin();
            return true;
            //if (getSuperAdmin() == null)
            //{
            //    return db.addSuperAdmin(id, username, password, firstName, lastName, birthDate, eMail);
            //}
            //else return false;
            //return false; //there is super admin by default
        }

        public bool signInSuperAdmin(string username, string password)
        {
            Member admin = GetMember("admin", -1);
            if(admin.username.Equals(username) && admin.password.Equals(password))
                return true;
            return false;
        }

        public Forum GetForum(string name)
        {
            return new Forum(db.GetForumIdByName(name), name);
        }

        public SubForum GetSubForum(int forumID, string subForumName)
        {
            Forum f = GetForum(forumID);
            if (f == null)
            {
                return null;
            }
            return f.GetSubForumByName(subForumName);
        }

        public Thread GetThreadByTitle(int forumid, int subforumid, string title)
        {
            return GetForum(forumid).getThreadByTitle(subforumid, title);
        }

        public ForumPolicy CreatePolicy()
        {
            return new ForumPolicy();
        }

        public bool DestructPost(int forumid, int subforumid, int threadID, List<int> postIDs)
        {
            Forum f = GetForum(forumid);
            foreach (int post in postIDs)
            {
                f.DestructPost(subforumid, threadID, post);
            }
            return true;
        }

        public bool DestructThread(int forumid, int subforumid, int threadID)
        {
            return GetForum(forumid).DestructThread(subforumid, threadID);
        }

        public bool SetGuestCanPostReplies(int forumid, bool setValue)
        {
            return GetForum(forumid).SetGuestCanPostReplies(setValue);
        }

        public bool SetGuestCanStartThread(int forumid, bool setValue)
        {
            return GetForum(forumid).SetGuestCanStartThread(setValue);
        }

        public bool SetMinUsernameLength(int forumid, int newLength)
        {
            return GetForum(forumid).SetMinUsernameLength(newLength);
        }

        public void SetMinPasswordLength(int iD, int newLength)
        {
            GetForum(iD).SetMinPasswordLength(newLength);
        }

        public List<Post> GetPostReplies(int forumid, int subforumid, int threadid, int postid)
        {
            return GetForum(forumid).GetPostReplies(subforumid, threadid, postid);
        }

        public List<MinimizedPost> GetThreadPosts(int forumid, int subforumid, int threadid)
        {
            return GetForum(forumid).GetThreadPosts(subforumid, threadid);
        }

        public bool IsUserRegisteredToForum(int forumid, string user)
        {
            return GetForum(forumid).IsUserRegistered(user);
        }

        public PrimitiveResult Login(int forumid, string username, string password)
        {
            bool isLoggedIn = false;
            Forum f = GetForum(forumid);
            if (f == null)
            {
                isLoggedIn = false;
            }
            else
            {
                isLoggedIn = f.Login(username, password);
            }
            //@TODO: change return type to PrimitiveResult
            return new PrimitiveResult() { Result=null,ErrorMessageg="user does not exist"};
            //return isLoggedIn;
        }

        //is this function nessecery?
        public bool SetForumPolicy(int forumid, ForumPolicy policy)
        {
            bool isSet = false;
            Forum f = GetForum(forumid);
            if (f == null)
            {
                isSet = false;
            }
            else
            {
                f.SetPolicy(policy);
                isSet = true;
            }
            return isSet;
        }

        public bool PromoteToAdmin(int forumid, string usernamePromoted, string usernamePromoter)
        {
            bool isPromoted = false;
            Forum f = GetForum(forumid);
            if (!IsUserRegisteredToForum(forumid, usernamePromoted) || !IsUserRegisteredToForum(forumid, usernamePromoter) || f == null)
            {
                isPromoted = false;
            }
            else
            {
                isPromoted = f.Promote(usernamePromoted, usernamePromoter);
            }
            return isPromoted;
        }

        public SuperAdmin initSuperAdmin(string username, string password)
        {
            //db.addSuperAdmin("111", username, password, "admin", "admin", new DateTime(1900, 1, 1), "admin@admin.com");
            //return db.getSuperAdmin();
            //return getSuperAdmin();
            return null;
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            //@TODO: change return to MinimizedPost
            return new MinimizedPost();//GetForum(forumid).findPost(subforumid, threadid, postid[0]);
        }

        private Post findPostObject(int forumid, int subforumid, int threadid, int postid)
        {
            return GetForum(forumid).findPostObject(subforumid, threadid, postid);
        }

        public MinimizedThread findThread(int forumid, int subforumid, int threadid)
        {
            // @TODO: change to return MinimizedThread
            return new MinimizedThread();//GetForum(forumid).findThread(subforumid, threadid);
        }

        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            return GetForum(forumid).DestructThread(subforumid, threadid, GetMember(deleter, forumid));
        }

        public string GetVerificationCode(int forumID, string username)
        {
            Forum f = GetForum(forumID);
            return f.GetVerificationCode(username);
        }

        public Member GetMember(string memberUserName, int forumID)
        {
            return GetForum(forumID).getMemberByName(memberUserName);
        }

        public int GetForumIdByName(string forumName)
        {
            return db.GetForumIdByName(forumName);
            //List<Forum> allForums = GetAllForums();
            //foreach(Forum f in allForums)
            //{
            //    if(f.Name.Equals(forumName))
            //    {
            //        return f.ID;
            //    }
            //}
            //return -1;
        }

        public bool IsUserAdminOfForum(int iD, string username)
        {
            Member m = GetMember(username, iD);
            if (m != null)
                return GetForum(iD).IsUserAdmin(m);
            else return false;
        }

        public int GetForumIdByName(string subForumName, int iD)
        {
            return GetForum(iD).GetSubForumByName(subForumName).ID;
        }

        public bool AddModerator(string username, int forumID, int subForumID)
        {
            return GetForum(forumID).AddModerator(GetForum(forumID).getSubForumByID(subForumID), GetForum(forumID).getMemberByName(username));
        }

        public void Destruct()
        {
            instance = null;
            db = null;
        }

        public void ResetToDefaultPolicy(int iD)
        {
            GetForum(iD).ResetToDefaultPolicy();
        }

        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            //@TODO: change return type to MinimizedThread
            return new MinimizedThread();//GetForum(forumid).findThreadByTitle(subforumid, title);
        }

        public bool CanGuestStartThread(int forumid)
        {
            return GetForum(forumid).isCanGuestStartThread();
        }

        public bool CanGuestReplyToPost(int forumid)
        {
            return GetForum(forumid).isCanGuestReplyToPost();
        }

        public int replyToPost(int forum, int subForum, int thread, List<int> post, string creator, string text)
        {
            if (!IsUserRegisteredToForum(forum, creator))
                throw new Exception("User " + creator + " not registered");
            GetForum(forum).ReplyToPost(subForum, thread, post, creator, text);
            return 1;
        }

        public int findThreadId(int forum, int subForum, string title)
        {
            return GetForum(forum).findThreadId(subForum, title);
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
            Forum f = GetForum(forumID);
            Member target = f.getMemberByName(targetUsername);
            Member sender = f.getMemberByName(senderUserName);
            if (target == null || sender == null)
            {
                return false;
            }
            sender.sendMessage(target, text);
            return true;
        }

        public bool isUserModeratorOfSubForum(int forum, int subforum, string username)
        {
            return GetForum(forum).getSubForumByID(subforum).IsModerator(GetMember(username, forum));
        }

        public DateTime? getModTimeLimit(int forum, int subforum, string username)
        {
            return GetForum(forum).getSubForumByID(subforum).getModTimeLimit(GetMember(username, forum));
        }

        public bool IsSystemIniaitlized()
        {
            return (db.getAllForums().Count) > 0;
            //return getSuperAdmin() != null;
        }

        Dictionary<int, string> IforumController.GetAllForums()
        {
            return null;
            //return db.getAllForums();
        }

        public string GetForumByID(int id)
        {
            return db.GetForumNameByID(id);
        }

        public bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen)
        {
            var f = GetForum(forum);
            var sb = f.getSubForumByID(subforum);
            return sb.editModTimeLimit(f.getMemberByName(promoted), newUntilWhen);
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            GetForum(GetForumIdByName(_forum)).EditPost(_subForum, _threadId, postId, poster, newContent);
            return true;
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            return GetForum(forumID).GetAllFriends(username);
        }

        public string getLastUserName(int forumId)
        {
            return GetForum(forumId).GetLastUsername();
        }

        public string getUsersEmail(int forumId, string username)
        {
            return GetForum(forumId).GetUserEmail(username);
        }

        public string getLastForumName()
        {
            return db.GetLastForumName();
        }

        public List<string> GetForumAdmins(int forumId)
        {
            return GetForum(forumId).GetAdmins();
        }

        public string getSecondLastForumName()
        {
            return db.getSecondLastForumName();
        }

        public string getLastThreadTitle(int forumid, string subForum)
        {
            return GetForum(forumid).getLastThreadTitle(subForum);
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            return GetForum(forumId).GetAllThreads(subForumID);
        }

        public ForumPolicy GetForumPolicy(int forumId)
        {
            return GetForum(forumId).GetPolicy();
        }

        public string getLastSubForumName(int forumId)
        {
            return GetForum(forumId).getLastSubForumName();
        }

        public List<Notification> GetNotifications(string forumname, string userName)
        {
            return GetForum(db.GetForumIdByName(forumname)).GetNotifications(userName);
        }

        public List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID)
        {
            return GetForum(forumID).GetSubForumModerators(subForumID);
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            return GetForum(forumId).UnpromoteModerator(subForumName, adminUnpromoting, moderatorToUnpromote);
        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            return GetForum(forumID).GetUserType(subForrumID, userName);
        }

        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            return GetForum(forumid).getAllMinimizedUserInSubForum(subforumid);
        }

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            return GetForum(forumID).GetCommentsForUser(username);
        }

        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID)
        {
            return GetForum(forum).GetAllComments(subForum, threadID, PostID);
        }

        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            return GetForum(forum).GetOpenningPost(subForum, ThreadID);
        }
    }
}