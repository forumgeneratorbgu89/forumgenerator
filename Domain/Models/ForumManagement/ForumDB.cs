﻿using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForumGenerator.Controllers
{
    public class ForumDB
    {
        public SuperAdmin superAdmin { get; set; }

        #region constructor
        public ForumDB()
        {
            ForumIndexID = 1;
            Forums = new List<Forum>();
            Forum walla = new Forum(IncreaseForumIndexID(), "Walla");
            walla.AddMember(new Member("111111111", "avichay13", "1234", "avichay", "attlan", new DateTime(1989, 01, 08), "mail@gmail.com"));
            walla.AddMember(new Member("333333333", "noah", "1234", "tom", "noah", new DateTime(1995, 01, 21), "mail@gmail.com"));
            walla.AddForumAdmin(new Member("444444444", "revivo", "1234", "liran", "revivo", new DateTime(1999, 12, 31), "mail@gmail.com"));
            SubForum wallaSport =  walla.AddSubForum("Sport", "Sport...",new List<string>());
            wallaSport.AddModerator(new Member("555555555", "navara", "1234", "ofir", "navara", new DateTime(1989, 01, 19), "mail@gmail.com"));
            Forums.Add(walla);

            Forum tapuz = new Forum(IncreaseForumIndexID(), "Tapuz");
            tapuz.AddMember(new Member("666666666", "rani", "1234", "rani", "zinger", new DateTime(1989, 01, 01), "mail@gmail.com"));
            tapuz.AddMember(new Member("777777777", "hemed", "1234", "tomer", "hemed", new DateTime(1989, 01, 08), "mail@gmail.com"));
            tapuz.AddForumAdmin(new Member("888888888", "benayun", "1234", "yosi", "benayun", new DateTime(1989, 01, 08), "mail@gmail.com"));
            SubForum tapuzFun = tapuz.AddSubForum("Fun", "Talk about everything:)", new List<string>());
            tapuzFun.AddModerator(new Member("999999999", "maor", "1234", "maor", "buzaglo", new DateTime(1989, 01, 08), "mail@gmail.com"));
            Forums.Add(tapuz);
        }

        private int IncreaseForumIndexID()
        {
            return ForumIndexID++;
        }

        public ForumDB(List<Forum> forums)
        {
            this.Forums = forums;
            ForumIndexID = 1;
            foreach (var f in Forums)
                if (f.ID > ForumIndexID)
                    ForumIndexID = f.ID;
            ForumIndexID++;
        }

        #endregion

        #region properties

        public List<Forum> Forums { get; set; }

        public int ForumIndexID
        {
            get
            {
                return _forumIndexID;
            }
            set
            {
                _forumIndexID = value;
            }
        }

        private int _forumIndexID;

        internal int NumOfForums()
        {
            return Forums.Count;
        }
        #endregion

        #region methods
        internal List<Forum> getAllForums()
        {
            return Forums;
        }

        internal bool addForum(string name, Member superAdmin)
        {
            foreach (var forum in Forums)
                if (forum.Name == name)
                    //return false;
                    throw new Exception("Forum whith this name already exist");
            Forum f = new Forum(ForumIndexID, name);
            ForumIndexID++;
            f.AddForumAdmin(superAdmin);
            Forums.Add(f);
            return true;
        }
        // this is a stupid comment only for merge [=
        internal SuperAdmin getSuperAdmin()
        {
            return superAdmin;
        }

        internal bool addSuperAdmin(string id, string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            superAdmin = new SuperAdmin(id, username, password, firstName, lastName, birthDate, eMail);
            return true;
        }

        internal Forum GetForumByName(string name)
        {
            foreach (var f in Forums)
                if (f.Name.Equals(name))
                    return f;
            return null;
        }
        #endregion

    }

}
