﻿using DAL;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.MinimizedModels;

namespace Domain.Models.ForumManagement
{
    public class SubForum
    {

        //constructor
        public SubForum(string name, string subject,int id, Forum forum, List<Member> moderators)
        {
            Name = name;
            Subject = subject;
            ID = id;
            Moderators = moderators;
            Forum = forum;
            ModsTimeLimit = new Dictionary<Member, DateTime>();
            ThreadIndexID++;
            dbSubForums = new SubForumMapper();
        }

        //fields
        private SubForumMapper dbSubForums;
        public Forum Forum { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public List<Member> Moderators { get; set; }
        private Dictionary<Member, DateTime> ModsTimeLimit;
        public int ThreadIndexID { get; set; }

        public int increaseThreadIndexID()
        {
            return ThreadIndexID++;
        }
        //methods
        public void AddThread(string Threadheader, User Postwriter, string postContent)
        {
            //dbSubForums.AddThreadWithInitialPost(Name,Threadheader, postContent, Postwriter.getUsername());
            //Thread t = new Thread(increaseThreadIndexID(), Threadheader, Postwriter, postContent, this);
            //Threads.Add(t.id,t);
        }

        internal void DeleteThread(int threadID)
        {
            dbSubForums.DeleteThread(threadID);
            
            //bool isDeleted = false;
            //if(Threads.ContainsKey(threadID))
            //{
            //    Threads.Remove(threadID);
            //    isDeleted = true;
            //}
            //return isDeleted;
        }

        internal bool DeleteThread(int threadid, User deleter)
        {
            bool isDeleted = false;
            if (isModerator(deleter) || deleter.GetType().Equals(typeof(SuperAdmin)) ||
                GetThreadByID(threadid).OpenningPost.Writer.getUsername().Equals(((Member)deleter).username))
            {
                DeleteThread(threadid);
                if (GetThreadByID(threadid) == null)
                {
                    isDeleted = true;
                }
            }
            return isDeleted;
        }

        public bool AddModerator(Member m)
        {
            dbSubForums.AddModerator(Name, m.username,DateTime.MaxValue, Forum.Name);
            return true;
            //try
            //{
            //    Moderators.Add(m);
            //    ModsTimeLimit.Add(m, DateTime.MaxValue);
            //    return true;
            //}
            //catch(Exception e)
            //{
            //    Console.WriteLine(e.Data);
            //    return false;
            //}
        }

        public void RemoveModerator(string forumName, string subForumName, string username)
        {
            dbSubForums.RemoveModerator(forumName, subForumName, username);
            //try
            //{
            //    Moderators.Remove(m);
            //    ModsTimeLimit.Remove(m);
            //    return true;
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Data);
            //    return false;
            //}
        }


        internal bool IsModerator(Member m)
        {
            return Moderators.Contains(m);
        }

        internal bool CanRemoveModerator()
        {
            return Moderators.Count > 1;
        }

        public Thread GetThreadByTitle(string title)
        {
            throw new NotImplementedException();
            /*foreach(Thread t in Threads.Values)
            {
                if(t.Header.Equals(title))
                {
                    return t;
                }
            }
            return null;*/
        }

        public Thread GetThreadByID(int threadID)
        {
            throw new NotImplementedException();
            /*foreach (Thread t in Threads.Values)
            {
                if (t.id.Equals(threadID))
                {
                    return t;
                }
            }
            return null;*/
        }

        public DateTime getModTimeLimit(Member m)
        {
            return ModsTimeLimit[m];
        }

        public bool editModTimeLimit(Member m, DateTime newUntilWhen)
        {
            ModsTimeLimit[m] = newUntilWhen;
            return true;
        }

        public bool findThread (int threadid)
        {
            return dbSubForums.ExistIDQuery(threadid, "Threads");

            
        }

        public bool findPost(int threadid,int postid)
        {
            return GetThreadByID(threadid).findPost(postid);
        }

        public bool isModerator(User deleter)
        {
            return Moderators.Contains(deleter);
        }

        internal void DestructPost(int threadID, int postID)
        {
            GetThreadByID(threadID).DeletePost(postID);
        }

        internal List<Post> GetPostReplies(int threadid, int postid)
        {
            return GetThreadByID(threadid).GetPostReplies(postid);
        }

        internal List<MinimizedPost> GetThreadPosts(string forumname, int threadid)
        {
            return GetThreadByID(threadid).GetThreadPosts(forumname,Name);
        }

        internal bool findThreadByTitle(string title)
        {
            return dbSubForums.ExistsQuery(title, "Header", "Threads");
            
        }

        internal bool CheckNumModerators(int policyid, int count)
        {
            return dbSubForums.CheckNumModerators(policyid, count);
        }

        internal int getModeratorsCount(string forumName)
        {
            return dbSubForums.GetModeratorsCount(forumName,Name);
        }

        internal Post findPostObject(int threadid, int postid)
        {
            return GetThreadByID(threadid).findPostObject(postid);
        }

        internal int findThreadId(string title)
        {
            return dbSubForums.GetThreadID(title);
        }

        internal void EditPost(string forumname, int _threadId, List<int> postId, string poster, string newContent)
        {
            GetThreadByID(_threadId).EditPost(forumname,Name, postId, poster, newContent);
        }

        internal List<MinimizedPost> GetPostByThread(string forumname, int threadID)
        {
            return GetThreadByID(threadID).GetThreadPosts(forumname,Name);
        }

        internal string getLastThreadTitle(string forumname)
        {
            return dbSubForums.getLastThreadTitle(forumname, Name);
        }

        internal List<MinimizedThread> GetAllThreads(string forumname)
        {
            //List<List<string>> threads = dbSubForums.GetAllThreads(forumname, Name);
            List<MinimizedThread> allthreads = new List<MinimizedThread>();
            //foreach (List<string> thread in threads)
            //{
            //    MinimizedThread min = new MinimizedThread();
            //    min.Title = thread[0];
            //    min.UserNameWriter = thread[1];
            //    min.Date = Convert.ToDateTime(thread[2]);
            //    min.ID = Int32.Parse(thread[3]);
            //    allthreads.Add(min);
            //}
            return allthreads;
        }

        internal List<Tuple<string, DateTime?>> GetSubForumModerators(string forumname)
        {
            return dbSubForums.GetSubForumModerators(forumname, Name);
        }

        internal bool UnpromoteModerator(string forumname, string adminUnpromoting, string moderatorToUnpromote)
        {
            dbSubForums.UnpromoteModerator(forumname, Name, adminUnpromoting, moderatorToUnpromote);
            return true;
        }

        internal List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(string forumname)
        {
            //List<string> users = dbSubForums.GetAllMembers(forumname, Name);
            //List<MinimizedUserInSubForum> min = new List<MinimizedUserInSubForum>();
            //foreach (string username in users)
            //{
            //    MinimizedUserInSubForum user = new MinimizedUserInSubForum();
            //    user.UserName = username;
            //    if (dbSubForums.isModerator(forumname, Name, username))
            //        user.State = UserTypeInSubForum.Moderator;
            //    if (dbSubForums.isForumAdmin(forumname, Name, username))
            //        user.State = UserTypeInSubForum.Admin;
            //    if (dbSubForums.isSuperAdmin(forumname, Name, username))
            //        user.State = UserTypeInSubForum.SuperAdmin;
            //    else
            //        user.State = UserTypeInSubForum.Member;
            //    min.Add(user);
            //}
            //return min;
            return null;
        }

        internal List<MinimizedPost> GetAllComments(string forumname, int threadID, List<int> postID)
        {
            return GetThreadByID(threadID).GetAllComments(forumname, Name, postID);
        }

        internal MinimizedPost GetOpenningPost(string forumname, int threadID)
        {
            return GetThreadByID(threadID).GetOpenningPost(forumname, Name);
        }

        internal MinimizedUserInSubForum GetUserType(string forumname, string username)
        {
            MinimizedUserInSubForum user = new MinimizedUserInSubForum();
            user.UserName = username;
            if (dbSubForums.isModerator(forumname, Name, username))
                user.State = UserTypeInSubForum.Moderator;
            if (dbSubForums.isForumAdmin(forumname, Name, username))
                user.State = UserTypeInSubForum.Admin;
            if (dbSubForums.isSuperAdmin(forumname, Name, username))
                user.State = UserTypeInSubForum.SuperAdmin;
            else
                user.State = UserTypeInSubForum.Member;
            return user;
        }

        internal void ReplyToPost(string forumname, int thread, List<int> post, string creator, string text)
        {
            GetThreadByID(thread).ReplyToPost(forumname, Name, post, creator, text);
        }
    }
}