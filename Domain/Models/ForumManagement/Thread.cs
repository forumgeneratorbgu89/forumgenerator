﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models.UserManagement;
using Domain.MinimizedModels;

namespace Domain.Models.ForumManagement
{
    public class Thread
    {
        public int id { get; set; }
        public int subforumId { get; set; }
        public string Header { get; set; }
        public Post OpenningPost { get; set; }
        private ThreadMapper db;
        private SubForumMapper sfdb;

        public Thread(int threadID, string threadheader, User postwriter, string postContent, int subForumId)
        {
            this.id = threadID;
            this.Header = threadheader;
            OpenningPost = new Post(0,postwriter, postContent, null, this);
            subforumId = subForumId;
            db = new ThreadMapper();
        }

        public void EditHeader(string header)
        {
            Header = header;
        }

        public bool Destructor()
        {
            db.DeleteThread(this.id, subforumId);
            return true;
        }

        internal bool DeletePost(int postID)
        {
            
            if (this.OpenningPost.id == postID)
            {
                return Destructor(); // delete the whole thread if no open post (?)
            }
            else
            {
                return OpenningPost.removeComment(postID);
            }
        }

        internal List<Post> GetPostReplies(int postid)
        {
            if (this.OpenningPost.Equals(postid))
            {
                return OpenningPost.getComtents();
            }
            else
            {
                //need to think about different iteration...
                foreach(Post p in OpenningPost.getComtents())
                {
                    if (p.id == postid)
                    {
                        return p.getComtents();
                    }
                }
            }
            return null;

            //Maor implemented before:
            //var posts = new List<Post>();
            //posts.Add(post);
            //posts.AddRange(post.getComtents());
            //return posts.ToArray();
        }

        internal List<MinimizedPost> GetThreadPosts(string forumname,string subforumname)
        {
            List<MinimizedPost> allposts = new List<MinimizedPost>();
            List<List<string>> s=null;//= db.GetThreadPosts(forumname,subforumname,id);

            foreach (List<string> post in s)
            {
                MinimizedPost min = new MinimizedPost();
                min.PostId = Int32.Parse(post[0]);
                min.Text = post[1];
                min.userNameWriter = post[2];
                min.Date = Convert.ToDateTime(post[3]);
                allposts.Add(min);
            }
            return allposts;
        }

        internal bool findPost(int postid)
        {
            if (OpenningPost.id == (postid))
            {
                return true;
            }
            else
            {
                foreach (Post p in OpenningPost.getComtents())
                {
                    if (p.id == postid)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        internal Post findPostObject(int postid)
        {
            if (OpenningPost.id == (postid))
            {
                return OpenningPost;
            }
            else
            {
                foreach (Post p in OpenningPost.getComtents())
                {
                    if (p.id == postid)
                    {
                        return p;
                    }
                }
            }
            return null;
        }

        public int getForumId()
        {
            //return db.getForumId(subforumId);
            return 0;
        }

        internal void EditPost(string forumname,string subforumname, List<int> postId, string poster, string newContent)
        {
            //db.EditPost(forumname, subforumname,id,  postId, poster, newContent);
        }

        internal List<MinimizedPost> GetAllComments(string forumname, string subforumname, List<int> postID)
        {
            List<List<string>> posts = null;// db.GetAllComments(forumname, subforumname,id, postID);
            List<MinimizedPost> minposts = new List<MinimizedPost>();
            foreach (List<string> post in posts)
            {
                MinimizedPost min = new MinimizedPost();
                min.PostId = Int32.Parse(post[0]);
                min.Text = post[1];
                min.userNameWriter = post[2];
                min.Date = Convert.ToDateTime(post[3]);
                minposts.Add(min);
            }
            return minposts;
        }

        internal MinimizedPost GetOpenningPost(string forumname, string subforumname)
        {
            List<string> post = db.GetOpenningPost(forumname, subforumname, id);
            MinimizedPost min = new MinimizedPost();
            min.PostId = Int32.Parse(post[0]);
            min.Text = post[1];
            min.userNameWriter = post[2];
            min.Date = Convert.ToDateTime(post[3]);
            return min;
        }

        internal void ReplyToPost(string forumname, string subforumname, List<int> post, string creator, string text)
        {
            //db.ReplyToPost(forumname, subforumname, id, post, creator, text);
        }
    }
}