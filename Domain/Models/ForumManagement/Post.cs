﻿using DAL;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;

namespace Domain.Models.ForumManagement
{
    public class Post
    {
        private int commentID;
        public int id { get; set; }
        public string Content { get; set; }
        public User Writer { get; set; }
        public Post Father { get; set; }
        private PostMapper db;
        public Thread Thread { get; set; }
        public DateTime Time { get; set; }

        public Post(int id,User writer, string content, Post father, Thread thread)
        {
            this.id = id;
            Content = content;
            Writer = writer;
            Father = father;
            Thread = thread;
            Time = DateTime.Now;
        }

        public bool Destructor()
        {
            bool isDestruct = false;
            if (Father == null)
            {
                isDestruct = Thread.Destructor();
            }
            else
            {
                isDestruct = Father.DeleteComment(this.id);
            }
            return isDestruct;
        }

        private bool DeleteComment(int postID)
        {
            bool isDeleted = false;
            if (db.ExistIDQuery(postID, "Posts") == false) { return false; }
            throw new NotImplementedException(); 
            /*if (Comments.ContainsKey(postID))
            {
                db.deletePost(postID, db.GetThreadID(Thread.Header));
                Comments.Remove(postID);
                isDeleted = true;
            }
            return isDeleted;*/
        }

        public int PostComment(User postWriter,string postContent)
        {
            Post post = new Post(increasePostID(), postWriter,postContent,this,this.Thread);
            int postId = increasePostID();
            db.addPost(postId, postWriter.getUsername, postContent, this.id, this.Thread.id);
            throw new NotImplementedException(); 
            /*Comments.Add(post.id,post);
            return postId;*/
        }

        private int increasePostID()
        {
            return commentID++;
        }

        // returns true if the user is the owner of message
        public bool CanEditContent(User m)
        {
            return Writer.Equals(m);
        }

        public void EditPost(string content)
        {
            Content = content;
        }

        public void AddThread(Thread t)
        {
            Thread = t;
        }

        internal bool ContainPost(int post)
        {
            /* find the post with the matching id (and same thread), and then climb up 
             "father post" until you reach this.postId or original (which has father -1) */
            string sql = "SELECT `Father Post ID` FROM `Posts` WHERE (`ID` = \"" + post + "\" AND `Thread ID` = \"" + this.Thread.id + "\")";
            string fatherPostId = db.getFieldFromQuery(sql, "Father Post ID");
            while (fatherPostId.Equals("-1") == false && fatherPostId.Equals(this.id) == false)
            {
                sql = "SELECT `Father Post ID` FROM `Posts` WHERE (`ID` = \"" + fatherPostId + "\" AND `Thread ID` = \"" + this.Thread.id + "\")"; ;
                fatherPostId = db.getFieldFromQuery(sql, "Father Post ID");
            }
            if (fatherPostId.Equals("-1") == true) { return false; }
            else { return true; }

            
        }

        public List<Post> getComtents()
        {
            var posts = new List<Post>();

            throw new NotImplementedException();
            /*foreach(var comments in Comments.Values)
            {
                posts.Add(comments);
                posts.AddRange(comments.getComtents().ToArray());
            }
            return posts;*/
        }

        internal bool removeComment(int postID)
        {
            if (db.ExistIDQuery(postID, "Posts") == false) { return false; }
            db.deletePost(postID, db.GetThreadID(Thread.Header));
            return true;
        }
    }
}