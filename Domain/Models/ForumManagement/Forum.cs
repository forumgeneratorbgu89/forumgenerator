﻿using DAL;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.MinimizedModels;
using System.Data.SqlClient;

namespace Domain.Models.ForumManagement
{
    public class Forum
    {
        //constructor
        public Forum(int id, string Name)
        {
            db = new ForumMapper();
            this.ID = id;
            this.Name = Name;
            SubForumIndex++;
            //Members = new Dictionary<string, Member>();
            //ForumAdmins = new Dictionary<string, Member>();
            //SubForums = new Dictionary<int, SubForum>();
            //Policy = new ForumPolicy();
        }

        //fields
        private ForumMapper db;
        public int ID { get; set; }
        public string Name { get; set; }

        internal void AddFriend(string sender, string reciver, string forumname)
        {
            db.AddFriend(sender, reciver, forumname);
        }

        internal void ConfirmFriendRequest(string sender, string reciver, string v)
        {
            throw new NotImplementedException();
        }

        internal List<MinimizedMessage> GetIncomingMessagesOfUser(int forumID, string usernameTarget, string userNameSender)
        {
            List<MinimizedMessage> output = new List<MinimizedMessage>();
            MinimizedMessage message;

            List<List<string>> messagesAsStringLists = db.GetIncomingMessagesOfUser(forumID, usernameTarget, userNameSender);

            for (int i=0; i<messagesAsStringLists.Count; i++)
            {
                message = new MinimizedMessage();
                message.from = userNameSender;
                message.to = usernameTarget;
                message.content = messagesAsStringLists.ElementAt(i).ElementAt(0);
                message.date = DateTime.Parse(messagesAsStringLists.ElementAt(i).ElementAt(1));
                output.Add(message);
            }
            return output;
        }

        //public Dictionary<string, Member> Members { get; set; }
        //public Dictionary<string, Member> ForumAdmins { get; set; }
        //public Dictionary<int, SubForum> SubForums { get; set; }
        //public ForumPolicy Policy { get; set; }

        //methods
        public int SubForumIndex { get; private set; }

        internal bool isVerified(string username)
        {
            return db.isUserVerified(Name, username);
        }

        public bool AddMember(Member member)
        {
            db.AddMember(ID, member.username, member.password, member.firstName, member.lastName, member.Email, member.Code, member.birthDate, member.suspendExpire, member.IsVerified());
            return true;
//            try {
//                Members.Add(member.username, member);
//                member.ForumID = ID;
//                return true;
//            }
//#pragma warning disable CS0168 // The variable 'e' is declared but never used
//            catch(Exception e)
//#pragma warning restore CS0168 // The variable 'e' is declared but never used
//            {
//                return false;
//            }
        }

        internal void AddPolicy(ForumPolicy Policy)
        {
            db.AddPolicy(Name, Policy.MaxModeratorsNumber, Policy.MinPasswordLetters, Policy.MinUserNameLetters, Policy.PasswordRequiredUpperLowerCase, Policy.PasswordReuqiredNumbers, Policy.SuspendModeratorMinTime, Policy.SuspendAdminMinTime, Policy.SuspendMemberMinTime, Policy.GuestCanPost, Policy.GuestCanStartThread, Policy.MinSeniorityForMediatorToEditAPost, Policy.MinSeniorityForMediatorToRemoveAPost);
        }

        public void RemoveMember(string username)
        {
            db.RemoveMember(username, Name);
            //Members.Remove(member.username);
        }

        public Member getMemberByName(string username)
        {
            List<string> m = db.GetMemberByName(username);
            return new Member(m[0], m[1], m[2], m[3], m[4], Convert.ToDateTime(m[7]), m[5]);
        }
        public Member searchMember(string username)
        {
            List<string> m = db.GetMemberByName(username);
            Member mem = new Member(m[0], m[1], m[2], m[3], m[4], Convert.ToDateTime(m[7]), m[5]);
            mem.Code = m[6];
            mem.suspendExpire = Convert.ToDateTime(m[8]);
            mem.isVerified = (m[9].Equals(0)) ? false : true;
            return mem;
            //return Members[username];
        }
        
        public SubForum GetSubForumByName(string subforumname)
        {
            List<string> sf = db.getSubForumByName(subforumname);
            return new SubForum(sf[1], sf[2], Convert.ToInt32(sf[0]), this, getSubForumModerators(subforumname));
        }
        public SubForum AddSubForum(string SubforumName,string SubForumSubject, List<string> moderators)
        {
            db.AddSubForum(Name, SubforumName, SubForumSubject, moderators);
            List<string> sf = db.getSubForumByName(SubforumName);
            return new SubForum(sf[1], sf[2], Convert.ToInt32(sf[0]), this, getSubForumModerators(SubforumName));

            //foreach (var subForum in SubForums.Values)
            //    if (subForum.Name.Equals(SubforumName))
            //        //return null;
            //        throw new Exception("Sub forum with this name is already exist.");
            //SubForum sb = new SubForum(SubforumName, SubForumSubject, IncreaseSubForumIndex(), this, GetModerators(moderators));
            //SubForums.Add(sb.ID, sb);
            //return sb;
        }

        private List<Member> getSubForumModerators(string SubforumName)
        {
            List<Member> moderators = new List<Member>();
            List<string> mods = db.GetSubForumModerators(SubforumName);
            foreach(string m in mods)
            {
                moderators.Add(searchMember(m));
            }
            return moderators;
        }

        private List<Member> GetModerators(List<string> moderators)
        {
            List<Member> mods = new List<Member>();
            foreach (string m in moderators)
            {
                mods.Add(searchMember(m));
            }
            return mods;

            //List<Member> moderatorsList = new List<Member>();
            //foreach(string userName in moderators)
            //{
            //    moderatorsList.Add(Members[userName]);
            //}
            //return moderatorsList;
        }

        //private int IncreaseSubForumIndex()
        //{
        //    return SubForumIndex++;
        //}

        public void AddForumAdmin(Member admin)
        {
            db.AddForumAdmin(Name, admin.username);
            //Members.Add(admin.username, admin);
            //ForumAdmins.Add(admin.username, admin);
        }

        public void ChangeForumName(string newName)
        {
            db.ChangeForumName(Name,newName);
            //this.Name = name;
        }

        public bool IsUserAdmin(Member m)
        {
            return db.isUserAdmin(Name,m.username);
            //return ForumAdmins.ContainsKey(m.username);
        }
        public bool AddModerator(SubForum sf, Member m)
        {
            bool isAdded = false;
            int policyid = db.GetPolicyIDByForumName(Name);
            if ((db.getSubForumByName(sf.Name) != null) && (db.GetMemberByName(m.username)) != null && sf.CheckNumModerators(policyid, sf.getModeratorsCount(Name)))
            {
                db.AddModeratorToSubForum(m.username,sf.Name, Name);
                isAdded = true;
            }
            return isAdded;

            //bool isAdded = false;
            //if(SubForums.ContainsKey(sf.ID) && Members.ContainsKey(m.username) && Policy.CheckNumModerators(sf.Moderators.Count))
            //{
            //    isAdded = sf.AddModerator(m);
            //}
            //return isAdded;
        }
        public bool RemoveModerators(SubForum sf, Member m)
        {
            bool isRemoved = false;
            int policyid = db.GetPolicyIDByForumName(Name);
            
            if ((db.getSubForumByName(sf.Name) != null) && (db.GetMemberByName(m.username)) != null)
            {
                sf.RemoveModerator(Name, sf.Name, m.username);
                isRemoved = true;
            }
            return isRemoved;

            //bool isRemoved = false;
            //if (SubForums.ContainsKey(sf.ID) && Members.ContainsKey(m.username) && sf.CanRemoveModerator())
            //{
            //   sf.RemoveModerator(m);
            //}
            //return isRemoved;
        }
        public bool BanModerator(SubForum sf, Member m, int days)
        {
            return BanMember(sf, m, days);

            //bool isBanned = false;
            //if(db.isModerator(sf.Name,m.username) && sf.CanRemoveModerator() && Policy.LegalModeratorsSuspensionTime(days))
            //{
            //    isBanned = BanMember(sf, m, days);
            //    m.suspend(days);
            //    isBanned = true;
            //}
            //return isBanned;

            //bool isBanned = false;
            //if(sf.IsModerator(m) && sf.CanRemoveModerator() && Policy.LegalModeratorsSuspensionTime(days))
            //{
            //    m.suspend(days);
            //    isBanned = true;
            //}
            //return isBanned;
        }

        public bool BanMember(SubForum sf,Member m, int days)
        {
            bool isBanned = false;
            //if (db.isModerator(sf.Name,m.username))
            //{
            //    foreach (SubForum sf in AllSubForumsModeratedBy(m))
            //    {
            //        isBanned = BanModerator(sf, m, days) || isBanned;
            //    }
            //}
            int policyid = db.GetPolicyIDByForumName(Name);
            if (db.LegalMemberSuspensionTime(policyid, days))
            {
                m.suspend(days);
                isBanned = true;
            }
            return isBanned;
        }

        internal bool AddThread(string subForumName, string header, string postcontent, User writer)
        {
            GetSubForumByName(subForumName).AddThread(header, writer, postcontent);
            return true;
        }

        internal bool VerifyUser(string username, string code)
        {
            Member member = getMemberByName(username);
            //Member member = searchMember(username);
            if (member.Code != code)
                return false;
            member.verify();
            return true;
        }

        public SubForum getSubForumByID(int subForumId)
        {
            List<string> sf = db.getSubForumByID(subForumId);
            return new SubForum(sf[1], sf[2], Convert.ToInt32(sf[0]), this, getSubForumModerators(sf[1]));

            //SubForums.TryGetValue(subForumId,out sb);
            //return sb;
        }

        internal void SetPolicy(ForumPolicy Policy)
        {
            db.setPolicy(db.GetPolicyIDByForumName(Name), Policy.MaxModeratorsNumber, Policy.MinPasswordLetters, Policy.MinUserNameLetters, Policy.PasswordRequiredUpperLowerCase, Policy.PasswordReuqiredNumbers, Policy.SuspendModeratorMinTime, Policy.SuspendAdminMinTime, Policy.SuspendMemberMinTime, Policy.GuestCanPost, Policy.GuestCanStartThread, Policy.MinSeniorityForMediatorToEditAPost, Policy.MinSeniorityForMediatorToRemoveAPost);
        }

        //public Member getMember(string userName)
        //{
        //    db.GetMemberByName(userName);
        //    Member m;
        //    Members.TryGetValue(userName,out m);
        //    return m;
        //}

        public void addModeratorsToSubForum(int subForumID, List<Member> moderators)
        {
            foreach (Member m in moderators)
                db.AddModeratorToSubForum(m.username, subForumID, Name);
            //SubForum sb = getSubForumByID(subForumID);
            //if (sb == null)
            //    return false;
            ////check if all the modetors are also members
            //foreach(var m in moderators)
            //{
            //    if (!Members.Keys.Contains(m.username))
            //        return false;
            //}
            //foreach (var m in moderators)
            //{
            //    sb.AddModerator(m);
            //}
            //return true;
        }

        internal bool DestructThread(int subforumid, int threadID)
        {
            getSubForumByID(subforumid).DeleteThread(threadID);
            return true;
        }

        internal bool SetGuestCanPostReplies(bool setValue)
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            //db.SetGuestCanPostReplies(policyid, setValue);
            return true;
        }

        internal bool DestructPost(int subforumid, int threadID, int postID)
        {
            getSubForumByID(subforumid).DestructPost(threadID, postID);
            return true;
        }

        internal bool SetMinUsernameLength(int newLength)
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            //db.SetMinUserNameLetters(policyid, newLength);
            //this.Policy.MinUserNameLetters = newLength;
            return true;
        }

        internal bool SetGuestCanStartThread(bool setValue)
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            //db.GuestCanStartThread(policyid, setValue);
            //this.Policy.GuestCanStartThread = setValue;
            return true;
        }

        internal List<Post> GetPostReplies(int subforumid, int threadid, int postid)
        {
            return getSubForumByID(subforumid).GetPostReplies(threadid, postid);
        }

        internal void SetMinPasswordLength(int newLength)
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            //db.SetMinPasswordLetters(policyid, newLength);
            //this.Policy.MinPasswordLetters = newLength;
        }

        internal List<MinimizedPost> GetThreadPosts(int subforumid, int threadid)
        {
            return getSubForumByID(subforumid).GetThreadPosts(Name,threadid);
        }

        internal Thread getThreadByTitle(int subforumid, string title)
        {
           return getSubForumByID(subforumid).GetThreadByTitle(title);
        }

        internal bool isCanGuestReplyToPost()
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            return (db.GetPolicy(policyid)[9]).Equals("1") ? true : false;
        }

        internal bool isCanGuestStartThread()
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            return (db.GetPolicy(policyid)[10]).Equals("1") ? true : false;
        }

        public List<SubForum> AllSubForumsModeratedBy(Member m)
        {
            List<string> subforums = db.GetAllSubForumsModeratedBy(m.username);
            List<SubForum> ret = new List<SubForum>();
            foreach(string sf in subforums)
            {
                ret.Add(GetSubForumByName(sf));
            }
            return ret;
            //List<SubForum> subForums = new List<SubForum>();
            //foreach (SubForum sf in SubForums.Values)
            //{
            //    if(sf.IsModerator(m))
            //    {
            //        subForums.Add(sf);
            //    }
            //}
            //return subForums;
        }

        internal void ReplyToPost(int subForum, int thread, List<int> post, string creator, string text)
        {
            getSubForumByID(subForum).ReplyToPost(Name, thread, post, creator, text);
        }

        //public SubForum GetSubForumByByName(string subForumName)
        //{

        //    List<string> sf = db.getSubForumByName(subForumName);
        //    return new SubForum(sf[1], sf[2], Convert.ToInt32(sf[0]), this, getSubForumModerators(sf[1]));

        //    //SubForum sb = null;
        //    //foreach(SubForum s in SubForums.Values)
        //    //{
        //    //    if(s.Name.Equals(subForumName))
        //    //    {
        //    //        sb = s;
        //    //        break;
        //    //    }
        //    //}
        //    //return sb;
        //}

        public bool checkValidation(string username, string password)
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            if (policyid == -1)
                throw new Exception("No policy");
            List<string> pol = db.GetPolicy(policyid);
            ForumPolicy policy = new ForumPolicy(Int32.Parse(pol[1]), Int32.Parse(pol[2]), Int32.Parse(pol[3]), ((pol[4]).Equals("1"))?true:false, ((pol[5]).Equals("1")) ? true : false, Int32.Parse(pol[6]), Int32.Parse(pol[7]), Int32.Parse(pol[8]), ((pol[9]).Equals("1")) ? true : false, ((pol[10]).Equals("1")) ? true : false, Int32.Parse(pol[11]), Int32.Parse(pol[12]));
            //return Policy.CheckUserName(username) && !Members.ContainsKey(username) && Policy.CheckPassword(password);
            bool ret = policy.CheckUserName(username) && policy.CheckPassword(password);
            if (db.GetMemberByName(username).Count>0)
                throw new Exception("Username " + username + " already exists, Choose another username");
            return ret;
        }

        public bool IsUserRegistered(string username)
        {
            if (username.Equals("admin"))
                return true;
            return db.isUserRegisteredToForum(Name, username);
            //List<string> member = db.GetMemberByName(username);
            //if (member.Count > 0)
            //    throw new Exception("Username " + username + " already exists, Choose another username");

            //else if (username.Equals("admin"))
            //    isRegistered = true;

            //else if
            //{
            //    isRegistered = memb == ID;
            //}
            //return isRegistered;
        }

        internal List<MinimizedSubForum> GetAllSubForums()
        {
            //List<MinimizedSubForum> subforums = new List<MinimizedSubForum>();
            //Dictionary<int, List<string>> s = db.GetAllSubForumsByForumName(Name);

            //foreach (KeyValuePair<int, List<string>> entry in s)
            //{
            //    MinimizedSubForum min = new MinimizedSubForum();
            //    min.ID = entry.Key;
            //    min.Name = entry.Value[1];
            //    min.Subject = entry.Value[2];
            //    subforums.Add(min);
            //}
            //return subforums;
            return null;
        }

        internal string GetUserEmail(string username)
        {
            return db.GetUserEmail(Name, username);
        }

        internal List<string> GetAdmins()
        {
            //return db.GetForumAdmins(Name);
            return null;
        }

        internal string GetLastUsername()
        {
            return db.GetLastUsername();
        }

        internal string getLastThreadTitle(string subForum)
        {
            return GetSubForumByName(subForum).getLastThreadTitle(Name);
        }

        internal List<MinimizedThread> GetAllThreads(int subForumID)
        {
            return getSubForumByID(subForumID).GetAllThreads(Name);
        }

        internal ForumPolicy GetPolicy()
        {
            int policyid = db.GetPolicyIDByForumName(Name);
            List<string> pol = db.GetPolicy(policyid);
            return new ForumPolicy(Int32.Parse(pol[1]), Int32.Parse(pol[2]), Int32.Parse(pol[3]), ((pol[4]).Equals("1")) ? true : false, ((pol[5]).Equals("1")) ? true : false, Int32.Parse(pol[6]), Int32.Parse(pol[7]), Int32.Parse(pol[8]), ((pol[9]).Equals("1")) ? true : false, ((pol[10]).Equals("1")) ? true : false, Int32.Parse(pol[11]), Int32.Parse(pol[12]));
        }

        internal List<Tuple<string, DateTime?>> GetSubForumModerators(int subForumID)
        {
            return getSubForumByID(subForumID).GetSubForumModerators(Name);
        }

        internal bool UnpromoteModerator(string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            return GetSubForumByName(subForumName).UnpromoteModerator(Name, adminUnpromoting, moderatorToUnpromote);
        }

        internal MinimizedUserInSubForum GetUserType(int subForrumID, string userName)
        {
            return getSubForumByID(subForrumID).GetUserType(Name, userName);
        }

        internal List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int subforumid)
        {
            return getSubForumByID(subforumid).getAllMinimizedUserInSubForum(Name);
        }

        internal List<MinimizedPost> GetCommentsForUser(string username)
        {
            List<List<string>> posts = db.GetAllPostsOfUser(Name, username);
            List<MinimizedPost> minposts = new List<MinimizedPost>();
            foreach (List<string> post in posts)
            {
                MinimizedPost min = new MinimizedPost();
                min.PostId = Int32.Parse(post[0]);
                min.Text = post[1];
                min.userNameWriter = post[2];
                min.Date = Convert.ToDateTime(post[3]);
                minposts.Add(min);
            }
            return minposts;
        }

        internal MinimizedPost GetOpenningPost(int subForum, int threadID)
        {
            return getSubForumByID(subForum).GetOpenningPost(Name, threadID);
        }

        internal List<MinimizedPost> GetAllComments(int subForum, int threadID, List<int> postID)
        {
            return getSubForumByID(subForum).GetAllComments(Name, threadID, postID);
        }

        internal List<Notification> GetNotifications(string userName)
        {
            List<List<string>> notifications = db.GetNotifications(Name, userName);
            List<Notification> nots = new List<Notification>();
            foreach (List<string> noti in notifications)
            {
                Notification n = new Notification();
                n.ID = Int32.Parse(noti[0]);
                //@TODO: complete...
                nots.Add(n);
            }
            return nots;
        }

        internal string getLastSubForumName()
        {
            return db.getLastSubForumName(Name);
        }

        internal List<string> GetAllModerators()
        {
            return db.GetAllModerators(Name);
        }

        internal List<MinimizedPost> GetPostBySubForumAndThread(int subForum, int threadID)
        {
            return getSubForumByID(subForum).GetPostByThread(Name, threadID);
        }

        internal List<string> GetAllFriends(string username)
        {
            return db.GetAllFriends(username);
        }

        internal void EditPost(string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            GetSubForumByName(_subForum).EditPost(Name, _threadId, postId, poster, newContent);
        }

        internal void DeleteSubForum(string subForumName)
        {
            db.DeleteSubForumByName(subForumName, Name);
        }

        internal bool findThread(int subforumid, int threadid)
        {
            return getSubForumByID(subforumid).findThread(threadid);
        }

        internal bool findPost(int subforumid, int threadid, int postid)
        {
            return getSubForumByID(subforumid).findPost(threadid, postid);
        }

        internal Post findPostObject(int subforumid, int threadid, int postid)
        {
            return getSubForumByID(subforumid).findPostObject(threadid, postid);
        }

        internal bool DestructThread(int subforumid, int threadid, User deleter)
        {
            return getSubForumByID(subforumid).DeleteThread(threadid, deleter);
        }

        internal string GetVerificationCode(string username)
        {
            return getMemberByName(username).Code;
        }

        public bool Login(string username, string password)
        {
            return db.LoginToForum(Name,username,password);
            //return (Members.ContainsKey(username) && Members[username].password.Equals(password)) ;
        }

        public bool Promote(string usernamePromoted, string usernamePromoter)
        {
            if(!db.isUserAdmin(Name,usernamePromoter))
                throw new Exception("Promoter usename " + usernamePromoter + " is not admin");
            //if(db.isUserAdmin(Name, usernamePromoted))
            //    throw new Exception("Promoted usename " + usernamePromoted + " is admin already");

            db.AddForumAdmin(Name, usernamePromoted);
            return true;
        }

        internal void ResetToDefaultPolicy()
        {
            ForumPolicy Policy = new ForumPolicy();
            db.setPolicy(db.GetPolicyIDByForumName(Name), Policy.MaxModeratorsNumber, Policy.MinPasswordLetters, Policy.MinUserNameLetters, Policy.PasswordRequiredUpperLowerCase, Policy.PasswordReuqiredNumbers, Policy.SuspendModeratorMinTime, Policy.SuspendAdminMinTime, Policy.SuspendMemberMinTime, Policy.GuestCanPost, Policy.GuestCanStartThread, Policy.MinSeniorityForMediatorToEditAPost, Policy.MinSeniorityForMediatorToRemoveAPost);
        }

        internal bool findThreadByTitle(int subforumid, string title)
        {
            return getSubForumByID(subforumid).findThreadByTitle(title);
        }

        internal int findThreadId(int subForum, string title)
        {
            return getSubForumByID(subForum).findThreadId(title);
        }
    }

}
