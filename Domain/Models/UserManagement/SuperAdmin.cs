﻿using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.UserManagement
{
    public class SuperAdmin:Member
    {
        public SuperAdmin (String id, String username, String password,
            string firstName, String lastName, DateTime birthDate, String eMail): 
            base (id, username, password, firstName, lastName, birthDate, eMail)
        {
            base.verify();
        }

        
    }
}
