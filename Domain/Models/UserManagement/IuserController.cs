﻿using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.MinimizedModels;

namespace ForumGenerator.Controllers
{
    public interface IuserController
    {
        List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender);

        // name, last, user, email
        Tuple<string, string, string, string> GetUserDetails(int forumID, string userName);

        bool AddFreind(string sender, string reciver, int forumid);

        bool ConfirmFriendRequest(string sender, string reciver, int forumid);

        bool IsUserApproved(string member, int forumid);
        
    }
}
