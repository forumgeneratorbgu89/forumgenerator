﻿using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.MinimizedModels;

namespace ForumGenerator.Controllers
{
    public class UserControllerLocal : IuserController
    {
        private static UserControllerLocal instance;
        private DAL.ForumMapper db;

        #region constuctor
        private UserControllerLocal(){}

        private UserControllerLocal(List<Forum> forums){}

        public static UserControllerLocal GetInstance()
        {
            if(instance == null)
            {
                instance = new UserControllerLocal();
            }
            return instance;
        }

        #endregion

        #region functions

        public Forum GetForum(int id)
        {
            return new Forum(id, db.GetForumNameByID(id));
        }

        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            return GetForum(forumID).GetIncomingMessagesOfUser(forumID, usernameTarget, userNameSender);
        }

        public Tuple<string, string, string, string> GetUserDetails(int forumID, string userName)
        {
            // returns: name, lastname, username, email
            Member member = GetForum(forumID).getMemberByName(userName);
            return Tuple.Create(member.firstName, member.lastName, member.username, member.Email);
        }

        public bool AddFreind(string sender, string reciver, int forumid)
        {
            GetForum(forumid).AddFriend(sender, reciver,db.GetForumNameByID(forumid));
            return true;
        }

        public bool ConfirmFriendRequest(string sender, string reciver, int forumid)
        {
            GetForum(forumid).ConfirmFriendRequest(sender, reciver, db.GetForumNameByID(forumid));
            return true;
        }

        public bool IsUserApproved(string username, int forumid)
        {
            return GetForum(forumid).isVerified(username);
        }

        #endregion
    }
}