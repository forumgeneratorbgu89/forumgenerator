﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models.UserManagement;

namespace Domain.Models.UserManagement
{
    public class Message
    {
        string message { get; set; }
        Member from { get; set; }
        Member to { get; set; }
        DateTime Time { get; set; }

        public Message (string message, Member from, Member to)
        {
            this.message = message;
            this.from = from;
            this.to = to;
            this.Time = DateTime.Now;
        }
    }
}
