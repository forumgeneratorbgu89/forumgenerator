﻿using System;
using System.Collections.Generic;
using DAL;

namespace Domain.Models.UserManagement
{
    public class Member:User
    {
        private MemberMapper db;
        public string ID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public DateTime birthDate { get; set; }
        public DateTime suspendExpire { get; set; }
        public int ForumID { get; set; }
        public bool isVerified { get; set; }
        public List<Notification> Notifications { get; set; }

        private List<Member> friends;
        private List<Member> friendrRequests;
        private Dictionary<string, LinkedList<Message>> friendMessages;
        private Dictionary<string, LinkedList<Message>> othersMessages;
        

        public Member(string id, string username, string password,
            string firstName, string lastName, DateTime birthDate, string eMail)
        {
            db = new MemberMapper();
            ID = id;
            this.username = username;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            Code = this.GetHashCode().ToString();
            suspendExpire = DateTime.Today;
            this.Email = eMail;
            friends = new List<Member>();
            friendrRequests = new List<Member>();
            friendMessages = new Dictionary<string, LinkedList<Message>>();
            othersMessages = new Dictionary<string, LinkedList<Message>>();
            isVerified = false;
        }

        public Member()
        {
        }

        public string getUsername()
        {
            return username;
        }

        public void verify () // is registration completed by answer the mail
        {
            if(!username.Equals("admin"))
                db.Verify(username);
            //isVerified = true;
        }

        public bool IsVerified()
        {
            return isVerified;
        }

        public bool isFriend(Member member)
        {
            return friends.Contains(member);
        }

        public bool confirmFriendRequest(Member member)
        {
            if(othersMessages.ContainsKey(member.username))
            {
                MoveFromOthersToFriends(member);
            }
            friends.Add(member);
            friendrRequests.Remove(member);
            friendMessages.Add(member.username, new LinkedList<Message>());
            member.friendRequestApprooved(this);
            return true;
        }

        private void recieveFriendRequests (Member member)
        {
            friendrRequests.Add(member);
        }

        public void removeFriend(Member member)
        {
            friends.Remove(member);
            member.removedBy(this);
        }

        private void removedBy(Member member)
        {
            friends.Remove(member);
        }

        private void friendRequestApprooved (Member member)
        {
            if (othersMessages.ContainsKey(member.username))
            {
                MoveFromOthersToFriends(member);
            }
            friends.Add(member);
            friendMessages.Add(member.username, new LinkedList<Message>());
        }

        private void MoveFromOthersToFriends(Member member)
        {
            friendMessages.Add(member.username, othersMessages[member.username]);
            othersMessages.Remove(member.username);
        }

        public bool sendFriendRequest(Member member)
        {
            member.recieveFriendRequests(this);
            return true;
        }

        public bool login (string username, string password)
        {

            return isVerified && (this.username == username & this.password == password);
        }

        public void suspend (int days)
        {
            //Nullable<DateTime> suspendDate = db.GetSuspendDate(username);
            //suspendDate = DateTime.Today.AddDays(days);
            db.UpdateSuspendDate(username, DateTime.Today.AddDays(days));
        }

        // Return true if the user is in a suspention right now
        public bool isSuspend()
        {
            if (suspendExpire.CompareTo(DateTime.Today) < 0)
                return true;
            return false;
        }

        public void sendMessage (Member member, string text)
        {
            string name = member.username;
            Message message = new Message(text, this, member);
            if(friendMessages.ContainsKey(name))
            {
                friendMessages[name].AddLast(message);
                member.recieveMessage(this, text);
            }
            else
            {
                if(!othersMessages.ContainsKey(name))
                {
                    othersMessages.Add(name, new LinkedList<Message>());
                }
                othersMessages[name].AddLast(message);
                member.recieveMessage(this, text);
            }
        }

        private void recieveMessage(Member member, string text)
        {
            string name = member.username;
            Message message = new Message(text, member, this);
            if (friendMessages.ContainsKey(name))
            {
                friendMessages[name].AddLast(message);
            }
            else
            {
                if (!othersMessages.ContainsKey(name))
                {
                    othersMessages.Add(name, new LinkedList<Message>());
                }
                othersMessages[name].AddLast(message);
            }
        }
    }
}
