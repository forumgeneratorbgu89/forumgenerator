﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MinimizedModels
{
    public class MinimizedUserInForum
    {
        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private UserTypeInForum state;

        public UserTypeInForum State
        {
            get { return state; }
            set { state = value; }
        }


    }
    public enum UserTypeInForum
    {
        Guest ,
        NotApproved ,
        Admin ,
        Member,
        SuperAdmin
    }
}
