﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MinimizedModels
{
   public class MinimizedThread
    {
        public string Title { get; set; }

        public string UserNameWriter { get; set; }

        public  DateTime Date { get; set; }

        public int ID { get; set; }
    }
}
