﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ReturnedValues
{
    public class MinimizedForum
    {
        private string foroumName;

        public string ForumName
        {
            get { return foroumName; }
            set { foroumName = value; }
        }

        private int forumID;

        public int ForumID
        {
            get { return forumID; }
            set { forumID = value; }
        }


    }
}
