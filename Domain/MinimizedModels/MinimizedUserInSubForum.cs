﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MinimizedModels
{
    public class MinimizedUserInSubForum
    {
        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private UserTypeInSubForum state;

        public UserTypeInSubForum State
        {
            get { return state; }
            set { state = value; }
        }


    }
    public enum UserTypeInSubForum
    {
        Guest,
        Admin,
        Moderator,
        Member,
        SuperAdmin
    }

}

