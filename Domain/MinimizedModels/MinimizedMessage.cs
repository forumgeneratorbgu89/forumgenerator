﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MinimizedModels
{
   public class MinimizedMessage
    {
        public string from { get; set; }
        public string to { get; set; }
        public string content { get; set; }
        public DateTime date { get; set; }
    }
}
