﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MinimizedModels
{
    public class MinimizedPost
    {
        public int PostId { get; set; }

        public string Text { get; set; }

        public string userNameWriter { get; set; }

        public DateTime Date { get; set; }
    }
}
