﻿using System;
using System.Linq;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    רישום (registration) . ( חבר לפורום יש להקפיד להתייחס למדיניות רישום של פורום כפי
    שניתן להגדיר בסיפור השימוש הקודם ).
    */
    [TestClass]
    public class Registration : ClientSideAcceptance
    {

        //[TestMethod]
        ///* register to forum without name */
        //public void RegisterToForumWithoutName()
        //{
        //    var isRegistered = imp.Register(forums[0].ForumID, string.Empty, "1234", "some", "sh", 1999, 1, 1,
        //        "someSheker@hell.com");
        //    Assert.IsFalse(isRegistered.Equals(string.Empty));
        //    if (isRegistered.Equals(string.Empty))
        //    {
        //        imp.deleteMember(forums[0].ForumID, string.Empty);
        //        Assert.IsTrue(false);
        //    }
        //}

        //[TestMethod]
        ///* register to forum without password */
        //public void RegisterToForumWithoutPassword()
        //{
        //    var isRegistered = imp.Register(forums[0].ForumID, "Morbo",string.Empty, "some", "sh", 1999, 1, 1,
        //        "someSheker@hell.com");
        //    Assert.IsFalse(isRegistered.Equals(string.Empty));
        //    if (isRegistered.Equals(string.Empty))
        //    {
        //        imp.deleteMember(forums[0].ForumID, "Morbo");
        //        Assert.IsTrue(false);
        //    }
        //}

        //[TestMethod]
        ///* register to forum without email */
        //public void RegisterToForumWithoutEmail()
        //{
        //    var isRegistered = imp.Register(forums[0].ForumID, "Morbo", "1234", "some", "sh", 1999, 1, 1, string.Empty);
        //    Assert.IsFalse(isRegistered.Equals(string.Empty));
        //    if (isRegistered.Equals(string.Empty))
        //    {
        //        imp.deleteMember(forums[0].ForumID, "Morbo");
        //        Assert.IsTrue(false);
        //    }
        //}

        [TestMethod]
        /* register to forum */
        public void RegisterToForum()
        {
            var isRegistered = imp.Register(forums[0].ForumID, "Morbo", "1234567", "some", "sh", 1999, 1, 1,
                "someSheker@hell.com", 1);
            Assert.IsTrue(isRegistered.Equals("success"));
            var details = imp.GetUserDetails(forums[0].ForumID, "Morbo");
            var expected = new List<string>();
            expected.Add("some");
            expected.Add("sh");
            expected.Add("Morbo");
            expected.Add("someSheker@hell.com");
            var lastUserName = imp.getAllUserNames(forums[0].ForumID).Last();
            Assert.AreEqual("Morbo", lastUserName);
            for (int i = 0; i < details.Count; i++)
            {
                Assert.AreEqual(expected[i], details[i]);
            }
        }

        [TestMethod]
        /* register to forum with name that already exists */
        public void RegisterToForumNameThatAlreadyExists()
        {
            var isRegistered = imp.Register(forums[0].ForumID, "rani el-kingo", "123456", "some", "sh", 1999, 1, 1, "aaa@bbb.ccc", 1);
            Assert.IsFalse(isRegistered.Equals("success"));
            if (isRegistered.Equals("success"))
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        /* register to forum with password that isnt approved by forum policy */
        public void RegisterToForumWithBadPasswordNotAccordingToPolicy()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.MinPasswordLetters = 5;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var isRegistered = imp.Register(forums[0].ForumID, "Morbo", "1234", "some", "sh", 1999, 1, 1, "aaa@bbb.ccc", 1);
            Assert.IsFalse(isRegistered.Equals("success"));
            if (isRegistered.Equals("success"))
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        /* register to forum with email that already exists */
        public void RegisterToForumWithEmailThatAlreadyExistsInside()
        {
            var isRegistered = imp.Register(forums[0].ForumID, "Morbo", "1234", "some", "sh", 1999, 1, 1, "ccc@bbb.grrr", 1);
            Assert.IsFalse(isRegistered.Equals("success"));
            if (isRegistered.Equals("success"))
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        /* register to forum with name that isnt approved by forum policy */
        public void RegisterToForumNameIsToShort()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.MinUserNameLetters = 6;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var isRegistered = imp.Register(forums[0].ForumID, "Morbo", "1234", "some", "sh", 1999, 1, 1, "aaa@bbb.ccc", 1);
            Assert.IsFalse(isRegistered.Equals("success"));
            if (isRegistered.Equals("success"))
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        /* login member */
        public void Login()
        {
            imp.LogOut(forums[0].ForumName, members[0].UserName);
            var ans = imp.Login(forums[0].ForumID, members[0].UserName, "123456");
            Assert.IsNull(ans.ErrorMessageg);
        }

        [TestMethod]
        /* login member with wrong password */
        public void LoginWrongPassword()
        {
            Assert.IsFalse(
                imp.Login(forums[0].ForumID, members[0].UserName, "12345")
                    .ErrorMessageg.Equals("success"));
        }

        [TestMethod]
        /* login member with wrong name */
        public void LoginWrongName()
        {
            Assert.IsFalse(imp.Login(forums[0].ForumID, members[0].UserName + "lalalaa", "123456")
                    .ErrorMessageg.Equals("success"));
        }

    }
}
