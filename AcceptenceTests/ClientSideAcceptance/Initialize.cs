﻿using System;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    הפעלה ראשונית של – מערכת הפורומים. , בסיום פעולה זו מערכת הפורומים תוכל
    בפרט יהיה לה מנהל על בעל הרשאות מיוחדות כגון
    הרשאה ליצירת פורומים ומינוי מנהל פורום
    */
    [TestClass]
    public class Initialize : ClientSideAcceptance
    {

        [TestMethod]
        /* create brand new system and login as super-admin */
        public void InitSystemAndSuperAdmin()
        {
            if (imp.signInSuperAdmin("admin", "admin"))
            {
                MinimizedUserInForum copy = superAdmin;
                Assert.AreEqual(superAdmin.UserName, copy.UserName);
            }
            else
            {
                Assert.IsTrue(false);
            }
        }

    }
}
