﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    חלק ממדיניות זיהוי )authentication( משתמש ב"פורום מאובטח": אורח
    הנרשם לפורום מספק כתובת דוא"ל, ולפני שהרישום מתבצע הוא צריך לאשר את הרישום אמצעות
    הודעה בדוא"ל.
    */
    [TestClass]
    public class MailApproval : ClientSideAcceptance
    {
        [TestMethod]
        /* sign up and log in as member after email approval */
        public void SignUpAndLoginAsMemberAfterEmailApproval()
        {
            if (imp.Register(forums[0].ForumID, "Morbo", "123456", "a", "b", 1999, 1, 1, "someSheker@hell.com", 1) !=
                "success")
            {
                Assert.IsTrue(false);
            }
            else
            {
                imp.VerifyUser(forums[0].ForumID, "Morbo", imp.GetVerificationCode(forums[0].ForumID, "Morbo"));
                var isApproved = imp.IsUserApproved("Morbo", forums[0].ForumID);
                Assert.IsTrue(isApproved);
                Assert.IsNotNull(imp.Login(forums[0].ForumID, "Morbo", "123456").Result);
                imp.deleteMember(forums[0].ForumID, "Member");
                Assert.IsFalse(false);
            }
        }

        [TestMethod]
        /* sign up and log in as member without email approval */
        public void SignUpAndLogInAsMemberBeforeEmailApproval()
        {
            if (!imp.Register(forums[0].ForumID, "Morbo", "123456", "a", "b", 1999, 1, 1, "someSheker@hell.com", 1).Equals("success"))
            {
                Assert.IsTrue(false);
            }
            else
            {
                string s = imp.Login(forums[0].ForumID, "Morbo", "123456").ErrorMessageg;
                Assert.IsFalse(String.IsNullOrEmpty(s));
            }
        }
    }
}
