﻿using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    קביעת שינוי מאפיינים אילוצים כללים מדיניות של פורום כגון 
    A. מספר מנחים מינימאלי לתת פורום - .
    B. מבנה סיסמא של חבר למשל לפחות שמונה תווים שמתוכם לפחות אחד הוא מספר ( , ).
    */
    [TestClass]
    public class Policy : ClientSideAcceptance
    {
        [TestMethod]
        /* change policy of forum - guest can post replies  */
        public void SetGuestCanPostReplies()
        {
            imp.ResetToDefaultPolicy(forums[0].ForumID);
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = true;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var guestCanReply = imp.CanGuestReplyToPost(forums[0].ForumID);
            Assert.IsTrue(guestCanReply);
        }

        [TestMethod]
        /* change policy of forum - guest can start a thread  */
        public void SetGuestCanStartAThread()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanStartThread = true;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var guestCanThread = imp.CanGuestStartThread(forums[0].ForumID);
            Assert.IsTrue(guestCanThread);
        }

        [TestMethod]
        /* change policy of forum - change minimum password length  */
        public void ChangeMinimumPasswordLength()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.MinPasswordLetters = 10;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var minPasswordLength = imp.GetForumPolicy(forums[0].ForumID).MinPasswordLetters;
            Assert.AreEqual(10, minPasswordLength);
        }

        [TestMethod]
        /* change policy of forum - change minimum user name length  */
        public void ChangeMinimumUserNameLength()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.MinUserNameLetters = 7;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            var minUserNameLength = imp.GetForumPolicy(forums[0].ForumID).MinUserNameLetters;
            Assert.AreEqual(7, minUserNameLength);
        }
    }
}
