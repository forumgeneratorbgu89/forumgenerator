﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    מנהל פורום ממנה משתמש להיות מנחה לתת-פורום. ההנחייה יכולה
    להיקבע לזמן מוגבל שיוגדר ע"י מנהל הפורום. מנהל הפורום שמינה את המנחה יכול לערוך את
    משך הזמן של המינוי.
    */
    [TestClass]
    public class Promoting : ClientSideAcceptance
    {

        [TestMethod]
        /* admin promotes member to mod in subforum */
        public void AdminPromotesAMemberToBeAModerator()
        {
            Assert.Equals("",imp.PromoteModerator(members[0].UserName, forums[0].ForumID, subforums[0].ID));
            var moderators = imp.GetSubForumModerators(forums[0].ForumID, subforums[0].ID);
            var lastModerator = moderators.Last().ElementAt(0);
            Assert.AreEqual(members[0].UserName, lastModerator);

        }

        [TestMethod]
        /* admin promotes member to admin */
        public void AdminPromotesAMemberToBeAnAdmin()
        {
            Assert.IsTrue(imp.PromoteToAdmin(forums[0].ForumID, members[0].UserName, members[1].UserName));
        }

        [TestMethod]
        /* admin promotes member to mod in subforum with time limit */
        public void AdminPromotesAMemberToBeAModeratorWithTimeLimit()
        {
            imp.PromoteModerator(members[0].UserName, forums[0].ForumID, subforums[0].ID);
            imp.editModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName,
                DateTime.Now.AddMilliseconds(10000));
            Assert.IsTrue(imp.isUserModeratorOfSubForum(forums[0].ForumID, subforums[0].ID, members[0].UserName));
            Thread.Sleep(10000);
            Assert.IsFalse(imp.isUserModeratorOfSubForum(forums[0].ForumID, subforums[0].ID, members[0].UserName));
        }

        [TestMethod]
        /* admin changes the time limit on for mod promotion */
        public void AdminChangesTheTimeLimitsOfOneOfHisModerators()
        {
            DateTime? oldModDuration;
            imp.PromoteModerator(members[0].UserName, forums[0].ForumID, subforums[0].ID);
            imp.editModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName,
                DateTime.Now.AddMilliseconds(10000));
            oldModDuration = imp.getModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName);
            if (oldModDuration == null)
                Assert.IsTrue(false);
            DateTime newDuration;
            try
            {
                newDuration = oldModDuration.Value.AddDays(1);
            }
            catch (Exception)
            {
                newDuration = (DateTime)oldModDuration;
            }
            imp.editModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName, newDuration);
            if (oldModDuration.Equals(DateTime.MaxValue))
            {
                Assert.AreEqual(oldModDuration.ToString(), imp.getModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName).ToString());
            }
            else
            {
                Assert.AreNotEqual(oldModDuration.ToString(), imp.getModTimeLimit(forums[0].ForumID, subforums[0].ID, members[0].UserName).ToString());
            }
        }

    }
}
