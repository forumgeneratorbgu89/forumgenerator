﻿using System.Collections.Generic;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    שירותי הודעות -- אינטראקטיביות:
    a. פרסום הודעה )ראשונה או תגובה(: גורר תגובה אינטראקטיבית: כל פורום מיידע את
    המשתמשים שלו )חברים ואורחים( על כל פרסום של הודעה חדשה.
    b. שינוי תוכן הודעה עצמית: ייעשה על ידי מפרסם ההודעה. השינוי מחייב אינטראקטיביות,
    המתבטאת ביידוע כל חברי הפורום שפרסמו תגובה להודעה שהשתנתה.
    c. מחיקת הודעה עצמית: ייעשה על ידי מפרסם ההודעה. השינוי מחייב:
    a. מחיקת כל הודעות התגובה
    b. אינטראקטיביות: מתבטאת ביידוע כל חברי הפורום שפרסמו תגובה להודעה
    שנמחקה.
    */
    [TestClass]
    public class InteractiveReactions : ClientSideAcceptance
    {
        [TestMethod]
        /* admin that thread gets notification after two comments */
        public void MemberGetNotificationsForCommentingOnHisPost()
        {
            MinimizedPost[] posts = new MinimizedPost[5];
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            posts[0] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0}, members[1].UserName,
                "good for you, dick.");
            posts[1] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[2].UserName,
                "aaaa.");
            posts[3] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "cccc.");
            List<string[]> notifications = imp.GetNotificationsString(forums[0].ForumName, members[0].UserName,false);
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.AreEqual(3, notifications.Count);
        }
        [TestMethod]
        /* members that commented on a post get notifications after the post is getting editted */
        public void NotificationsOnPostYouCommentedOn()
        {
            MinimizedPost[] posts = new MinimizedPost[5];
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            posts[0] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "good for you, dick.");
            imp.EditPost(forums[0].ForumName, subforums[0].Name, thread.ID, new List<int> {0}, members[0].UserName,
                "new content here");
            List<string[]> notifications = imp.GetNotificationsString(forums[0].ForumName, members[1].UserName,false);
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.AreEqual(1, notifications.Count);
        }

        [TestMethod]
        /* members that commented on a post get notifications after the post got deleted */
        public void DeletingAPostDeletesCommentsAndNotifyTheCommenters()
        {
            MinimizedPost[] posts = new MinimizedPost[5];
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            posts[0] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "good for you, dick.");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            List<string[]> notifications = imp.GetNotificationsString(forums[0].ForumName, members[1].UserName, false);
            Assert.AreEqual(1, notifications.Count);
        }
    }
}
