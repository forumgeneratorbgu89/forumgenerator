﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    צירת תת פורום על ידי מנהל הפורום גם כאן יש לשים לב למאפיינים של תת פורום
    */
    [TestClass]
    public class AddSubForum : ClientSideAcceptance
    {
        [TestMethod]
        /* add subforum when not logged in as admin */
        public void AddSubForumNotAdmin()
        {
            if (!imp.IsUserAdminOfForum(forums[0].ForumID, members[0].UserName))
            {
                var isAdded = imp.AddSubForum(forums[0].ForumID, "life-sucks", new List<string> {members[0].UserName}, "some-subject");
                if (isAdded)
                {
                    imp.deleteSubForum(forums[0].ForumName, "life-sucks");
                }
                Assert.IsFalse(isAdded);
            }
            else { Assert.IsTrue(false); }
        }

        [TestMethod]
        /* add subforum when logged in as admin */
        public void AddSubForumAsAdmin()
        {
            if (imp.IsUserAdminOfForum(forums[0].ForumID, members[1].UserName))
            {
                var beforeSubForums = imp.getAllSubForum(forums[0].ForumID).Count;
                var isAdded = imp.AddSubForum(forums[0].ForumID, "life-sucks", new List<string> { members[1].UserName }, "some-subject");
                var afterSubForums = imp.getAllSubForum(forums[0].ForumID).Count;
                if (isAdded)
                {
                    imp.deleteSubForum(forums[0].ForumName, "life-sucks");
                    Assert.AreEqual(beforeSubForums + 1, afterSubForums);
                }
                Assert.IsTrue(isAdded);
            }
            else { Assert.IsTrue(false); }
        }

        //[TestMethod]
        ///* add subforum when logged in as admin but missing subforum name */
        //public void AddSubForumWithNoName()
        //{
        //    if (imp.IsUserAdminOfForum(forums[0].ForumID, members[1].UserName))
        //    {
        //        var isAdded = imp.AddSubForum(forums[0].ForumID, string.Empty, new List<string> { members[1].UserName }, "some-subject");
        //        if (isAdded)
        //        {
        //            imp.deleteSubForum(forums[0].ForumName, string.Empty);
        //        }
        //        Assert.IsFalse(isAdded);
        //    }
        //    else { Assert.IsTrue(false); }
        //}

        [TestMethod]
        /* add subforum when logged in as admin of another forum */
        public void AddSubForumNotAdminOfTheForum()
        {
            member = members[1];
            if(imp.IsUserAdminOfForum(forums[0].ForumID, members[1].UserName) && !imp.IsUserAdminOfForum(forums[1].ForumID, members[1].UserName))
            {
                var isAdded = imp.AddSubForum(forums[1].ForumID, "life-sucks", new List<string> { members[0].UserName }, "some-subject");
                if (isAdded)
                {
                    imp.deleteSubForum(forums[1].ForumName, "life-sucks");
                }
                Assert.IsFalse(isAdded);
            }
            else { Assert.IsTrue(false); }
        }
    }
}
