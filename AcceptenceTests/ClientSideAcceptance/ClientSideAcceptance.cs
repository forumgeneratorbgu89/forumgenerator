﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System.Threading;

namespace AcceptenceTests.ClientSideAcceptance
{
    [TestClass]
    public class ClientSideAcceptance
    {

        //private Acceptance.TestsImplementation imp;
        public Iservice imp;
        public MinimizedUserInForum superAdmin;
        public MinimizedForum forum;
        public MinimizedUserInForum member;
        public MinimizedThread thread;
        public MinimizedPost post;
        public MinimizedUserInForum guest;
        public ForumPolicy policy;
        public MinimizedUserInForum[] members;
        public MinimizedForum[] forums;
        public MinimizedSubForum[] subforums;


        protected MinimizedUserInForum signUpApproveAndLogin(MinimizedForum f, String username, String password,
            String fname, String lname,
            int bdateY, int bdateM, int bdateD, String email)
        {
            MinimizedUserInForum output = null;
            string registerMessage = imp.Register(f.ForumID, username, password, fname, lname, bdateY, bdateM, bdateD, email, 1);
            if (registerMessage.Equals("success"))
            {
                output = new MinimizedUserInForum();
                output.State = UserTypeInForum.Member;
                output.UserName = username;
            }
            imp.VerifyUser(f.ForumID, username, imp.GetVerificationCode(f.ForumID, username));
            imp.Login(f.ForumID, username, password);
            return output;
        }

        [TestInitialize]
        public void InitTest()
        {
            imp = new WebApiService();
            System.Threading.Thread.Sleep(200);
            imp.clearDB();

            if (imp.IsSystemIniaitlized())
            {
                if (imp.signInSuperAdmin("admin", "admin"))
                {
                    InitSuperAdmin();
                }
            }
            else
            {
                if (imp.signUpSuperAdmin("admin", "admin", "aaa", "bbb", DateTime.Now, "aaa@bbb.ccc"))
                {
                    InitSuperAdmin();
                }
            }

            forums = new MinimizedForum[2] { new MinimizedForum(), new MinimizedForum() };
            // make forums
            string forum1name = "Forum[0]all!";
            int forumsid1 = imp.AddForum(forum1name, superAdmin.UserName, new ForumPolicy());
            forums[0].ForumName = forum1name;
            forums[0].ForumID = forumsid1;
            int forumindex = 0;
            if (forumsid1 != -1)
            {
                StoreAForum(forumindex, forumsid1, forum1name);
            }
            string forum2name = "Forum[1]time!";
            int forumsid2 = imp.AddForum(forum2name, superAdmin.UserName, new ForumPolicy());
            forums[1].ForumName = forum2name;
            forums[1].ForumID = forumsid2;
            int forum2index = 1;
            if (forumsid2 != -1)
            {
                StoreAForum(forum2index, forumsid2, forum2name);
            }
            members = new MinimizedUserInForum[3];
            // make members
            members[0] = signUpApproveAndLogin(forums[0], "rani el-kingo", "123456", "rani", "zinger", 1989, 1, 1,
                "aaa@bbb.grrr"); // this will be a regular member of forums[0]
            members[1] = signUpApproveAndLogin(forums[0], "gargamel", "234567", "gar", "amel", 1700, 1, 1,
                "ccc@bbb.grrr"); // this will be an admin of forums[0]
            imp.PromoteToAdmin(forums[0].ForumID, members[1].UserName, superAdmin.UserName);
            members[2] = signUpApproveAndLogin(forums[0], "tov-tov", "xy4567", "tov", "tov", 1800, 1, 1, "dddd@bbb.grrr");
            // this will be a mod of forums[0]

            subforums = new MinimizedSubForum[1];
            // make subforums
            string subforum1name = "SubForum[0] - where the party is at!";
            string subforum1subject = "a subject ";
            int subforum1index = 0;
            if (imp.AddSubForum(forums[0].ForumID, subforum1name, new List<string> { members[1].UserName },
                subforum1subject))
            {
                int subforum1id = imp.GetSubForumIdByName(subforum1name, forums[0].ForumID);
                subforums[subforum1index] = new MinimizedSubForum();
                subforums[subforum1index].ID = subforum1id;
                subforums[subforum1index].Name = subforum1name;
                subforums[subforum1index].Subject = subforum1subject;
            }
            imp.PromoteModerator(members[2].UserName, forums[0].ForumID, subforums[0].ID);

            guest = new MinimizedUserInForum();
            guest.UserName = "Guest";
            guest.State = UserTypeInForum.Guest;
            policy = new ForumPolicy();
        }

        private void StoreAForum(int forumindex, int forumsid1, string forum1name)
        {
            forums[forumindex] = new MinimizedForum();
            forums[forumindex].ForumID = forumsid1;
            forums[forumindex].ForumName = forum1name;
        }

        public void InitSuperAdmin()
        {
            superAdmin = new MinimizedUserInForum();
            superAdmin.UserName = "admin";
            superAdmin.State = UserTypeInForum.SuperAdmin;
        }

        public MinimizedForum AddForum(MinimizedForum forum, string forumName, MinimizedUserInForum member)
        {
            forum = null;
            if (member.State != UserTypeInForum.SuperAdmin)
            {
                return forum;
            }
            var forumId = imp.AddForum(forumName, member.UserName, new ForumPolicy());
            if (forumId != -1)
            {
                forum = new MinimizedForum();
                forum.ForumID = forumId;
                forum.ForumName = forumName;
            }
            return forum;
        }

        public MinimizedPost postComment(int forumID, int subForumID, int threadID, List<int> posts, string memberUserName, string comment)
        {
            MinimizedPost post = null;
            var postid = imp.replyToPost(forumID, subForumID, threadID, posts, memberUserName, comment);
            if (postid[postid.Count - 1] != -1)
            {
                post = new MinimizedPost();
                post.PostId = postid[postid.Count - 1];
                post.Text = comment;
                post.userNameWriter = memberUserName;
            }
            return post;
        }

        public MinimizedThread postAThread(int forumID, string subForumName, string userName, string title, string content)
        {
            MinimizedThread thread = null;
            if (imp.AddThread(forumID, subForumName, title, content, userName))
            {
                thread = new MinimizedThread();
                var subForumID = imp.GetSubForumIdByName(subForumName, forumID);
                thread.ID = imp.findThreadId(forumID, subForumID, title);
                thread.Title = title;
                thread.UserNameWriter = userName;
            }
            return thread;
        }

        [TestCleanup]
        public void CleanLastTest()
        {
            if (forum != null)
            {
                imp.deleteForum(forum.ForumName);
            }
            forum = null;
            member = null;
            thread = null;
            post = null;
            guest = null;
            policy = null;
            members = null;
            subforums = null;
            forums = null;
            imp.clearDB();
            imp = null;
        }

    }
}