﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainEntity.MinimizedModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    שירותי מנהל פורום:
    a. ביטול מינוי של מנחה של תת-פורום )לפני תום תקופת המינוי(.
    b. קבלת דיווחים:
    a. מספר הודעות כולל בתת פורום.
    b. רשימת ההודעות שנכתבו על ידי חבר.
    c. רשימת מנחים, מי מינה אותם, מתי ולאיזה תת-פורום, וההודעות שפרסמו.
    */
    [TestClass]
    public class ForumAdminService : ClientSideAcceptance
    {
        [TestMethod]
        // admin unpromote a moderator
        public void AdminCanUnpromoteAModerator()
        {
            var isUnpromoted = imp.UnpromoteModerator(forums[0].ForumID, subforums[0].Name, members[1].UserName,
                members[2].UserName);
            var isModeratorOfSubForum = imp.isUserModeratorOfSubForum(forums[0].ForumID, subforums[0].ID,
                members[2].UserName);
            Assert.IsTrue(isUnpromoted);
            Assert.IsFalse(isModeratorOfSubForum);
        }

        [TestMethod]
        // admin get details on the forum
        public void GetDetailsOnTheForum()
        {
            // forums[0] subforums[0] admin : members[1]
            MinimizedPost[] posts = new MinimizedPost[5];
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            
            posts[0] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0}, members[1].UserName,
                "good for you, dick.");
            posts[1] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0}, members[2].UserName,
                "aaaa.");
            posts[2] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[0].UserName,
                "bbbb.");
            posts[3] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "cccc.");
            posts[4] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, posts[0].PostId }, members[2].UserName,
                "dont be a dick member[1].");
            // check for moderators
            List<string> moderators = imp.GetAllModerators(forums[0].ForumID);
            Assert.IsNotNull(moderators);
            MinimizedPost openinngPost = imp.GetOpenningPost(forums[0].ForumID, subforums[0].ID, thread.ID);
            List<MinimizedPost> postsInForum = imp.GetAllComments(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { openinngPost.PostId });
            List<MinimizedThread> threads = imp.GetAllThreads(forums[0].ForumID, subforums[0].ID);
            var lastThread = threads.Last();
            // עד כאן
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.AreEqual(lastThread.Title, "I AM A THREAD MADE BY A MEMBER!");
            Assert.AreEqual(2, moderators.Count);
            Assert.IsNotNull(postsInForum);
            Assert.AreEqual(4, postsInForum.Count);
        }
    }
}
