﻿using System;
using System.Collections.Generic;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    חבר בפורום יכול למחוק הודעה שפרסם בעבר. המחיקה גוררת מחיקת כל הודעות
    התגובה. אם זו הודעה פותחת אז כל הדיון נמחק. כמובן – יש לשים לב לאילוצים נוספים שעלולים
    להיות מופרים.
    */
    [TestClass]
    public class Unpost : ClientSideAcceptance
    {
        [TestMethod]
        /* guest deleting thread by member */
        public void GuestDeletingThread()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, guest.UserName);
            Assert.IsNotNull(imp.findThread(forums[0].ForumID, subforums[0].ID, thread.ID));
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
        }

        [TestMethod]
        /* member deleting his own thread */
        public void MemberDeletingThread()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsNull(imp.findThread(forums[0].ForumID, subforums[0].ID, thread.ID));
        }

        [TestMethod]
        /* member trying to delete a thread started by another user */
        public void MemberTriesToDeleteThread()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[1].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsNotNull(imp.findThread(forums[0].ForumID, subforums[0].ID, thread.ID));
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
        }

        [TestMethod]
        /* admin deleting a thread started by another */
        public void AdminDeletingThread()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
            var find = imp.findThread(forums[0].ForumID, subforums[0].ID, thread.ID);
            if (find == null)
            {
                Assert.IsNull(find);
            }
            else
            {
                imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        /* deleting a thread also deletes all posts replying to it */
        public void DeletingAThreadDeletesAllItsComments()
        {
            MinimizedPost[] posts = new MinimizedPost[5];
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");

            posts[0] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "good for you, dick.");
            posts[1] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[2].UserName,
                "aaaa.");
            posts[2] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[0].UserName,
                "bbbb.");
            posts[3] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "cccc.");
            posts[4] = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, posts[0].PostId }, members[2].UserName,
                "dont be a dick member[1].");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
            for (int i = 0; i < posts.Length; i++)
            {
                if (i != 4)
                {
                    if (imp.findPost(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0, posts[i].PostId }) != null)
                    {
                        Assert.IsTrue(false);
                    } // fail test if any of the posts still exists
                }
                else
                {
                    if (imp.findPost(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, posts[0].PostId, posts[i].PostId }) != null)
                    {
                        Assert.IsTrue(false);
                    } // fail test if any of the posts still exists

                }
            }
            Assert.IsTrue(true); // if code got here then no posts were found and they were deleted
        }

        [TestMethod]
        /* guest deleting his own reply */
        public void GuestDeletingHisOwnReply()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");

            MinimizedPost post1 = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[0].UserName,
                "new post is here");
            post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, post1.PostId }, guest.UserName,
                "good for you, dick.");

            imp.deletePost(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0, post1.PostId, post.PostId}, guest.UserName);

            bool exists = imp.GetAllComments(forums[0].ForumID, subforums[0].ID, thread.ID,
                new List<int> {0, post1.PostId})
                .Count==1;
            Assert.IsTrue(exists);
        }

        [TestMethod]
        /* member deleting his reply */
        public void MemberDeletingHisReply()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");

            post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[0].UserName,
                "good for you, dick.");

            imp.deletePost(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, post.PostId }, members[0].UserName);

            Assert.IsNull(imp.findPost(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0, post.PostId }));

            var deleteThread = imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);

            Assert.IsTrue(deleteThread);
        }
        
        [TestMethod]
        /* guest deleting thread by guest */
        public void GuestdeletesAThreadByGuest()
        {
            int numofthreads = imp.GetAllThreads(forums[0].ForumID, subforums[0].ID).Count;
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanStartThread = true;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            thread = postAThread(forums[0].ForumID, subforums[0].Name, guest.UserName,
                "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, guest.UserName);
            Assert.AreEqual(imp.GetAllThreads(forums[0].ForumID, subforums[0].ID).Count, numofthreads+1);
        }
    }
}
