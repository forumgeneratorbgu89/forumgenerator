﻿using System;
using DomainEntity.Models.ForumManagement;
using DomainEntity.ReturnedValues;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    מנהל העל יכול ליצור פורום חדש על ידי פירוט מאפיינים קונקרטיים יש
    להקפיד להתייחס לדרישות הכלליות לגבי פורום שימו לב מנהל של פורום הוא חבר בפורום
    */
    [TestClass]
    public class AddForum : ClientSideAcceptance
    {
        [TestMethod]
        /* add forum when logged in as super-admin */
        public void AddForumAsSuperAdmin()
        {
            superAdmin = null;
            if (imp.signInSuperAdmin("admin", "admin"))
            {
                InitSuperAdmin();
            }
            var beforeNumForums = imp.GetAllForums().Count;
            forum = AddForum(forum, "testForum", superAdmin);
            var forumID = imp.GetForumIdByName("testForum");
            var afterNumForums = imp.GetAllForums().Count;
            var afterNumForumsOtherFunction = imp.NumOfForums();
            Assert.AreEqual(afterNumForums, afterNumForumsOtherFunction);
            Assert.IsNotNull(forum);
            Assert.AreEqual(beforeNumForums + 1, afterNumForums);
            Assert.AreEqual(forum.ForumID, forumID);
        }

        //[TestMethod]
        ///* add forum when logged in as super-admin with missing forum name */
        //public void AddForumAsSuperAdminWithMissingForumName()
        //{
        //    forum = AddForum(forum, string.Empty, superAdmin);
        //    Assert.IsNull(forum);
        //}
    }
}