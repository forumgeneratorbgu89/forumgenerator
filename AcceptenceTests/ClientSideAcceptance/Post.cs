﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    פרסום הודעה בתת פורום ראשונית או תגובה
    */
    [TestClass]
    public class Post : ClientSideAcceptance
    {

        [TestMethod]
        /* guest starting thread when allowed in forum policy */
        public void GuestStartsPostWhenAllowed()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanStartThread = true;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            string threadTitle = "I AM A THREAD MADE BY A GUEST!";
            string text = "...and thats it :)";
            thread = postAThread(forums[0].ForumID, subforums[0].Name, guest.UserName, threadTitle, text);
            var ans = imp.findThreadByTitle(forums[0].ForumID, subforums[0].ID, threadTitle);
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
            Assert.IsTrue(ans.Title.Equals(threadTitle));
        }

        [TestMethod]
        /* guest starting thread when not-allowed in forum policy */
        public void GuestStartsPostWhenNotAllowed()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanStartThread = false;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            string threadTitle = "I AM A THREAD MADE BY A GUEST!";
            string text = "...and thats it :)";
            thread = postAThread(forums[0].ForumID, subforums[0].Name, guest.UserName, threadTitle, text);
            var ans = imp.findThreadByTitle(forums[0].ForumID, subforums[0].ID, threadTitle);
            if (ans != null)
            {
                imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[1].UserName);
            }
            Assert.IsNull(ans);
        }

        [TestMethod]
        /* member starting thread */
        public void MemberStartingThread()
        {
            string threadTitle = "I AM A THREAD MADE BY A MEMBER!";
            string text = "...and thats it :)";
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, threadTitle, text);
            var ans = imp.findThreadByTitle(forums[0].ForumID, subforums[0].ID, threadTitle);
            var openningPost = imp.GetOpenningPost(forums[0].ForumID, subforums[0].ID, ans.ID);
            Assert.AreEqual("...and thats it :)", openningPost.Text);
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsTrue(ans.Title.Equals(threadTitle));
        }

        [TestMethod]
        /* guest replying to post when allowed by forum policy */
        public void GuestReplingToPostWhenAllowed()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = true;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            string threadTitle = "I AM A THREAD MADE BY A MEMBER!";
            string text = "...and thats it :)";
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, threadTitle, text);
            var post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0}, guest.UserName,
                "good for you, dick.");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsNotNull(post);
        }
        
        [TestMethod]
        /* guest replying to post when not allowed by forum policy */
        public void GuestReplyingToPostWhenNotAllowed()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = false;
            imp.EditForumPolicy(forums[0].ForumID, fp);
            string threadTitle = "I AM A THREAD MADE BY A MEMBER!";
            string text = "...and thats it :)";
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, threadTitle, text);
            var post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, guest.UserName,
                "good for you, dick.");
            Assert.IsNull(post);
        }

        [TestMethod]
        /* member replying to a thread started by another */
        public void MemberReplyingToThread()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            var post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "good for you, dick");
            var comments = imp.GetCommentsForUser(forums[0].ForumID, members[1].UserName, members[1].UserName);
            var lastComment = comments.Last();
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsNotNull(post);
            Assert.AreEqual("good for you, dick", lastComment.Text);
        }

        [TestMethod]
        /* member replying to a post by another */
        public void MemberReplyingToPost()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            var post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[1].UserName,
                "good for you, dick");
            var post2 = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> {0, post.PostId},
                members[2].UserName, "dont be a dick member[1]");
            imp.deleteThread(forums[0].ForumID, subforums[0].ID, thread.ID, members[0].UserName);
            Assert.IsNotNull(post2);
        }

        [TestMethod]
        /* member replying to his own post */
        public void MemberReplyingHisOwnPost()
        {
            thread = postAThread(forums[0].ForumID, subforums[0].Name, members[0].UserName, "I AM A THREAD MADE BY A MEMBER!", "...and thats it :)");
            var post = postComment(forums[0].ForumID, subforums[0].ID, thread.ID, new List<int> { 0 }, members[0].UserName,
                "and i am replying to myself because i have no friends");
            Assert.IsNotNull(post);
        }

    }
}
