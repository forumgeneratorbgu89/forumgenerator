﻿using System;
using System.Linq;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ClientSideAcceptance
{
    /*
    משתמש א' יכול לשלוח הודעה פרטית, שאינה חלק משרשור שאף תת-
    פורום, ישירות למשתמש כלשהו ב'. משתמש א' לא מכיר את כתובת המייל של משתמש ב', אלא
    רק את שם המשתמש שלו.
    */
    [TestClass]
    public class PersonalMessage : ClientSideAcceptance
    {

        [TestMethod]
        /* member sending PM to member */
        public void SendPrivateMessageMemberToMember()
        {
            var isSentRequest = imp.AddFreind(members[1].UserName, members[0].UserName, forums[0].ForumID);
            var isApproved = imp.ConfirmFriendRequest(members[1].UserName, members[0].UserName, forums[0].ForumID);
            Assert.AreEqual(true, isSentRequest);
            Assert.AreEqual(true, isApproved);
            var friends = imp.GetAllFriends(forums[0].ForumID, members[0].UserName);
            var lastFriend = friends.Last();
            Assert.AreEqual(members[1].UserName, lastFriend);
            // member1 gets the message
            Assert.IsTrue(imp.sendPrivateMessage("if you are reading this, you are wasting your time.", forums[0].ForumID, members[1].UserName, members[0].UserName));
            var messages = imp.GetIncomingMessages(forums[0].ForumID, members[1].UserName, members[0].UserName);
            var lastMessage = messages.Last();
            Assert.AreEqual("if you are reading this, you are wasting your time.", lastMessage.content);
        }

        [TestMethod]
        /* member sending PM to member name that doesnt exist at all*/
        public void SendingPrivateMessageMemberToMemberThatDoesNotExist()
        {
            String badname = "nwkeol";
            for (int i = 0; i < members.Length; i++) { if (members[0].UserName == badname) { throw new Exception("test data created wrong"); } }
            Assert.IsFalse(imp.sendPrivateMessage("if you are reading this, you are wasting your time.", forums[0].ForumID, badname, members[0].UserName));
        }

        //[TestMethod]
        ////member sending PM to an empty member name 
        //public void SendingPrivateMessageMemberToEmptyMemberName()
        //{
        //    Assert.IsFalse(imp.sendPrivateMessage("if you are reading this, you are wasting your time.", forums[0].ForumID, "", members[0].UserName));
        //}
    }
}
