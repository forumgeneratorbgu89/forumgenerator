﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    שירותי הודעות -- אינטראקטיביות:
    a. פרסום הודעה )ראשונה או תגובה(: גורר תגובה אינטראקטיבית: כל פורום מיידע את
    המשתמשים שלו )חברים ואורחים( על כל פרסום של הודעה חדשה.
    b. שינוי תוכן הודעה עצמית: ייעשה על ידי מפרסם ההודעה. השינוי מחייב אינטראקטיביות,
    המתבטאת ביידוע כל חברי הפורום שפרסמו תגובה להודעה שהשתנתה.
    c. מחיקת הודעה עצמית: ייעשה על ידי מפרסם ההודעה. השינוי מחייב:
    a. מחיקת כל הודעות התגובה
    b. אינטראקטיביות: מתבטאת ביידוע כל חברי הפורום שפרסמו תגובה להודעה
    שנמחקה.
    */
    [TestClass]
    public class InteractiveReactions : ServerSideAcceptance
    {
        private string _forum1;
        private int _forum1Id;
        private string _subForum;
        private int _subForumId;
        private List<string> _adminsForum1;
        private string _lastUserName;
        private string _lastThreadTitle;
        private int _threadId;
        private List<int> _fisst; // 1st comment
        private List<int> _second; // 2nd comment

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _subForum = FImp.getLastSubForumName(_forum1Id)+"rani";
            FImp.AddSubForum(_forum1Id, _subForum, new List<string> { "admin" }, "how to commit suicide?");
            _subForumId = FImp.GetSubForumIdByName(_subForum, _forum1Id);
            _adminsForum1 = FImp.GetForumAdmins(_forum1Id);
            _lastUserName = FImp.getLastUserName(_forum1Id);
            _lastThreadTitle = FImp.getLastThreadTitle(_forum1Id, _subForum) + "c";
            FImp.AddThread(_forum1Id, _subForum, _lastThreadTitle, "this is a little content", _adminsForum1[0]);
            _threadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle); // TODO: this doesnt work. continue from here!
            _fisst = FImp.replyToPost(_forum1Id, _subForumId, _threadId, new List<int> { 0 }, _lastUserName, "first comment");
            _second = FImp.replyToPost(_forum1Id, _subForumId, _threadId, new List<int> { 0 }, _adminsForum1[0], "second comment");
        }

        [TestMethod]
        /* admin that thread gets notification after two comments */
        public void MemberGetNotificationsForCommentingOnHisPost()
        {
            // notification is a string to parse like -f forum -s subforum -t thread -p post -m member ...
            List<Notification> notifications = FImp.GetNotifications(_forum1, _adminsForum1[0]); // admin shpuld have 2 notifications about 2 comments
            Assert.AreEqual(2, notifications.Count);
        }
        [TestMethod]
        /* members that commented on a post get notifications after the post is getting editted */
        public void NotificationsOnPostYouCommentedOn()
        {
            // the admin who posted the post is editing it
            bool isEdited = FImp.EditPost(_forum1, _subForum, _threadId, new List<int> { 0 }, _adminsForum1[0], "this is the new text for this post");
            Assert.IsTrue(isEdited);
            // now one of the commenters should get notified
            var notifications = FImp.GetNotifications(_forum1, _lastUserName);
            Assert.IsTrue(notifications.Count > 0);
        }

        [TestMethod]
        /* members that commented on a post get notifications after the post got deleted */
        public void DeletingAPostDeletesCommentsAndNotifyTheCommenters()
        {
            FImp.deleteThread(_forum1Id, _subForumId, _threadId, _adminsForum1[0]); // we deleted the thread
            // now one of the commenters should get notified
            var notifications = FImp.GetNotifications(_forum1, _lastUserName);
            Assert.IsTrue(notifications.Count > 0);
        }

    }
}
