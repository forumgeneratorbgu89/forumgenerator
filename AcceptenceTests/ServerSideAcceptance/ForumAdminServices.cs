﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    שירותי מנהל פורום:
    a. ביטול מינוי של מנחה של תת-פורום )לפני תום תקופת המינוי(.
    b. קבלת דיווחים:
    a. מספר הודעות כולל בתת פורום.
    b. רשימת ההודעות שנכתבו על ידי חבר.
    c. רשימת מנחים, מי מינה אותם, מתי ולאיזה תת-פורום, וההודעות שפרסמו.
    */
    [TestClass]
    public class ForumAdminService : ServerSideAcceptance
    {

        private string _forum1;
        private int _forum1Id;
        private string _subForum;
        private int _subForumId;
        private List<string> _adminsForum1;
        private string _lastUserName;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _subForum = FImp.getLastSubForumName(_forum1Id);
            _subForumId = FImp.GetSubForumIdByName(_subForum, _forum1Id);
            _adminsForum1 = FImp.GetForumAdmins(_forum1Id);
            _lastUserName = FImp.getLastUserName(_forum1Id);
        }

        [TestMethod]
        // admin unpromote a moderator
        public void AdminCanUnpromoteAModerator()
        {
            var forumName = FImp.getLastForumName() + "c";
            FImp.AddForum(forumName, "admin", new ForumPolicy());
            var forumId = FImp.GetForumIdByName(forumName);
            var newUserName = FImp.getLastUserName(forumId) + "c";
            var isMemberAdded1 = FImp.Register(forumId, newUserName, "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(forumId, _lastUserName) + "l", 1);
            Assert.IsTrue(isMemberAdded1.Equals("success"));
            FImp.VerifyUser(forumId, _lastUserName + "c", FImp.GetVerificationCode(forumId, newUserName)); // added new forum and new user
            FImp.PromoteToAdmin(forumId, newUserName, "admin"); // new forum with an admin - now his is an admin
            var newMember = FImp.getLastUserName(forumId) + "c";
            var isMemberAdded2 = FImp.Register(forumId, newMember, "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(forumId, _lastUserName) + "l", 1);
            Assert.IsTrue(isMemberAdded2.Equals("success"));
            FImp.VerifyUser(forumId, newMember, FImp.GetVerificationCode(forumId, newMember)); // now we have a new member
            var subForumName = FImp.getLastSubForumName(forumId) + "c";
            AddSubForumToForum(forumId, newUserName, subForumName); // now we have a subforum
            FImp.PromoteModerator(newMember, forumId, FImp.GetSubForumIdByName(subForumName, forumId)); // now new member is a moderator
            // now we check if the admin can unpromote him
            var isUnpromoted = FImp.UnpromoteModerator(forumId, subForumName, newUserName, newMember);
            Assert.IsTrue(isUnpromoted);
        }

        [TestMethod]
        // admin get details on the forum
        public void GetDetailsOnTheForum()
        {
            var forumName = FImp.getLastForumName() + "c";
            FImp.AddForum(forumName, "admin", new ForumPolicy());
            var forumId = FImp.GetForumIdByName(forumName);
            var newUserName = FImp.getLastUserName(forumId) + "c";
            var isMemberAdded = FImp.Register(forumId, newUserName, "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(forumId, _lastUserName) + "l", 1);
            Assert.IsTrue(isMemberAdded.Equals("success"));
            FImp.VerifyUser(forumId, _lastUserName + "c", FImp.GetVerificationCode(forumId, newUserName));
            FImp.PromoteToAdmin(forumId, newUserName, "admin"); // new forum with an admin
            for (int i = 0; i < 10; i++)
            {
                AddSubForumToForumAndPostThreads(forumId, newUserName, FImp.getLastSubForumName(forumId) + "c");
            }
            // now the admin apply some queries
            var numOfSubForums = FImp.GetNumOfSubForumInForum(forumId, newUserName);
            Assert.AreEqual(10, numOfSubForums);
            int numOfThreadsInForum = FImp.getNumOfThreadsInForum(forumId, newUserName);
            Assert.AreEqual(100, numOfThreadsInForum);
        }

        private void AddSubForumToForumAndPostThreads(int forumId, string newUserName, string newName)
        {
            var moderators = new List<string> { newUserName };
            FImp.AddSubForum(forumId, newName, moderators, newName + "c");
            for (var i = 0; i < 10; i++)
            {
                FImp.AddThread(forumId, newName, FImp.getLastThreadTitle(forumId, newName) + "c", "lalaaa", newUserName);
            }
        }

        private void AddSubForumToForum(int forumId, string newUserName, string newName)
        {
            var moderators = new List<string> { newUserName };
            FImp.AddSubForum(forumId, newName, moderators, newName + "c");
        }
    }
}
