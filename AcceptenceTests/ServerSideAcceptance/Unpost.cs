﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    חבר בפורום יכול למחוק הודעה שפרסם בעבר. המחיקה גוררת מחיקת כל הודעות
    התגובה. אם זו הודעה פותחת אז כל הדיון נמחק. כמובן – יש לשים לב לאילוצים נוספים שעלולים
    להיות מופרים.
    */
    [TestClass]
    public class Unpost : ServerSideAcceptance
    {
        private string _forum1;
        private int _forum1Id;
        private string _subForum;
        private int _subForumId;
        private string _forum2;
        private int _forum2Id;
        private string _userNameFromForum2;
        private List<string> _adminsForum1;
        private List<string> _adminsForum2;
        private string _lastUserName;
        private string _newUserToPromote;
        private string _lastThreadTitle;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _subForum = FImp.getLastSubForumName(_forum1Id);
            _subForumId = FImp.GetSubForumIdByName(_subForum, _forum1Id);
            _forum2 = FImp.getSecondLastForumName();
            _forum2Id = FImp.GetForumIdByName(_forum2);
            _adminsForum1 = FImp.GetForumAdmins(_forum1Id);
            _adminsForum2 = FImp.GetForumAdmins(_forum2Id);
            _lastUserName = FImp.getLastUserName(_forum1Id);
            _userNameFromForum2 = FImp.getLastUserName(_forum2Id);
            _newUserToPromote = _lastUserName + "c";
            RegisterOneMore();
            _lastThreadTitle = FImp.getLastThreadTitle(_forum1Id, _subForum);
        }

        private bool RegisterOneMore()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            FImp.Register(_forum1Id, _newUserToPromote, "123456789", "aaa", "bbb", 1999, 7, 2,
                FImp.getUsersEmail(_forum1Id, _lastUserName) + "l", 1);
            return FImp.VerifyUser(_forum1Id, _newUserToPromote, FImp.GetVerificationCode(_forum1Id, _newUserToPromote));
        }

        [TestMethod]
        /* guest deleting thread by member */
        public void GuestDeletingThread()
        {
            var title = _lastThreadTitle + "c";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _adminsForum1[0]);
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            FImp.deleteThread(_forum1Id, _subForumId, threadId, "guest");
            Assert.IsNotNull(FImp.findThread(_forum1Id, _subForumId, threadId));
        }

        [TestMethod]
        /* member deleting his own thread */
        public void MemberDeletingThread()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _adminsForum1[0]);
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            FImp.deleteThread(_forum1Id, _subForumId, threadId, _adminsForum1[0]);
            Assert.IsNull(FImp.findThread(_forum1Id, _subForumId, threadId));
        }

        [TestMethod]
        /* member trying to delete a thread started by another user */
        public void MemberTriesToDeleteThread()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _adminsForum1[0]); // admin starts a thread
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            // now a user tries to delete it
            var isDeleted = FImp.deleteThread(_forum1Id, _subForumId, threadId, _newUserToPromote);
            Assert.IsNotNull(FImp.findThread(_forum1Id, _subForumId, threadId));
        }

        [TestMethod]
        /* mod deleting a thread started by another */
        public void ModeratorDeletingThread()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.PromoteModerator(_newUserToPromote, _forum1Id, _subForumId); // now we promote the user to be moderator
            FImp.AddThread(_forum1Id, _subForum, title, content, _lastUserName); // member starts a thread
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            var isDeleted = FImp.deleteThread(_forum1Id, _subForumId, threadId, _newUserToPromote);
            Assert.IsTrue(isDeleted);
        }

        [TestMethod]
        /* admin deleting a thread started by another */
        public void AdminDeletingThread()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _lastUserName); // member starts a thread
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            var isDeleted = FImp.deleteThread(_forum1Id, _subForumId, threadId, _adminsForum1[0]);
            Assert.IsTrue(isDeleted);
        }

        [TestMethod]
        /* deleting a thread also deletes all posts replying to it */
        public void DeletingAThreadDeletesAllItsComments()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _lastUserName); // member starts a thread
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            var fisst = FImp.replyToPost(_forum1Id, _subForumId, threadId, new List<int> { 0 }, _newUserToPromote, "first comment");
            var second = FImp.replyToPost(_forum1Id, _subForumId, threadId, new List<int> { 0 }, _adminsForum1[0], "second comment");
            FImp.deleteThread(_forum1Id, _subForumId, threadId, _lastUserName);
            var isFirstDeleted = FImp.findPost(_forum1Id, _subForumId, threadId, fisst );
            var isSecondDeleted = FImp.findPost(_forum1Id, _subForumId, threadId, second);
            Assert.IsNull(isFirstDeleted);
            Assert.IsNull(isSecondDeleted);
        }

        [TestMethod]
        /* member deleting his reply */
        public void MemberDeletingHisReply()
        {
            var title = _lastThreadTitle + "''";
            const string content = "... and thats it!";
            FImp.AddThread(_forum1Id, _subForum, title, content, _lastUserName); // member starts a thread
            var threadId = FImp.findThreadId(_forum1Id, _subForumId, title);
            var fisst = FImp.replyToPost(_forum1Id, _subForumId, threadId, new List<int> { 0 }, _newUserToPromote, "first comment");
            var second = FImp.replyToPost(_forum1Id, _subForumId, threadId, new List<int> { 0 }, _adminsForum1[0], "second comment");
            var isDeleted = FImp.deletePost(_forum1Id, _subForumId, threadId, fisst, _newUserToPromote); // deletes his own reply  
            Assert.IsTrue(isDeleted);
        }

        //[TestMethod]
        ///* member who was mod with time limit tries to delete post of another after mod time limit ends */
        //public void ModeratorTryingToDeletePostAfterHeIsNotStillModerator()
        //{
        //    member = signUpApproveAndLogin(forums[0], "Morbo", "123456", "a", "b", 1999, 1, 1, "someSheker@hell.com");

        //    thread = imp.createNewThread("I AM A THREAD MADE BY A MEMBER!", "...and thats it :)", member, subforums[0]);
        //    post = imp.replyToPost("and i am replying to myself because i have no friends.", member, thread.OpenningPost);

        //    imp.makeModOfSubforumWithTimeLimit(members[0], subforums[0], members[1], DateTime.Now.AddMilliseconds(100));
        //    imp.editModTimeLimit(members[0], subforums[0], members[1], DateTime.Now.AddMilliseconds(-100));
        //    imp.deletePost(post, subforums[0], members[0]);

        //    Assert.IsTrue(imp.findPost(post, subforums[0]));
        //}

        //[TestMethod]
        ///* member who was mod with time limit tries to delete thread of another after mod time limit ends */
        //public void ModeratorTryingToDeleteThreadtAfterHeIsNotStillModerator()
        //{
        //    member = signUpApproveAndLogin(forums[0], "Morbo", "123456", "a", "b", 1999, 1, 1, "someSheker@hell.com");

        //    thread = imp.createNewThread("I AM A THREAD MADE BY A MEMBER!", "...and thats it :)", member, subforums[0]);
        //    post = imp.replyToPost("and i am replying to myself because i have no friends.", member, thread.OpenningPost);

        //    imp.makeModOfSubforumWithTimeLimit(members[0], subforums[0], members[1], DateTime.Now.AddMilliseconds(100));
        //    imp.editModTimeLimit(members[0], subforums[0], members[1], DateTime.Now.AddMilliseconds(-100));
        //    imp.deleteThread(thread, subforums[0], members[0]);

        //    Assert.IsNotNull(imp.findThread(thread, subforums[0]));
        //}
        //[TestMethod]
        ///* guest deleting his own reply */
        //public void GuestDeletingHisOwnReply()
        //{
        //    thread = imp.createNewThread("I AM A THREAD MADE BY A MEMBER!", "...and thats it :)", members[0], subforums[0]);
        //    post = imp.replyToPost("good for you, dick.", guest, thread.OpenningPost);

        //    imp.deletePost(post, subforums[0], guest);

        //    Assert.IsTrue(imp.findPost(post, subforums[0]));
        //}
        //[TestMethod]
        ///* guest deleting thread by guest */
        //public void GuestdeletesAThreadByGuest()
        //{
        //    imp.ResetToDefaultPolicy(forums[0].ID);
        //    imp.editPolicy_SetGuestCanStartThread(forums[0].ID, true);
        //    Assert.IsFalse(false);
        //    thread = imp.createNewThread("I AM A THREAD MADE BY A MEMBER!", "...and thats it :)", guest, subforums[0]);
        //    imp.deleteThread(thread, subforums[0], guest);
        //    Assert.IsNotNull(imp.findThread(thread, subforums[0]));
        //}
    }
}
