﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    מנהל פורום ממנה משתמש להיות מנחה לתת-פורום. ההנחייה יכולה
    להיקבע לזמן מוגבל שיוגדר ע"י מנהל הפורום. מנהל הפורום שמינה את המנחה יכול לערוך את
    משך הזמן של המינוי.
    */
    [TestClass]
    public class Promoting : ServerSideAcceptance
    {

        private string _forum1;
        private int _forum1Id;
        private string _subForum;
        private int _subForumId;
        private string _forum2;
        private int _forum2Id;
        private string _userNameFromForum2;
        private List<string> _adminsForum1;
        private List<string> _adminsForum2;
        private string _lastUserName;
        private string _newUserToPromote;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _subForum = FImp.getLastSubForumName(_forum1Id);
            _subForumId = FImp.GetSubForumIdByName(_subForum, _forum1Id);
            _forum2 = FImp.getSecondLastForumName();
            _forum2Id = FImp.GetForumIdByName(_forum2);
            _adminsForum1 = FImp.GetForumAdmins(_forum1Id);
            _adminsForum2 = FImp.GetForumAdmins(_forum2Id);
            _lastUserName = FImp.getLastUserName(_forum1Id);
            _userNameFromForum2 = FImp.getLastUserName(_forum2Id);
            _newUserToPromote = _lastUserName + "c";
            RegisterOneMore();
        }

        private bool RegisterOneMore()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            FImp.Register(_forum1Id, _newUserToPromote, "123456789", "aaa", "bbb", 1999, 7, 2,
                FImp.getUsersEmail(_forum1Id, _lastUserName) + "l", 1);
            return FImp.VerifyUser(_forum1Id, _newUserToPromote, FImp.GetVerificationCode(_forum1Id, _newUserToPromote));
        }

        public bool editModTimeLimit(string promoted, int forum, int subforum, string promoter, DateTime newUntilWhen)
        {
            return FImp.isUserModeratorOfSubForum(forum, subforum, promoted) && FImp.editModTimeLimit(forum, subforum, promoted, newUntilWhen);
        }

        //ofir
        public DateTime? getModTimeLimit(string member, int forum, int subforum)
        {
            return FImp.isUserModeratorOfSubForum(forum, subforum, member) ? FImp.getModTimeLimit(forum, subforum, member) : null;
        }

        //ofir
        public bool makeModOfSubforumWithTimeLimit(string promoted, int forum, int subforum, string promoter, DateTime untilWhen)
        {
            return MakeModOfSubforum(promoter, forum, subforum, promoted) && editModTimeLimit(promoted, forum, subforum, promoter, untilWhen);
        }

        [TestMethod]
        /* admin promotes member to mod in subforum */
        public void AdminPromotesAMemberToBeAModerator()
        {
            var fp = FImp.GetForumPolicy(_forum1Id);
            fp.MaxModeratorsNumber = Int32.MaxValue;
            FImp.EditForumPolicy(_forum1Id, fp);
            var beforeNumModerators = FImp.GetSubForumModerators(_forum1Id, _subForumId).Count;
            var beforeNumModeratorsOTHERFUNCTION = FImp.GetAllModerators(_forum1Id).Count;
            Assert.AreEqual(beforeNumModerators, beforeNumModeratorsOTHERFUNCTION);
            var isPromoted = MakeModOfSubforum(_adminsForum1[0], _forum1Id, _subForumId, _newUserToPromote);
            var afterNumModerators = FImp.GetSubForumModerators(_forum1Id, _subForumId).Count;
            Assert.IsTrue(isPromoted);
            Assert.AreEqual(beforeNumModerators + 1, afterNumModerators);
        }

        private bool MakeModOfSubforum(string promoter, int forum, int subForum, string promoted)
        {
            if (!FImp.IsUserRegisteredToForum(forum, promoted))
            {
                return false;
            }
            return FImp.IsUserAdminOfForum(forum, promoter) && "success".Equals(FImp.PromoteModerator(promoted, forum, subForum));
        }

        [TestMethod]
        /* admin from one forum promotes member from another forum to mod in one of his subforum */
        public void AdminPromotesAMemberToBeAModeratorButNotInHisForum()
        {
            var makeModOfSubforum = MakeModOfSubforum(_adminsForum1[0], _forum1Id, _subForumId, _userNameFromForum2);

            Assert.IsFalse(makeModOfSubforum);
        }

        [TestMethod]
        /* admin promotes member to admin */
        public void AdminPromotesAMemberToBeAnAdmin()
        {
            var isPromoted = FImp.PromoteToAdmin(_forum1Id, _newUserToPromote, _adminsForum1[0]);
            Assert.IsTrue(isPromoted);
        }

        [TestMethod]
        /* admin from one forum promotes member from another forum to admin in one of his subforum */
        public void AdminPromotesAMemberNotFromHisForumToBeAnAdminInHisForum()
        {
            var isPromoted = FImp.PromoteToAdmin(_forum2Id, _newUserToPromote, _adminsForum2[0]);
            Assert.IsFalse(isPromoted);
        }

        [TestMethod]
        /* admin promotes member to mod in subforum with time limit */
        public void AdminPromotesAMemberToBeAModeratorWithTimeLimit()
        {
            var isPromoted = makeModOfSubforumWithTimeLimit(_newUserToPromote, _forum1Id, _subForumId, _adminsForum1[0],
                DateTime.Now.AddMilliseconds(100));
            Assert.IsTrue(isPromoted);
        }

        [TestMethod]
        /* admin changes the time limit on for mod promotion */
        public void AdminChangesTheTimeLimitsOfOneOfHisModerators()
        {
            DateTime? oldModDuration;
            var isPromoted = makeModOfSubforumWithTimeLimit(_newUserToPromote, _forum1Id, _subForumId, _adminsForum1[0],
                DateTime.Now.AddMilliseconds(100));
            oldModDuration = FImp.getModTimeLimit(_forum1Id, _subForumId, _newUserToPromote);
            if (oldModDuration == null) { throw new Exception("testing API method: 'getModTimeLimit' not working correctly"); }
            DateTime newDuration;
            try
            {
                newDuration = oldModDuration.Value.AddDays(1);
            }
            catch (Exception)
            {
                newDuration = (DateTime)oldModDuration;
            }
            editModTimeLimit(_newUserToPromote, _forum1Id, _subForumId, _adminsForum1[0], newDuration);
            if (oldModDuration.Equals(DateTime.MaxValue))
            {
                Assert.AreEqual(oldModDuration.ToString(), getModTimeLimit(_newUserToPromote, _forum1Id, _subForumId).ToString());
            }
            else
            {
                Assert.AreNotEqual(oldModDuration.ToString(), getModTimeLimit(_newUserToPromote, _forum1Id, _subForumId).ToString());
            }
        }

    }
}
