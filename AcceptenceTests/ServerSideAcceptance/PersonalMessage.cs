﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    משתמש א' יכול לשלוח הודעה פרטית, שאינה חלק משרשור שאף תת-
    פורום, ישירות למשתמש כלשהו ב'. משתמש א' לא מכיר את כתובת המייל של משתמש ב', אלא
    רק את שם המשתמש שלו.
    */
    [TestClass]
    public class PersonalMessage : ServerSideAcceptance
    {
        private string _forum1;
        private int _forum1Id;
        private string _forum2;
        private int _forum2Id;
        private string _userNameFromForum11;
        private string _userNameFromForum12;
        private string _userNameFromForum2;
        private string _targetUsernameDoesNotExists;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _userNameFromForum11 = FImp.getLastUserName(_forum1Id); // an existing user in the from
            _userNameFromForum12 = _userNameFromForum11 + "c"; // we will make another one in case there is not
            RegisterOneMore();
            _forum2 = FImp.getSecondLastForumName();
            _forum2Id = FImp.GetForumIdByName(_forum2);
            _userNameFromForum2 = FImp.getLastUserName(_forum2Id);
        }

        private bool RegisterOneMore()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            FImp.Register(_forum1Id, _userNameFromForum12, "123456789", "aaa", "bbb", 1999, 7, 2,
                FImp.getUsersEmail(_forum1Id, _userNameFromForum11) + "l", 1);
            return FImp.VerifyUser(_forum1Id, _userNameFromForum12, FImp.GetVerificationCode(_forum1Id, _userNameFromForum12));
        }

        [TestMethod]
        /* member sending PM to member */
        public void SendPrivateMessageMemberToMember()
        {
            // add friend
            var beforeNumFriends = FImp.GetAllFriends(_forum1Id, _userNameFromForum11).Count;
            var isSentRequest = UImp.AddFreind(_userNameFromForum11, _userNameFromForum12, _forum1Id);
            Assert.IsTrue(isSentRequest);
            // approve friend request
            var isApproved = UImp.ConfirmFriendRequest(_userNameFromForum11, _userNameFromForum12, _forum1Id);
            Assert.IsTrue(isApproved);
            var afterNumFriends = FImp.GetAllFriends(_forum1Id, _userNameFromForum11).Count;
            var sendPrivateMessage = FImp.sendPrivateMessage("if you are reading this, you are wasting your time.", _forum1Id,
                _userNameFromForum11, _userNameFromForum12);
            var messages = UImp.GetIncomingMessages(_forum1Id, _userNameFromForum11, _userNameFromForum12);
            var lastMessage = messages.Last();
            Assert.IsTrue(sendPrivateMessage);
            Assert.AreEqual("if you are reading this, you are wasting your time.", lastMessage.content);
            Assert.AreEqual(beforeNumFriends + 1, afterNumFriends);
        }

        [TestMethod]
        /* member sending PM to member name that doesnt exist at all*/
        public void SendingPrivateMessageMemberToMemberThatDoesNotExist()
        {
            _targetUsernameDoesNotExists = _userNameFromForum2 + "c";
            var sendPrivateMessage = FImp.sendPrivateMessage("if you are reading this, you are wasting your time.", _forum2Id,
                _targetUsernameDoesNotExists, _userNameFromForum2);
            Assert.IsFalse(sendPrivateMessage);
        }

        [TestMethod]
        //member sending PM to an empty member name 
        public void SendingPrivateMessageMemberToEmptyMemberName()
        {
            var sendPrivateMessage = FImp.sendPrivateMessage("if you are reading this, you are wasting your time.", _forum2Id,
                string.Empty, _userNameFromForum2);
            Assert.IsFalse(sendPrivateMessage);
        }

        [TestMethod]
        /* member sending PM to member name that doesnt exist in the subforum he is member of, but exists in another subforum */
        public void SendingPrivateMessageToUserInOtherSubForum()
        {
            var sendPrivateMessage = FImp.sendPrivateMessage("if you are reading this, you are wasting your time.", _forum1Id,
                _userNameFromForum2, _userNameFromForum12);
            Assert.IsFalse(sendPrivateMessage);
        }

    }
}
