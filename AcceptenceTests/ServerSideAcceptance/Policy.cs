﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    קביעת שינוי מאפיינים אילוצים כללים מדיניות של פורום כגון 
    A. מספר מנחים מינימאלי לתת פורום - .
    B. מבנה סיסמא של חבר למשל לפחות שמונה תווים שמתוכם לפחות אחד הוא מספר ( , ).
    */

    [TestClass]
    public class Policy : ServerSideAcceptance
    {

        private string _forum1;
        private int _forum1Id;
        private string _lastUserName;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _lastUserName = FImp.getLastUserName(_forum1Id);
        }

        [TestMethod]
        /* change policy of forum - guest can post replies  */
        public void SetGuestCanPostReplies()
        {
            ForumPolicy fp = FImp.GetForumPolicy(_forum1Id);
            fp.GuestCanPost = true;
            FImp.EditForumPolicy(_forum1Id, fp);
            var isSet = FImp.CanGuestReplyToPost(_forum1Id);
            Assert.IsTrue(isSet);
        }

        [TestMethod]
        /* change policy of forum - guest can start a thread  */
        public void SetGuestCanStartAThread()
        {
            ForumPolicy fp = FImp.GetForumPolicy(_forum1Id);
            fp.GuestCanStartThread = true;
            FImp.EditForumPolicy(_forum1Id, fp);
            var isSet = FImp.CanGuestStartThread(_forum1Id);
            Assert.IsTrue(isSet);
        }

        [TestMethod]
        /* change policy of forum - change minimum password length  */
        public void ChangeMinimumPasswordLength()
        {
            ForumPolicy fp = FImp.GetForumPolicy(_forum1Id);
            fp.MinPasswordLetters = 100;
            FImp.EditForumPolicy(_forum1Id, fp);
            var mail = FImp.getUsersEmail(_forum1Id, FImp.getLastUserName(_forum1Id)) + "c";
            var isOkPassword = FImp.Register(_forum1Id, _lastUserName + "c", "bbb", "lalala", "kukuku3uuuuu3=33kuu",
                1990, 9, 5, mail, 1);
            Assert.IsFalse(isOkPassword.Equals(string.Empty));
        }

        [TestMethod]
        /* change policy of forum - change minimum user name length  */
        public void ChangeMinimumUserNameLength()
        {
            ForumPolicy fp = FImp.GetForumPolicy(_forum1Id);
            fp.MinUserNameLetters = 100;
            FImp.EditForumPolicy(_forum1Id, fp);
            var mail = FImp.getUsersEmail(_forum1Id, FImp.getLastUserName(_forum1Id)) + "c";
            var isOkUserName = FImp.Register(_forum1Id, _lastUserName + "c", "bbb", "lalala", "kukuku3uuuuu3=33kuu",
                1990, 9, 5, mail, 1);
            Assert.IsFalse(isOkUserName.Equals(string.Empty));
        }
    }
}
