﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    רישום (registration) . ( חבר לפורום יש להקפיד להתייחס למדיניות רישום של פורום כפי
    שניתן להגדיר בסיפור השימוש הקודם ).
    */
    [TestClass]
    public class Registration : ServerSideAcceptance
    {
        private string _forum1;
        private int _forum1Id;
        private string _lastUserName;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _lastUserName = FImp.getLastUserName(_forum1Id);
        }
        [TestMethod]
        /* register to forum without name */
        public void RegisterToForumWithoutName()
        {
            var isRegistered = FImp.Register(_forum1Id, string.Empty, "1234567", "aaa", "bbb", 1999, 2, 5,
                FImp.getUsersEmail(_forum1Id, _lastUserName) + "c", 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }

        [TestMethod]
        /* register to forum without password */
        public void RegisterToForumWithoutPassword()
        {
            var isRegistered = FImp.Register(_forum1Id, _lastUserName + "c", string.Empty, "aaa", "bbb", 1999, 2, 5,
                FImp.getUsersEmail(_forum1Id, _lastUserName) + "c", 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }

        [TestMethod]
        /* register to forum without email */
        public void RegisterToForumWithoutEmail()
        {
            var isRegistered = FImp.Register(_forum1Id, _lastUserName + "c", "1234567", "aaa", "bbb", 1999, 2, 5,
                            string.Empty, 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }

        [TestMethod]
        /* register to forum */
        public void RegisterToForum()
        {
            var email = FImp.getUsersEmail(_forum1Id, _lastUserName) + "c";
            var isRegistered = FImp.Register(_forum1Id, _lastUserName + "c", "1234567", "aaa", "bbb", 1999, 2, 5, email, 1);
            var details = UImp.GetUserDetails(_forum1Id, _lastUserName);
            var expected = new Tuple<string, string, string, string>("aaa", "bbb", _lastUserName + "c", email);
            Assert.IsTrue(isRegistered.Equals(string.Empty));
            Assert.AreEqual(expected, details);
        }

        [TestMethod]
        /* register to forum with name that already exists */
        public void RegisterToForumNameThatAlreadyExists()
        {
            var isRegistered = FImp.Register(_forum1Id, _lastUserName, "1234567", "aaa", "bbb", 1999, 2, 5,
                            FImp.getUsersEmail(_forum1Id, _lastUserName) + "c", 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }

        [TestMethod]
        /* register to forum with password that isnt approved by forum policy */
        public void RegisterToForumWithBadPasswordNotAccordingToPolicy()
        {
            ForumPolicy fp = new ForumPolicy();
            fp.MinPasswordLetters = 5;
            FImp.EditForumPolicy(_forum1Id, fp);
            var isRegistered = FImp.Register(_forum1Id, _lastUserName + "c", "17", "aaa", "bbb", 1999, 2, 5,
                                       FImp.getUsersEmail(_forum1Id, _lastUserName) + "c", 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }

        [TestMethod]
        /* register to forum with email that already exists */
        public void RegisterToForumWithEmailThatAlreadyExistsInside()
        {
            var isRegistered = FImp.Register(_forum1Id, _lastUserName + "c", "177777777", "aaa", "bbb", 1999, 2, 5,
                                       FImp.getUsersEmail(_forum1Id, _lastUserName), 1);
            Assert.IsFalse(isRegistered.Equals(string.Empty));
        }
    }
}
