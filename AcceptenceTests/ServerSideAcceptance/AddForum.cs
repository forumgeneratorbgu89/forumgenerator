﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    מנהל העל יכול ליצור פורום חדש על ידי פירוט מאפיינים קונקרטיים יש
    להקפיד להתייחס לדרישות הכלליות לגבי פורום שימו לב מנהל של פורום הוא חבר בפורום
    */
    [TestClass]
    public class AddForum : ServerSideAcceptance
    {

        [TestMethod]
        /* add forum when logged in as super-admin */
        public void AddForumAsSuperAdmin()
        {

            bool isLoggedIn = FImp.signInSuperAdmin("admin", "admin");
            if (isLoggedIn)
            {
                var beforeNumOfForums = FImp.NumOfForums();
                var lastForumName = FImp.getLastForumName();
                var isAdded = FImp.AddForum(lastForumName + "f", "admin", new ForumPolicy());
                var afterNumOfForums = FImp.NumOfForums();
                var testGetAllForums = FImp.GetAllForums().Count;
                Assert.AreNotEqual(isAdded, -1);
                Assert.AreEqual(beforeNumOfForums + 1, afterNumOfForums);
                Assert.AreEqual(afterNumOfForums, testGetAllForums);
            }
            Assert.IsTrue(isLoggedIn);
        }

        [TestMethod]
        /* add forum when logged in as super-admin with missing forum name */
        public void AddForumAsSuperAdminWithMissingForumName()
        {

            var isLoggedIn = FImp.signInSuperAdmin("admin", "admin");
            if (isLoggedIn)
            {
                var isAdded = FImp.AddForum(string.Empty, "admin", new ForumPolicy());
                Assert.AreEqual(-1, isAdded);
            }
            Assert.IsTrue(isLoggedIn);
        }

        [TestMethod]
        /* add forum when logged in as super-admin with existing forum name */
        public void AddForumAsSuperAdminWithExistingForumName()
        {

            bool isLoggedIn = FImp.signInSuperAdmin("admin", "admin");
            if (isLoggedIn)
            {
                var lastForumName = FImp.getLastForumName();
                var isAdded = FImp.AddForum(lastForumName, "admin", new ForumPolicy());
                Assert.AreEqual(-1, isAdded);
            }
            Assert.IsTrue(isLoggedIn);
        }

    }
}
