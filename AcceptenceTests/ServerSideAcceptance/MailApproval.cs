﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    חלק ממדיניות זיהוי )authentication( משתמש ב"פורום מאובטח": אורח
    הנרשם לפורום מספק כתובת דוא"ל, ולפני שהרישום מתבצע הוא צריך לאשר את הרישום אמצעות
    הודעה בדוא"ל.
    */
    [TestClass]
    public class MailApproval : ServerSideAcceptance
    {

        private string _lastForumName;
        private int _forumId;
        private string _lastUserName;
        private string _newUserName;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _lastForumName = FImp.getLastForumName();
            _forumId = FImp.GetForumIdByName(_lastForumName);
            _lastUserName = FImp.getLastUserName(_forumId);
            _newUserName = _lastUserName + "c";
        }

        [TestMethod]
        /* sign up and log in as member after email approval */
        public void SignUpAndLoginAsMemberAfterEmailApproval()
        {
            FImp.ResetToDefaultPolicy(_forumId);
            var isMemberAdded = FImp.Register(_forumId, _newUserName, "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(_forumId, _lastUserName) + "l", 1);
            Assert.IsTrue(isMemberAdded.Equals(string.Empty));
            FImp.VerifyUser(_forumId, _lastUserName + "c", FImp.GetVerificationCode(_forumId, _newUserName));
            var isApproved = UImp.IsUserApproved(_lastUserName, _forumId);
            Assert.IsTrue(isApproved);
            var isLoggedIn = FImp.Login(_forumId, _newUserName, "123456789");
            Assert.AreNotEqual(isLoggedIn.Result, null);
        }

        [TestMethod]
        /* sign up and log in as member without email approval */
        public void SignUpAndLogInAsMemberBeforeEmailApproval()
        {
            FImp.ResetToDefaultPolicy(_forumId);
            var isMemberAdded = FImp.Register(_forumId, _newUserName, "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(_forumId, _lastUserName) + "l", 1);
            Assert.IsTrue(isMemberAdded.Equals(string.Empty));
            var isLoggedIn = FImp.Login(_forumId, _newUserName, "123456789");
            Assert.AreEqual(isLoggedIn.Result, null);
        }

    }
}
