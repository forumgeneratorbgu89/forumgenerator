﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    צירת תת פורום על ידי מנהל הפורום גם כאן יש לשים לב למאפיינים של תת פורום
    */
    [TestClass]
    public class AddSubForum : ServerSideAcceptance
    {
        private string _lastForumName;
        private int _forumId;
        private string _lastSubForumName;
        private string _newSubForumName;
        private int _lastSubForumId;
        private List<string> _admins;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _lastForumName = FImp.getLastForumName();
            _forumId = FImp.GetForumIdByName(_lastForumName);
            _lastSubForumName = FImp.getLastSubForumName(_forumId);
            _newSubForumName = _lastSubForumName + "c";
            _lastSubForumId = FImp.GetSubForumIdByName(_lastSubForumName, _forumId);
            _admins = FImp.GetForumAdmins(_forumId);
        }

        [TestMethod]
        /* add subforum when not logged in as admin */
        public void AddSubForumNotAdmin()
        {
            var lastUserName = FImp.getLastUserName(_forumId);
            var isMemberAdded = FImp.Register(_forumId, lastUserName + "c", "123456789", "aaa", "bbb", 1999, 7, 2, FImp.getUsersEmail(_forumId, lastUserName) + "l", 1);
            if (isMemberAdded.Equals("success"))
            {

                // user must be activated - and he is in the forum and he is not an admin of this forum
                FImp.VerifyUser(_forumId, lastUserName + "c", FImp.GetVerificationCode(_forumId, lastUserName + "c"));
                var moderatorsList = new List<string> { lastUserName + "c" };
                //TODO: this test fails because we never test who is trying to add the subforum - need to verify it is an admin
                var isSubForumAdded = FImp.AddSubForum(_forumId, FImp.getLastSubForumName(_forumId), moderatorsList,
                    "some groovy subject");
                Assert.IsFalse(isSubForumAdded);

            }
            Assert.IsTrue(false);
        }

        [TestMethod]
        /* add subforum when logged in as admin */
        public void AddSubForumAsAdmin()
        {
            var moderatorsList = new List<string> { _admins[0] };
            var beforeSubForum = FImp.getAllSubForum(_forumId).Count;
            var isSubForumAdded = FImp.AddSubForum(_forumId, _lastForumName + "c", moderatorsList, "some subject");
            var afterSubForum = FImp.getAllSubForum(_forumId).Count;

            // first of all check if the subforum added
            Assert.IsTrue(isSubForumAdded);
            var actualNewSubForumId = FImp.GetSubForumIdByName(_lastForumName + "c", _forumId);

            // then enter the subforum
            var userInSubForum = FImp.UserEnterToSubForum(_forumId, actualNewSubForumId, _admins[0]);
            Assert.AreEqual(_admins[0], userInSubForum.UserName);

            // then check the id of this subforum
            Assert.IsTrue(actualNewSubForumId > _lastSubForumId, "Id cant be like that");
            Assert.AreEqual(beforeSubForum + 1, afterSubForum);

        }

        [TestMethod]
        /* add subforum when logged in as admin but missing subforum name */
        public void AddSubForumWithNoName()
        {
            var moderatorsList = new List<string> { _admins[0] };
            var isSubForumAdded = FImp.AddSubForum(_forumId, "", moderatorsList, "some subject");

            // check if the subforum added
            Assert.IsFalse(isSubForumAdded);
        }

    }
}
