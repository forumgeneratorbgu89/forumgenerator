﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    הפעלה ראשונית של – מערכת הפורומים. , בסיום פעולה זו מערכת הפורומים תוכל
    בפרט יהיה לה מנהל על בעל הרשאות מיוחדות כגון
    הרשאה ליצירת פורומים ומינוי מנהל פורום
    */
    [TestClass]
    public class Initialize : ServerSideAcceptance
    {
        [TestMethod]
        /* create brand new system and login as super-admin */
        public void InitSystemAndSuperAdmin()
        {
            bool isInitialized = FImp.signUpSuperAdmin("admin", "admin", "aaa", "bbb", DateTime.Now, "aaa@bbb.ccc");
            Assert.IsTrue(isInitialized);
            bool isSystemInitialized = FImp.IsSystemIniaitlized();
            Assert.IsTrue(isSystemInitialized);
        }
    }
}
