﻿using System;
using ForumGenerator;
using ForumGenerator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DomainEntity.Models.ForumManagement;

namespace AcceptenceTests.ServerSideAcceptance
{
    [TestClass]
    public class ServerSideAcceptance
    {

        public IForumService FImp;
        public IUserService UImp;

        [TestInitialize]
        public void InitTest()
        {
            //FImp = ForumServiceProxy.getInstance();
            //UImp = new UserServiceProxy();
            FImp = new ForumService();
            FImp.clearDatabase();
            UImp = new UserService();
            if (!FImp.IsSystemIniaitlized())
            {
                FImp.signUpSuperAdmin("admin", "admin", "aaa", "bbb", DateTime.Now, "aaa@bbb.ccc");
            }

            try {
                FImp.AddForum("rani el-kingo2", "admin", new ForumPolicy());
                FImp.AddSubForum(FImp.GetForumIdByName(FImp.getLastForumName()), "some cool subforum", 
                    new System.Collections.Generic.List<string>(), "bunnies.");
            }
            catch { }
        }

        [TestCleanup]
        public void CleanLastTest()
        {
            FImp.deleteForum("rani el-kingo2");
            FImp.clearDatabase();
            ForumControllerLocal.ClearInstance();
            //FImp.Destruct();
            UImp = null;
        }

    }
}