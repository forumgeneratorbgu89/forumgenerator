﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcceptenceTests.ServerSideAcceptance
{
    /*
    פרסום הודעה בתת פורום ראשונית או תגובה
    */
    [TestClass]
    public class Post : ServerSideAcceptance
    {
        private string _forum1;
        private int _forum1Id;
        private string _subForum;
        private int _subForumId;
        private List<string> _adminsForum1;
        private string _lastUserName;
        private string _lastThreadTitle;

        [TestInitialize]
        public new void InitTest()
        {
            base.InitTest();
            _forum1 = FImp.getLastForumName();
            _forum1Id = FImp.GetForumIdByName(_forum1);
            _subForum = FImp.getLastSubForumName(_forum1Id);
            _subForumId = FImp.GetSubForumIdByName(_subForum, _forum1Id);
            _adminsForum1 = FImp.GetForumAdmins(_forum1Id);
            _lastUserName = FImp.getLastUserName(_forum1Id);
            _lastThreadTitle = FImp.getLastThreadTitle(_forum1Id, _subForum) + "c";
            FImp.AddThread(_forum1Id, _subForum, _lastThreadTitle, "this is a little content", _adminsForum1[0]);
        }

        [TestMethod]
        /* guest starting thread when allowed in forum policy */
        public void GuestStartsPostWhenAllowed()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            var threadTitle = _lastThreadTitle + "c";
            const string text = "...and thats it :)";
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanStartThread = true;
            FImp.EditForumPolicy(_forum1Id, fp);
            var beforeNumThread = FImp.GetAllThreads(_forum1Id, _subForumId).Count;
            FImp.AddThread(_forum1Id, _subForum, threadTitle, text, "Guest");
            var afterNumThread = FImp.GetAllThreads(_forum1Id, _subForumId).Count;
            var ans = FImp.findThreadByTitle(_forum1Id, _subForumId, threadTitle);
            Assert.IsNotNull(ans);
            Assert.AreEqual(beforeNumThread + 1, afterNumThread);
        }

        [TestMethod]
        /* guest starting thread when not-allowed in forum policy */
        public void GuestStartsPostWhenNotAllowed()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            var threadTitle = _lastThreadTitle + "c";
            const string text = "...and thats it :)";
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = false;
            FImp.EditForumPolicy(_forum1Id, fp);
            FImp.AddThread(_forum1Id, _subForum, threadTitle, text, "Guest");
            var ans = FImp.findThreadByTitle(_forum1Id, _subForumId, threadTitle);
            Assert.IsNull(ans);
        }

        [TestMethod]
        /* member starting thread */
        public void MemberStartingThread()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            var threadTitle = _lastThreadTitle + "c";
            const string text = "...and thats it :)";
            FImp.AddThread(_forum1Id, _subForum, threadTitle, text, _lastUserName);
            var ans = FImp.findThreadByTitle(_forum1Id, _subForumId, threadTitle);
            var openningPost = FImp.GetOpenningPost(_forum1Id, _subForumId, ans.ID).Text;
            Assert.IsNotNull(ans);
            Assert.AreEqual(text, openningPost);
        }

        [TestMethod]
        /* guest replying to post when allowed by forum policy */
        public void GuestReplingToPostWhenAllowed()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            const string text = "...and thats it :)";
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = true;
            FImp.EditForumPolicy(_forum1Id, fp);
            var lastThreadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle);
            var postId = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> {0}, _lastUserName, text);
            var ans = FImp.findPost(_forum1Id, _subForumId, lastThreadId, postId);
            Assert.IsNull(ans);
        }

        [TestMethod]
        /* guest replying to post when not allowed by forum policy */
        public void GuestReplyingToPostWhenNotAllowed()
        {
            FImp.ResetToDefaultPolicy(_forum1Id);
            var threadTitle = _lastThreadTitle + "c";
            const string text = "...and thats it :)";
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = false;
            FImp.EditForumPolicy(_forum1Id, fp);
            var lastThreadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle);
            var postId = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _lastUserName, text); // when not posting return -1
            Assert.AreEqual(-1, postId);
        }

        [TestMethod]
        /* member replying to a thread started by another */
        public void MemberReplyingToThread()
        {
            var lastThreadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle);
            var beforeNumOfPosts = FImp.GetAllComments(_forum1Id, _subForumId, lastThreadId, new List<int> {0}).Count;
            var postId = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _lastUserName, "some reply here");
            var afterNumOfPosts = FImp.GetAllComments(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }).Count;
            Assert.IsTrue(postId.Count > 1);
            Assert.AreEqual(beforeNumOfPosts + 1, afterNumOfPosts);
        }

        [TestMethod]
        /* member replying to a post by another */
        public void MemberReplyingToPost()
        {
            var lastThreadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle);
            var postId1 = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _lastUserName, "some reply here");
            Assert.IsTrue(postId1.Count > 1);
            var postId2 = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _adminsForum1[0], "some reply here");
            Assert.IsTrue(postId2.Count > 1);
            var posts = FImp.GetCommentsForUser(_forum1Id, _adminsForum1[0], _lastUserName);
            MinimizedPost lastPosts = posts.Last();
            Assert.AreEqual("some reply here", lastPosts.Text);
        }

        [TestMethod]
        /* member replying to his own post */
        public void MemberReplyingHisOwnPost()
        {
            var lastThreadId = FImp.findThreadId(_forum1Id, _subForumId, _lastThreadTitle);
            var postId1 = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _lastUserName, "some reply here");
            Assert.IsTrue(postId1.Count > 1);
            var postId2 = FImp.replyToPost(_forum1Id, _subForumId, lastThreadId, new List<int> { 0 }, _lastUserName, "some reply here");
            Assert.IsTrue(postId2.Count > 1);
        }
    }
}
