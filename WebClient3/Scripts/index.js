
(function () {

    $('#live-chat header').on('click', function () {

        $('.chat').slideToggle(300, 'swing');
        $('.chat-message-counter').fadeToggle(300, 'swing');

    });

    $('.chat-close').on('click', function (e) {

        e.preventDefault();
        $('#live-chat').fadeOut(300);

    });

})();

$('input[type="submit"]').mousedown(function () {
  $(this).css('background', '#2ecc71');
});

$('input[type="submit"]').mouseup(function () {
  $(this).css('background', '#1abc9c');
});
