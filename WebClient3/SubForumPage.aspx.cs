﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebClient3
{
    public partial class SubForumPage : System.Web.UI.Page
    {
        Iservice service;
        MinimizedForum forum;
        MinimizedUserInSubForum member;
        MinimizedSubForum subForum;
        List<MinimizedThread> threads;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                service = WebApiService.getInstance();
                forum = (MinimizedForum)(Session[SessionStrings.FORUM]);
                member = (MinimizedUserInSubForum)(Session[SessionStrings.USER_IN_SUB_FORUM]);
                subForum = (MinimizedSubForum)(Session[SessionStrings.SUB_FORUM]);
                threads = service.GetAllThreads(forum.ForumID, subForum.ID);
                SubForumName.Text = subForum.Name;
                TableThreads.CssClass = "responstable";
                TableHeaderRow header = new TableHeaderRow();
                TableHeaderCell userName = new TableHeaderCell();
                userName.Text = "User name";
                userName.Width = 70;
                TableHeaderCell content = new TableHeaderCell();
                content.Text = "Content";
                TableHeaderCell date = new TableHeaderCell();
                date.Text = "Publish date";
                date.Width = 200;
                header.Cells.Add(userName);
                header.Cells.Add(content);
                header.Cells.Add(date);
                TableThreads.Rows.AddAt(0, header);
                foreach (MinimizedThread mt in threads)
                {
                    TableRow tRow = new TableRow();
                    tRow.Height = 4;
                    TableCell userCell = new TableCell();
                    userCell.Text = mt.UserNameWriter;
                    userCell.Width = 70;
                    TableCell contentCell = new TableCell();
                    Button bt = new Button();
                    bt.Text = mt.Title;
                    bt.Click += new EventHandler(Choose_Thread_Click);
                    contentCell.Controls.Add(bt);
                    contentCell.CssClass = "link";

                    TableCell dateCell = new TableCell();
                    dateCell.Text = mt.Date.ToString();
                    dateCell.Width = 200;
                    tRow.Cells.Add(userCell);
                    tRow.Cells.Add(contentCell);
                    tRow.Cells.Add(dateCell);
                    TableThreads.Rows.Add(tRow);
                }
            }
            catch (Exception)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void Choose_Thread_Click(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            MinimizedThread chosen = threads.Find(p => p.Title.Equals(clickedButton.Text));
            Session[SessionStrings.FORUM] = forum;
            Session[SessionStrings.SUB_FORUM] = subForum;
            Session[SessionStrings.USER_IN_SUB_FORUM] = member;
            Session[SessionStrings.THREAD] = chosen;
            Session[SessionStrings.POST] = service.GetOpenningPost(forum.ForumID, subForum.ID, chosen.ID);
            //Session[SessionStrings.POST] = new MinimizedPost( 0, "to fix opening post", chosen.UserNameWriter, chosen.Date);
            Session[SessionStrings.POST_ID] = new List<int>() { 0 };

            Response.Redirect("PostPage.aspx");
        }

        protected void New_Thread_Click(object sender, EventArgs e)
        {
            Session[SessionStrings.FORUM] = forum;
            Session[SessionStrings.SUB_FORUM] = subForum;
            Session[SessionStrings.USER_IN_SUB_FORUM] = member;

            Response.Redirect("NewThreadPage.aspx");
        }
    }
}