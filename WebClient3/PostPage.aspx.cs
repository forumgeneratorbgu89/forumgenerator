﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebClient3
{
    public partial class PostPage : System.Web.UI.Page
    {
        Iservice service;
        MinimizedForum forum;
        MinimizedUserInSubForum member;
        MinimizedSubForum subForum;
        MinimizedThread thread;
        MinimizedPost Post;
        List<int> postID;
        List<MinimizedPost> comments;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                service = WebApiService.getInstance();
                forum = (MinimizedForum)(Session[SessionStrings.FORUM]);
                member = (MinimizedUserInSubForum)(Session[SessionStrings.USER_IN_SUB_FORUM]);
                subForum = (MinimizedSubForum)(Session[SessionStrings.SUB_FORUM]);
                thread = (MinimizedThread)(Session[SessionStrings.THREAD]);
                Post = (MinimizedPost)(Session[SessionStrings.POST]);
                postID = (List<int>)(Session[SessionStrings.POST_ID]);

                comments = service.GetAllComments(forum.ForumID, subForum.ID, thread.ID, postID);
                comments = comments.OrderBy(o => o.Date).ToList();
                foreach (MinimizedPost mp in comments)
                {
                    System.Web.UI.HtmlControls.HtmlGenericControl NewDiv = new
                    System.Web.UI.HtmlControls.HtmlGenericControl();
                    //NewDiv.ID = "postAttribute";
                    NewDiv.InnerHtml = @"<span style='display:inline-block;font-weight: bold;color:Black;font-size:12pt;width:100%;text-align:left'>" + mp.userNameWriter + @"</span>
                                      <span style='display:inline-block;color:Black; font-size:12pt; width:100 %; text-align:left'>"+mp.Date.ToString()+"</span>";
                    TableRow tRow = new TableRow();
                    TableCell tCell = new TableCell();
                    Button bt = new Button();
                    bt.Text = mp.Text;
                    bt.Click += new EventHandler(Choose_Post_Click);
                    tCell.Controls.Add(NewDiv);
                    tCell.Controls.Add(bt);
                    tCell.CssClass = "link2";
                    tRow.Cells.Add(tCell);
                    TablePosts.Rows.Add(tRow);
                }

                ThreadTitle.Text = thread.Title;
                Message.Text = Post.Text;
                PostUserName.Text = Post.userNameWriter;
                PostDate.Text = Post.Date.ToString();
            }
            catch (Exception)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void Choose_Post_Click(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            MinimizedPost chosen = comments.Find(p => p.Text.Equals(clickedButton.Text));

            Session[SessionStrings.FORUM] = forum;
            Session[SessionStrings.SUB_FORUM] = subForum;
            Session[SessionStrings.USER_IN_SUB_FORUM] = member;
            Session[SessionStrings.THREAD] = thread;
            Session[SessionStrings.POST] = chosen;
            List<int> postIDComment = postID;
            postIDComment.Add(chosen.PostId);
            Session[SessionStrings.POST_ID] = postIDComment;

            Response.Redirect("PostPage.aspx");
        }

        protected void Add_Comment_Click(object sender, EventArgs e)
        {
            addComent.Visible = !addComent.Visible;
        }

        private bool validData()
        {
            if(String.IsNullOrEmpty(comment.Text))
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please insert comment";
                return false;
            }
            return true;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (validData())
            {
                if (service.replyToPost(forum.ForumID, subForum.ID, thread.ID, postID, member.UserName, comment.Text) == null)
                {
                    divErrorMessage.Visible = true;
                    divErrorMessage.InnerText = "We can't publish your comment now";
                    return;
                }

                Response.Redirect("PostPage.aspx");
            }

        }
    }
}