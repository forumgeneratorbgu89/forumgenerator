﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebClient3
{
    public partial class _Default : Page
    {
        List<MinimizedForum> mForums;
        Iservice service;

        protected void Page_Load(object sender, EventArgs e)
        {
            service = WebApiService.getInstance();
            mForums = service.GetAllForums();
            var forums = mForums.Select(p => p.ForumName).ToList();
            DropDownList1.DataSource = forums;
            DropDownList1.DataBind();
            divErrorMessage.Visible = false;
        }

        protected void login_click(object sender, EventArgs e)
        {
            if (!ValidData())
                return;

            string selectedForum = DropDownList1.SelectedValue;
            MinimizedForum forum = mForums.Find(p => p.ForumName.Equals(selectedForum));
            int forumID = forum.ForumID;
            PrimitiveResult primitiveResult = service.Login(forumID, userName.Text, password.Text);
            if (String.IsNullOrEmpty(primitiveResult.ErrorMessageg))
            {
                Session[SessionStrings.FORUM] = forum;
                Session[SessionStrings.USER_IN_FORUM] = primitiveResult.Result;
                Session[SessionStrings.IsUserLogIn] = true;
                Response.Redirect("ForumPage.aspx");
            }
            else
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = primitiveResult.ErrorMessageg;
            }
        }

        private bool ValidData()
        {
            if (DropDownList1.SelectedValue == null)
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please select forum";
                return false;
            }
            if (String.IsNullOrEmpty(userName.Text))
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please insert user name";
                return false;
            }
            if (string.IsNullOrEmpty(password.Text))
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please insert password";
                return false;
            }
            return true;
        }
    }
}