﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebClient3
{
    public partial class ForumPage : System.Web.UI.Page
    {
        Iservice service;
        MinimizedForum forum;
        MinimizedUserInForum member;
        List<MinimizedSubForum> subForums;
        MinimizedSubForum chosen = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            service = WebApiService.getInstance();
            forum = (MinimizedForum)(Session[SessionStrings.FORUM]);
            member = (MinimizedUserInForum)(Session[SessionStrings.USER_IN_FORUM]);
            subForums = service.getAllSubForum(forum.ForumID);
            ForumName.Text = forum.ForumName;
            divErrorMessage.Visible = true;
            lblmessage.Text = "Write down your session password for further logins : " + member.SessionPassword;
            if (!IsPostBack)
            {
                DropDownListSubForum.DataSource = subForums.Select(p => p.Name);
                DropDownListSubForum.DataBind();
            }
            HelloMessage.Text = member.UserName + " welcome back and enjoy!";
            DropDownListSubForum_SelectedIndexChanged(null, null);
            divErrorMessage.Visible = false;
        }
        
        protected void logout_click(object sender, EventArgs e)
        {
            service.LogOut(forum.ForumName, member.UserName);
            Response.Redirect("Default.aspx");
        }


        protected void getin_click(object sender, EventArgs e)
        {
            if(chosen == null)
            {
                divErrorMessage.Visible = true;
                lblmessage.Text = "Please choose sub forum";
            }
            Session[SessionStrings.FORUM] = forum;
            Session[SessionStrings.SUB_FORUM] = chosen;
            Session[SessionStrings.USER_IN_SUB_FORUM] = service.UserEnterToSubForum(forum.ForumID, chosen.ID, member.UserName);
            Response.Redirect("SubForumPage.aspx");
        }

        protected void DropDownListSubForum_SelectedIndexChanged(object sender, EventArgs e)
        {
            chosen = subForums.Find(p => p.Name.Equals(DropDownListSubForum.SelectedValue));
            subForumSubject.Text = chosen.Subject;
        }
    }
}