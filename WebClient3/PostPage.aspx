﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PostPage.aspx.cs" Inherits="WebClient3.PostPage" %>

<asp:Content ID="PostPage" ContentPlaceHolderID="MainContent" runat="server">
    <center>    
        <div class="post" style="display: block;">
            <div class="formholder">
                <div class="randompad">
                    <fieldset>
                        <asp:Label id="ThreadTitle" runat="server"  Width="100%" class="green" style="font-size:20px; font-weight:bold" type="text" />
                        <asp:Label id="PostUserName" ForeColor="Black" Font-Size="12" Width="100%" style="font-weight: bold;text-align:left" runat="server" />
                        <asp:Label id="PostDate" ForeColor="Black" Font-Size="12" Width="100%" runat="server" style="text-align:left" />
                        <asp:Label id="Message" runat="server"  Width="100%"  class="green" style="font-size:16px; font-weight:bold" type="text" />
                        <asp:Table id="TablePosts" runat="server"></asp:Table>
                        <asp:Button runat="server" OnClick="Add_Comment_Click" style="background-color:#2c3e50" Text="Add comment"/>
                        <div id="addComent" runat="server" visible="false">
                                <asp:TextBox ID="comment" Height="200"  runat="server" TextMode="MultiLine" style="resize:none"  type="text"/>
                                <asp:Button runat="server" OnClick="Submit_Click" style="background-color:#2c3e50" Text="Submit"/>
                        </div>
                        <div class="custompopup" id="divErrorMessage" runat="server" visible="false">
                        <p>
                        <asp:Label ID="lblmessage" runat="server"></asp:Label>
                        </p>
                        </div >
                    </fieldset>
                 </div>
            </div>
        </div>
    </center>
</asp:Content>
