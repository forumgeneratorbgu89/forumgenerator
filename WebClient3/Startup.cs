﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebClient3.Startup))]
namespace WebClient3
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
