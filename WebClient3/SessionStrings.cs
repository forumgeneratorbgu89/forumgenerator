﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebClient3
{
    public static class SessionStrings
    {

        public static string FORUM = "Forum";
        public static string USER_IN_FORUM = "UserInForum";
        public static string SUB_FORUM = "SubForum";
        public static string USER_IN_SUB_FORUM = "UserInSubForum";
        public static string THREAD = "Thread";
        public static string POST = "Post";
        public static string POST_ID = "PostID";

        public static string IsUserLogIn = "IsUserLogIn";
    }
}