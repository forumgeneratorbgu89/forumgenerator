﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebClient3
{
    public partial class NewThreadPage : System.Web.UI.Page
    {
        Iservice service;
        MinimizedForum forum;
        MinimizedUserInSubForum member;
        MinimizedSubForum subForum;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                    service = WebApiService.getInstance();
                    forum = (MinimizedForum)(Session[SessionStrings.FORUM]);
                    member = (MinimizedUserInSubForum)(Session[SessionStrings.USER_IN_SUB_FORUM]);
                    subForum = (MinimizedSubForum)(Session[SessionStrings.SUB_FORUM]);
                    if(forum==null || member==null || subForum==null)
                        Response.Redirect("Default.aspx");

            }
            catch (Exception)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (validData()) {
                bool result = service.AddThread(forum.ForumID, subForum.Name, TitleInput.Text, ContentInput.Text, member.UserName);
                if (!result)
                {
                    divErrorMessage.Visible = true;
                    divErrorMessage.InnerText = "We can't publish your thread";
                }
                else
                {
                    Response.Redirect("SubForumPage.aspx");
                }
            }

        }

        private bool validData()
        {
            if (String.IsNullOrEmpty(TitleInput.Text))
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please insert title";
                return false;
            }
            if (String.IsNullOrEmpty(ContentInput.Text))
            {
                divErrorMessage.Visible = true;
                divErrorMessage.InnerText = "Please insert Content";
                return false;
            }
            return true;
        }
    }
}