﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewThreadPage.aspx.cs" Inherits="WebClient3.NewThreadPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <center>    
                    <div class="login" style="display: block;">
                        <div class="formholder">
                            <div class="randompad">
                                <fieldset>
                                    <label >Title</label>
                                    <asp:TextBox runat="server" ID="TitleInput" type="text"/>
                                    <label >Content</label>
                                    <asp:TextBox Height="200"  runat="server" TextMode="MultiLine" style="resize:none"  ID="ContentInput" type="text"/>
                                    <asp:Button runat="server" OnClick="Submit_Click" Text="Submit"/>
                                    <div class="custompopup" id="divErrorMessage" runat="server" visible="false">
                                        <p>
                                        <asp:Label ID="lblmessage" runat="server"></asp:Label>
                                        </p>
                                    </div>

                                </fieldset>
                            </div>
                        </div>
                    </div>
    </center>
</asp:Content>
