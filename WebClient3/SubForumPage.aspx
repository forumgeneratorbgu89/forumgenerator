﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubForumPage.aspx.cs" Inherits="WebClient3.SubForumPage" %>

<asp:Content ID="SubForumPage" ContentPlaceHolderID="MainContent" runat="server">

    <center>    
        <div class="subforum" style="display: block;">
            <div class="formholder" style="width:700px;">
                <div class="randompad">
                    <fieldset>
                        <label>Sub Forum Name</label>
                        <asp:Label runat="server" ID="SubForumName" type="text" />
                        <label>Choose thread</label>
                        <asp:Table id="TableThreads" runat="server"></asp:Table>
                        <asp:Button runat="server" OnClick="New_Thread_Click" Text="Create new thread" />
                    </fieldset>
                 </div>
            </div>
        </div>
    </center>

</asp:Content>
