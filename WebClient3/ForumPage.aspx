﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ForumPage.aspx.cs" Inherits="WebClient3.ForumPage" %>

<asp:Content ID="ForumPageContent" ContentPlaceHolderID="MainContent" runat="server">
    <center>    
        <div class="forum" style="display: block;">
            <div class="formholder">
                <div class="randompad">
                    <fieldset>
                        <label>Forum Name</label>
                        <asp:Label runat="server" ID="ForumName" type="text" />
                        <label>Hello</label>
                        <asp:Label runat="server" ID="HelloMessage" type="text" />
                        <label>Choose sub forum</label>
                        <asp:DropDownList id="DropDownListSubForum" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListSubForum_SelectedIndexChanged"></asp:DropDownList>
                        <label >Subject</label>
                        <asp:Label runat="server" ID="subForumSubject" type="text" />
                        <asp:Button runat="server" OnClick="getin_click" Text="Get in" />
                        <div class="custompopup" id="divErrorMessage" runat="server" />
                        <p>
                        <asp:Label ID="lblmessage" runat="server"></asp:Label>
                        </p>
                    </fieldset>
                 </div>
            </div>
        </div>
    </center>
</asp:Content>