﻿using System;
using System.Collections.Generic;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DomainEntity.MinimizedModels;

namespace DomainEntityTests.Models.UserManagement
{
    /// <summary>
    /// Summary description for UserControllerLocalTests
    /// </summary>
    [TestClass]
    public class UserControllerLocalTests
    {
        private IuserController UserController;
        private Forum forum;
        private Member avichay;
        private Member maor;
        protected IforumController fc;
        protected string forumName1 = "Rani El-Kingo";
        protected string forumName2 = "Cat recipes";
        protected string superAdminName = "rani";
        protected string superAdminPassword = "6fre8h3";

        [TestInitialize]
        public void TestInit()
        {
            fc = ForumControllerLocal.GetInstance();
            fc.ClearDatabase();
            fc.initSuperAdmin(superAdminName, superAdminPassword);
            UserController = UserControllerLocal.GetInstance();
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            forum = fc.GetForum(fc.GetForumIdByName(forumName1));
        }

        [TestCleanup]
        public void TestClean()
        {
            fc.ClearDatabase();
            UserController = null;
            fc = null;
        }

        private void MessageSending()
        {
            fc.sendPrivateMessage("hey hey hey", forum.ID, avichay.username, maor.username);
            fc.sendPrivateMessage("hey hey hey 2", forum.ID, avichay.username, maor.username);
            fc.sendPrivateMessage("hey hey hey 3", forum.ID, maor.username, avichay.username);
        }

        [TestMethod]
        public void GetIncomingMessages_Success()
        {
            string avichayName = "avichay13";
            string maorName = "maorbenrey";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            MessageSending();
            List<MinimizedMessage> avichayIncoming = UserController.GetIncomingMessages(forum.ID, avichay.username, maor.username);
            List<MinimizedMessage> maorIncoming = UserController.GetIncomingMessages(forum.ID, maor.username, avichay.username);
            Assert.AreEqual(4, avichayIncoming.Count);
            Assert.AreEqual(3, maorIncoming.Count);
        }

        [TestMethod]
        public void GetIncomingMessages_Fail_Bad_User_Name()
        {
            string avichayName = "avichay132";
            string maorName = "maorbenrey2";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            MessageSending();
            List<MinimizedMessage> avichayIncoming = UserController.GetIncomingMessages(forum.ID, avichay.username, maor.username + "problem4");
            Assert.AreEqual(0, avichayIncoming.Count);
        }

        [TestMethod]
        public void GetIncomingMessages_Fail_Bad_Forum_Id()
        {
            string avichayName = "avichay133";
            string maorName = "maorbenrey3";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            MessageSending();
            List<MinimizedMessage> avichayIncoming = UserController.GetIncomingMessages(forum.ID + 1, avichay.username, maor.username);
            Assert.IsNull(avichayIncoming);
        }

        [TestMethod]
        public void GetUserDetails_Success()
        {
            string avichayName = "avichay134";
            string maorName = "maorbenrey4";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            Tuple<string, string, string, string> details = UserController.GetUserDetails(forum.ID, maor.username);
            Assert.AreEqual(new Tuple<string, string, string, string>("maorbenrey", "Ben Rey", maorName, "maorbrem@gmail.com"), details);
        }

        [TestMethod]
        public void GetUserDetails_Fail_Bad_User_Name()
        {
            string avichayName = "avichay135";
            string maorName = "maorbenrey5";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            Tuple<string, string, string, string> details = UserController.GetUserDetails(forum.ID, maor.username + "problem5");
            Assert.IsNull(details);
        }

        [TestMethod]
        public void GetUserDetails_Fail_Bad_Forum_Id()
        {
            string avichayName = "avichay136";
            string maorName = "maorbenrey6";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            Tuple<string, string, string, string> details = UserController.GetUserDetails(forum.ID + 1, maor.username);
            Assert.IsNull(details);
        }

        [TestMethod]
        public void AddFreind_Success()
        {
            fc.AddForum(forumName1 + 3, superAdminName, new ForumPolicy());
            forum = fc.GetForum(fc.GetForumIdByName(forumName1 + 3));
            string avichayName = "avichay137";
            string maorName = "maorbenrey7";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username, forum.ID);
            Assert.IsTrue(isAdded);
        }

        [TestMethod]
        public void AddFreind_Fail_Bad_User_Name()
        {
            string avichayName = "avichay138";
            string maorName = "maorbenrey8";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username + "problem6", forum.ID);
            Assert.IsFalse(isAdded);
        }

        [TestMethod]
        public void AddFreind_Fail_Bad_Forum_Id()
        {
            string avichayName = "avichay139";
            string maorName = "maorbenrey9";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username, forum.ID + 1);
            Assert.IsFalse(isAdded);
        }

        [TestMethod]
        public void ConfirmFriendRequest_Success()
        {
            fc.AddForum(forumName1 + 4, superAdminName, new ForumPolicy());
            forum = fc.GetForum(fc.GetForumIdByName(forumName1 + 4));
            string avichayName = "avichay1310";
            string maorName = "maorbenrey10";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username, forum.ID);
            bool isConfirmed = UserController.ConfirmFriendRequest(avichay.username, maor.username, forum.ID);
            Assert.IsTrue(isAdded);
            Assert.IsTrue(isConfirmed);
            Assert.AreEqual(fc.GetAllFriends(forum.ID, maor.username).Count, fc.GetAllFriends(forum.ID, avichay.username).Count);
        }

        [TestMethod]
        public void ConfirmFriendRequestd_Fail_Bad_User_Name()
        {
            fc.AddForum(forumName1 + 6, superAdminName, new ForumPolicy());
            forum = fc.GetForum(fc.GetForumIdByName(forumName1 + 6));
            string avichayName = "avichay1311";
            string maorName = "maorbenrey11";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username, forum.ID);
            bool isConfirmed = UserController.ConfirmFriendRequest(avichay.username, maor.username + "problem2", forum.ID);
            Assert.IsTrue(isAdded);
            Assert.IsFalse(isConfirmed);
            Assert.AreEqual(fc.GetAllFriends(forum.ID, maor.username).Count, 0);
        }

        [TestMethod]
        public void ConfirmFriendRequest_Fail_Bad_Forum_Id()
        {
            fc.AddForum(forumName1 + 5, superAdminName, new ForumPolicy());
            forum = fc.GetForum(fc.GetForumIdByName(forumName1 + 5));
            string avichayName = "avichay1312";
            string maorName = "maorbenrey12";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            bool isAdded = UserController.AddFreind(avichay.username, maor.username, forum.ID);
            bool isConfirmed = UserController.ConfirmFriendRequest(avichay.username, maor.username, forum.ID + 1);
            Assert.IsTrue(isAdded);
            Assert.IsFalse(isConfirmed);
            Assert.AreEqual(fc.GetAllFriends(forum.ID, maor.username).Count, 0);
        }

        [TestMethod]
        public void IsUserApproved_Success()
        {
            string avichayName = "avichay1313";
            string maorName = "maorbenrey13";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            string username = "navara";
            bool isRegistered = fc.Register(forum.ID, username, "12345", "Ofir", "Navara", 1989, 1, 16, "navara@gmail.com", 1);
            string code = fc.GetVerificationCode(forum.ID, username);
            bool isVerified = fc.VerifyUser(forum.ID, username, code);
            Assert.IsTrue(isRegistered);
            Assert.IsTrue(isVerified);
            Assert.AreEqual(isVerified, UserController.IsUserApproved(username, forum.ID));
        }

        [TestMethod]
        public void IsUserApproved_Fail_Bad_User_Name()
        {
            string avichayName = "avichay1314";
            string maorName = "maorbenrey14";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            string username = "tomnoah";
            bool isRegistered = fc.Register(forum.ID, username, "12345", "Tom", "Noah", 1989, 1, 16, "tom@gmail.com", 1);
            string code = fc.GetVerificationCode(forum.ID, username);
            bool isVerified = fc.VerifyUser(forum.ID, username + "problem3", code);
            Assert.IsTrue(isRegistered);
            Assert.IsFalse(isVerified);
            Assert.AreEqual(isVerified, UserController.IsUserApproved(username, forum.ID));
        }

        [TestMethod]
        public void IsUserApproved_Fail_Bad_Forum_Id()
        {
            string avichayName = "avichay1315";
            string maorName = "maorbenrey15";
            fc.Register(forum.ID, avichayName, "12345", "Avichay", "Attlan", 1989, 1, 8, "avichay13@gmail.com", 1);
            fc.Register(forum.ID, maorName, "12345", "maorbenrey", "Ben Rey", 1989, 1, 8, "maorbrem@gmail.com", 1);
            fc.VerifyUser(forum.ID, avichayName, fc.GetVerificationCode(forum.ID, avichayName));
            fc.VerifyUser(forum.ID, maorName, fc.GetVerificationCode(forum.ID, maorName));
            avichay = fc.GetMember(avichayName, forum.ID);
            maor = fc.GetMember(maorName, forum.ID);

            string username = "ranizinger";
            bool isRegistered = fc.Register(forum.ID, username, "12345", "Rani", "Zinger", 1989, 1, 16, "rani@gmail.com", 1);
            string code = fc.GetVerificationCode(forum.ID, username);
            bool isVerified = fc.VerifyUser(forum.ID + 1, username, code);
            Assert.IsTrue(isRegistered);
            Assert.IsFalse(isVerified);
            Assert.AreEqual(isVerified, UserController.IsUserApproved(username, forum.ID));
        }
    }
}
