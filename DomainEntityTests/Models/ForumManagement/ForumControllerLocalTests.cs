﻿using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace DomainEntityTests.Models.ForumManagement
{
    [TestClass()]
    public class ForumControllerLocalTests
    {
        protected IforumController fc;
        protected const string forumName1 = "Rani El-Kingo";
        protected const string forumName2 = "Cat recipes";
        protected const string forumName3 = "Your Mama";
        protected const string forumName4 = "The Big Scary Penguin";
        protected const string superAdminName = "rani";
        protected const string superAdminPassword = "6fre8h3";
        protected const string subForumName1 = "Cannibalism for Dummies";
        protected const string subForumName2 = "The Wonders of the Armpit";
        protected const string threadHeader1 = "HUNGER FOR HUMANS";
        protected const string postContent1 = "I am hungry. anyone got a manwhich?";
        protected const string userName1 = "theFirst";
        protected const string userPassword1 = "g789fdy";
        protected Member member = null;
        protected int forumId = -1;
        protected int subforumId = -1;
        protected int threadId = -1;

        private void raniBigInit()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, userName1, userPassword1, "some", "guy", 1900, 1, 1, "asc@fsd.vd", 1);
            fc.VerifyUser(forumId, userName1, fc.GetMember(userName1, forumId).Code);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            member = fc.GetMember(userName1, forumId);
            threadId = fc.AddThread(forumId, subForumName1, threadHeader1, postContent1, member);
        }        

        [TestInitialize]
        public void testInit()
        {
            ForumControllerLocal.ClearInstance();
            fc = ForumControllerLocal.GetInstance();
            fc.ClearDatabase();
            fc.initSuperAdmin(superAdminName, superAdminPassword);
        }

        [TestCleanup]
        public void testCleanup()
        {
            fc.ClearDatabase();
            fc = null;
            member = null;
            forumId = -1;
            subforumId = -1;
            threadId = -1;
        }

        [TestMethod()]
        public void GetInstanceTest()
        {
            Assert.IsNotNull(ForumControllerLocal.GetInstance());
        }

        [TestMethod()]
        public void GetCommentsForUserTest_noPosts()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.AreEqual(0, fc.GetCommentsForUser(fc.GetForumIdByName(forumName1), superAdminName, superAdminName).Count);
        }

        [TestMethod()]
        public void GetCommentsForUserTest_PostsInSameSubforum()
        {
            raniBigInit();
            Thread t = fc.GetThreadByTitle(forumId, subforumId, threadHeader1);
            List<int> post1 = fc.replyToPost(forumId, subforumId, t.id, new List<int> { t.OpenningPost.ID }, userName1, "a comment");
            List<int> post2 = fc.replyToPost(forumId, subforumId, t.id, post1, superAdminName, "a comment 2");
            List<int> post3 = fc.replyToPost(forumId, subforumId, t.id, post2, userName1, "a comment 3");
            List<int> post4 = fc.replyToPost(forumId, subforumId, t.id, post2, userName1, "a comment 4");
            List<int> post5 = fc.replyToPost(forumId, subforumId, t.id, post3, userName1, "a comment 4");
            // needs to be 5 comments : one is the initial test and 4 comments
            Assert.AreEqual(5, fc.GetCommentsForUser(fc.GetForumIdByName(forumName1), superAdminName, userName1).Count);
        }

        //[TestMethod()]
        //public void GetCommentsForUserTest_PostsInDifferentSubforums()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void GetAllCommentsTest()
        {
            raniBigInit();
            Thread t = fc.GetThreadByTitle(forumId, subforumId, threadHeader1);
            List<int> post1 = fc.replyToPost(forumId, subforumId, t.id, new List<int> { t.OpenningPost.ID }, userName1, "a comment");
            List<int> post2 = fc.replyToPost(forumId, subforumId, t.id, post1, superAdminName, "a comment 2");
            List<int> post3 = fc.replyToPost(forumId, subforumId, t.id, post1, userName1, "a comment 3");
            List<int> post4 = fc.replyToPost(forumId, subforumId, t.id, post2, userName1, "a comment 4");
            List<int> post5 = fc.replyToPost(forumId, subforumId, t.id, post3, userName1, "a comment 4");
            // 5 comments (with sub-comments) to the original post
            List<MinimizedPost> comments = fc.GetAllComments(forumId, subforumId, threadId, new List<int> { t.OpenningPost.ID });
            Assert.AreEqual(1, comments.Count);
            // 3 comments (with sub-comments) to the post2
            comments = fc.GetAllComments(forumId, subforumId, threadId, post2);
            Assert.AreEqual(1, comments.Count);
        }

        [TestMethod()]
        public void GetOpenningPostTest()
        {
            raniBigInit();
            Assert.AreEqual(fc.GetOpenningPost(forumId, subforumId, threadId).Text, postContent1);
        }

        [TestMethod()]
        public void GetAllModeratorsTest_NoMods()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, userName1, userPassword1, "some", "guy", 1900, 1, 1, "asc@fsd.vd", 1);
            fc.VerifyUser(forumId, userName1, fc.GetMember(userName1, forumId).Code);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string>(), subForumName1);
            Assert.AreEqual(fc.GetAllModerators(forumId).Count, 0);
        }

        [TestMethod()]
        public void GetAllModeratorsTest_ModsInOneSubforum()
        {
            raniBigInit();
            fc.AddModerator(member.username, forumId, subforumId);
            Assert.AreEqual(fc.GetAllModerators(forumId).Count, 2);
        }

        [TestMethod()]
        public void GetAllModeratorsTest_ModsInTwoSubforum()
        {
            raniBigInit();
            fc.AddModerator(member.username, forumId, subforumId);
            subforumId = fc.AddSubForum(forumId, subForumName2, new List<string> { superAdminName }, subForumName2);
            Assert.AreEqual(fc.GetAllModerators(forumId).Count, 3); // TODO: currently returns same member many times if he is modding many subforums!
        }

        [TestMethod()]
        public void deleteForumTest()
        {
            fc.AddForum(forumName1, superAdminName);
            fc.deleteForum(forumName1);
            Assert.AreSame(null, fc.GetForum(forumName1));
        }

        [TestMethod()]
        public void deleteMemberTest_MemberExists()
        {
            bool didItWork = true;
            raniBigInit();
            try { fc.deleteMember(forumId, member.username); }
            catch { didItWork = false; }
            Assert.IsTrue(didItWork);
        }

        [TestMethod()]
        public void deleteMemberTest_MemberDoesntExists()
        {
            bool didItWork = false;
            raniBigInit();
            try { fc.deleteMember(forumId, "dssadfrd"); }
            catch { didItWork = true; }
            Assert.IsTrue(didItWork);
        }

        [TestMethod()]
        public void deleteSubForumTest_SubforumExists()
        {
            bool didItWork = true;
            raniBigInit();
            try { fc.deleteSubForum(forumName1, subForumName1); }
            catch { didItWork = false; }
            Assert.IsTrue(didItWork);
        }

        [TestMethod()]
        public void deleteSubForumTest_SubforumDoesntExists()
        {
            bool didItWork = false;
            raniBigInit();
            try { fc.deleteSubForum(forumName1, "Fsdfgds"); }
            catch { didItWork = true; }
            Assert.IsTrue(didItWork);
        }

        [TestMethod()]
        public void getAllSubForumTest_NoSubforums()
        {
            int forumId = fc.AddForum(forumName1, superAdminName);
            Assert.AreEqual(0, fc.getAllSubForum(forumId).Count);
        }

        [TestMethod()]
        public void getAllSubForumTest_TwoSubforums()
        {
            raniBigInit();
            fc.AddSubForum(forumId, subForumName2, new List<string> { superAdminName }, subForumName2);
            Assert.AreEqual(2, fc.getAllSubForum(forumId).Count);
        }

        [TestMethod()]
        public void NumOfForumsTest_NoForums()
        {
            Assert.AreEqual(fc.NumOfForums(), 0);
        }

        [TestMethod()]
        public void NumOfForumsTest_TwoForums()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            fc.AddForum(forumName2, superAdminName, new ForumPolicy());
            Assert.AreEqual(fc.NumOfForums(), 2);
        }

        [TestMethod()]
        public void NumOfForumsTest_TwoDuplicateForums()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.AreEqual(fc.NumOfForums(), 1);
        }

        [TestMethod()]
        public void AddForumTest_OneForum()
        {
            Assert.IsTrue(fc.AddForum(forumName1, superAdminName, new ForumPolicy()) != -1);
        }

        [TestMethod()]
        public void AddForumTest_DuplicateForum()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.IsTrue(fc.AddForum(forumName1, superAdminName, new ForumPolicy()) == -1);
        }

        [TestMethod()]
        public void AddForumTest_NotAdmin()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.IsTrue(fc.AddForum(forumName1, "IAmNotASuperAdmin", new ForumPolicy()) == -1);
        }

        [TestMethod()]
        public void AddSubForumTest_Success()
        {
            fc.AddForum(forumName1, superAdminName);
            forumId = fc.GetForumIdByName(forumName1);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            Assert.AreNotEqual(-1, subforumId);
        }

        [TestMethod()]
        public void AddSubForumTest_DuplicateSubforum()
        {
            fc.AddForum(forumName1, superAdminName);
            forumId = fc.GetForumIdByName(forumName1);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            Assert.AreEqual(-1, subforumId);
        }

        [TestMethod()]
        public void AddThreadTest_Success()
        {
            raniBigInit();
            Assert.AreNotEqual(-1, threadId);
        }

        [TestMethod()]
        public void AddThreadTest_Fail()
        {
            ForumPolicy policy = new ForumPolicy();
            policy.GuestCanStartThread = false;
            fc.AddForum(forumName1, superAdminName, policy);
            forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, userName1, userPassword1, "some", "guy", 1900, 1, 1, "asc@fsd.vd", 1);
            fc.VerifyUser(forumId, userName1, fc.GetMember(userName1, forumId).Code);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            threadId = fc.AddThread(forumId, subForumName1, threadHeader1, postContent1, new Guest());
            Assert.AreEqual(-1, threadId);
        }

        [TestMethod()]
        public void GetForumTest_Exists_ById()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            if (forumId < 0) { Assert.Fail(); }
            Assert.AreEqual(fc.GetForum(forumId).Name, forumName1);
        }

        [TestMethod()]
        public void GetForumTest_NotExists_ById()
        {
            int forumId = fc.GetForumIdByName(forumName1);
            Assert.IsNull(fc.GetForum(forumId));
        }

        [TestMethod()]
        public void GetForumTest_Exists_ByName()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.AreEqual(fc.GetForum(forumName1).Name, forumName1);
        }

        [TestMethod()]
        public void GetForumTest_NotExists_ByName()
        {
            Assert.IsNull(fc.GetForum(forumName1));
        }

        [TestMethod()]
        public void SendMailTest_Success()
        {
            Assert.IsTrue(fc.SendMail("test mail??", "how do you know my language??", "test@mail.test"));
        }

        [TestMethod()]
        public void SendMailTest_Fail()
        {
            Assert.IsFalse(fc.SendMail("test mail??", "how do you know my language??", "sdfsdf"));
        }

        [TestMethod()]
        public void RegisterTest_Success()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            fc.Register(fc.GetForum(forumName1).ID, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            Assert.AreEqual(fc.GetMember("Morbo", fc.GetForum(forumName1).ID).username, "Morbo");
        }

        [TestMethod()]
        public void RegisterTest_DuplicateRegistration()
        {
            bool output = false;
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            fc.Register(fc.GetForum(forumName1).ID, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            try { fc.Register(fc.GetForum(forumName1).ID, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);}
            catch (Exception)
            {
                output = true;
            }
            Assert.IsTrue(output);

        }
        // TODO: RANI DO UP TO THIS POINT

        [TestMethod()]
        public void VerifyUserTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, "Morbo23", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            bool isVerified = fc.VerifyUser(forumId, "Morbo23", fc.GetVerificationCode(forumId, "Morbo23"));
            Assert.IsTrue(isVerified);
        }

        [TestMethod()]
        public void VerifyUserTest_BadCode()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            bool isVerified = fc.VerifyUser(forumId, "Morbo", fc.GetVerificationCode(forumId, "Morbo") + "problem");
            Assert.IsFalse(isVerified);
        }

        [TestMethod()]
        public void GetMemberTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            Member member = fc.GetMember("Morbo", forumId);
            Assert.AreEqual(member.username, "Morbo");
            Assert.AreEqual(member.ForumID, forumName1);
        }

        [TestMethod()]
        public void GetMemberTest_NotExistsMember()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            Member member = fc.GetMember("Morbo2", forumId);
            Assert.IsNull(member);
        }

        [TestMethod()]
        public void signUpSuperAdminTest()
        {
            Assert.AreNotEqual(fc.signUpSuperAdmin("admin", "admin", "admin", "admin", DateTime.Now, "admin@admin.admin"), null);
        }

        [TestMethod()]
        public void signUpSuperAdminTest_ThisAdminExists()
        {
            Assert.AreEqual(fc.signUpSuperAdmin(superAdminName, "admin", "admin", "admin", DateTime.Now, "admin@admin.admin"), null);
        }

        [TestMethod()]
        public void signInSuperAdminTest()
        {
            Assert.IsTrue(fc.signInSuperAdmin(superAdminName, superAdminPassword));
        }

        [TestMethod()]
        public void signInSuperAdminTest_NotExists()
        {
            Assert.IsFalse(fc.signInSuperAdmin(superAdminName + "problem", superAdminPassword));
        }

        [TestMethod()]
        public void signInSuperAdminTest_BadPassword()
        {
            Assert.IsFalse(fc.signInSuperAdmin(superAdminName, superAdminPassword + "problem"));
        }

        [TestMethod()]
        public void GetForumTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            Forum forum = fc.GetForum(forumId);
            Assert.AreEqual(forum.ID, forumId);
            Assert.AreEqual(forum.Name, forumName1);
        }
        
        [TestMethod()]
        public void GetForumTest_ForumIdNotExists()
        {
            Forum forum = fc.GetForum(-1);
            Assert.IsNull(forum);
        }

        [TestMethod()]
        public void GetSubForumTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            Assert.AreEqual(sf.Name, "subforum");
            Assert.AreEqual(sf.Subject, "heyheyhey");
        }

        [TestMethod()]
        public void GetSubForumTest_SubForumNotExists()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            SubForum sf = fc.GetSubForum(forumId, "subforum2");
            Assert.IsNull(sf);
        }

        [TestMethod()]
        public void GetThreadByTitleTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            Assert.AreEqual(t.Header, "a thread");
            Assert.AreEqual(t.forum, fc.GetForum(forumId));
            Assert.AreEqual(t.subforum, sf);
        }

        [TestMethod()]
        public void GetThreadByTitleTest_NoThreadExists()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread 2");
            Assert.IsNull(t);
        }

        [TestMethod()]
        public void CreatePolicyTest()
        {
            Assert.IsNotNull(fc.CreatePolicy());
        }

        [TestMethod()]
        public void DestructPostTest()
        {
            int id = fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, theFirstComment, superAdminName, "a comment 2");
            bool isDestructed = fc.DestructPost(forumId, sf.ID, t.id, theFirstComment);
            if(isDestructed)
            {
                MinimizedPost isThere = fc.findPost(forumId, sf.ID, t.id, theFirstComment);
                Assert.IsNull(isThere);
                MinimizedPost isThere2 = fc.findPost(forumId, sf.ID, t.id, theSecondComment);
                Assert.IsNull(isThere2);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void DestructThreadTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, theFirstComment, superAdminName, "a comment 2");
            bool isDestructed = fc.DestructThread(forumId, sf.ID, t.id);
            if (isDestructed)
            {
                MinimizedThread isThere = fc.findThread(forumId, sf.ID, t.id);
                Assert.IsNull(isThere);
                MinimizedPost isThere1 = fc.findPost(forumId, sf.ID, t.id, theFirstComment);
                Assert.IsNull(isThere1);
                MinimizedPost isThere2 = fc.findPost(forumId, sf.ID, t.id, theSecondComment);
                Assert.IsNull(isThere2);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void GetPostRepliesTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment 2");
            List<Post> comments = fc.GetPostReplies(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID });
            Assert.AreEqual(2, comments.Count);
        }

        [TestMethod()]
        public void IsUserRegisteredToForumTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            bool isRegistered = fc.Register(forumId, "Morbo50", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            bool isVerified = fc.VerifyUser(forumId, "Morbo50", fc.GetVerificationCode(forumId, "Morbo50"));
            if (isRegistered && isVerified)
            {
                bool isThere = fc.IsUserRegisteredToForum(forumId, "Morbo50");
                Assert.IsTrue(isThere);
                bool isThere2 = fc.IsUserRegisteredToForum(forumId, superAdminName);
                Assert.IsTrue(isThere2);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void IsUserRegisteredToForumTest_NotRegistered()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            bool isThere = fc.IsUserRegisteredToForum(forumId, superAdminName + "problem");
            Assert.IsFalse(isThere);
        }

        [TestMethod()]
        public void LoginTest()
        {
            fc.AddForum(forumName1, superAdminName);
            int forumId = fc.GetForumIdByName(forumName1);
            PrimitiveResult admin = fc.Login(forumId, superAdminName, superAdminPassword);
            Assert.AreEqual(admin.Result.UserName, superAdminName);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
        }

        [TestMethod()]
        public void LoginTest_multipleLoginsToSession()
        {
            fc.AddForum(forumName1, superAdminName);
            int forumId = fc.GetForumIdByName(forumName1);
            PrimitiveResult admin = fc.Login(forumId, superAdminName, superAdminPassword);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
            string sessionNum = fc.getLastSessionNum();
            admin = fc.Login(forumId, superAdminName, sessionNum);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
            admin = fc.Login(forumId, superAdminName, sessionNum);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
        }

        [TestMethod()]
        public void LoginTest_BadPassword()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            PrimitiveResult admin = fc.Login(forumId, superAdminName, superAdminPassword + "problem");
            Assert.IsNull(admin.Result);
            Assert.AreEqual(admin.ErrorMessageg, "can't find matching username/password, and this guy isn't a guest.");
        }

        [TestMethod()]
        public void LoginTest_NotVerified()
        {
            fc.AddForum(forumName1, superAdminName);
            int forumId = fc.GetForumIdByName(forumName1);
            fc.Register(forumId, "qwersa", "qwersa", "qwersa", "qwersa", 2000, 1, 1, "qwersa@qwersa0", 0);
            PrimitiveResult admin = fc.Login(forumId, "qwersa", "qwersa");
            Assert.AreEqual(admin.Result.State, UserTypeInForum.NotApproved);
        }

        [TestMethod()]
        public void LoginTest_Guest()
        {
            fc.AddForum(forumName1, superAdminName);
            int forumId = fc.GetForumIdByName(forumName1);
            PrimitiveResult admin = fc.Login(forumId, "guest", "guest");
            Assert.AreEqual(admin.Result.State, UserTypeInForum.Guest);
        }

        [TestMethod()]
        public void SetForumPolicyTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            ForumPolicy fp = new ForumPolicy();
            fp.GuestCanPost = true;
            fp.MaxModeratorsNumber = 2005;
            fc.SetForumPolicy(forumId, fp);
            ForumPolicy policy = fc.GetForumPolicy(forumId);
            Assert.AreEqual(fp, policy);
        }

        [TestMethod()]
        public void PromoteToAdminTest()
        {
            string newUserName = "Morbo2";
            string forumName = forumName1 + 2;
            fc.AddForum(forumName, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName);
            bool isRegistered = fc.Register(forumId, newUserName, "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd2", 1);
            bool isVerified = fc.VerifyUser(forumId, newUserName, fc.GetVerificationCode(forumId, newUserName));
            bool isPromoted = fc.PromoteToAdmin(forumId, newUserName, superAdminName);
            if (isRegistered && isVerified && isPromoted)
            {
                bool isAdmin = fc.IsUserAdminOfForum(forumId, newUserName);
                Assert.IsTrue(isAdmin);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void PromoteToAdminTest_NotAdminPromoting()
        {
            string newUserName = "Morbo70";
            string newUserName2 = "KokoShanel";
            string forumName = forumName1 + 5;
            fc.AddForum(forumName, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName);
            bool isRegistered = fc.Register(forumId, newUserName, "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            bool isVerified = fc.VerifyUser(forumId, newUserName, fc.GetVerificationCode(forumId, newUserName));
            bool isRegistered2 = fc.Register(forumId, newUserName2, "dqjei", "some", "one", 2000, 10, 10, "aaa2@fds.cdsd", 1);
            bool isVerified2 = fc.VerifyUser(forumId, newUserName2, fc.GetVerificationCode(forumId, newUserName2));
            try
            {
                bool isPromoted = fc.PromoteToAdmin(forumId, newUserName2, newUserName);
            }
            catch (Exception)
            {
                if (isRegistered && isVerified && isRegistered2 && isVerified2)
                {
                    bool isAdmin = fc.IsUserAdminOfForum(forumId, newUserName2);
                    Assert.IsFalse(isAdmin);
                }
                else
                {
                    Assert.Fail();
                }
            }
        }

        [TestMethod()]
        public void initSuperAdminTest()
        {
            SuperAdmin sa = fc.initSuperAdmin("admin", "admin");
            Assert.IsNotNull(sa);
        }

        [TestMethod()]
        public void initSuperAdminTest_AlreadyExists()
        {
            SuperAdmin sa = null;
            try
            {
                sa = fc.initSuperAdmin(superAdminName, superAdminPassword);
            }
            catch
            {
                Assert.IsNull(sa);
            }
        }

        [TestMethod()]
        public void findThreadTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment 2");
            MinimizedThread mt = fc.findThread(forumId, sf.ID, t.id);
            Assert.AreEqual(mt.ID, t.id);
            Assert.AreEqual(mt.Title, t.Header);
        }
        
        [TestMethod()]
        public void findThreadTest_NotExists()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            MinimizedThread mt = fc.findThread(forumId, sf.ID, 7);
            Assert.IsNull(mt);
        }

        [TestMethod()]
        public void findPostTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment 2");
            MinimizedPost mp = fc.findPost(forumId, sf.ID, t.id, theFirstComment);
            Assert.AreEqual(mp.PostId, theFirstComment[theFirstComment.Count - 1]);
            Assert.AreEqual(mp.Text, "a comment");
        }

        [TestMethod()]
        public void findPostTest_NotExists()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            MinimizedPost mp = fc.findPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID, 1 });
            Assert.IsNull(mp);
        }

        [TestMethod()]
        public void deleteThreadTest_SuperAdmin()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, theFirstComment, superAdminName, "a comment 2");
            bool isDestructed = fc.deleteThread(forumId, sf.ID, t.id, superAdminName);
            if (isDestructed)
            {
                MinimizedThread isThere = fc.findThread(forumId, sf.ID, t.id);
                Assert.IsNull(isThere);
                MinimizedPost isThere1 = fc.findPost(forumId, sf.ID, t.id, theFirstComment);
                Assert.IsNull(isThere1);
                MinimizedPost isThere2 = fc.findPost(forumId, sf.ID, t.id, theSecondComment);
                Assert.IsNull(isThere2);
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod()]
        public void deleteThreadTest_Guest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            fc.AddSubForum(forumId, "subforum", new List<string> { superAdminName }, "heyheyhey");
            SubForum sf = fc.GetSubForum(forumId, "subforum");
            fc.AddThread(forumId, sf.Name, "a thread", "a content", fc.GetMember(superAdminName, forumId));
            Thread t = fc.GetThreadByTitle(forumId, sf.ID, "a thread");
            List<int> theFirstComment = fc.replyToPost(forumId, sf.ID, t.id, new List<int> { t.OpenningPost.ID }, superAdminName, "a comment");
            List<int> theSecondComment = fc.replyToPost(forumId, sf.ID, t.id, theFirstComment, superAdminName, "a comment 2");
            bool isDestructed = fc.deleteThread(forumId, sf.ID, t.id, "guest");
            Assert.IsFalse(isDestructed);
        }

        [TestMethod()]
        public void GetVerificationCodeTest()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            int forumId = fc.GetForumIdByName(forumName1);
            bool isRegistered = fc.Register(forumId, "Morbo", "dqjei", "some", "one", 2000, 10, 10, "aaa@fds.cdsd", 1);
            Member member = fc.GetMember("Morbo", forumId);
            Assert.AreEqual(member.Code, fc.GetVerificationCode(forumId, "Morbo"));
        }

        [TestMethod()]
        public void GetForumIdByNameTest_Exists()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.IsTrue(fc.GetForumIdByName(forumName1) > -1);
        }
        [TestMethod()]
        public void GetForumIdByNameTest_NotExists()
        {
            Assert.IsTrue(fc.GetForumIdByName(forumName1) == -1);
        }

        [TestMethod()]
        public void IsUserAdminOfForumTest_True()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.IsTrue(fc.IsUserAdminOfForum(fc.GetForumIdByName(forumName1), superAdminName));
        }

        [TestMethod()]
        public void IsUserAdminOfForumTest_False()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.IsFalse(fc.IsUserAdminOfForum(fc.GetForumIdByName(forumName1), "notAdmin"));
        }

        //[TestMethod()]
        //public void GetForumIdByNameTest1()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void AddModeratorTest()
        {
            raniBigInit();
            fc.AddModerator(member.username, forumId, subforumId);
            Assert.IsTrue(fc.isUserModeratorOfSubForum(forumId, subforumId, member.username));
        }

        //[TestMethod()]
        //public void DestructTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ResetToDefaultPolicyTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void findThreadByTitleTest()
        {
            raniBigInit();
            Thread t = fc.GetSubForum(forumId, subForumName1).Threads[threadId];
            Assert.AreSame(t, fc.GetThreadByTitle(forumId, subforumId, threadHeader1));
        }

        [TestMethod()]
        public void CanGuestStartThreadTest_True()
        {
            forumId = fc.AddForum(forumName1, superAdminName);
            Assert.IsTrue(fc.CanGuestStartThread(forumId));
        }

        [TestMethod()]
        public void CanGuestStartThreadTest_False()
        {
            ForumPolicy p = new ForumPolicy();
            p.GuestCanStartThread = false;
            forumId = fc.AddForum(forumName1, superAdminName, p);
            Assert.IsFalse(fc.CanGuestStartThread(forumId));
        }

        [TestMethod()]
        public void CanGuestReplyToPostTest_True()
        {
            forumId = fc.AddForum(forumName1, superAdminName);
            Assert.IsTrue(fc.CanGuestReplyToPost(forumId));
        }

        [TestMethod()]
        public void CanGuestReplyToPostTest_False()
        {
            ForumPolicy p = new ForumPolicy();
            p.GuestCanPost = false;
            forumId = fc.AddForum(forumName1, superAdminName, p);
            Assert.IsFalse(fc.CanGuestReplyToPost(forumId));
        }

        //[TestMethod()]
        //public void replyToPostTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void findThreadIdTest_Success()
        {
            raniBigInit();
            Assert.AreNotEqual(-1, fc.findThreadId(forumId, subforumId, threadHeader1));
        }

        [TestMethod()]
        public void findThreadIdTest_Fail()
        {
            raniBigInit();
            Assert.AreEqual(-1, fc.findThreadId(forumId, subforumId, "fdsfsd"));
        }

        //[TestMethod()]
        //public void sendPrivateMessageTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void isUserModeratorOfSubForumTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void getModTimeLimitTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void IsSystemIniaitlizedTest_Success()
        {
            Assert.IsTrue(fc.IsSystemIniaitlized());
        }

        [TestMethod()]
        public void IsSystemIniaitlizedTest_Fail()
        {
            testCleanup();
            ForumControllerLocal.ClearInstance();
            fc = ForumControllerLocal.GetInstance();
            Assert.IsFalse(fc.IsSystemIniaitlized());
        }

        [TestMethod()]
        public void GetAllForumsTest()
        {
            fc.AddForum(forumName1, superAdminName);
            fc.AddForum(forumName2, superAdminName);
            fc.AddForum(forumName3, superAdminName);
            fc.AddForum(forumName4, superAdminName);
            fc.clearForumsFromRAM();
            Assert.AreEqual(4, fc.GetAllForums().Count);
            Assert.IsTrue(fc.GetAllForums().ContainsValue(forumName1));
            Assert.IsTrue(fc.GetAllForums().ContainsValue(forumName2));
            Assert.IsTrue(fc.GetAllForums().ContainsValue(forumName3));
            Assert.IsTrue(fc.GetAllForums().ContainsValue(forumName4));
        }

        [TestMethod()]
        public void GetForumByIDTest_Success()
        {
            fc.AddForum(forumName1, superAdminName, new ForumPolicy());
            Assert.AreNotEqual(-1, fc.GetForumIdByName(forumName1));
        }

        [TestMethod()]
        public void GetForumByIDTest_Fail()
        {
            Assert.AreEqual(-1, fc.GetForumIdByName(forumName1));
        }

        //[TestMethod()]
        //public void editModTimeLimitTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void EditPostTest()
        {
            raniBigInit();
            Thread t = fc.GetThreadByTitle(forumId, subforumId, threadHeader1);
            List<int> post1 = fc.replyToPost(forumId, subforumId, t.id, new List<int> { t.OpenningPost.ID }, userName1, "a comment");
            List<int> post2 = fc.replyToPost(forumId, subforumId, t.id, post1, superAdminName, "a comment 2");
            List<int> post3 = fc.replyToPost(forumId, subforumId, t.id, post2, userName1, "a comment 3");
            List<int> post4 = fc.replyToPost(forumId, subforumId, t.id, post2, userName1, "a comment 4");
            List<int> post5 = fc.replyToPost(forumId, subforumId, t.id, post4, userName1, "a comment 4");
            List<int> post6 = fc.replyToPost(forumId, subforumId, t.id, post4, userName1, "a comment 4");
            List<int> post7 = fc.replyToPost(forumId, subforumId, t.id, post4, userName1, "a comment 4");

            Assert.IsTrue(fc.EditPost(forumName1, subForumName1, threadId, post3, userName1, "edited comment biatch!"));
            Assert.AreEqual("edited comment biatch!", fc.findPost(forumId, subforumId, threadId, post3).Text);
        }

        //[TestMethod()]
        //public void GetAllFriendsTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void GetAllThreadsTest()
        {
            raniBigInit();
            Assert.AreEqual(1, fc.GetAllThreads(forumId, subforumId).Count);
        }

        [TestMethod()]
        public void GetForumPolicyTest()
        {
            ForumPolicy policy1 = new ForumPolicy(3, 3, 3, true, true, 3, 3, 3, true, true, 3, 3, 1);
            fc.AddForum(forumName1, superAdminName, policy1);
            forumId = fc.GetForumIdByName(forumName1);
            fc.clearForumsFromRAM();
            ForumPolicy policy2 = fc.GetForumPolicy(forumId);
            Assert.AreEqual(policy1.MaxModeratorsNumber, policy2.MaxModeratorsNumber);
        }

        [TestMethod()]
        public void getLastSubForumNameTest_1()
        {
            raniBigInit();
            Assert.AreEqual(subForumName1, fc.getLastSubForumName(forumId));
        }

        [TestMethod()]
        public void getLastSubForumNameTest_2()
        {
            raniBigInit();
            fc.AddSubForum(forumId, subForumName2, new List<string> { superAdminName }, subForumName2);
            Assert.AreEqual(subForumName2, fc.getLastSubForumName(forumId));
        }

        //[TestMethod()]
        //public void GetNotificationsTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void GetSubForumModeratorsTest_1()
        {
            raniBigInit();
            SubForum sf = fc.GetSubForum(forumId, subForumName1);
            Assert.AreEqual(1, fc.GetSubForumModerators(forumId, subforumId).Count);
        }

        [TestMethod()]
        public void GetSubForumModeratorsTest_2()
        {
            raniBigInit();
            fc.AddModerator(member.username, forumId, subforumId);
            SubForum sf = fc.GetSubForum(forumId, subForumName1);
            Assert.AreEqual(2, fc.GetSubForumModerators(forumId, subforumId).Count);
        }

        //[TestMethod()]
        //public void UnpromoteModeratorTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void UserEnterToSubForumTest_Guest()
        {
            raniBigInit();
            fc.Login(forumId, "guest", "guest");
            Assert.AreEqual(UserTypeInSubForum.Guest, fc.UserEnterToSubForum(forumId, subforumId, "guest").State);
        }

        //[TestMethod()]
        //public void getLastUserNameTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void getUsersEmailTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void getLastForumNameTest_1()
        {
            fc.AddForum(forumName1, superAdminName);
            Assert.AreEqual(forumName1, fc.getLastForumName());
        }

        [TestMethod()]
        public void getLastForumNameTest_2()
        {
            fc.AddForum(forumName1, superAdminName);
            fc.AddForum(forumName2, superAdminName);
            Assert.AreEqual(forumName2, fc.getLastForumName());
        }

        [TestMethod()]
        public void getLastForumNameTest_NoForums()
        {
            Assert.AreEqual("", fc.getLastForumName());
        }

        [TestMethod()]
        public void GetForumAdminsTest_SingleAdmin()
        {
            int forumId = fc.AddForum(forumName1, superAdminName);
            Assert.AreEqual(1, fc.GetForumAdmins(forumId).Count);
        }

        [TestMethod()]
        public void GetForumAdminsTest_TwoAdmins()
        {
            int forumId = fc.AddForum(forumName1, superAdminName);
            fc.Register(forumId, userName1, userPassword1, "some", "guy", 1900, 1, 1, "asc@fsd.vd", 1);
            fc.VerifyUser(forumId, userName1, fc.GetMember(userName1, forumId).Code);
            member = fc.GetMember(userName1, forumId);
            fc.PromoteToAdmin(forumId, member.username, superAdminName);
            Assert.AreEqual(2, fc.GetForumAdmins(forumId).Count);
        }

        [TestMethod()]
        public void getSecondLastForumNameTest()
        {
            fc.AddForum(forumName1, superAdminName);
            fc.AddForum(forumName2, superAdminName);
            Assert.AreEqual(forumName1, fc.getSecondLastForumName());
        }

        [TestMethod()]
        public void getLastThreadTitleTest_AfterThread()
        {
            raniBigInit();
            Assert.AreEqual(threadHeader1, fc.getLastThreadTitle(forumId, subForumName1));
        }

        [TestMethod()]
        public void getLastThreadTitleTest_WithoutThread()
        {
            fc.AddForum(forumName1, superAdminName);
            forumId = fc.GetForumIdByName(forumName1);
            subforumId = fc.AddSubForum(forumId, subForumName1, new List<string> { superAdminName }, subForumName1);
            Assert.AreEqual("", fc.getLastThreadTitle(forumId, subForumName1));
        }

        [TestMethod()]
        public void getAllMinimizedUserInForumTest()
        {
            raniBigInit();
            List<MinimizedUserInForum> list = fc.getAllMinimizedUserInForum(forumId);
            Assert.AreEqual(2, list.Count);
        }

        [TestMethod()]
        public void getAllMinimizedUserInSubForumTest()
        {
            raniBigInit();
            List<MinimizedUserInSubForum> list = fc.getAllMinimizedUserInSubForum(forumId, subforumId);
            Assert.AreEqual(2, list.Count);
        }

        [TestMethod()]
        public void getAllUserNamesTest()
        {
            raniBigInit();
            Assert.AreEqual(2, fc.getAllUserNames(forumId).Count);
        }

        [TestMethod()]
        public void LogoutTest()
        {
            fc.AddForum(forumName1, superAdminName);
            int forumId = fc.GetForumIdByName(forumName1);
            PrimitiveResult admin = fc.Login(forumId, superAdminName, superAdminPassword);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
            string sessionNum = fc.getLastSessionNum();
            admin = fc.Login(forumId, superAdminName, sessionNum);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);
            admin = fc.Login(forumId, superAdminName, sessionNum);
            Assert.AreEqual(admin.Result.State, UserTypeInForum.SuperAdmin);

            fc.LogOut(forumName1, superAdminName);
            fc.LogOut(forumName1, superAdminName);
            fc.LogOut(forumName1, superAdminName);
            Assert.IsTrue(true); // here you logged out 3 times after logging in from 3 devices
            try
            {
                fc.LogOut(forumName1, superAdminName);
                Assert.Fail(); // cant logout 4 times when logged in only from 3 devices
            }
            catch
            {
                Assert.IsTrue(true); // if it can't logout 4th time after 3 logins then everything is ok
            }
            
        }
    }
}