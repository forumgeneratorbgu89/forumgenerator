﻿using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Logger
{
    public static class ForumGeneratorLogger
    {
        private static FileStream fs;

        public static void init(string logFilePath, Level level)
        {
            string subPath;
            if (logFilePath.Equals(string.Empty))
            {
                subPath = @"c:\sadna\"; // your code goes here
            }
            else
            {
                subPath = logFilePath;
            }
            try
            {
                System.IO.Directory.CreateDirectory(subPath);
                string fileName = "log_" + string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}.txt",
            DateTime.Now);
                __log_file_path = subPath + fileName;
                __level = level;
                if (File.Exists(__log_file_path))
                {
                    File.Delete(__log_file_path);
                }

            }
            catch (Exception)
            {
            }
            
            log(MethodBase.GetCurrentMethod().Name, "Logger is up");

        }
        public enum Level
        {
            i,
            d,
            e
        }

        private static Level __level;

        /** 
         * __log_file_path get/set 
         */
        public static Level LogerLevel
        {
            get { return ForumGeneratorLogger.__level; }
            set { ForumGeneratorLogger.__level = value; }
        }
        /** 
             * Log file path 
             *  
             * @var string 
             */
        private static string __log_file_path;

        /** 
         * __log_file_path get/set 
         */
        public static string filePath
        {
            get { return ForumGeneratorLogger.__log_file_path; }
            set { if (value.Length > 0) ForumGeneratorLogger.__log_file_path = value; }
        }
        
        public static void log(string context, string msg)
        {
            if (msg.Length > 0)
            {
                string data = string.Format("{0} {1} - {2}: {3}", DateTime.Now.ToShortDateString(),
                    DateTime.Now.ToShortTimeString(), context,
                    msg);
                try
                {
                    using (StreamWriter sw = File.AppendText(__log_file_path))
                    {
                        sw.WriteLine(data);
                    }
                }
                catch (Exception)
                {
                    
                }
            }
        }

        public static void i(string context, string msg)
        {
            if (__level == Level.i)
            {
                log(context, "INFO: " + msg);
            }
        }

        public static void d(string context, string msg)
        {
            if(__level == Level.d || __level == Level.i)
            {
                log(context, "DEBUG : " + msg);
            }
        }

        public static void e(string context, string msg)
        {
            log(context, "ERROR: " + msg);
        }
    }
}
