﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    public abstract class Subject : MarshalByRefObject
    {
        private Dictionary<string, Observer> _observers = new Dictionary<string, Observer>();
        
        public void Attach(Observer observer, int forum, string username)
        {
            _observers.Add(forum + " " + username, observer);
        }

        public void Detach(Observer observer, int forum, string username)
        {
            _observers.Remove(forum + " " + username);
        }

        public void Notify(string title, string message, List<string> toSend, ForumMapper db, string forumname)
        {
            foreach (string s in toSend)
            {
                if (_observers.ContainsKey(s))
                {
                    _observers[s].Update(title, message);
                }
                else
                {
                    //string sql = "INSERT INTO " + DbConsts.Table_Notifications 
                    //    + " (" + DbConsts.Notifications_ForumName + ", " 
                    //    + DbConsts.Notifications_Username + ", " + DbConsts.Notifications_Content
                    //    + ") VALUES('" + forumname + "', '" + s.Split(' ')[1] + "', '" + title + " " + message 
                    //    + "')";
                    //db.ExecuteQuery(sql);
                }
                
            }
        }
    }
}
