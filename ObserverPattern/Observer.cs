﻿using System;
using System.Collections.Generic;

namespace ObserverPattern
{
    public class Observer : MarshalByRefObject
    {
        public virtual void Unregister()
        {
            
        }

        public virtual void Update(string title, string message)
        {

        }

        public virtual void ShowPendingNotification(List<string[]> allNotifications)
        {
            
        }
    }
}