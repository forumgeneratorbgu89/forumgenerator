﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    חבר בפורום יכול למחוק הודעה שפרסם בעבר. המחיקה גוררת מחיקת כל הודעות
    התגובה. אם זו הודעה פותחת אז כל הדיון נמחק. כמובן – יש לשים לב לאילוצים נוספים שעלולים
    להיות מופרים.
    */
    [TestClass]
    public class Unpost : ClientSideAcceptance
    {
    }
}
