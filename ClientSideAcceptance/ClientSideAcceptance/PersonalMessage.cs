﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    משתמש א' יכול לשלוח הודעה פרטית, שאינה חלק משרשור שאף תת-
    פורום, ישירות למשתמש כלשהו ב'. משתמש א' לא מכיר את כתובת המייל של משתמש ב', אלא
    רק את שם המשתמש שלו.
    */
    [TestClass]
    public class PersonalMessage : ClientSideAcceptance
    {
    }
}
