﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    רישום (registration) . ( חבר לפורום יש להקפיד להתייחס למדיניות רישום של פורום כפי
    שניתן להגדיר בסיפור השימוש הקודם ).
    */
    [TestClass]
    public class Registration : ClientSideAcceptance
    {
    }
}
