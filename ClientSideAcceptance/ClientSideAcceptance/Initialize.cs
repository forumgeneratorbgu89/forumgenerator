﻿using ClientWPF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    הפעלה ראשונית של – מערכת הפורומים. , בסיום פעולה זו מערכת הפורומים תוכל
    בפרט יהיה לה מנהל על בעל הרשאות מיוחדות כגון
    הרשאה ליצירת פורומים ומינוי מנהל פורום
    */
    [TestClass]
    public class Initialize : ClientSideAcceptance
    {
    }
}
