﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    מנהל פורום ממנה משתמש להיות מנחה לתת-פורום. ההנחייה יכולה
    להיקבע לזמן מוגבל שיוגדר ע"י מנהל הפורום. מנהל הפורום שמינה את המנחה יכול לערוך את
    משך הזמן של המינוי.
    */
    [TestClass]
    public class Promoting : ClientSideAcceptance
    {
    }
}
