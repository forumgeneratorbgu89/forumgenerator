﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    /*
    שירותי מנהל פורום:
    a. ביטול מינוי של מנחה של תת-פורום )לפני תום תקופת המינוי(.
    b. קבלת דיווחים:
    a. מספר הודעות כולל בתת פורום.
    b. רשימת ההודעות שנכתבו על ידי חבר.
    c. רשימת מנחים, מי מינה אותם, מתי ולאיזה תת-פורום, וההודעות שפרסמו.
    */
    [TestClass]
    public class ForumAdminService : ClientSideAcceptance
    {
    }
}
