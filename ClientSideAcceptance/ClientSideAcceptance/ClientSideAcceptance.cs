﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain.Models.ForumManagement;
using Domain.Models.UserManagement;
using AcceptenceTests.Service;

namespace ClientSideAcceptance.ClientSideAcceptance
{
    [TestClass]
    public class ClientSideAcceptance
    {

        //private Acceptance.TestsImplementation imp;
        public Iservice imp;
        public SuperAdmin superAdmin;
        public Forum forum;
        public Member member;
        public Thread thread;
        public Post post;
        public Guest guest;
        public ForumPolicy policy;
        public Member[] members;
        public Forum[] forums;
        public SubForum[] subforums;

        protected Member signUpApproveAndLogin(Forum f, String username, String password, String fname, String lname,
            int bdateY, int bdateM, int bdateD, String email)
        {//public Member signUpToForum(Forum forum, string username, string password, string fname, string lname, int bdateY, int bdateM, int bdateD, string email)
            Member output = imp.signUpToForum(f, username, password, fname, lname, bdateY, bdateM, bdateD, email);
            imp.approveSignup(output);
            imp.loginToForum(output.username, output.password, f);
            return output;
        }

        [TestInitialize]
        public void InitTest()
        {
            imp = new ServiceLocal();
            superAdmin = imp.initSuperAdmin("admin", "admin");

            forums = new Forum[2];
            // make forums
            forums[0] = imp.createNewForum("Forum[0] - the greatest testing ground of all!", superAdmin);
            forums[1] = imp.createNewForum("Forum[1] - a barren wasteland of nothing most of the time!", superAdmin);

            members = new Member[3];
            // make members
            members[0] = signUpApproveAndLogin(forums[0], "rani el-kingo", "123456", "rani", "zinger", 1989, 1, 1, "aaa@bbb.grrr"); // this will be a regular member of forums[0]
            members[1] = signUpApproveAndLogin(forums[0], "gargamel", "234567", "gar", "amel", 1700, 1, 1, "ccc@bbb.grrr"); // this will be an admin of forums[0]
            imp.makeAdminOfForum(members[1], forums[0], superAdmin);
            members[2] = signUpApproveAndLogin(forums[0], "tov-tov", "xy4567", "tov", "tov", 1800, 1, 1, "dddd@bbb.grrr"); // this will be a mod of forums[0]

            subforums = new SubForum[1];
            // make subforums
            subforums[0] = imp.createNewSubForum("SubForum[0] - where the party is at!", members[1], forums[0]); // subforum of forums[0]
            imp.makeModOfSubforum(members[2], subforums[0], members[1]);

            guest = imp.createGuest();
            policy = imp.createPolicy();
        }

        [TestCleanup]
        public void CleanLastTest()
        {
            imp.Destruct();
            imp = null;
            forum = null;
            member = null;
            thread = null;
            post = null;
            guest = null;
            policy = null;
        }

    }
}