﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DomainEntity.MinimizedModels;

namespace ForumGenerator.Controllers
{
    public class UserController : ApiController, IUserController
    {
        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            throw new NotImplementedException();
        }

        public List<string> GetUserDetails(int forumID, string userName)
        {
            throw new NotImplementedException();
        }

        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {
            throw new NotImplementedException();
        }

        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            throw new NotImplementedException();
        }

        public bool IsUserApproved(string memberUserName, int forumID)
        {
            throw new NotImplementedException();
        }
    }
}
