﻿using DomainEntity.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.ReturnedValues;
using ObserverPattern;

namespace ForumGenerator
{
    public interface IForumController
    {
        List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender);

        List<string> GetAllRequestFriedns(int forumID, string userName);

        // name, last, user, email
        List<string> GetUserDetails(int forumID, string userName);

        bool AddFreind(string senderUserName, string reciverUserName, int forumID);

        bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID);

        bool IsUserApproved(string memberUserName, int forumID);

        // functions for forum statistics
        List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username);

        List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid);

        List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid);

        int GetForumIdByName(string forumName);

        List<MinimizedForum> GetAllForums();

        int NumOfForums();

        int GetNumOfPosts(int forumid);

        //return -1 if not valid 
        int AddForum(string admin, List<Object> name_policy);

        //if initialized system add only admin to moderator list
        bool AddSubForum(int forumID, List<List<string>> lst);

        MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName);

        bool AddThread(int forumID, string SubForumName, List<string> header_content, string Postwriter);//not tested yet

        bool IsSystemIniaitlized();

        //return error message or String.empty if everything is ok.
        string Register(int forumID, string username, string password,
                string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive);

        ForumPolicy GetForumPolicy(int forumId);

        bool VerifyUser(int forumid, string username, string code);

        //SuperAdmin getSuperAdmin(); //what should be returned?

        bool signUpSuperAdmin(String username, String password,
            string firstName, String lastName, DateTime birthDate, String eMail);

        List<MinimizedSubForum> getAllSubForum(int forumId);

        bool signInSuperAdmin(String username, String password);

        void unSuspend(int forumID, string friendUserName);

        void suspend(int forumID, string friendUserName);

        bool isUserSusspend(int forumID, string friendUserName);

        string getLastSubForumName(int forumId);

        bool EditForumPolicy(int forumID, ForumPolicy policy);

        bool IsUserAdminOfForum(int iD, string username);

        PrimitiveResult Login(int forumID, string username, string password);

        int GetSubForumIdByName(string subForumName, int iD);

        List<MinimizedThread> GetAllThreads(int forumId, int subForumID);

        List<string> getAllUserNames(int forumID);

        List<Notification> GetNotifications(string _forum1, string userName);

        bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter);

        //SuperAdmin initSuperAdmin(string username, string password); //what should be returned?

        MinimizedThread findThread(int forumid, int subforumid, int threadid);

        bool EditPost(string _forum, string _subForum, int _threadId, List<string> postpath_content, string poster);

        bool CanGuestStartThread(int iD);

        bool CanGuestReplyToPost(int iD);

        MinimizedThread findThreadByTitle(int forumid, int subforumid, string title);

        MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid);

        bool deleteThread(int forumid, int subforumid, int threadid, string deleter);

        string GetVerificationCode(int forumID, string username);

        string PromoteModerator(string username, int forumID, int subForumID);

        void ResetToDefaultPolicy(int forumID);

        int findThreadId(int forumiD, int subForumiD, string title);

        // -1 for error
        List<int> replyToPost(int forum, int subForum, int Thread, List<string> postPath_text, string creator);

        bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName);

        bool isUserModeratorOfSubForum(int forumiD, int subForumiD, string username);

        DateTime? getModTimeLimit(int forumiD, int subForumiD, string username);

        bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen);

        // username and time
        List<List<string>> GetSubForumModerators(int forumID, int subForumID);

        bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string username);

        bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote);

        void deleteForum(string name);

        void deleteMember(int forumID, string username);

        void deleteSubForum(List<string> forum_subforum);

        List<string> GetAllModerators(int forum);

        void Unfriend(int forumid, string username, string username_to_unfriend);

        List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID);

        MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID);

        // username
        List<string> GetAllFriends(int forumID, string username);

        void clearDB();

        void AttachObserver(Observer growlNotifiactions, int forumID, string username);
        void DetachObserver(Observer growlNotifiactions, int forumID, string username);


    }
}
