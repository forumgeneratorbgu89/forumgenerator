﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.MinimizedModels;

namespace ForumGenerator
{
    public interface IUserController
    {
        List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender);

        // name, last, user, email
        List<string> GetUserDetails(int forumID, string userName);

        bool AddFreind(string senderUserName, string reciverUserName, int forumID);

        bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID);

        bool IsUserApproved(string memberUserName, int forumID);
    }
}
