﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using DomainEntity.ReturnedValues;
using DomainEntity.MinimizedModels;
using Microsoft.Web.WebSockets;
using System.Web;
using ForumGenerator.Hubs;
using Microsoft.AspNet.SignalR;
using ObserverPattern;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using Logger;
using Newtonsoft.Json;
//using Microsoft.Owin.Logging.LoggerExtensions;

namespace ForumGenerator.Controllers
{
    public class ForumController : ApiControllerWithHub<TestHub>, IForumController
    {
        IForumService fSer;
        IUserService fUs;

        public static class ConnectionMapper
        {
            
            public static Dictionary<string, string> _connections = new Dictionary<string, string>();
        }

        public ForumController()
        {
            fSer = new ForumService();
            fUs = new UserService();
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<MinimizedForum> GetAllForums()
        {

            return fSer.GetAllForums();
        }
        
        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/IsForumInitilazie")]
        public bool IsForumInitilazie()
        {
            //fSer.initSuperAdmin("maor", "123456");
            //fSer.AddForum("Tapuz", "maor", new ForumPolicy());
            //fSer.AddForum("Walla", "maor", new ForumPolicy());

            return fSer.IsSystemIniaitlized();
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/login/{fourmId}/{userName}/{password}")]
        public PrimitiveResult Login(int fourmId, string userName, string password)
        {
            return fSer.Login(fourmId,userName,password);
        }
            
        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getAllSubForum/{fourmId}")]
        public List<MinimizedSubForum> getAllSubForum(int fourmId)
        {
            return fSer.getAllSubForum(fourmId);
        }

        public void sendMail(String subject, String text, String eMail)
        {
            MailMessage msg = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            msg.From = new MailAddress("forumCompany2016@gmail.com");
            msg.To.Add(eMail);
            msg.Subject = subject;
            msg.Body = text;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("forumCompany2016", "revivo123");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(msg);
        }


        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/NumOfForums")]
        public int NumOfForums()
        {
            return fSer.NumOfForums();
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetNumOfPosts/{forumID}")]
        public int GetNumOfPosts(int forumID)
        {
            return fSer.GetNumOfPosts(forumID);
        }


        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/UserEnterToSubForum/{forumID}/{subForrumID}/{userName}")]
        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            return fSer.UserEnterToSubForum(forumID, subForrumID, userName);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetForumPolicy/{forumId}")]
        public ForumPolicy GetForumPolicy(int forumId)
        {
            return fSer.GetForumPolicy(forumId);
        }

        //move it into to user controller 
        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/VerifyUser/{forumid}/{username}/{code}")]
        public bool VerifyUser(int forumid, string username, string code)
        {
            return fSer.VerifyUser(forumid, username, code);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetAllThreads/{forumId}/{subForumID}")]
        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            return fSer.GetAllThreads(forumId, subForumID);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetIncomingMessages/{forumID}/{usernameTarget}/{userNameSender}")]
        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            return fUs.GetIncomingMessages(forumID, usernameTarget, userNameSender);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetUserDetails/{forumID}/{userName}")]
        public List<String> GetUserDetails(int forumID, string userName)
        {
            var detailsList = fUs.GetUserDetails(forumID, userName);
            List<string> details = new List<string>();
            details.Add(detailsList.Item1);
            details.Add(detailsList.Item2);
            details.Add(detailsList.Item3);
            details.Add(detailsList.Item4);
            return details;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/AddFreind/{senderUserName}/{reciverUserName}/{forumID}")]
        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {

            bool isSent = fUs.AddFreind(senderUserName, reciverUserName, forumID);

            if (isSent)
            {
                SendNotifications(new List<string> { forumID + " " + reciverUserName }, "Friend request", " sent you a friend request", senderUserName );
            }

            return isSent;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/ConfirmFriendRequest/{senderUserName}/{reciverUserName}/{forumID}")]
        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            bool ans = fUs.ConfirmFriendRequest(senderUserName, reciverUserName, forumID);

            if (ans)
            {
                SendNotifications(new List<string> { forumID + " " + senderUserName }, "Friend request confirmed", " confirmed your friend request", reciverUserName);
            }

            return ans;
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/IsUserApproved/{memberUserName}/{forumID}")]
        public bool IsUserApproved(string memberUserName, int forumID)
        {
            return fUs.IsUserApproved(memberUserName, forumID);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/isUserSusspend/{forumID}/{friendUserName}")]
        public bool isUserSusspend(int forumID, string friendUserName)
        {
            return fSer.isUserSuspended(forumID, friendUserName);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetCommentsForUser/{forumID}/{adminUserName}/{username}")]
        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            return fSer.GetCommentsForUser(forumID, adminUserName, username);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getAllMinimizedUserInSubForum/{forumid}/{subforumid}")]
        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            return fSer.getAllMinimizedUserInSubForum(forumid, subforumid);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getAllMinimizedUserInForum/{forumid}")]
        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            return fSer.getAllMinimizedUserInForum(forumid);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetForumIdByName/{forumName}")]
        public int GetForumIdByName(string forumName)
        {
            return fSer.GetForumIdByName(forumName);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/AddForum/{admin}")]
        public int AddForum(string admin, [FromBody]List<Object> name_policy)
        {
            // TODO: check if user is admin
            ForumPolicy forumpolicy = JsonConvert.DeserializeObject<ForumPolicy>(name_policy[1].ToString());
            string name = (string)name_policy[0];
            return fSer.AddForum(name, admin, forumpolicy);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/AddSubForum/{forumID}")]
        public bool AddSubForum(int forumID, [FromBody]List<List<string>> moderators_name_subject)
        {
            // TODO: check if user is admin
            List<string> moderators = moderators_name_subject[0];
            string name = moderators_name_subject[1][0];
            string subject = moderators_name_subject[2][0];
            return fSer.AddSubForum(forumID, name, moderators, subject);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/AddThread/{forumid}/{subforumname}/{postwriter}")]
        public bool AddThread(int forumid, string subforumname,[FromBody] List<string> header_content, string postwriter)
        {
            string header = header_content[0];
            string postcontent = header_content[1];
            return fSer.AddThread(forumid, subforumname, header, postcontent, postwriter);
        }

        public bool IsSystemIniaitlized()
        {
            return true;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/Register/{forumid}/{username}/{password}/{firstName}/{lastName}/{birthyear}/{birthmonth}/{birthday}/{email}/{offlineInteractive}")]
        public string Register(int forumid, string username, string password, string firstname, string lastname, int birthyear, int birthmonth, int birthday, string email, int offlineInteractive)
        {
            return fSer.Register(forumid, username, password, firstname, lastname, birthyear, birthmonth, birthday,
                email, offlineInteractive);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/signUpSuperAdmin/{username}/{password}/{firstname}/{lastname}/{email}")]
        public bool signUpSuperAdmin(string username, string password, string firstname, string lastname, [FromBody] DateTime birthdate, string email)
        {
            return fSer.signUpSuperAdmin(username, password, firstname, lastname, birthdate, email);
        }
        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/signInSuperAdmin/{username}/{password}")]
        public bool signInSuperAdmin(string username, string password)
        {
            return fSer.signInSuperAdmin(username, password);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getLastSubForumName/{forumId}")]
        public string getLastSubForumName(int forumId)
        {
            return fSer.getLastSubForumName(forumId);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/EditForumPolicy/{forumID}")]
        public bool EditForumPolicy(int forumID, [FromBody] ForumPolicy policy)
        {
            return fSer.EditForumPolicy(forumID, policy);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/IsUserAdminOfForum/{iD}/{username}")]
        public bool IsUserAdminOfForum(int iD, string username)
        {
            return fSer.IsUserAdminOfForum(iD, username);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetSubForumIdByName/{subforumname}/{id}")]
        public int GetSubForumIdByName(string subforumname, int id)
        {
            return fSer.GetSubForumIdByName(subforumname, id);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetNotifications/{_forum1}/{userName}")]
        public List<Notification> GetNotifications(string _forum1, string userName)
        {
            return fSer.GetNotifications(_forum1, userName);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/PromoteToAdmin/{forumID}/{usernamePromoted}/{usernamePromoter}")]
        public bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter)
        {
            bool isSent = fSer.PromoteToAdmin(forumID, usernamePromoted, usernamePromoter);

            if (isSent)
            {
                SendNotifications(new List<string> { forumID + " " + usernamePromoted }, "Congratulations! You have been promoted :)", " promoted you to be an admin in this forum", usernamePromoter);
            }

            return isSent;
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/findThread/{forumid}/{subforumid}/{threadid}")]
        public MinimizedThread findThread(int forumid, int subforumid, int threadid)
        {
            return fSer.findThread(forumid, subforumid, threadid);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/EditPost/{_forum}/{_subForum}/{_threadId}/{poster}")]
        public bool EditPost(string _forum, string _subForum, int _threadId, [FromBody]List<string> postpath_contant, string poster)
        {
            try
            {
                List<int> postId = convertPath(postpath_contant[0]);
                string newContent = postpath_contant[1];

                int forumID = GetForumIdByName(_forum);
                MinimizedPost mp = findPost(forumID, GetSubForumIdByName(_subForum, forumID), _threadId, postId);
                var comments = GetAllComments(forumID, GetSubForumIdByName(_subForum, forumID), _threadId, postId);
                List<string> commenters = comments.Select(mp1 => forumID + " " + mp1.userNameWriter).Distinct().ToList();
                commenters.Remove(forumID + " " + poster);
                commenters.Remove(forumID + " " + "guest");
                commenters.Remove(forumID + " " + "Guest");
                commenters.Add(forumID + " " + mp.userNameWriter);
                commenters = commenters.Distinct().ToList();

                bool isSent = fSer.EditPost(_forum, _subForum, _threadId, postId, poster, newContent);

                if (isSent)
                {
                    SendNotifications(commenters, "Post edited", " edited the post : " + mp.Text, poster);
                }

                return isSent;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/CanGuestStartThread/{iD}")]
        public bool CanGuestStartThread(int iD)
        {
            return fSer.CanGuestStartThread(iD);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/CanGuestReplyToPost/{iD}")]
        public bool CanGuestReplyToPost(int iD)
        {
            return fSer.CanGuestReplyToPost(iD);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/findThreadByTitle/{forumid}/{subforumid}/{title}")]
        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            return fSer.findThreadByTitle(forumid, subforumid, title);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/findPost/{forumid}/{subforumid}/{threadid}")]
        public MinimizedPost findPost(int forumid, int subforumid, int threadid, [FromBody]List<int> postid)
        {
            return fSer.findPost(forumid, subforumid, threadid, postid);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/deleteThread/{forumid}/{subforumid}/{threadid}/{deleter}")]
        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            try
            {
                MinimizedThread mt = findThread(forumid, subforumid, threadid);
                var comments = GetAllComments(forumid, subforumid, threadid, new List<int> {0});
                List<string> commenters = comments.Select(mp => forumid + " " + mp.userNameWriter).Distinct().ToList();
                commenters.Remove(forumid + " " + deleter);
                commenters.Remove(forumid + " " + "guest");
                commenters.Remove(forumid + " " + "Guest");
                commenters.Add(forumid + " " + mt.UserNameWriter);
                commenters = commenters.Distinct().ToList();

                bool isSent = fSer.deleteThread(forumid, subforumid, threadid, deleter);

                if (isSent)
                {
                    SendNotifications(commenters, "Thread was deleted", " deleted the thread : " + mt.Title, deleter);
                }

                return isSent;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetVerificationCode/{forumid}/{username}")]
        public string GetVerificationCode(int forumid, string username)
        {
            return fSer.GetVerificationCode(forumid, username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/PromoteModerator/{username}/{forumid}/{subForumid}")]
        public string PromoteModerator(string username, int forumid, int subForumid)
        {
            string mesage = fSer.PromoteModerator(username, forumid, subForumid);

            if (mesage.Equals("success"))
            {
                SendNotifications(new List<string> { forumid + " " + username }, "Congratulations! You have been promoted :)", " you are now a moderator in : " + fSer.GetsubForumNameByID(forumid, subForumid), String.Empty);
            }

            return mesage;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/ResetToDefaultPolicy/{forumID}")]
        public void ResetToDefaultPolicy(int forumID)
        {
            fSer.ResetToDefaultPolicy(forumID);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/findThreadId/{forumiD}/{subForumiD}/{title}")]
        public int findThreadId(int forumiD, int subForumiD, string title)
        {
            return fSer.findThreadId(forumiD, subForumiD, title);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/replyToPost/{forum}/{subForum}/{Thread}/{creator}")]
        public List<int> replyToPost(int forum, int subForum, int Thread, [FromBody]List<string> postPath_text, string creator)
        {
            try
            {
                List<int> post = convertPath(postPath_text[0]);
                string text = postPath_text[1];
                MinimizedThread mt = findThread(forum, subForum, Thread);
                var comments = GetAllComments(forum, subForum, Thread, post);
                var commenters = comments.Select(mp => forum + " " + mp.userNameWriter).ToList().Distinct().ToList();
                commenters.Remove(forum + " " + creator);
                commenters.Remove(forum + " " + mt.UserNameWriter);
                commenters.Remove(forum + " guest");
                commenters.Remove(forum + " Guest");
                var threadTitle = mt.Title;

                List<int> postId = fSer.replyToPost(forum, subForum, Thread, post, creator, text);

                if (!creator.Equals(mt.UserNameWriter))
                {
                    // send notification for the user that started the thread
                    SendNotifications(new List<string> {forum + " " + mt.UserNameWriter},
                        "Someone commented on your thread", " commented in : " + threadTitle, creator);
                }
                // send notifications for all the user that commented on that specific post
                SendNotifications(commenters, "Someone wrote a post to", @" also commented in : " + threadTitle, creator);

                return postId;
            }
            catch (Exception)
            {
                return new List<int> {-1};
            }
        }

        private void SendNotifications(List<string> users, string title, string message, string invoker)
        {
            int forumid;
            string user;
            int read = 1, notRead = 0;
            foreach (string username in users)
            {
                try
                {
                    forumid  = Int32.Parse(username.Split(' ')[0]);
                    user = username.Split(' ')[1];
                    var a = ConnectionMapper._connections[username];
                    Hub.Clients.Client(a).ReceiveNotification(new string[] { title, invoker + message });
                    fSer.SaveNotification(forumid, user, title, invoker + message, read);
                }
                catch (Exception e)
                {
                    ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                    forumid = Int32.Parse(username.Split(' ')[0]);
                    user = username.Split(' ')[1];
                    fSer.SaveNotification(forumid, user, title, invoker + message, notRead);
                }
            }
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/replyToPostDebug/{forum}/{subForum}/{Thread}/{creator}")]
        public string replyToPostDebug(int forum, int subForum, int Thread,
            [FromBody] List<string> postPath_text, string creator)
        {
            List<int> post = convertPath(postPath_text[0]);
            string text = postPath_text[1];
            try
            {
                fSer.replyToPost(forum, subForum, Thread, post, creator, text);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return e.ToString();
            }
            return "ok";
        }

        private List<int> convertPath(string path)
        {
            string[] ids = path.Split(',');
            List<int> post = new List<int>();
            for (int i = 0; i < ids.Length; i++)
            {
                post.Add(Int32.Parse(ids[i]));
            }
            return post;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/sendPrivateMessage/{forumID}/{targetUsername}/{senderUserName}")]
        public bool sendPrivateMessage([FromBody]string text, int forumID, string targetUsername, string senderUserName)
        {
            bool ans = fSer.sendPrivateMessage(text, forumID, targetUsername, senderUserName);

            if (ans)
            {
                SendNotifications(new List<string> { forumID + " " + targetUsername }, "You have a new message", @" sent you a message", senderUserName);
            }

            return ans;
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/isUserModeratorOfSubForum/{forumiD}/{subForumiD}/{username}")]
        public bool isUserModeratorOfSubForum(int forumiD, int subForumiD, string username)
        {
            return fSer.isUserModeratorOfSubForum(forumiD, subForumiD, username);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getModTimeLimit/{forumiD}/{subForumiD}/{username}")]
        public DateTime? getModTimeLimit(int forumiD, int subForumiD, string username)
        {
            return fSer.getModTimeLimit(forumiD, subForumiD, username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/editModTimeLimit/{forum}/{subforum}/{promoted}")]
        public bool editModTimeLimit(int forum, int subforum, string promoted,[FromBody]DateTime newUntilWhen)
        {
            return fSer.editModTimeLimit(forum, subforum, promoted, newUntilWhen);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetSubForumModerators/{forumID}/{subForumID}")]
        public List<List<string>> GetSubForumModerators(int forumID, int subForumID)
        {
            List<Tuple<string, DateTime?>> moderators = fSer.GetSubForumModerators(forumID, subForumID);
            List<List<string>> moderatorsList = new List<List<string>>();
            foreach (var m in moderators)
            {
                List<string> n = new List<string>();
                n.Add(m.Item1);
                n.Add(m.Item2.ToString());
                moderatorsList.Add(n);
            }
            return moderatorsList;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/deletePost/{_forum1Id}/{_subForumId}/{threadId}/{username}")]
        public bool deletePost(int _forum1Id, int _subForumId, int threadId, [FromBody]List<int> postID, string username)
        {
            try
            {
                MinimizedPost mp = findPost(_forum1Id, _subForumId, threadId, postID);
                var comments = GetAllComments(_forum1Id, _subForumId, threadId, postID);
                List<string> commenters =
                    comments.Select(mp1 => _forum1Id + " " + mp1.userNameWriter).Distinct().ToList();
                commenters.Remove(_forum1Id + " " + username);
                commenters.Remove(_forum1Id + " " + "guest");
                commenters.Remove(_forum1Id + " " + "Guest");
                commenters.Add(_forum1Id + " " + mp.userNameWriter);
                commenters = commenters.Distinct().ToList();

                bool isSent = fSer.deletePost(_forum1Id, _subForumId, threadId, postID, username);

                if (isSent)
                {
                    SendNotifications(commenters, "Post was deleted", " deleted the thread : " + mp.Text, username);
                }

                return isSent;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/UnpromoteModerator/{forumId}/{subForumName}/{adminUnpromoting}/{moderatorToUnpromote}")]
        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            bool isSent = fSer.UnpromoteModerator(forumId, subForumName, adminUnpromoting, moderatorToUnpromote);

            if (isSent)
            {
                SendNotifications(new List<string> { forumId + " " + moderatorToUnpromote }, "Ohhhh! You have been unpromoted :(", " unpromoted you in subfrom : " + subForumName, adminUnpromoting);
            }

            return isSent;
        }


        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/UnpromoteAdmin/{forumid}/{superadminunpromoting}/{admintounpromote}")]
        public bool UnpromoteAdmin(int forumid, string superadminunpromoting, string admintounpromote)
        {
            bool isSent = fSer.UnpromoteAdmin(forumid, superadminunpromoting, admintounpromote);
            if (isSent)
            {
                SendNotifications(new List<string> { forumid + " " + admintounpromote }, "Fuck you!", " has unpromoted you to a worthless member!", superadminunpromoting);
            }
            return isSent;
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/deleteForum")]
        public void deleteForum([FromBody]string name)
        {
            fSer.deleteForum(name);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/deleteMember/{forumID}/{username}")]
        public void deleteMember(int forumID, string username)
        {
            fSer.deleteMember(forumID, username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/deleteSubForum")]
        public void deleteSubForum([FromBody]List<string> forum_subforum)
        {
            string forumName = forum_subforum[0];
            string subForumName = forum_subforum[1];
            fSer.deleteSubForum(forumName, subForumName);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetAllModerators/{forum}")]
        public List<string> GetAllModerators(int forum)
        {
            return fSer.GetAllModerators(forum);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/Unfriend/{forumid}/{username}/{username_to_unfriend}")]
        public void Unfriend(int forumid, string username, string username_to_unfriend)
        {
            fSer.Unfriend(forumid, username, username_to_unfriend);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetAllFriends/{forumID}/{username}")]
        public List<string> GetAllFriends(int forumID, string username)
        {
            return fSer.GetAllFriends(forumID, username);
        }
        

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/getAllUserNames/{forumID}")]
        public List<string> getAllUserNames(int forumID)
        {
            return fSer.getAllUserNames(forumID);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetAllComments/{forum}/{subForum}/{threadID}")]
        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, [FromBody]List<int> PostID)
        {
            return fSer.GetAllComments(forum, subForum, threadID, PostID);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetOpenningPost/{forum}/{subForum}/{ThreadID}")]
        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            return fSer.GetOpenningPost(forum, subForum, ThreadID);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/clearDB")]
        public void clearDB()
        {
            fSer.clearDatabase();
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/AttachObserver/{forumID}/{username}")]
        public void AttachObserver([FromBody]ObserverPattern.Observer growlNotifiactions, int forumID, string username)
        {
            fSer.AttachObserver(growlNotifiactions, forumID, username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/DetachObserver/{forumID}/{username}")]
        public void DetachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            fSer.DetachObserver(growlNotifiactions, forumID, username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetNotificationsString/{forumName}/{userName}/{getUnreadNotifications}")]
        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            return fSer.GetNotificationsString(forumName, userName, getUnreadNotifications);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/LogOut/{forumName}/{userName}")]
        public void LogOut(string forumname, string username)
        {
            fSer.LogOut(forumname, username);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/changePassword/{forumID}/{username}/{newPass}")]
        public bool changePassword(int forumID, string username, string newPass)
        {
            return fSer.changePassword(forumID, username, newPass);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/SendNewCodeToMail/{forumID}/{username}")]
        public bool SendNewCodeToMail(int forumID, string username)
        {
            return fSer.SendNewCodeToMail(forumID, username);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/GetAllRequestFriedns/{forumID}/{userName}")]
        public List<string> GetAllRequestFriedns(int forumID, string userName)
        {
            return fSer.GetAllRequestFriends(forumID,userName);
        }

        [HttpGet]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/IsUserSuperAdmin/{username}")]
        public bool IsUserSuperAdmin(string username)
        {
            return fSer.IsUserSuperAdmin(username);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/unSuspend/{forumID}/{friendUserName}")]
        public void unSuspend(int forumID, string friendUserName)
        {
            fSer.unSuspend(forumID, friendUserName);
        }

        [HttpPost]
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [Route("api/forum/suspend/{forumID}/{friendUserName}")]
        public void suspend(int forumID, string friendUserName)
        {
            fSer.suspend(forumID, friendUserName);
            var a = ConnectionMapper._connections[forumID+" "+friendUserName];
            Hub.Clients.Client(a).Suspend();
        }
    }
}