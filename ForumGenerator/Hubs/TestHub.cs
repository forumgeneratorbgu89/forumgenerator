﻿using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using ForumGenerator.Controllers;
using Logger;
using Microsoft.AspNet.SignalR;

namespace ForumGenerator.Hubs
{
    [HubName("ForumGeneratorHub")]
    public class TestHub : Hub
    {

        public void Register(string uniqueId)
        {
            try
            {
                ForumController.ConnectionMapper._connections.Add(uniqueId, Context.ConnectionId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                ForumController.ConnectionMapper._connections[uniqueId] = Context.ConnectionId;
            }
            
        }

        public void UnRegister(string uniqueId)
        {
            try
            {
                ForumController.ConnectionMapper._connections.Remove(uniqueId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
            }

        }


        public void AddFriend(string from, string to)
        {
            var a = ForumController.ConnectionMapper._connections[to];
            Clients.Client(a).sentFriendRequest(from);
        }
    }
}