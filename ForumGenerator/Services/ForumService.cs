﻿using DomainEntity.Models.UserManagement;
using ForumGenerator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using DomainEntity.Models.ForumManagement;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using Logger;
using ObserverPattern;

namespace ForumGenerator
{
    public class ForumService : IForumService
    {
        public IforumController fController;

        public ForumService()
        {
            fController = ForumControllerLocal.GetInstance();
        }

        public void clearDatabase()
        {
            fController.ClearDatabase();
        }

        public int AddForum(string name, string admin, ForumPolicy forumPolicy)
        {
            int ret = -1;
            try
            {
                ret = fController.AddForum(name, admin, forumPolicy);
            }
            catch (Exception e )
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return -1;
            }
            return ret;
        }

        public bool AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            bool ret = false;
            try
            {
                ret = (fController.AddSubForum(forumID, name, moderators, subject) != -1);
            }
            catch (Exception e )
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
            return ret;
        }

        public bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter)
        {
            try
            {
                User user;
                if (Postwriter.IndexOf("guest", StringComparison.OrdinalIgnoreCase) >= 0 ||
                    Postwriter.IndexOf("Guest", StringComparison.OrdinalIgnoreCase) >= 0) //if it's guest
                    user = new Guest();
                else user = fController.GetMember(Postwriter, forumID);
                if (user == null)
                {
                    return false;
                }
                return fController.AddThread(forumID, SubForumName, header, Postcontent, user) != -1;
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool CanGuestReplyToPost(int iD)
        {
            try
            {
                return fController.CanGuestReplyToPost(iD);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool CanGuestStartThread(int iD)
        {
            try
            {
                return fController.CanGuestStartThread(iD);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public void deleteForum(string name)
        {
            try
            {
                fController.deleteForum(name);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
            }
        }

        public void deleteMember(int forumID, string username)
        {
            try
            {
                fController.deleteMember(forumID, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
            }
        }

        public bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string _newUserToPromote)
        {
            bool isDeleted = false;
            try
            {
                if ((_newUserToPromote.IndexOf("guest", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (_newUserToPromote.IndexOf("Guest", StringComparison.OrdinalIgnoreCase) >= 0))
                    // if user is guest
                {
                    throw new Exception("Guest cannot delete post!");
                }
                if (fController.IsUserAdminOfForum(_forum1Id, _newUserToPromote)
                    || fController.findPost(_forum1Id, _subForumId, threadId, postID).userNameWriter.Equals(_newUserToPromote))
                    //if user is admin or the writer of the post her
                {
                    isDeleted = fController.DestructPost(_forum1Id, _subForumId, threadId, postID);
                }
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                isDeleted = false;
            }
            return isDeleted;
        }

        public void deleteSubForum(string forumName, string subForumName)
        {
            try
            {
                fController.deleteSubForum(forumName, subForumName);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
            }
        }

        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            try
            {
                return fController.deleteThread(forumid, subforumid, threadid, deleter);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool EditForumPolicy(int forumID, ForumPolicy policy)
        {
            bool isEdited = false;
            try
            {
                isEdited = fController.SetForumPolicy(forumID, policy);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                isEdited = false;
            }
            return isEdited;
        }

        public bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen)
        {
            try
            {
                return fController.editModTimeLimit(forum, subforum, promoted, newUntilWhen);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            bool isEdited = false;
            try
            {
                isEdited = fController.EditPost(_forum, _subForum, _threadId, postId, poster, newContent);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                isEdited = false;
            }
            return isEdited;
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            try
            {
                return fController.findPost(forumid, subforumid, threadid, postid);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public MinimizedThread findThread(int forumid, int subforumid, int threadid)
        {
            try
            {
                return fController.findThread(forumid, subforumid, threadid);

            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            try
            {
                return fController.findThreadByTitle(forumid, subforumid, title);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public int findThreadId(int forumiD, int subForumiD, string title)
        {
        try
            {
                return fController.findThreadId(forumiD, subForumiD, title);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return -1;
            }
        }

        public List<MinimizedForum> GetAllForums()
        {
            List<MinimizedForum> forums = null;
            try
            {
                var forumsDictionary = fController.GetAllForums();
                forums = new List<MinimizedForum>();
                foreach (var forumID in forumsDictionary.Keys)
                {
                    var mf = new MinimizedForum();
                    mf.ForumID = forumID;
                    mf.ForumName = forumsDictionary[forumID];
                    forums.Add(mf);
                }
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                forums = null;
            }
            return forums;
        }

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            List<MinimizedPost> posts = null;
            try
            {
                if (!fController.IsUserAdminOfForum(forumID, adminUserName))
                {
                    throw new Exception();
                }
                posts = fController.GetCommentsForUser(forumID, adminUserName, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                posts = null;
            }
            return posts;
        }

        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            List<MinimizedUserInSubForum> users = null;
            try
            {
                users = fController.getAllMinimizedUserInSubForum(forumid, subforumid);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                users = null;
            }
            return users;
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            List<string> friends = null;
            try
            {
                friends = fController.GetAllFriends(forumID, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                friends = null;
            }
            return friends;
        }

        public List<string> GetAllModerators(int forum)
        {
            List<string> moderatorsList = null;
            try
            {
                moderatorsList = fController.GetAllModerators(forum);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                moderatorsList = null;
            }
            return moderatorsList;
        }

        //public List<MinimizedPost> GetAllPosts(int forum, int subForum, int threadID)
        //{
        //    List<MinimizedPost> posts = null;
        //    try
        //    {
        //        posts = fController.GetAllPosts(forum, subForum, threadID);
        //    }
        //    catch (Exception)
        //    {
        //        posts = null;
        //    }
        //    return posts;

        //}

        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            List<MinimizedSubForum> subForums = null;
            try
            {
                subForums = fController.getAllSubForum(forumId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                subForums = null;
            }
            return subForums;
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            List<MinimizedThread> threads = null;
            try
            {
                threads = fController.GetAllThreads(forumId, subForumID);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                threads = null;
            }
            return threads;
        }

        public List<string> GetForumAdmins(int forumId)
        {
            List<string> admins = null;
            try
            {
                admins = fController.GetForumAdmins(forumId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                admins = null;
            }
            return admins;

        }

        public int GetForumIdByName(string forumName)
        {
            int ret = -1;
            try
            {
                ret = fController.GetForumIdByName(forumName);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return -1;
            }
            return ret;
        }

        public ForumPolicy GetForumPolicy(int forumId)
        {
            ForumPolicy policy = null;
            try
            {
                policy = fController.GetForumPolicy(forumId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                policy = null;
            }
            return policy;
        }

        public string getLastForumName()
        {
            string lastForumName = string.Empty;
            try
            {
                lastForumName = fController.getLastForumName();
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                lastForumName = string.Empty;
            }
            return lastForumName;
        }

        public string getLastSubForumName(int forumId)
        {
            string lastSubForumName = string.Empty;
            try
            {
                lastSubForumName = fController.getLastSubForumName(forumId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                lastSubForumName = string.Empty;
            }
            return lastSubForumName;
        }

        public string getLastThreadTitle(int forum1Id, string subForum)
        {
            string lastThreadTitle = string.Empty;
            try
            {
                lastThreadTitle = fController.getLastThreadTitle(forum1Id, subForum);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                lastThreadTitle = string.Empty;
            }
            return lastThreadTitle;
        }

        public string getLastUserName(int forumId)
        {
            string lastUserName = string.Empty;
            try
            {
                lastUserName = fController.getLastUserName(forumId);
            }
            catch (Exception e )
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                lastUserName = string.Empty;
            }
            return lastUserName;
        }

        public DateTime? getModTimeLimit(int forum, int subforum, string username)
        {
        try
            {
                return fController.getModTimeLimit(forum, subforum, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public List<Notification> GetNotifications(string _forum1, string userName)
        {
            List<Notification> notifications = null;
            try
            {
                notifications = fController.GetNotifications(_forum1, userName);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                notifications = null;
            }
            return notifications;
        }

        public int GetNumOfSubForumInForum(int forumId, string newUserName)
        {
            int num;
            try
            {
                if (!fController.IsUserAdminOfForum(forumId, newUserName))
                {
                    throw new Exception();
                }
                num = fController.getAllSubForum(forumId).Count;
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                num = -1;
            }
            return num;

        }

        public int getNumOfThreadsInForum(int forumId, string newUserName)
        {
            int num = 0;
            try
            {
                if (!fController.IsUserAdminOfForum(forumId, newUserName))
                {
                    throw new Exception();
                }
                var subForums = fController.getAllSubForum(forumId);
                foreach (var msf in subForums)
                {
                    num += fController.GetAllThreads(forumId, msf.ID).Count;
                }
            }
            catch (Exception e)
            {

                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                num = -1;
            }
            return num;
        }

        public string getSecondLastForumName()
        {
            string secondLastForumName = string.Empty;
            try
            {
                secondLastForumName = fController.getSecondLastForumName();
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                secondLastForumName = string.Empty;
            }
            return secondLastForumName;
        }

        public int GetSubForumIdByName(string subForumName, int forumId)
        {
            int ret = -1;
            try
            {
                ret = fController.GetSubForumIdByName(subForumName, forumId);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return -1;
            }
            return ret;
        }

        public List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID)
        {
            List<Tuple<string, DateTime?>> moderators = null;
            try
            {
                moderators = fController.GetSubForumModerators(forumID, subForumID);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                moderators = null;
            }
            return moderators;
        }

        public string getUsersEmail(int forumId, string lastUserName)
        {
            string email = string.Empty;
            try
            {
                email = fController.getUsersEmail(forumId, lastUserName);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                email = string.Empty;
            }
            return email;
        }

        public string GetVerificationCode(int forumID, string username)
        {
            try
            {
                return fController.GetVerificationCode(forumID, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return string.Empty;
            }
        }

        public bool initSuperAdmin(string username, string password)
        {
            try
            {
                return fController.initSuperAdmin(username, password) != null;
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool IsSystemIniaitlized()
        {
        return fController.IsSystemIniaitlized();
    }

        public bool IsUserAdminOfForum(int iD, string username)
        {
            bool ret = false;
            try
            {
                ret = fController.IsUserAdminOfForum(iD, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
            return ret;
        }

        public bool isUserModeratorOfSubForum(int forum, int subforum, string username)
        {
        try
            {
                return fController.isUserModeratorOfSubForum(forum, subforum, username);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public bool IsUserRegisteredToForum(int forum, string user)
        {
            try
            {
                return fController.IsUserRegisteredToForum(forum, user);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return false;
            }
        }

        public PrimitiveResult Login(int forumID, string username, string password)
        {
            try
            {
                return fController.Login(forumID, username, password);
            }
            catch (Exception e)
            {
                ForumGeneratorLogger.e(MethodBase.GetCurrentMethod().ToString(), e.Message);
                return null;
            }
        }

        public int NumOfForums()
        {
            int num = 0;
            try
            {
                num = fController.NumOfForums();
            }
            catch (Exception)
            {
                num = -1;
            }
            return num;
        }

        public string PromoteModerator(string username, int forumID, int subForumID)
        {
            string ret = "success";
            try
            {
                ForumPolicy fp = fController.GetForumPolicy(forumID);
                int maxModerators = fp.MaxModeratorsNumber;
                fController.AddModerator(username, forumID, subForumID);
            }
            catch (Exception e)
            {
                ret = e.Message;
            }
            return ret;
        }

        public bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter)
        {
            bool isPromoted = false;
            try
            {
                if (fController.IsUserAdminOfForum(forumID, usernamePromoter))
                {
                    isPromoted = fController.PromoteToAdmin(forumID, usernamePromoted, usernamePromoter);
                }
            }
            catch (Exception)
            {
                isPromoted = false;
            }
            return isPromoted;
        }

        public string Register(int forumID, string username, string password, string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive)
        {
            string ret = "success";
            try
            {
                fController.Register(forumID, username, password, firstName, lastName, birthYear, birthMonth, birthDay, eMail, offlineInteractive);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return ret;
        }

        public List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text)
        {
        try
            {
                return fController.replyToPost(forum, subForum, Thread, post, creator, text);
            }
            catch (Exception)
            {
                return new List<int> { -1 };
            }
        }

        public void ResetToDefaultPolicy(int iD)
        {
            try
            {
                fController.ResetToDefaultPolicy(iD);
            }
            catch (Exception)
            {

                return;
            }
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
        try
            {
                return fController.sendPrivateMessage(text, forumID, targetUsername, senderUserName);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool signInSuperAdmin(string username, string password)
        {
            bool isSignedIn = false;
            try
            {
                isSignedIn = fController.signInSuperAdmin(username, password);
            }
            catch (Exception)
            {
                isSignedIn = false;
            }
            return isSignedIn;
        }

        public bool signUpSuperAdmin(string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            bool isSignedUp = false;
            try
            {
                fController.signUpSuperAdmin(username, password, firstName, lastName, birthDate, eMail);
                isSignedUp = true;
                /*isSignedUp = fController.signUpSuperAdmin(string.Empty, username, password, firstName, lastName,
                    birthDate, eMail);*/
            }
            catch (Exception)
            {
                isSignedUp = false;
            }
            return isSignedUp;
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            bool isUnpromoted = false;
            try
            {
                if (!(fController.IsUserAdminOfForum(forumId, adminUnpromoting) 
                    || GetSubForumModerators(forumId, GetSubForumIdByName(subForumName, forumId)).Count > 1))
                {
                    throw new Exception();
                }
                isUnpromoted = fController.UnpromoteModerator(forumId, subForumName, adminUnpromoting,
                    moderatorToUnpromote);
            }
            catch (Exception)
            {
                isUnpromoted = false;
            }
            return isUnpromoted;
        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            MinimizedUserInSubForum user = null;
            try
            {
                user = fController.UserEnterToSubForum(forumID, subForrumID, userName);
            }
            catch (Exception)
            {
                user = null;
            }
            return user;
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            try
            {
                return fController.VerifyUser(forumid, username, code);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID)
        {
            List<MinimizedPost> posts = null;
            try
            {
                posts = fController.GetAllComments(forum, subForum, threadID, PostID);
            }
            catch (Exception)
            {
                posts = null;
            }
            return posts;
        }

        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            MinimizedPost mp = null;
            try
            {
                mp = fController.GetOpenningPost(forum, subForum, ThreadID);
            }
            catch (Exception)
            {
                mp = null;
            }
            return mp;
        }

        public List<string> getAllUserNames(int forumID)
        {
            List<string> un = null;
            try
            {
                un = fController.getAllUserNames(forumID);
            }
            catch (Exception)
            {
                un = null;
            }
            return un;
        }

        public void AttachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            try
            {
                fController.AttachObserver(growlNotifiactions, forumID, username);
            }
            catch (Exception e)
            {

            }
        }

        public void DetachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            try
            {
                fController.DetachObserver(growlNotifiactions, forumID, username);
            }
            catch (Exception)
            {
                return;
            }
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            try
            {
                return fController.GetNotificationsString(forumName, userName, getUnreadNotifications);
            }
            catch (Exception)
            {
                return new List<string[]>();
            }
        }

        public void LogOut(string forumname, string username)
        {
            try
            {
                fController.LogOut(forumname, username);
            }
            catch (Exception)
            {
                
            }
        }

        public void SaveNotification(int forumid, string user, string title, string v, int isRead)
        {
            try
            {
                fController.SaveNotification(forumid, user, title, v, isRead);
            }
            catch (Exception)
            {

            }
        }

        public List<string> GetAllRequestFriends(int forumId, string userName)
        {
            try
            {
                return fController.GetAllRequestFriends(forumId, userName);
            }
            catch (Exception)
            {

            }
            return null;
        }

        public void Unfriend(int forumid, string username, string usernameToUnfriend)
        {
            try
            {
                fController.Unfriend(forumid, username, usernameToUnfriend);
            }
            catch (Exception)
            {

            }
        }

        public int GetNumOfPosts(int forumid)
        {
            try
            {
                return fController.GetNumOfPosts(forumid);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            List<MinimizedUserInForum> users = null;
            try
            {
                users = fController.getAllMinimizedUserInForum(forumid);
            }
            catch (Exception)
            {
                users = null;
            }
            return users;
        }

        public string GetsubForumNameByID(int forumid, int subForumid)
        {
            try
            {
                return fController.GetsubForumNameByID(forumid, subForumid);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public bool SendNewCodeToMail(int forumID, string username)
        {
            try
            {
                return fController.SendNewCodeToMail(forumID, username);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool changePassword(int forumId, string username, string newPass)
        {
            try
            {
                return fController.changePassword(forumId, username, newPass);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote)
        {
            try
            {
                fController.UnpromoteAdmin(forumId, superAdminUnpromoting, adminToUnpromote);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool IsUserSuperAdmin(string username)
        {
            try
            {
                return fController.IsUserSuperAdmin(username);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool isUserSuspended(int forumID, string friendUserName)
        {
            try
            {
                return fController.isUserSuspended(forumID, friendUserName);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void suspend(int forumID, string friendUserName)
        {
            try
            {
                fController.suspend(forumID, friendUserName);
            }
            catch (Exception e){}
        }

        public void unSuspend(int forumID, string friendUserName)
        {
            try
            {
                fController.unSuspend(forumID, friendUserName);
            }
            catch (Exception e){}
        }
    }
}