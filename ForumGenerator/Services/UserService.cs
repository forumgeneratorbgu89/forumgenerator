﻿using ForumGenerator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;

namespace ForumGenerator
{
    public class UserService : IUserService
    {
        IuserController uController;

        public UserService()
        {
            uController = UserControllerLocal.GetInstance();
        }

        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            List<MinimizedMessage> messages = null;
            try
            {
                messages = uController.GetIncomingMessages(forumID, usernameTarget, userNameSender);
            }
            catch (Exception)
            {
                messages = null;
            }
            return messages;
        }

        public Tuple<string, string, string, string> GetUserDetails(int forumID, string userName)
        {
            Tuple<string, string, string, string> details = null;
            try
            {
                details = uController.GetUserDetails(forumID, userName);
            }
            catch (Exception)
            {
                details = null;
            }
            return details;
        }

        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {
            bool isAdded = false;
            try
            {
                isAdded = uController.AddFreind(senderUserName, reciverUserName, forumID);
            }
            catch (Exception)
            {
                isAdded = false;
            }
            return isAdded;
        }

        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            bool isConfirmed = false;
            try
            {
                isConfirmed = uController.ConfirmFriendRequest(senderUserName, reciverUserName, forumID);
            }
            catch (Exception)
            {
                isConfirmed = false;
            }
            return isConfirmed;
        }

        public bool IsUserApproved(string memberUserName, int forumID)
        {
            bool isApproved = false;
            try
            {
                isApproved = uController.IsUserApproved(memberUserName, forumID);
            }
            catch (Exception)
            {
                isApproved = false;
            }
            return isApproved;
        }
    }
}