﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using DomainEntity.MinimizedModels;

namespace ForumGenerator.Services
{
    public class UserServiceProxy : IUserService
    {
        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            var mm = new MinimizedMessage();
            mm.content = "if you are reading this, you are wasting your time.";
            return new List<MinimizedMessage> { mm };
        }

        public Tuple<string, string, string, string> GetUserDetails(int forumID, string userName)
        {
            return new Tuple<string, string, string, string>("aaa", "bbb", "aaa", "aaa@bbb.ccc");
        }

        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {
            return true;
        }

        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            return true;
        }

        public bool IsUserApproved(string memberUserName, int forumID)
        {
            return true;
        }
    }
}