﻿using DomainEntity.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.Models.ForumManagement;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using ObserverPattern;

namespace ForumGenerator
{
    public interface IForumService
    {
        // functions for forum statistics
        List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username);
        
        List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid);
        
        // username
        List<string> GetAllFriends(int forumID, string username);

        int GetForumIdByName(string forumName);

        int NumOfForums();
        
        //return -1 if not valid 
        int AddForum(string name, string admin, ForumPolicy forumPolicy);

        //if initialized system add only admin to moderator list
        bool AddSubForum(int forumID, string name, List<string> moderators, string subject);
        void clearDatabase();
        MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName);

        bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter);//not tested yet

        bool initSuperAdmin(string username, string password);

        bool IsSystemIniaitlized();

        //return error message or String.empty if everything is ok.
        string Register(int forumID, string username, string password,
                string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive);

        List<MinimizedForum> GetAllForums();

        ForumPolicy GetForumPolicy(int forumId);

        bool VerifyUser(int forumid, string username, string code);

        bool signUpSuperAdmin(String username, String password,
            string firstName, String lastName, DateTime birthDate, String eMail);

        bool signInSuperAdmin(String username, String password);

        string getLastSubForumName(int forumId);

        bool EditForumPolicy(int forumID, ForumPolicy policy);

        bool IsUserAdminOfForum(int iD, string username);

        bool IsUserRegisteredToForum(int forum, string user);

        PrimitiveResult Login(int forumID, string username, string password);

        int GetSubForumIdByName(string subForumName, int iD);

        List<MinimizedThread> GetAllThreads(int forumId, int subForumID);

        List<Notification> GetNotifications(string _forum1, string userNamegetUnreadNotifications);

        bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter);

        MinimizedThread findThread(int forumid, int subforumid, int threadid);

        bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent);

        bool CanGuestStartThread(int iD);

        bool CanGuestReplyToPost(int iD);

        MinimizedThread findThreadByTitle(int forumid, int subforumid, string title);

        MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid);

        bool deleteThread(int forumid, int subforumid, int threadid, string deleter);

        string GetVerificationCode(int forumID, string username);

        string PromoteModerator(string username, int forumID, int subForumID);

        void ResetToDefaultPolicy(int iD);

        int findThreadId(int forumiD, int subForumiD, string title);

        // -1 for error
        List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text);

        bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName);

        bool isUserModeratorOfSubForum(int iD1, int iD2, string username);

        DateTime? getModTimeLimit(int iD1, int iD2, string username);

        bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen);

        // username and time
        List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID);

        bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string _newUserToPromote);

        bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote);

        bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote);

        List<string> GetAllModerators(int forum);

        List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID);

        MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID);

        void deleteForum(string name);

        void deleteMember(int forumID, string username);

        void deleteSubForum(string forumID, string subForumName);

        List<MinimizedSubForum> getAllSubForum(int forumId);







        // returns the last user name registered to the forum - if no users return string empty
        string getLastUserName(int forumId);

        string getUsersEmail(int forumId, string lastUserName);

        // returns the last forum name - if forums return string empty
        string getLastForumName();

        // returns a list of usernames of the admins, no way it would be an empty list - minimum 1 admin
        List<string> GetForumAdmins(int forumId);

        string getSecondLastForumName();
        
        string getLastThreadTitle(int forum1Id, string subForum);

        int GetNumOfSubForumInForum(int forumId, string newUserName);

        int getNumOfThreadsInForum(int forumId, string newUserName);

        List<string> getAllUserNames(int forumID);

        void AttachObserver(Observer growlNotifiactions, int forumID, string username);

        void DetachObserver(Observer growlNotifiactions, int forumID, string username);

        List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications);

        void LogOut(string forumname, string username);

        void SaveNotification(int forumid, string user, string title, string v, int isRead);

        List<string> GetAllRequestFriends(int forumId, string userName);

        void Unfriend(int forumid, string username, string usernameToUnfriend);

        int GetNumOfPosts(int forumid);

        List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid);

        string GetsubForumNameByID(int forumid, int subForumid);

        bool SendNewCodeToMail(int forumID, string username);

        bool changePassword(int forumId, string username, string newPass);
        bool IsUserSuperAdmin(string username);
        bool isUserSuspended(int forumID, string friendUserName);

        void suspend(int forumID, string friendUserName);

        void unSuspend(int forumID, string friendUserName);
    }
}
