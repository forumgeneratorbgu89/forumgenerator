﻿using System;
using System.Collections.Generic;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using DomainEntity.ReturnedValues;
using ObserverPattern;

namespace ForumGenerator.Services
{
    public class ForumServiceProxy : IForumService
    {
        private static ForumServiceProxy instance;

        private ForumServiceProxy()
        {
            
        }

        public static IForumService getInstance()
        {
            if (instance == null)
            {
                instance = new ForumServiceProxy();
            }
            return instance;
        }

        public void clearDatabase()
        {
            // no imp in proxy
        }

        public int GetForumIdByName(string forumName)
        {
            return 1;
        }

        public int NumOfForums()
        {
            return 1;
        }

        public int AddForum(string name, string admin, ForumPolicy forumPolicy)
        {
            return 1;
        }

        public bool AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            return true;
        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            return new MinimizedUserInSubForum();
        }

        public bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter)
        {
            return true;
        }

        public bool initSuperAdmin(string username, string password)
        {
            return true;
        }

        public bool IsSystemIniaitlized()
        {
            return true;
        }

        public string Register(int forumID, string username, string password, string firstName, string lastName, int birthYear,
            int birthMonth, int birthDay, string eMail, int offlineInteractive)
        {
            return string.Empty;
        }

        List<MinimizedForum> IForumService.GetAllForums()
        {
            return new List<MinimizedForum>();
        }

        public ForumPolicy GetForumPolicy(int forumId)
        {
            return new ForumPolicy();
        }

        public string getUsersEmail(int forumId, string lastUserName)
        {
            return string.Empty;
        }

        public string getLastForumName()
        {
            return string.Empty;
        }

        public List<string> GetForumAdmins(int forumId)
        {
            return new List<string>{"admin", "kajabubu", "lalalalalala"};
        }

        public string getSecondLastForumName()
        {
            return string.Empty;
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            return true;
        }

        public bool signUpSuperAdmin(string username, string password, string firstName, string lastName, DateTime birthDate,
            string eMail)
        {
            return true;
        }
        public bool signInSuperAdmin(string username, string password)
        {
            return true;
        }

        public string getLastSubForumName(int forumId)
        {
            return string.Empty;
        }

        public bool EditForumPolicy(int forumID, ForumPolicy policy)
        {
            return true;
        }

        public bool IsUserAdminOfForum(int iD, string username)
        {
            return true;
        }

        public bool IsUserRegisteredToForum(int forum, string user)
        {
            return true;
        }

        PrimitiveResult IForumService.Login(int forumID, string username, string password)
        {
            return new PrimitiveResult();
        }

        public int GetSubForumIdByName(string subForumName, int iD)
        {
            return 1;
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            return new List<MinimizedThread>();
        }

        public List<Notification> GetNotifications(string _forum1, string userName)
        {
            return new List<Notification>();
        }

        public bool PromoteToAdmin(int iD, string usernamePromoted, string usernamePromoter)
        {
            return true;
        }

        MinimizedThread IForumService.findThread(int forumid, int subforumid, int threadid)
        {
            return new MinimizedThread();
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            return true;
        }

        public bool CanGuestStartThread(int iD)
        {
            return true;
        }

        public bool CanGuestReplyToPost(int iD)
        {
            return true;
        }

        MinimizedThread IForumService.findThreadByTitle(int forumid, int subforumid, string title)
        {
            return new MinimizedThread();
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            return new MinimizedPost();
        }

        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            return true;
        }

        public string GetVerificationCode(int forumID, string username)
        {
            return string.Empty;
        }

        public string PromoteModerator(string username, int forumID, int subForumID)
        {
            return "";
        }

        public void ResetToDefaultPolicy(int iD)
        {
            return;
        }

        public int findThreadId(int iD1, int iD2, string title)
        {
            return 1;
        }

        public List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text)
        {
            return new List<int> { 0, 1 };
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
            return true;
        }

        public bool isUserModeratorOfSubForum(int iD1, int iD2, string username)
        {
            return true;
        }

        public DateTime? getModTimeLimit(int iD1, int iD2, string username)
        {
            return DateTime.MaxValue;
        }

        public string getLastUserName(int forumId)
        {
            return string.Empty;
        }

        public bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen)
        {
            return true;
        }

        public List<Tuple<string, DateTime?>> GetSubForumModerators(int forumID, int subForumID)
        {
            return new List<Tuple<string, DateTime?>>();
        }

        public bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string _newUserToPromote)
        {
            return true;
        }

        public string getLastThreadTitle(int forum1Id, string subForum)
        {
            return string.Empty;
        }

        public int GetNumOfSubForumInForum(int forumId, string newUserName)
        {
            return 10;
        }

        public int getNumOfThreadsInForum(int forumId, string newUserName)
        {
            return 1;
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            return true;
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            return new List<string>();
        }

        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            var mp = new MinimizedPost();
            mp.Date = DateTime.Now;
            mp.PostId = 1;
            mp.Text = "heyheyhey";
            mp.userNameWriter = "admin";
            return mp;
        }

        public void deleteForum(string name)
        {
            return;
        }

        public void deleteMember(int forumID, string username)
        {
            return;
        }

        public void deleteSubForum(string name1, string name2)
        {
            return;
        }

        public List<string> GetAllModerators(int forum)
        {
            return new List<string>();
        }

        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID)
        {
            return new List<MinimizedPost>();
        }

        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            return new List<MinimizedSubForum>();
        }

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            var mp = new MinimizedPost(1, "aaa", username, DateTime.Now);
            mp.Text = "some reply here";
            return new List<MinimizedPost> { mp };
        }

        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            return new List<MinimizedUserInSubForum>();
        }

        public List<string> getAllUserNames(int forumID)
        {
            throw new NotImplementedException();
        }

        public void AttachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            throw new NotImplementedException();
        }

        public void DetachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            throw new NotImplementedException();
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            throw new NotImplementedException();
        }

        public void LogOut(string forumname, string username)
        {
            throw new NotImplementedException();
        }

        public void SaveNotification(int forumid, string user, string title, string v, int isRead)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllRequestFriends(int forumId, string userName)
        {
            throw new NotImplementedException();
        }

        public void Unfriend(int forumid, string username, string usernameToUnfriend)
        {
            throw new NotImplementedException();
        }

        public int GetNumOfPosts(int forumid)
        {
            throw new NotImplementedException();
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            throw new NotImplementedException();
        }

        public string GetsubForumNameByID(int forumid, int subForumid)
        {
            throw new NotImplementedException();
        }

        public bool SendNewCodeToMail(int forumID, string username)
        {
            throw new NotImplementedException();
        }

        public bool changePassword(int forumId, string username, string newPass)
        {
            throw new NotImplementedException();
        }

        public bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote)
        {
            throw new NotImplementedException();
        }

        public bool IsUserSuperAdmin(string username)
        {
            throw new NotImplementedException();
        }

        public bool isUserSuspended(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }

        public void suspend(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }

        public void unSuspend(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }
    }
}