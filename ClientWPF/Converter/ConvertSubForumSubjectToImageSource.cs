﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ClientWPF.Converter
{
    public class ConvertSubForumSubjectToImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value.ToString())
            {
                case "music":
                    return "../Icons/folder-blue-music-icon.png";
                case "news":
                    return "../Icons/News-Mic-iPhone-icon.png";
                case "computer":
                    return "../Icons/Portable-Computer-icon.png";
                case "sport":
                    return "../Icons/Sport-football-icon.png";
                default:
                    return "../Icons/Megaphone.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
