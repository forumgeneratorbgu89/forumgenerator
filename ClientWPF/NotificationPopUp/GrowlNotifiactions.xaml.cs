﻿using ClientWPF.NotificationPopUp;
using ClientWPF.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DomainEntity.Models.UserManagement;

namespace ClientWPF.NotificationPopUp
{
    /// <summary>
    /// Interaction logic for GrowlNotifiactions.xaml
    /// </summary>
    public partial class GrowlNotifiactions
    {
        private const byte MAX_NOTIFICATIONS = 4;
        private int count;
        public Notifications Notifications = new Notifications();
        private readonly Notifications buffer = new Notifications();
        private Dictionary<string, DateTime> ToSendPrivateMeassageNotification = new Dictionary<string, DateTime>();

        public GrowlNotifiactions()
        {
            InitializeComponent();
            NotificationsControl.DataContext = Notifications;
        }

        public class Observe : ObserverPattern.Observer
        {
            GrowlNotifiactions parent;
            private int forumID;
            private string username;

            public Observe(GrowlNotifiactions parent, int forumID, string username)
            {
                // here was attachobserver
                this.parent = parent;
                this.forumID = forumID;
                this.username = username;
            }

            public override void Update(string title, string message)
            {
                if (parent == null)
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                        parent = new GrowlNotifiactions(); // Call same setter, but on the UI thread
                    });
                }

                parent.AddNotification(new NotificationControl { Title = title, ImageUrl = "../Icons/user.png", Message = message });
            }

            public override void ShowPendingNotification(List<string[]> allNotifications)
            {
                if (parent == null)
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                        parent = new GrowlNotifiactions(); // Call same setter, but on the UI thread
                    });
                }
                foreach (string[] notification in allNotifications)
                {
                    parent.AddNotification(new NotificationControl { Title = notification[0], ImageUrl = "../Icons/user.png", Message = notification[1] });
                }
            }

        }

        public void AddNotification(NotificationControl notification)
        {
            string userNameSender = string.Empty;
            bool isPrivateMessage = notification.Title.Equals("You have a new message");
            try
            {
                userNameSender = notification.Message.Split(' ')[0];
                if (!ToSendPrivateMeassageNotification.ContainsKey(userNameSender) && isPrivateMessage)
                {
                    ToSendPrivateMeassageNotification.Add(userNameSender, DateTime.Now);
                }
                else if (isPrivateMessage)
                {
                    // here this user name is already in the dictionary and this is a private message, now check if it is at least 10 minutes
                    DateTime time = ToSendPrivateMeassageNotification[userNameSender];
                    if (time.AddMinutes(1) < DateTime.Now)
                    {
                        // now it says that 1 minutes past since the last time
                        ToSendPrivateMeassageNotification[userNameSender] = DateTime.Now;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception)
            {
                
            }
            notification.Id = count++;
            if (Notifications.Count + 1 > MAX_NOTIFICATIONS)
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                    buffer.Add(notification);
                });
            else

                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    Notifications.Add(notification);
                });


            App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
            {
                //Show window if there're notifications
                if (Notifications.Count > 0 && !IsActive)
                    Show();
            });
        }

        public void RemoveNotification(NotificationControl notification)
        {
            if (Notifications.Contains(notification))
                Notifications.Remove(notification);

            if (buffer.Count > 0)
            {
                Notifications.Add(buffer[0]);
                buffer.RemoveAt(0);
            }

            //Close window if there's nothing to show
            if (Notifications.Count < 1)
                Hide();
        }

        private void NotificationWindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height != 0.0)
                return;
            var element = sender as Grid;
            RemoveNotification(Notifications.First(n => n.Id == Int32.Parse(element.Tag.ToString())));
        }

        public void Update(string title, string message)
        {
            NotificationControl notification = new NotificationControl
            {
                Title = title
                ,
                ImageUrl = "Icons/News-Mic-iPhone-icon.png",
                Message = message
            };
            AddNotification(notification);
        }

    }
}
