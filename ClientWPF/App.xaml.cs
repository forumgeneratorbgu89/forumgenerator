﻿using ClientWPF.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Threading.Tasks;
using System.Windows;
using ClientWPF.NotificationPopUp;
using MahApps.Metro;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            
            
            var service =WebApiService.getInstance();
            if (!service.IsSystemIniaitlized())
            {
                InitializeWindow initialize = new InitializeWindow();
                initialize.Show();
            }
            else
            {
                SelectForumAndLogin win = new SelectForumAndLogin();
                win.Show();
            }


        }
    }
}
