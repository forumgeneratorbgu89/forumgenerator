﻿using ClientWPF.View;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for InitializeWindow.xaml
    /// </summary>
    public partial class InitializeWindow : MetroWindow
    {
        private string text;

        public InitializeWindow()
        {
            InitializeComponent();
            SignUpAdminPage log = new SignUpAdminPage();
            frame.NavigationService.Navigate(log);
        }

        public InitializeWindow(string userName)
        {
            InitializeComponent();
            frame.NavigationService.Navigate(new ForumCreationPage(userName));
        }

       

    }
}
