﻿using ClientWPF.View;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using ClientWPF.Service;
using ClientWPF.NotificationPopUp;
using System.Net.WebSockets;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using static ClientWPF.NotificationPopUp.GrowlNotifiactions;
using System.Windows.Threading;
using System.Threading;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.AspNet.SignalR.Client;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for ForumWindow.xaml
    /// </summary>

    public partial class ForumWindow : MetroWindow
    {
        List<string> nameList;
        Iservice service;
        private MinimizedUserInForum user;
        private MinimizedForum forum;
        private Observe growlNotifications;
        private IHubProxy _hub;

        public ForumWindow()
        {
            InitializeComponent();
            CenterPage();
            DisableBack();
            service = WebApiService.getInstance();
        }

        private void DisableBack()
        {
            back.IsEnabled = false;
            backImage.Opacity = 0.4;
        }


        private void EnableeBack()
        {
            back.IsEnabled = true;
            backImage.Opacity = 1;
        }


        private void CenterPage()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        public ForumWindow(MinimizedUserInForum result, MinimizedForum selectedItem)
        {
            InitializeComponent();
            service = WebApiService.getInstance();
            this.user = result;
            this.forum = selectedItem;           
            nameList = service.getAllUserNames(this.forum.ForumID);
            Forum.Content = forum.ForumName;
            UserName.Content = $"Hello {result.UserName} :)" ;
            txtAuto.TextChanged += new TextChangedEventHandler(txtAuto_TextChanged);
            ForumFirstPage firstPage = new ForumFirstPage(user,forum);
            frame.Navigate(firstPage);
        }


        #region TextBox-TextChanged-txtAuto
        private void txtAuto_TextChanged(object sender, TextChangedEventArgs e)
        {
            string typedString = txtAuto.Text;
            List<string> autoList = new List<string>();
            autoList.Clear();

            foreach (string item in nameList)
            {
                if (!string.IsNullOrEmpty(txtAuto.Text))
                {
                    if (item.ToLower().Contains(typedString))
                    {
                        autoList.Add(item);
                    }
                }
            }

            if (autoList.Count > 0)
            {
                lbSuggestion.ItemsSource = autoList;
                lbSuggestion.Visibility = Visibility.Visible;
            }
            else if (txtAuto.Text.Equals(""))
            {
                lbSuggestion.Visibility = Visibility.Collapsed;
                lbSuggestion.ItemsSource = null;
            }
            else
            {
                lbSuggestion.Visibility = Visibility.Collapsed;
                lbSuggestion.ItemsSource = null;
            }
        }
        #endregion

        #region ListBox-SelectionChanged-lbSuggestion
        private void lbSuggestion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSuggestion.ItemsSource != null)
            {
                lbSuggestion.Visibility = Visibility.Collapsed;
                txtAuto.TextChanged -= new TextChangedEventHandler(txtAuto_TextChanged);
                if (lbSuggestion.SelectedIndex != -1)
                {
                    txtAuto.Text = lbSuggestion.SelectedItem.ToString();
                    frame.NavigationService.Navigate(new UserProfileView(forum.ForumID, user.UserName, txtAuto.Text));
                }
                txtAuto.TextChanged += new TextChangedEventHandler(txtAuto_TextChanged);
            }
        }
        #endregion

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                frame.GoBack();
                //handler.Unregister();
            }
            catch (Exception)
            {

            }
        }



        private void frame_Navigated_1(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
                if (frame.CanGoBack)
                EnableeBack();
            else
                DisableBack();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            service.LogOut(forum.ForumName, user.UserName);
            _hub.Invoke("UnRegister", forum.ForumID + " " + user.UserName).Wait();
        }

        private async void ForumWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!user.State.Equals(UserTypeInForum.Guest))
            {
                await this.ShowMessageAsync("Session Login",
                    $"Hello {user.UserName} and welcome to forum {forum.ForumName}.{Environment.NewLine}" +
                     $"Your session password is {user.SessionPassword}{Environment.NewLine}." +
                        $"It has been copied to your clipboard please keep it for further logins."
);
                Clipboard.SetText(user.SessionPassword);
            }
            growlNotifications = new Observe(null, forum.ForumID, user.UserName);

            // get all pending notifications
            if (service.GetForumPolicy(forum.ForumID).Interactive == ForumPolicy.OFFLINE_NOTIFICATION
                || (service.GetForumPolicy(forum.ForumID).Interactive == ForumPolicy.USER_CHOISE_NOTIFICATIONS
                && user.OfflineInteractive == Member.OfflineInteractiveOn))
            {
                List<string[]> allNotifications = service.GetNotificationsString(forum.ForumName, user.UserName, true);
                growlNotifications.ShowPendingNotification(allNotifications);
            }

            string url = $"http://{WebApiService.host}:64760/";
            var connection = new HubConnection(url);
            _hub = connection.CreateHubProxy("ForumGeneratorHub");

            connection.Start().Wait();

            // here we configure how to react to notifications
            _hub.On("ReceiveNotification", notification =>
            {
                System.Threading.Thread t = new System.Threading.Thread(() =>
                {
                    System.Threading.Thread.CurrentThread.IsBackground = true;
                    string title = notification[0];
                    string message = notification[1];
                    growlNotifications.Update(title, message);
                });

                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                    frame.NavigationService.Refresh();
                });
            }


            );

            _hub.On("Suspend", () =>
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)async delegate
                {
                    await this.ShowMessageAsync("Suspend",
                        $"Due to our forum pollicy , one of our admins suspends you! :)"
                      );
                    this.Close();
                });
            }
            );

            _hub.Invoke("Register", forum.ForumID + " " + user.UserName).Wait();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Friends_Click(object sender, RoutedEventArgs e)
        {
            frame.NavigationService.Navigate(new FriendsPage(user, forum));
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            frame.NavigationService.Navigate(new NotificationPage(user, forum));
        }
        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                frame.NavigationService.Refresh();
            });
        }

        private void home_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                frame.NavigationService.Navigate(new ForumFirstPage(user, forum));
            });
        }
    }
}
