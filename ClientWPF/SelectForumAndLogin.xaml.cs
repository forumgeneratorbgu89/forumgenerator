﻿using ClientWPF.View;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for SelectForumAndLogin.xaml
    /// </summary>
    public partial class SelectForumAndLogin : MetroWindow
    {
        public SelectForumAndLogin()
        {
            InitializeComponent();
            Loaded += MyWindow_Loaded;
            DisableBack();
            var log = new LoginPage();
            frame.Navigate(log);
        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var log = new LoginPage();
            frame.Navigate(log);
        }
        private void DisableBack()
        {
            back.IsEnabled = false;
            backImage.Opacity = 0.4;
        }


        private void EnableeBack()
        {
            back.IsEnabled = true;
            backImage.Opacity = 1;
        }


        private void back_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                frame.GoBack();
                //handler.Unregister();
            }
            catch (Exception)
            {

            }
        }


        private void frame_Navigated_1(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            if (frame.CanGoBack)
                EnableeBack();
            else
                DisableBack();
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                frame.NavigationService.Refresh();
            });
        }
    }
}
