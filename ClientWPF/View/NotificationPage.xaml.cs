﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using ClientWPF.Service;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for NotificationPage.xaml
    /// </summary>
    public partial class NotificationPage : Page
    {
        private MinimizedForum forum;
        private MinimizedUserInForum user;


        public NotificationPage(MinimizedUserInForum user, MinimizedForum forum)
        {
            InitializeComponent();
            this.user = user;
            this.forum = forum;
            var result = WebApiService.getInstance().GetNotificationsString(forum.ForumName, user.UserName, false);

        }
        
        public class MinimizedNotification
        {
            public string Title { get; set; }
            public string Message { get; set; }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var result = WebApiService.getInstance().GetNotificationsString(forum.ForumName, user.UserName, false);
            List<MinimizedNotification> lst = result.Select(str => new MinimizedNotification() { Title = str[0], Message = str[1] }).ToList();
            notifications.ItemsSource = lst;
        }
    }
}
