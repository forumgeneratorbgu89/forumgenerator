﻿using ClientWPF.Service;
using DomainEntity.Models.ForumManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ForumPolicyView.xaml
    /// </summary>
    public partial class ForumPolicyView : Page
    {
        Iservice service;
        private readonly string online = "Online";
        private readonly string offLine = "Offline";
        private readonly string userChoise = "User Choise";
        public ForumPolicyView(int forumID)
        {
            InitializeComponent();
            Interactive.ItemsSource = new List<string>() { online, offLine, userChoise };
            Interactive.SelectedIndex = 1;
            this.forumID = forumID;
            service = WebApiService.getInstance();
            var policy = service.GetForumPolicy(forumID);
            canGuestPostSwitch.IsChecked = policy.GuestCanPost;
            canGuestStartThreadSwitch.IsChecked = policy.GuestCanStartThread;
            maxModerators.Value = policy.MaxModeratorsNumber;
            minPasswordLength.Value = policy.MinPasswordLetters;
            SeniotryNeedForEditing.Value =policy.MinSeniorityForMediatorToEditAPost;
            SeniotryNeedForRemoving.Value = policy.MinSeniorityForMediatorToRemoveAPost;
            minUsernameLength.Value = policy.MinUserNameLetters;
            passwordRequiresUppercase.IsChecked = policy.PasswordRequiredUpperLowerCase;
            passwordRequiresNumbers.IsChecked = policy.PasswordReuqiredNumbers;
            moderatorSuspensionTime.Value = policy.SuspendModeratorMinTime;
            memberSuspensionTime.Value = policy.SuspendMemberMinTime;
            adminSuspensionTime.Value = policy.SuspendAdminMinTime;
        }

        public int forumID { get; private set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ForumPolicy policty = new ForumPolicy()
            {
                GuestCanPost = canGuestPostSwitch.IsChecked == true,
                GuestCanStartThread = canGuestStartThreadSwitch.IsChecked == true,
                MaxModeratorsNumber = (int)maxModerators.Value,
                MinPasswordLetters = (int)minPasswordLength.Value,
                MinSeniorityForMediatorToEditAPost = (int)SeniotryNeedForEditing.Value,
                MinSeniorityForMediatorToRemoveAPost = (int)SeniotryNeedForRemoving.Value,
                MinUserNameLetters = (int)minUsernameLength.Value,
                PasswordRequiredUpperLowerCase = passwordRequiresUppercase.IsChecked == true,
                PasswordReuqiredNumbers = passwordRequiresNumbers.IsChecked == true,
                SuspendModeratorMinTime = (int)moderatorSuspensionTime.Value,
                SuspendMemberMinTime = (int)memberSuspensionTime.Value,
                SuspendAdminMinTime = (int)adminSuspensionTime.Value,
                Interactive = Interactive.SelectedIndex
            };
            service.EditForumPolicy(forumID,policty);
            NavigationService.GoBack() ;
        }
    }
}
