﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for RestorePassPage.xaml
    /// </summary>
    public partial class RestorePassPage : Page
    {
        MinimizedForum f;
        string username;
        Iservice service;

        public RestorePassPage(MinimizedForum forumSelected, string username)
        {
            InitializeComponent();

            this.f = forumSelected;
            this.username = username;
            service = WebApiService.getInstance();
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(code.Text) || string.IsNullOrEmpty(newPass.Password))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("Not valid", "Code and password shouldn't be empty.");
                return;
            }

            if (service.VerifyUser(f.ForumID, username, code.Text))
            {
                if (service.changePassword(f.ForumID, username, newPass.Password))
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("Success", "Password changed successfully.");
                    NavigationService.Navigate(new LoginPage());
                }
                else
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("No security", "Please insert a stronger password.");
            }
            else
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No confirmation", "confirmation code is incorret.");
            }
        }
    }
}
