﻿using ClientWPF.Service;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for LoginAdminPage.xaml
    /// </summary>
    public partial class SignUpAdminPage : Page
    {
        public SignUpAdminPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (await ValidData())
            {
                DateTime date = BirthDate.SelectedDate != null ? (DateTime)BirthDate.SelectedDate : DateTime.Now;
                WebApiService.getInstance().signUpSuperAdmin(Username.Text, password.Password, FirstName.Text
                    , LastName.Text, date, Email.Text);
                NavigationService.Navigate(new ForumCreationPage(Username.Text));
            }

        }

        private async Task<bool> ValidData()
        {
            if (String.IsNullOrEmpty(FirstName.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No name", "Please insert a first name.");
                return false;
            }
            if (String.IsNullOrEmpty(LastName.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No last name", "Please insert a last name.");
                return false;
            }
            if (String.IsNullOrEmpty(Username.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No username", "Please insert a username.");
                return false;
            }
            if (String.IsNullOrEmpty(Email.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No email", "Please insert a email.");
                return false;
            }
            if (String.IsNullOrEmpty(password.Password))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No password", "Please insert a password.");
                return false;
            }
            if (BirthDate.SelectedDate == null)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No birthday", "Please insert a birthday.");
                return false;
            }
            if (BirthDate.SelectedDate > DateTime.Today)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("ALIAN???", "nice to meet friend from future! :) please select you'r real birth date");
                return false;
            }
            return true;

        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
