﻿using ClientWPF.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for CreateSubForum.xaml
    /// </summary>
    public partial class SubForumCreation2 : Page
    {
        private Iservice service;
        public int forumID { get; private set; }
        public string userName { get; private set; }

        public SubForumCreation2(int forumID, string userName)
        {
            service = WebApiService.getInstance();
            this.forumID = forumID;
            this.userName = userName;
            InitializeComponent();
        }

        private string SelectedSubject()
        {
            if (MusicCheckBox.IsChecked == true)
                return "music";
            else if (SportCheckBox.IsChecked == true)
                return "sport";
            else if (ComputerCheckBox.IsChecked == true)
                return "computer";
            else if (NewsCheckBox.IsChecked == true)
                return "news";
            else
                return "default";
        }

        private async Task<bool> ValidData()
        {
            if (String.IsNullOrEmpty(forumName.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No sub forum name", "Please insert a sub forum name.");
                return false;
            }
            else if (SportCheckBox.IsChecked == false && MusicCheckBox.IsChecked == false && NewsCheckBox.IsChecked == false && ComputerCheckBox.IsChecked == false && GeneralCheckBox.IsChecked == false)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No sub forum subject", "Please insert a sub forum subject.");
                return false;
            }
            return true;
        }
        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (await ValidData())
            {
                var a = Application.Current.MainWindow;
                service.AddSubForum(forumID, forumName.Text, new List<string>() { userName }, SelectedSubject());
                NavigationService.GoBack();
            }

        }
    }
}
