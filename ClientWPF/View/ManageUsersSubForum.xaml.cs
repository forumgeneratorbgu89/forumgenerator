﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.ReturnedValues;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Threading;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ManageUsers.xaml
    /// </summary>
    public partial class ManageUsersSubForum : Page
    {
        private int forumid;
        private MinimizedSubForum subForum;
        private string username;
        private List<string> membersList;
        private List<string> moderatorsList;

        public ManageUsersSubForum(string username,int forumid,MinimizedSubForum subForum)
        {
            InitializeComponent();
            this.username = username;
            this.forumid = forumid;
            this.subForum = subForum;
           
        }

        private async void addModerator_Click(object sender, RoutedEventArgs e)
        {
            Button a = sender as Button;
            string user = a.DataContext as string;
            string result = WebApiService.getInstance().PromoteModerator(user, forumid, subForum.ID);
            if (result.Equals("success"))
            {
                Thread.Sleep(150);
                NavigationService.Refresh();
            }
            else
                MessageBox.Show(result);
        }

        private void removemoderator_Click(object sender, RoutedEventArgs e)
        {
            Button a = sender as Button;
            string moderator = a.DataContext as string;
            WebApiService.getInstance().UnpromoteModerator(forumid, subForum.Name, username, moderator);
            Thread.Sleep(150);
            NavigationService.Refresh();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            membersList = new List<string>();
            moderatorsList = new List<string>();
            List<MinimizedUserInSubForum> allUsers = WebApiService.getInstance().getAllMinimizedUserInSubForum(forumid, subForum.ID);
            foreach (MinimizedUserInSubForum user in allUsers)
            {
                if (user.State.Equals(UserTypeInSubForum.Member))
                    membersList.Add(user.UserName);
                else if (user.State.Equals(UserTypeInSubForum.Moderator))
                    moderatorsList.Add(user.UserName);
            }
            moderators.ItemsSource = moderatorsList;
            members.ItemsSource = membersList;
        }
    }

    
}
