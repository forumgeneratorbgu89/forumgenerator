﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for AddThread.xaml
    /// </summary>
    public partial class AddThread : Page
    {
        public MinimizedUserInSubForum user;
        public MinimizedForum forum;
        public MinimizedSubForum subForum;
        public Iservice service;


        public AddThread(MinimizedForum forum, MinimizedSubForum subForum, MinimizedUserInSubForum user)
        {
            InitializeComponent();
            this.forum = forum;
            this.subForum = subForum;
            this.user = user;
            this.service = WebApiService.getInstance();
        }

        private async void add_Click(object sender, RoutedEventArgs e)
        {
            if (await ValidData())
            {
                service.AddThread(forum.ForumID, subForum.Name, Title.Text, Content.Text, user.UserName);
                //SubForumView sf = new SubForumView(subForum, user, forum);
                //NavigationService.Navigate(sf);
                NavigationService.GoBack();
            }
        }

        private async Task<bool> ValidData()
        {
            if (String.IsNullOrEmpty(Title.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No title", "Please insert the thread's title.");
          
                return false;
            }
            if (string.IsNullOrEmpty(Content.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No content", "Please insert the thread's content.");
                return false;
            }
            return true;
        }
    }
}
