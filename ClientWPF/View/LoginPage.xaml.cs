﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using System.Threading;
using System.Windows.Threading;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for Page1.xaml/
    /// </summary>
    public partial class LoginPage : Page
    {
        public string forumName;
        Iservice service;
        public LoginPage()
        {
            InitializeComponent();
            service = WebApiService.getInstance();
            var forumList = service.GetAllForums();
            forumList.Add(new MinimizedForum() { ForumID = -1, ForumName = "Admin Page" });
            forums.ItemsSource = forumList;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {

            if (!await ValidData())
                return;
            int forumID = ((MinimizedForum)forums.SelectedItem).ForumID;
            if (forumID == -1)
            {
                if (service.signInSuperAdmin(username.Text, password.Password))
                {
                    InitializeWindow f = new InitializeWindow(username.Text);
                    f.ShowDialog();
                }
                else
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("Wrong details", "The inserted username of password are incorrect.");
                }
                return;
            }

            else
            {

                PrimitiveResult primitiveResult = service.Login(forumID, username.Text, password.Password);

                if (primitiveResult.Result != null && primitiveResult.Result.State.Equals(UserTypeInForum.NotApproved))
                {
                    NavigationService.Navigate(new EmailConfirmationWindow((MinimizedForum)forums.SelectedItem,
                        primitiveResult.Result));
                }
                else if (String.IsNullOrEmpty(primitiveResult.ErrorMessageg))
                {
                    Thread newWindowThread = new Thread(new ThreadStart(()=>
                                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate
                                {
                                    ForumWindow f = new ForumWindow(primitiveResult.Result, (MinimizedForum)forums.SelectedItem);
                                    f.Show();
                                    System.Windows.Threading.Dispatcher.Run();
                                })
                    ));
                    newWindowThread.SetApartmentState(ApartmentState.STA);
                    newWindowThread.IsBackground = true;
                    newWindowThread.Start();
                }
                else
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("error", primitiveResult.ErrorMessageg);
                }
            }
        }

        private void ThreadStartingPoint()
        {

        }

        private async Task<bool> ValidData()
        {
            if (forums.SelectedItem == null)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("Unselected forum", "Please select a forum.");
                return false;
            }
            if (String.IsNullOrEmpty(username.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("empty username", "Please insert a username.");
                return false;
            }
            if (string.IsNullOrEmpty(password.Password))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("empty password", "Please insert a password.");
                return false;
            }
            return true;
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (forums.SelectedItem == null)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No forum", "Please select a forum");
                return;
            }
            if (((MinimizedForum)forums.SelectedItem).ForumID == -1)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("failed", "registration unsuccessful.");
                return;
            }
            var reg = new RegisterToForum((MinimizedForum)forums.SelectedItem);
            NavigationService.Navigate(reg);
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (forums.SelectedItem == null)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No selection", "Please select a forum.");
                return;
            }
            if (((MinimizedForum)forums.SelectedItem).ForumID == -1)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No permission", "can't login to admin page as a user.");
                return;
            }
            ForumWindow f = new ForumWindow(new MinimizedUserInForum() { State = UserTypeInForum.Guest, UserName = "guest" }, (MinimizedForum)forums.SelectedItem);
            f.Show();

        }

        private async void RestorePass_Click(object sender, RoutedEventArgs e)
        {
            if(service.IsUserSuperAdmin(username.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("Super admin", "Super admin should call our office between 8:00-18:00: 052-646-4356.");
                return;
            }

            if (forums.SelectedItem == null)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No selection", "Please select a forum.");
                return;
            }
            else if (username.Text == "")
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No username", "Please enter a username");
                return;
            }
            else if (!service.IsUserApproved(username.Text, ((MinimizedForum)forums.SelectedItem).ForumID) == true)
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No user", "The requested user does not exsist.");
                return;
            }

            service.SendNewCodeToMail(((MinimizedForum)forums.SelectedItem).ForumID, username.Text);
            NavigationService.Navigate(new RestorePassPage((MinimizedForum)forums.SelectedItem, username.Text));
        }
    }
}
