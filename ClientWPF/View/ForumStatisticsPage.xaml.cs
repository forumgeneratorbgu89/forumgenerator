﻿using ClientWPF.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for MangeUsers.xaml
    /// </summary>
    public partial class ForumStatisticsPage : Page
    {
        private Iservice serivce;
        private string username;
        private bool isEntered;

        public ForumStatisticsPage(string username, int forumID)
        {
            InitializeComponent();
            this.forumID = forumID;
            this.username = username;
            serivce = WebApiService.getInstance();
            NumOfMessages.Content = $"Total Message is {serivce.GetNumOfPosts(forumID)}";
            isEntered = false;
        }

        public int forumID { get; private set; }

        private async void commentForUser_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(usernameLabel.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No user name", "Please insert user name.");
            }
           else if (!serivce.IsUserApproved(usernameLabel.Text, forumID))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No user", "The requested user does not exsist.");
            }
            else
            {
                var comments = WebApiService.getInstance().GetCommentsForUser(forumID, this.username, usernameLabel.Text);
                if (comments.Count == 0)
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("No comments", "There are no comments by this user");
                }
                else
                {
                    CommentForUser.ItemsSource = comments;
                }
            }
        }

        private void mouseEnterComment(object sender, MouseEventArgs e)
        {
            if (!isEntered)
            {
                usernameLabel.Text = "";
                isEntered = true;
            }
        }
    }
}
