﻿using ClientWPF.Service;
using ClientWPF.View;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ClientWPF
{
    /// <summary>
    /// Interaction logic for SubForum.xaml
    /// </summary>
    public partial class SubForumView : Page
    {
        public MinimizedUserInSubForum user;
        public List<MinimizedThread> threadsData;
        public MinimizedForum forum;
        public Iservice service;
        private MinimizedSubForum subforum;

        public SubForumView(MinimizedSubForum sf, MinimizedUserInForum user, MinimizedForum forum)
        {
            InitializeComponent();
            service = WebApiService.getInstance();
            if(user.State == UserTypeInForum.Guest)
            {
                this.user = new MinimizedUserInSubForum();
                this.user.State = UserTypeInSubForum.Guest;
                this.user.UserName = user.UserName;
            }
            else
                foreach (var u in service.getAllMinimizedUserInSubForum(forum.ForumID, sf.ID))
                {
                    if (u.UserName.Equals(user.UserName))
                        this.user = u;
                }
            this.forum = forum;
            this.subforum = sf;
            InitialData();   
        }

        private void InitialData()
        {
            subForumName.Content = subforum.Name;
            threadsData = service.GetAllThreads(forum.ForumID, subforum.ID);
            threads.ItemsSource = threadsData;
            updateVisibility();
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            if (row != null)
            {
                MinimizedThread curThread = row.DataContext as MinimizedThread;
                var post = service.GetOpenningPost(forum.ForumID, subforum.ID, curThread.ID);
                    PostPage th = new PostPage(post, curThread, user, forum, subforum, new List<int>());
                NavigationService.Navigate(th);
            }
            // Some operations with this row
        }

        private void ManagerUsers_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ManageUsersSubForum(user.UserName,forum.ForumID,subforum));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            AddThread at = new AddThread(forum, subforum, user);
            NavigationService.Navigate(at);
        }

        public void updateVisibility()
        {
            if(user.State == UserTypeInSubForum.Guest)
            {
                if (service.CanGuestStartThread(forum.ForumID) == false)
                    addThread.Visibility = Visibility.Hidden;
            }
            if (user.State != UserTypeInSubForum.Admin && user.State != UserTypeInSubForum.SuperAdmin)
            {
                RemoveSubForum.Visibility = Visibility.Hidden;
                ManagerUsers.Visibility = Visibility.Hidden;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            InitialData();
        }

        private void RemoveSubForum_Click(object sender, RoutedEventArgs e)
        {
            service.deleteSubForum(forum.ForumName, subforum.Name);
            Thread.Sleep(150);
            NavigationService.GoBack();
           
        }
    }
}
