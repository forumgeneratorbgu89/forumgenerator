﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Domain.Models.UserManagement;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ModeratorTimeDuration.xaml
    /// </summary>
    public partial class ModeratorTimeDuration : Page
    {
        public ModeratorTimeDuration()
        {
            InitializeComponent();

            Dictionary<Member, DateTime> a = new System.Collections.Generic.Dictionary< Member, DateTime>();
         
            Member g = new Member("666666666", "rani", "1234", "rani", "zinger", new DateTime(1989, 01, 01), "mail@gmail.com");
            Member b = new Member("777777777", "hemed", "1234", "tomer", "hemed", new DateTime(1989, 01, 08), "mail@gmail.com");

            a.Add(g, DateTime.Now);
            a.Add(b, DateTime.Now);


            comments.ItemsSource = a;
        }
    }
}
