﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using ClientWPF.Service;
using System.Threading;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for FriendsPage.xaml
    /// </summary>
    public partial class FriendsPage : Page
    {
        private MinimizedForum forum;
        private MinimizedUserInForum user;
        private Iservice service;

        public FriendsPage(MinimizedUserInForum user, MinimizedForum forum)
        {
            InitializeComponent();
            this.user = user;
            this.forum = forum;
            service = WebApiService.getInstance();
        }

        private void Unfriend_Click(object sender, RoutedEventArgs e)
        {
            var a = ((Button)sender).DataContext as string;
            service.Unfriend(forum.ForumID, user.UserName, a);
            Thread.Sleep(150);

            NavigationService.Refresh();
        }

        private async void ConfirmFriend_Click(object sender, RoutedEventArgs e)
        {
            var a = ((Button)sender).DataContext as string;
            if (!service.ConfirmFriendRequest(a, user.UserName, forum.ForumID))
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("Oops", "Something unexpected happend. please try again later.");
            else
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("Friendship", $"You and {a} now friends");
            }
            Thread.Sleep(150);
            NavigationService.Refresh();
        }

        private void FriendsPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            service = WebApiService.getInstance();
            var friendList = service.GetAllFriends(forum.ForumID, user.UserName);
            var requestList = service.GetAllRequestFriedns(forum.ForumID, user.UserName);
            requested.ItemsSource = requestList;
            friends.ItemsSource = friendList;
        }

        private void Chat_Click(object sender, RoutedEventArgs e)
        {
            var friendUserName = ((Button)sender).DataContext as string;
            NavigationService.Navigate(new chatPage(user.UserName, friendUserName, forum.ForumID));        
        }
    }
}
