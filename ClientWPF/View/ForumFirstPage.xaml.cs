﻿using DomainEntity.MinimizedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.ReturnedValues;
using ClientWPF.Service;
using System.Windows.Threading;
using System.Threading;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ForumFirstPage.xaml
    /// </summary>
    public partial class ForumFirstPage : Page
    {
        private MinimizedUserInForum user;
        private MinimizedForum forum;
        private Iservice service;
        
        public int counter { get; private set; }

        public ForumFirstPage()
        {
            InitializeComponent();

        }

        public ForumFirstPage(MinimizedUserInForum user, MinimizedForum forum)
        {
            InitializeComponent();
            service = WebApiService.getInstance();
            this.user = user;
            this.forum = forum;
        }

        private void ForumFirst_Loaded(object sender, RoutedEventArgs e)
        {
            List<MinimizedSubForum> subForums = service.getAllSubForum(forum.ForumID);
            items.ItemsSource = subForums;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            MinimizedSubForum subfroum = _button.DataContext as MinimizedSubForum;
            SubForumView sf = new SubForumView(subfroum, user, forum);
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (ThreadStart)delegate {
                NavigationService.Navigate(sf);
            });

        }

        private void Edit_Policy(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new EditForumPolicyView(forum.ForumID));
        }


        private void Create_Sub_Forum(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new SubForumCreation2(forum.ForumID, user.UserName));

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ForumStatisticsPage(user.UserName,forum.ForumID));
        }

        private void ManageUsers_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new ManageUsersForum(user.UserName, forum.ForumID ));
        }

        private void ForumFirst_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<MinimizedSubForum> subForums = service.getAllSubForum(forum.ForumID);
            items.ItemsSource = subForums;
            if (user.State == UserTypeInForum.Admin || user.State == UserTypeInForum.SuperAdmin)
                AdminOptions.Visibility = Visibility.Visible;
            if (user.State == UserTypeInForum.SuperAdmin)
                ManageUsers.Visibility = Visibility.Visible;
        }
    }
  

}
