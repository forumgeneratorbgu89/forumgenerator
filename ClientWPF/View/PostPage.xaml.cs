﻿using ClientWPF.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for PostPage.xaml
    /// </summary>
    public partial class PostPage : Page
    {
        public MinimizedThread curThread;
        public MinimizedForum forum;
        public List<MinimizedPost> commentsData;
        public Iservice service;
        public MinimizedPost curPost;
        public MinimizedSubForum sf;
        public List<int> prevPosts;
        public bool entered;
        private MinimizedUserInSubForum user;

        public PostPage(MinimizedPost curPost, MinimizedThread currThread, MinimizedUserInSubForum user, MinimizedForum forum, MinimizedSubForum subForum, List<int> _prevPosts)
        {
            //aaa
            InitializeComponent();
            service = WebApiService.getInstance();
            var allUsers = service.getAllMinimizedUserInSubForum(forum.ForumID, subForum.ID);
            this.user = user;
            this.curPost = curPost;
            this.curThread = currThread;
            this.sf = subForum;
            this.forum = forum;
            this.prevPosts = _prevPosts;
            prevPosts.Add(curPost.PostId);
        }


        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;

        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            HeadComment.Visibility = Visibility.Collapsed;
            HeadCommentEdit.Visibility = Visibility.Visible;
            save.Visibility = Visibility.Visible;
            edit.Visibility = Visibility.Collapsed;
            HeadCommentEdit.Text = HeadComment.Text;
        }

        private async void save_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(HeadCommentEdit.Text))
            {
                curPost.Text = HeadCommentEdit.Text;
                var result = service.EditPost(forum.ForumName, sf.Name, curThread.ID, prevPosts, user.UserName, HeadCommentEdit.Text);
                save.Visibility = Visibility.Collapsed;
                edit.Visibility = Visibility.Visible;
                HeadComment.Visibility = Visibility.Visible;
                HeadCommentEdit.Visibility = Visibility.Collapsed;
                NavigationService.Refresh();
            }
            else
            { await this.TryFindParent<MetroWindow>().ShowMessageAsync("No comment", "Please insert a comment."); }
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            if (prevPosts.Count() == 1)
                service.deleteThread(forum.ForumID, sf.ID, curThread.ID, user.UserName);
            else
                service.deletePost(forum.ForumID, sf.ID, curThread.ID, prevPosts, user.UserName);
            NavigationService.GoBack();
        }

        private void comments_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            MinimizedPost curPost = b.DataContext as MinimizedPost;
            PostPage postPage = new PostPage(curPost, curThread, user, forum, sf,new List<int>(prevPosts));
            NavigationService.Navigate(postPage);
        }

        private void newComment_TextChanged(object sender, TextChangedEventArgs e)
        {
            string comment = newComment.Text;
            service.replyToPost(forum.ForumID, sf.ID, curThread.ID, prevPosts, user.UserName, comment);
        }


        public void updateVisibility()
        {
            if (user.State.Equals(UserTypeInSubForum.Guest) || ( curPost.userNameWriter != user.UserName && !user.State.Equals(UserTypeInSubForum.Moderator) && !user.State.Equals(UserTypeInSubForum.Admin) && !user.State.Equals(UserTypeInSubForum.SuperAdmin)))
            {
                edit.Visibility = Visibility.Collapsed;
                delete.Visibility = Visibility.Collapsed;
            }

            if (user.State == UserTypeInSubForum.Guest)
            {
                if (service.CanGuestReplyToPost(forum.ForumID) == false)
                {
                    newComment.Visibility = Visibility.Collapsed;
                    Add_Comment.Visibility = Visibility.Hidden;
                    addCommentButton.Visibility = Visibility.Hidden;
                }
            }
        }

        private void mouseEnterComment(object sender, MouseEventArgs e)
        {
            if(!entered)
                newComment.Text = "";
            entered = true;
        }

        private void Add_Comment_Click(object sender, RoutedEventArgs e)
        {
            Add_Comment.Visibility = Add_Comment.Visibility.Equals(Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
            scroll.ScrollToBottom();
        }

        private async void Submit_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(newComment.Text))
            {
                string comment = newComment.Text;
                newComment.Text = "";
                service.replyToPost(forum.ForumID, sf.ID, curThread.ID, prevPosts, user.UserName, comment);
                Add_Comment.Visibility = Visibility.Collapsed;
                commentsData = service.GetAllComments(forum.ForumID, sf.ID, curThread.ID, prevPosts);
                comments.ItemsSource = commentsData;
            }
            else
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No comment", "Please insert a comment.");
        }

        void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                prevPosts.RemoveAt(prevPosts.Count);
                // TODO: whatever state management you're going to do
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            commentsData = service.GetAllComments(forum.ForumID, sf.ID, curThread.ID, prevPosts);
            updateVisibility();
            PublisherName.Content = curPost.userNameWriter;
            PublishDate.Content = curPost.Date;
            HeadComment.Text = curPost.Text;
            HeadCommentEdit.Text = curPost.Text;
            comments.ItemsSource = commentsData;
            entered = false;

        }
    }

}