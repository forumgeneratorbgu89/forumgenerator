﻿using ClientWPF.Service;
using DomainEntity.Models.ForumManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ForumCreationPage.xaml
    /// </summary>
    public partial class ForumCreationPage : Page
    {
        private Iservice service;
        private string userName;
        private readonly string online = "Online";
        private readonly string offLine = "Offline";
        private readonly string userChoise = "User Choise";
        public ForumCreationPage(string username)
        {
            InitializeComponent();
            Interactive.ItemsSource = new List<string>() { online, offLine, userChoise };
            Interactive.SelectedIndex = 1;
            service = WebApiService.getInstance();
            userName = username;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(forumName.Text))
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No name", "Please insert the forum's name.");

            }
            else
            {
                int id = service.AddForum(forumName.Text, userName, new ForumPolicy()
                {
                    GuestCanPost = canGuestPostSwitch.IsChecked == true,
                    GuestCanStartThread = canGuestStartThreadSwitch.IsChecked == true,
                    MaxModeratorsNumber = (int)maxModerators.Value,
                    MinPasswordLetters = (int)minPasswordLength.Value,
                    MinSeniorityForMediatorToEditAPost = (int)SeniotryNeedForEditing.Value,
                    MinSeniorityForMediatorToRemoveAPost = (int)SeniotryNeedForRemoving.Value,
                    MinUserNameLetters = (int)minUsernameLength.Value,
                    PasswordRequiredUpperLowerCase = passwordRequiresUppercase.IsChecked == true,
                    PasswordReuqiredNumbers = passwordRequiresNumbers.IsChecked == true,
                    SuspendModeratorMinTime = (int)moderatorSuspensionTime.Value,
                    SuspendMemberMinTime = (int)memberSuspensionTime.Value,
                    SuspendAdminMinTime = (int)adminSuspensionTime.Value,
                    Interactive = Interactive.SelectedIndex
                });
                if (id != -1)
                {
                    SubForumCreation sfc = new SubForumCreation(id, userName);
                    NavigationService.Navigate(sfc);
                }
                else
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("Duplicate error", "A forum with this very name is already exsists.");
                }
            }
        }
    }
}
