﻿using DomainEntity.MinimizedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientWPF.Service;


namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for chatPage.xaml
    /// </summary>
    public partial class chatPage : Page
    {
        public string username;
        public string friendUserName;
        public int forumID { get; private set; }
        List<MinimizedMessage> messegasList;
        public Iservice service;
        public chatPage(string username, string friendUserName, int forumID)
        {
            InitializeComponent();
            this.username = username;
            this.friendUserName = friendUserName;
            this.forumID = forumID;
            friend.Text = friendUserName;
            service = WebApiService.getInstance();
        }

        private void enter_Click(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SendMessage();
            }
        }

        private void SendMessage()
        {
            if (!String.IsNullOrEmpty(newMessage.Text))
            {
                string message = newMessage.Text;
                service.sendPrivateMessage(message, forumID, friendUserName, username);
                newMessage.Text = "";
                //NavigationService.Navigate(new chatPage(username, friendUserName, forumID));
                NavigationService.Refresh();
            }
        }

        private void Send_CLick(object sender, RoutedEventArgs e)
        {
            SendMessage();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            messegasList = service.GetIncomingMessages(forumID, username, friendUserName);
            messages.ItemsSource = messegasList;
            scroll.ScrollToBottom();
            newMessage.Focusable = true;
            Keyboard.Focus(newMessage);
        }

    }
}
