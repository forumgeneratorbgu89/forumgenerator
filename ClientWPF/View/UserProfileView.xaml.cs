﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.MinimizedModels;
using ClientWPF.Service;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for UserProfileView.xaml
    /// </summary>
    public partial class UserProfileView : Page
    {
        private string friendUserName;
        private string username;

        public int forumID { get; private set; }

        public UserProfileView(int forumID, string username, string friendUsername)
        {
            InitializeComponent();
            this.friendUserName = friendUsername;
            this.username = username;
            this.forumID = forumID;
        }

        private void updateVisibility()
        {
            if (WebApiService.getInstance().IsUserAdminOfForum(forumID, username))
            {
                if (!WebApiService.getInstance().isUserSusspend(forumID, friendUserName))
                {
                    suspendButton.Visibility = Visibility.Visible;
                    unSuspendButton.Visibility = Visibility.Hidden;

                }
                else
                {
                    unSuspendButton.Visibility = Visibility.Visible;
                    suspendButton.Visibility = Visibility.Hidden;
                }
            }
            if (!WebApiService.getInstance().getAllUserNames(forumID).Contains(username))
            {
                addFriendButton.Visibility = Visibility.Hidden;
                chatButton.Visibility = Visibility.Hidden;
                return;
            }
            if (friendUserName.Equals(username))
            {
                addFriendButton.Visibility = Visibility.Hidden;
                chatButton.Visibility = Visibility.Hidden;
            }
            if (WebApiService.getInstance().GetAllFriends(forumID, username).Contains(friendUserName))
            {
                addFriendButton.Visibility = Visibility.Hidden;
                confirmFriendButton.Visibility = Visibility.Hidden;
                removeFriendButton.Visibility = Visibility.Visible;
            }
            if (WebApiService.getInstance().GetAllRequestFriedns(forumID, friendUserName).Contains(username))
            {
                addFriendButton.Visibility = Visibility.Hidden;
                confirmFriendButton.Visibility = Visibility.Hidden;
                removeFriendButton.Visibility = Visibility.Hidden;
            }
            if (WebApiService.getInstance().GetAllRequestFriedns(forumID, username).Contains(friendUserName))
            {
                addFriendButton.Visibility = Visibility.Hidden;
                confirmFriendButton.Visibility = Visibility.Visible;
                removeFriendButton.Visibility = Visibility.Hidden;
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            WebApiService.getInstance().AddFreind(username, friendUserName, forumID);
            addFriendButton.Visibility = Visibility.Hidden;
            await this.TryFindParent<MetroWindow>().ShowMessageAsync("Request sent", $"friend request sent to {this.friendUserName}");
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new chatPage(username, friendUserName, forumID));
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            WebApiService.getInstance().Unfriend(forumID, username, friendUserName);
            removeFriendButton.Visibility = Visibility.Hidden;
            addFriendButton.Visibility = Visibility.Visible;
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            WebApiService.getInstance().ConfirmFriendRequest(friendUserName, username, forumID);
            confirmFriendButton.Visibility = Visibility.Hidden;
            removeFriendButton.Visibility = Visibility.Visible;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var friend = WebApiService.getInstance().GetUserDetails(forumID, friendUserName);
            FirstName.Content = friend.ElementAt(0);
            LastName.Content = friend.ElementAt(1);
            userName.Content = friendUserName;
            email.Content = friend.ElementAt(3);
            updateVisibility();
        }

        private void suspend_Click(object sender, RoutedEventArgs e)
        {
            WebApiService.getInstance().suspend(forumID, friendUserName);
            NavigationService.Refresh();
        }

        private void unSuspend_Click(object sender, RoutedEventArgs e)
        {
            WebApiService.getInstance().unSuspend(forumID, friendUserName);
            NavigationService.Refresh();

        }
    }
}
