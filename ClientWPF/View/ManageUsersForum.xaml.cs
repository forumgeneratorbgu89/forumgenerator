﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainEntity.ReturnedValues;
using System.Threading;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for ManageUsers.xaml
    /// </summary>
    public partial class ManageUsersForum : Page
    {
        private int forumid;
        private string username;
        private List<string> membersList;
        private List<string> adminList;

        public ManageUsersForum(string username, int forumid)
        {
            InitializeComponent();
            this.username = username;
            this.forumid = forumid;
        }

        private void addAdmin_Click(object sender, RoutedEventArgs e)
        {
            Button a = sender as Button;
            string user = a.DataContext as string;
            bool result = WebApiService.getInstance().PromoteToAdmin(forumid,user,username);
            if (result)
            {
                Thread.Sleep(150);
                NavigationService.Refresh();
            }
            else
                MessageBox.Show($"Can't promot {user} to admin");
        }

        private void removeAdmin_Click(object sender, RoutedEventArgs e)
        {
            Button a = sender as Button;
            string admin = a.DataContext as string;
            bool result = WebApiService.getInstance().UnpromoteAdmin(forumid, username, admin);
            if (result)
            {
                Thread.Sleep(150);
                NavigationService.Refresh();
            }
            else
                MessageBox.Show($"Can't unpromote {admin} to normal user");
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            membersList = new List<string>();
            adminList = new List<string>();
            List<MinimizedUserInForum> allUsers = WebApiService.getInstance().getAllMinimizedUserInForum(forumid);
            foreach (MinimizedUserInForum user in allUsers)
            {
                if (user.State.Equals(UserTypeInForum.Member))
                    membersList.Add(user.UserName);
                else if (user.State.Equals(UserTypeInForum.Admin))
                    adminList.Add(user.UserName);
            }
            admins.ItemsSource = adminList;
            members.ItemsSource = membersList;
        }
    }


}
