﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for EmailConfirmationWindow.xaml
    /// </summary>
    public partial class EmailConfirmationWindow : Page
    {
        MinimizedForum f;
        MinimizedUserInForum u;
        Iservice service;
        bool entered;

        public EmailConfirmationWindow(MinimizedForum forumSelected, MinimizedUserInForum minimizedUserInForum)
        {
            InitializeComponent();

            this.f = forumSelected;
            this.u = minimizedUserInForum;
            service = WebApiService.getInstance();
            entered = false;
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(code.Text))
            {
                if (service.VerifyUser(f.ForumID, u.UserName, code.Text))
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("Success", "Welcome to ForumSpring.");
                    NavigationService.Navigate(new LoginPage());
                }
                else
                {
                    await this.TryFindParent<MetroWindow>().ShowMessageAsync("No permission", "code is incorrect.");
                }
            }
            else
            {
                await this.TryFindParent<MetroWindow>().ShowMessageAsync("No code", "Please insert a code.");
            }
        }

        private void mouseEnterComment(object sender, MouseEventArgs e)
        {
            if (!entered)
                code.Text = "";
            entered = true;
        }
    }
}