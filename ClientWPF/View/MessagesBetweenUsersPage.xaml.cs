﻿using ClientWPF.Service;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWPF.View
{
    /// <summary>
    /// Interaction logic for MessagesBetweenUsersPage.xaml
    /// </summary>
    public partial class MessagesBetweenUsersPage : Page
    {
        Iservice service;
        public MessagesBetweenUsersPage()
        {
            InitializeComponent();
        }
        public MessagesBetweenUsersPage(string user,string user2)
        {
            InitializeComponent();
            service = WebApiService.getInstance();
            this.user = user;
            this.user2 = user2;
            List<MinimizedMessage> mess = new List<MinimizedMessage>()
            {
                new MinimizedMessage () {from=user2,to=user,content="היי?",date=DateTime.Today },
                new MinimizedMessage () {from=user2,to=user,content="ערה?",date=DateTime.Today },
                new MinimizedMessage () {from=user2,to=user,content="תעני",date=DateTime.Today },
                new MinimizedMessage () {from=user2,to=user,content="אני יודע שאת מחוברת",date=DateTime.Today },
                new MinimizedMessage () {from=user2,to=user,content="נווו",date=DateTime.Today },
                new MinimizedMessage () {from=user2,to=user,content="תעניייי",date=DateTime.Today },

            };
            items.Items.Clear();
            items.ItemsSource = mess;
        }

        public string user { get; private set; }
        public string user2 { get; private set; }
    }
}
