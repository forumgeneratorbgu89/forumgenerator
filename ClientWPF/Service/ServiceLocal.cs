﻿//using Domain.Models.ForumManagement;
//using Domain.Models.UserManagement;
//using ForumGenerator;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ClientWPF.Service
//{
//    public class ServiceLocal : Iservice
//    {
//        IForumService fService;
//        IUserService uService;

//        private static ServiceLocal instance;

//        public ServiceLocal()
//        {
//            fService = new ForumService();
//            uService = new UserService();
//        }

//        public bool IsSystemIniaitlized()
//        {
//            return fService.IsSystemIniaitlized();
//        }

//        //public bool AddFreind(int forumID, string senderUserName, string recieveUserName)
//        //{
//        //    Forum f = fController.GetForum(forumID);
//        //    Member sender = f.getMember(senderUserName);
//        //    Member recieve = f.getMember(recieveUserName);
//        //    return uController.AddFreind(sender, recieve);
//        //}

//        //public bool ConfirmFriendRequest(int forumID, string senderUserName, string reciveUserName)
//        //{
//        //    Forum f = fController.GetForum(forumID);
//        //    Member sender = f.getMember(senderUserName);
//        //    Member reciever = f.getMember(reciveUserName);
//        //    return uController.ConfirmFriendRequest(sender, reciever);
//        //}

//        public Guest createGuest()
//        {
//            return new Guest();
//        }

//        public Forum createNewForum(string forumName, SuperAdmin creator)
//        {
//            Forum forum = null;
//            if (fService.AddForum(forumName, creator.username))
//            {
//                int id = fService.GetForumIdByName(forumName);
//                if (id != -1)
//                    forum = new Forum(id, forumName);
//                else return null;
//            }
//            return forum;
//        }

//        public void removeSuperAdmin()
//        {
//            throw new NotImplementedException();
//        }

//        public SubForum createNewSubForum(string subForumName, Member creator, Forum mainForum)
//        {
//            if(subForumName.Equals(string.Empty))
//            {
//                return null;
//            }
//            if (fService.IsUserAdminOfForum(mainForum.ID, creator.username))
//            {
//                SubForum subForum = null;
//                List<string> moderator = new List<string>();
//                moderator.Add(creator.username);
//                if (fService.AddSubForum(mainForum.ID, subForumName, moderator, ""))
//                {
//                    List<Member> moderators = new List<Member>();
//                    moderators.Add(creator);
//                    int id = fService.GetSubForumIdByName(subForumName, mainForum.ID);
//                    if (id != -1)
//                        subForum = new SubForum(subForumName, string.Empty, id, mainForum, moderators);
//                    else return null;
//                }
//                return subForum;
//            }
//            else
//                return null;
//        }

//        public Thread createNewThread(string title, string text, User creator, SubForum subforum)
//        {
//            if (fService.findThreadByTitle(subforum.Forum.ID,subforum.ID, title) != false)
//            {
//                return null;
//            }
//            if (fService.CanGuestStartThread(subforum.Forum.ID) || creator.GetType() != typeof(Guest))
//            {
//                fService.AddThread(subforum.Forum.ID, subforum.Name, title, text, creator.getUsername());
//                if(fService.findThreadByTitle(subforum.Forum.ID, subforum.ID, title))
//                {
//                    return new Thread(fService.findThreadId(subforum.Forum.ID, subforum.ID, title), title, creator, text, subforum);
//                }
//            }

//            return null;
//        }

//        public bool CanGuestPostThread(int iD)
//        {
//            return fService.CanGuestStartThread(iD);
//        }

//        public ForumPolicy createPolicy()
//        {
//            return new ForumPolicy();
//        }

//        public bool deletePost(Post post, SubForum subforum, User deleter)
//        {
//            bool isDeleted = false;
//            if (deleter.GetType() == typeof(Guest))
//            {
//                isDeleted = false;
//            }
//            else
//            {
//                if (post.Writer.getUsername().Equals(deleter.getUsername()))
//                {
//                    fService.DestructPost(post.Thread.SubForum.Forum.ID, post.Thread.SubForum.ID, post.Thread.id, post.id);
//                    isDeleted = true;
//                }
//            }
//            return isDeleted;
//        }

//        public bool deleteThread(Thread thread, SubForum subforum, User deleter)
//        {
//            return fService.deleteThread(subforum.Forum.ID, subforum.ID, thread.id, deleter.getUsername());
//        }

//        public void editPolicy_SetGuestCanPostReplies(int iD, bool allow)
//        {
//            fService.SetGuestCanPostReplies(iD, allow);
//        }

//        public void editPolicy_SetGuestCanStartThread(int iD, bool allow)
//        {
//            fService.SetGuestCanStartThread(iD, allow);
//        }

//        public void editPolicy_SetMinPasswordLength(int iD, int newLength)
//        {
//            fService.SetMinPasswordLength(iD, newLength);
//        }

//        public void editPolicy_SetMinUsernameLength(int iD, int newLength)
//        {
//            fService.SetMinUsernameLength(iD, newLength);
//        }

//        //public Post[] getPostReplies(Post post, SubForum subforum)
//        //{
//        //    return fController.GetPostReplies(post);
//        //}

//        //public Post[] getThreadPosts(Thread thread, SubForum subforum)
//        //{
//        //    return fController.GetThreadPosts(thread);
//        //}

//        public bool isUserRegisteredToSubforum(User user, SubForum subforum)
//        {
//            bool isRegistered = false;
//            if (user.GetType() == typeof(Guest))
//            {
//                isRegistered = false;
//            }
//            else
//            {
//                isRegistered = fService.IsUserRegisteredToForum(subforum.Forum.ID, user.getUsername());
//            }
//            return isRegistered;
//        }

//        public bool loginToForum(string username, string password, Forum forum)
//        {
//            return fService.Login(forum.ID, username, password);
//        }

//        public int MinPasswordLength(int iD)
//        {
//            return fService.SetMinPasswordLength(iD);
//        }

//        public List<Notification> GetNotifications(string iD, string username)
//        {
//            return fService.GetNotifications(iD, username);
//        }

//        public SuperAdmin signInAsSuperAdmin(string name, string password)
//        {
//            SuperAdmin sAdmin = new SuperAdmin("0", name, password, string.Empty, string.Empty, DateTime.Now, string.Empty);
//            if (sAdmin.login(name, password))
//            {
//                return sAdmin;
//            }
//            else return null;
//        }

//        //public bool setForumPolicy(Forum forum, ForumPolicy policy)
//        //{
//        //    return fController.SetForumPolicy(forum.ID, policy);
//        //}

//        /*ofir
//         * promoted is moderator of this subforum
//         * promoter is ?
//         * 
//        */
//        public bool editModTimeLimit(Member promoted, SubForum subforum, Member promoter, DateTime newUntilWhen)
//        {
//            if (fService.isUserModeratorOfSubForum(subforum.Forum.ID, subforum.ID, promoted.username))
//                return subforum.editModTimeLimit(promoted, newUntilWhen);
//            else return false;
//        }

//        //ofir
//        public Nullable<DateTime> getModTimeLimit(Member member, SubForum subforum)
//        {
//            if (fService.isUserModeratorOfSubForum(subforum.Forum.ID, subforum.ID, member.username))
//                return fService.getModTimeLimit(subforum.Forum.ID, subforum.ID, member.username);
//            else return null;
//        }

//        //ofir
//        public bool makeModOfSubforumWithTimeLimit(Member promoted, SubForum subforum, Member promoter, DateTime untilWhen)
//        {
//            if (makeModOfSubforum(promoted, subforum, promoter))
//                return editModTimeLimit(promoted, subforum, promoter, untilWhen);
//            return false;
//        }

//        //avichay
//        public bool approveSignup(Member member)
//        {
//            fService.VerifyUser(member.ForumID, member.username, fService.GetVerificationCode(member.ForumID, member.username));
//            return uService.IsUserApproved(member.username, member.ForumID);
//        }

//        //avichay
//        public bool makeAdminOfForum(Member promoted, Forum forum, Member promoter)
//        {
//            bool isPromoted = false;
//            if (promoted.GetType() == typeof(Guest) || promoter.GetType() == typeof(Guest))
//            {
//                isPromoted = false;
//            }
//            else
//            {
//                isPromoted = fService.PromoteToAdmin(forum.ID, promoted.username, promoter.username);
//            }
//            return isPromoted;
//        }

//        public int MinUserNameLength(int iD)
//        {
//            return fService.MinUserNameLength(iD);
//        }

//        //maor
//        public bool findPost(Post post, SubForum subforum)
//        {
//            return fService.findPost(subforum.Forum.ID, subforum.ID, post.Thread.id, post.id);
//        }

//        //maor
//        public bool findThread(Thread thread, SubForum subforum)
//        {
//            return fService.findThread(subforum.Forum.ID, subforum.ID, thread.id);
//        }

//        //tom
//        public bool makeModOfSubforum(Member promoted, SubForum subforum, Member promoter)
//        {
//            if (promoted.ForumID != promoter.ForumID)
//                return false;
//            if (!fService.IsUserAdminOfForum(subforum.Forum.ID, promoter.username))
//                return false;
//            return fService.AddModerator(promoted.username, subforum.Forum.ID, subforum.ID);
//        }

//        //tom
//        public Post replyToPost(string text, User creator, Post parentPost)
//        {
//            if (!fService.CanGuestReplyToPost(parentPost.Thread.SubForum.Forum.ID) && creator.GetType() == typeof(Guest))
//            {
//                return null;
//            }
//            int postId = fService.replyToPost(parentPost.Thread.SubForum.Forum.ID, parentPost.Thread.SubForum.ID, parentPost.Thread.id, parentPost.id, creator.getUsername(), text);
//            return new Post(postId, creator, text, parentPost, parentPost.Thread);
//        }

//        //tom
//        public Message sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
//        {
//            if (fService.sendPrivateMessage(text, forumID, targetUsername, senderUserName))
//            {
//                return new Message(text, new Member("0", "aaa", "bb", "just", "forTest", DateTime.Now, "bad"), new Member("0", "aaa", "bb", "just", "forTest", DateTime.Now, "bad"));
//            }
//            return null;
//        }

//        public SuperAdmin initSuperAdmin(string username, string password)
//        {
//            if (fService.initSuperAdmin(username, password))
//            {
//                SuperAdmin sa = new SuperAdmin("0", username, password,
//            string.Empty, string.Empty, DateTime.MinValue, string.Empty);
//                sa.ForumID = -1;
//                return sa;
//            }
//            else return null;
//        }

//        public Member signUpToForum(Forum forum, string username, string password, string fname, string lname, int bdateY, int bdateM, int bdateD, string email)
//        {
//            if (fService.Register(forum.ID, "", username, password, fname, lname, bdateY, bdateM, bdateD, email))
//            {
//                Member m = new Member("0", username, password, fname, lname, new DateTime(bdateY, bdateM, bdateD), email);
//                m.ForumID = forum.ID;
//                return m;
//            }
//            else return null;
//        }

//        public void Destruct()
//        {
//            fService.Destruct();
//        }

//        public void ResetToDefaultPolicy(int iD)
//        {
//            fService.ResetToDefaultPolicy(iD);
//        }

//        public bool isUserAdmin(int forumID, string username)
//        {
//            return fService.IsUserAdminOfForum(forumID, username);
//        }

//        public bool findThreadByTitle(string threadTitle, SubForum subForum)
//        {
//            return fService.findThreadByTitle(subForum.Forum.ID, subForum.ID, threadTitle);
//        }

//        public List<string> GetAllModerators(int forum, string admin)
//        {
//            List<string> moderators = null;
//            if (isUserAdmin(forum, admin))
//            {
//                moderators = fService.GetAllModerators(forum);
//            }
//            return moderators;
//        }

//        public List<Post> GetAllPosts(int forum, string admin)
//        {
//            List<Post> posts = null;
//            if (isUserAdmin(forum, admin))
//            {
//                posts = fService.GetAllPosts(forum);
//            }
//            return posts;
//        }

//        public bool CanGuestReply(int id)
//        {
//            return fService.CanGuestReplyToPost(id);
//        }

//        public void deleteForum(Forum forum)
//        {
//            fService.deleteForum(forum.Name);
//        }

//        public void deleteMember(Member member)
//        {
//            fService.deleteMember(member.ForumID, member.username);
//        }

//        public void deleteSubForum(SubForum sb)
//        {
//            fService.deleteSubForum(sb.Forum.Name, sb.Name);
//        }

//        public void EditPost(string forum, string subforum, int threadid, int postID, string username, string newText)
//        {
//            fService.EditPost(forum, subforum, threadid, postID, username, newText);
//        }

//        public static Iservice getInstance()
//        {
//            if (instance == null)
//                instance = new ServiceLocal();
//            return instance;
//        }
//    }
//}