﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainEntity.ReturnedValues;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using ObserverPattern;
using ClientWPF.NotificationPopUp;

namespace ClientWPF.Service
{
    public class ServiceProxy : Iservice
    {
        private static ServiceProxy instance;

        private ServiceProxy()
        {

        }

        public static Iservice getInstance()
        {
            if (instance == null)
                instance = new ServiceProxy();
            return instance;
        }

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            return new List<MinimizedPost>()
            {
                new MinimizedPost() {Date=DateTime.Today,PostId=1,Text="Post1",userNameWriter="writer1" },
                new MinimizedPost() {Date=DateTime.Today,PostId=2,Text="Post2",userNameWriter="writer2" },
                new MinimizedPost() {Date=DateTime.Today,PostId=3,Text="Post3",userNameWriter="writer3" },
            };
        }

        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            var mm = new MinimizedMessage();
            mm.content = "if you are reading this, you are wasting your time.";
            return new List<MinimizedMessage> { mm };
        }

        public List<string> GetUserDetails(int forumID, string userName)
        {
            return new List<string> { "some", "sh", "Morbo", "someSheker@hell.com" };
        }

        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            return new List<MinimizedUserInSubForum>()
            {
                new MinimizedUserInSubForum() {UserName="member1",State=UserTypeInSubForum.Member },
                new MinimizedUserInSubForum() {UserName="member2",State=UserTypeInSubForum.Member },
                new MinimizedUserInSubForum() {UserName="member3",State=UserTypeInSubForum.Member },
                new MinimizedUserInSubForum() {UserName="moderator1",State=UserTypeInSubForum.Moderator },
                new MinimizedUserInSubForum() {UserName="moderator2",State=UserTypeInSubForum.Moderator },

            };
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            throw new NotImplementedException();
        }

        public int AddForum(string name, string admin, ForumPolicy forumPolicy)
        {
            return 1;
        }

        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {
            return true;
        }

        public void ConnectToHub(int forumid, string username, Observer observer)
        {
            throw new NotImplementedException();
        }

        public bool AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            return true;
        }

        public bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter)
        {
            return true;
        }

        public bool CanGuestReplyToPost(int iD)
        {
            return true;
        }

        public bool CanGuestStartThread(int iD)
        {
            return true;
        }

        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            return true;
        }

        public void deleteForum(string name)
        {
            return;
        }

        public void deleteMember(int forumID, string username)
        {
            return;
        }

        public bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string username)
        {
            return true;
        }

        public void deleteSubForum(string forumName, string subForumName)
        {
            return;
        }

        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            return true;
        }

        public bool EditForumPolicy(int forumID, ForumPolicy policy)
        {
            return true;
        }

        public bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen)
        {
            return true;
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            return true;
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            return new MinimizedPost()
            {
                Date = DateTime.Today,
                PostId = 1,
                Text = "post!",
                userNameWriter = "benrey"
            };
        }
    
        public MinimizedThread findThread(int forumid, int subforumid, int threadid)
        {
           return new MinimizedThread() { Date = DateTime.Today, ID = 1, Title = "Title", UserNameWriter = "writer" };
        }

        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            return new MinimizedThread()
            {
                Date = DateTime.Today,
                ID = 1,
                Title = "thread!",
                UserNameWriter= "benrey"
            };
        }

        public int findThreadId(int forumiD, int subForumiD, string title)
        {
            return 1;
        }

        public List<MinimizedForum> GetAllForums()
        {
            return new List<MinimizedForum>()
            {
                new MinimizedForum() {ForumID=1,ForumName="Tapuz" },
                new MinimizedForum() {ForumID=2,ForumName="Walla" },
                new MinimizedForum() {ForumID=3,ForumName="One" },
                new MinimizedForum() {ForumID=4,ForumName="Ynet" }
            };
        }

        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            return new MinimizedPost() { Date = DateTime.Today, PostId = 1, Text = "oopsss.....", userNameWriter = "someone" };
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            return new List<string> { "gargamel", "Moshe", "Benayoun", "Eli" };
        }


        public List<string> GetAllModerators(int forum)
        {
            return new List<string>()
            {
                "Lior","Danny"
            };
        }

        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID)
        {
            return new List<MinimizedPost>()
            {
                new MinimizedPost() {Date=DateTime.Today,PostId=1,userNameWriter="user",Text="Hello! :)" },
                new MinimizedPost() {Date=DateTime.Today,PostId=2,userNameWriter="sad user",Text="Why?!" },
                new MinimizedPost() {Date=DateTime.Today,PostId=1,userNameWriter="user",Text="good idea!! :D" },
            };
        }

        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            return new List<MinimizedSubForum>()
            {
                new MinimizedSubForum() {ID=1,Name="Maccabi Haifa",Subject="sport" },
                new MinimizedSubForum() {ID=1,Name="Nirvana",Subject="music" },
                new MinimizedSubForum() {ID=1,Name="0404",Subject="news" },
                new MinimizedSubForum() {ID=1,Name="Geeks",Subject="computer" },
                new MinimizedSubForum() {ID=1,Name="sub forum",Subject="general" }
            };
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            return new List<MinimizedThread> {
                new MinimizedThread() {Date=DateTime.Today,ID=1,Title="First Thread",UserNameWriter="Dani rop" },
                new MinimizedThread() {Date=DateTime.Today,ID=1,Title="Second Thread",UserNameWriter="Dana" },
                new MinimizedThread() {Date=DateTime.Today,ID=1,Title="Thirs Thread",UserNameWriter="Boris" },
            };
        }

        public List<string> getAllUserNames(int forumID)
        {
            return new List<string> { "Dudu","Dima","Sima","Avi","Daniel" };
        }

        public int GetForumIdByName(string forumName)
        {
            return 1;
        }

        public ForumPolicy GetForumPolicy(int forumId)
        {
            return new ForumPolicy()
            {
                GuestCanPost = true,
                GuestCanStartThread = true,
                MaxModeratorsNumber = 4,
                MinPasswordLetters = 2,
                MinSeniorityForMediatorToEditAPost = 5,
                MinSeniorityForMediatorToRemoveAPost = 4,
                MinUserNameLetters = 3,
                PasswordRequiredUpperLowerCase = true,
                PasswordReuqiredNumbers = false,
                SuspendAdminMinTime = 2,
                SuspendMemberMinTime = 4,
                SuspendModeratorMinTime = 6
            };
        }

        public string getLastSubForumName(int forumId)
        {
            return string.Empty;
        }

        public DateTime? getModTimeLimit(int forumiD, int subForumiD, string username)
        {
            return DateTime.Now;
        }

        public List<Notification> GetNotifications(string _forum1, string userName)
        {
            return new List<Notification>();
        }

        public int GetSubForumIdByName(string subForumName, int iD)
        {
            return 1;
        }

        public List<List<string>> GetSubForumModerators(int forumID, int subForumID)
        {
            return new List<List<string>> { new List<string> { "rani el-kingo", DateTime.Now.ToString() } };
        }

        public string GetVerificationCode(int forumID, string username)
        {
            return string.Empty;
        }

        public bool IsSystemIniaitlized()
        {
            return false;
        }

        public bool IsUserAdminOfForum(int iD, string username)
        {
            return true;
        }

        public bool IsUserApproved(string memberUserName, int forumID)
        {
            return true;
        }

        public bool isUserModeratorOfSubForum(int forumiD, int subForumiD, string username)
        {
            return true;
        }

        public PrimitiveResult Login(int forumID, string username, string password)
        {
            var pr = new PrimitiveResult();
            if (username.ToLower().Equals("admin"))
            {
                pr.ErrorMessageg = string.Empty;
                pr.Result = new MinimizedUserInForum()
                {
                    State = UserTypeInForum.Admin,
                    UserName = "userName"
                };
            }
            else
            {
                pr.ErrorMessageg = string.Empty;
                pr.Result = new MinimizedUserInForum()
                {
                    State = UserTypeInForum.Member,
                    UserName = "userName"
                };
            }
            return pr;
        }

        public int NumOfForums()
        {
            return 1;
        }

        public int GetNumOfPosts(int forumid)
        {
            throw new NotImplementedException();
        }

        public string PromoteModerator(string username, int forumID, int subForumID)
        {
            return "";
        }

        public bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter)
        {
            return true;
        }

        public string Register(int forumID, string username, string password, string firstName, string lastName,
            int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive)
        {
            return string.Empty;
        }

        public List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text)
        {
            return new List<int> { 0, 1 };
        }

        public void ResetToDefaultPolicy(int forumID)
        {
            return;
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
            return true;
        }

        public bool signInSuperAdmin(string username, string password)
        {
            return true;
        }

        public bool signUpSuperAdmin(string username, string password, string firstName, string lastName, DateTime birthDate, string eMail)
        {
            return true;
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            return true;
        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            return new MinimizedUserInSubForum()
            {
                UserName = "userNameInSubForum",
                State = UserTypeInSubForum.Member
            };
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            return true;
        }

        public void clearDB()
        {
            //
        }

        public void AttachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            throw new NotImplementedException();
        }

        public void DetachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            throw new NotImplementedException();
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            throw new NotImplementedException();
        }
        

        string Iservice.replyToPostdebug(int forumId, int subForumId, int threadId, List<int> posts, string memberUserName, string comment)
        {
            throw new NotImplementedException();
        }

        public void LogOut(string forumname, string username)
        {
            throw new NotImplementedException();
        }

        public void UnConnectToHub(int forumId, string userName)
        {
            throw new NotImplementedException();
        }

        public void Unfriend(int forumid, string username, string username_to_unfriend)
        {
            throw new NotImplementedException();
        }

        public bool changePassword(int forumID, string username, string newPass)
        {
            throw new NotImplementedException();
        }

        public void SendNewCodeToMail(int forumID, string text)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllRequestFriedns(int forumID, string userName)
        {
            throw new NotImplementedException();
        }

        string Iservice.PromoteModerator(string username, int forumID, int subForumID)
        {
            throw new NotImplementedException();
        }

        public bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote)
        {
            throw new NotImplementedException();
        }

        public bool IsUserSuperAdmin(string text)
        {
            throw new NotImplementedException();
        }

        public bool isUserSusspend(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }

        public void suspend(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }

        public void unSuspend(int forumID, string friendUserName)
        {
            throw new NotImplementedException();
        }
    }
}
