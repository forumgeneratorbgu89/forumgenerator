﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;
using System.Threading;
using DomainEntity.ReturnedValues;
using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using Newtonsoft.Json;
using ObserverPattern;
using Microsoft.AspNet.SignalR.Client;
using System.Security.Cryptography;
using static System.Net.Mime.MediaTypeNames;
using System.Windows.Threading;

namespace ClientWPF.Service
{
    public class WebApiService : Iservice
    {
        IHubProxy _hub;
        HttpClient client;
        HttpResponseMessage response;
        string port;
        // for localhost - localhost
        public static string host = "132.73.196.42";
        JavaScriptSerializer serializer;
        private static WebApiService instance;
        private Observer observer;

        public WebApiService()
        {
            client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(200)
            };
            client.BaseAddress = new Uri($"http://{host}:64760/");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            serializer = new JavaScriptSerializer();
        }

        private void OnData(string[] notification)
        {
            try
            {
                observer.Update(notification[0], notification[1]);
            }
            catch (Exception e)
            {

            }
        }


        public static Iservice getInstance()
        {
            if (instance == null)
                instance = new WebApiService();
            return instance;
        }



        public bool IsSystemIniaitlized()
        { 
            HttpResponseMessage response = client.GetAsync("api/forum/IsForumInitilazie").Result;

            bool users = response.Content.ReadAsAsync<bool>().Result;

            return users;

        }

        public PrimitiveResult Login(int forumId, string username, string password)
        {
            //string encPW = encrypt(password);

            HttpResponseMessage response = client.GetAsync($"api/forum/login/{forumId}/{username}/{password}").Result;

            PrimitiveResult res = response.Content.ReadAsAsync<PrimitiveResult>().Result;

            return res;
        }
        public List<MinimizedForum> GetAllForums()
        {
            
            HttpResponseMessage response = client.GetAsync("api/forum").Result;

            var forums = response.Content.ReadAsAsync<List<MinimizedForum>>().Result;

            return forums;
        }



        public List<MinimizedSubForum> getAllSubForum(int forumId)
        {
            
            HttpResponseMessage response = client.GetAsync($"api/forum/getAllSubForum/{forumId}").Result;

            var subForums = response.Content.ReadAsAsync<List<MinimizedSubForum>>().Result;

            return subForums;
        }

        public List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/getAllMinimizedUserInForum/{forumid}").Result;

            List<MinimizedUserInForum> fp = response.Content.ReadAsAsync<List<MinimizedUserInForum>>().Result;

            return fp;
        }

        public string Register(int forumID, string username, string password, string firstName, string lastName, int birthYear,
            int birthMonth, int birthDay, string eMail, int offlineInteractive)
        {
            //string encPW = encrypt(password);

            StringContent content = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/Register/{forumID}/{username}/{password}/{firstName}/{lastName}/{birthYear}/{birthMonth}/{birthDay}/{eMail}/{offlineInteractive}/", content).Result;

            string id = response.Content.ReadAsAsync<string>().Result;

            return id;
        }

        public int GetForumIdByName(string forumName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetForumIdByName/{forumName}").Result;

            var forumID = response.Content.ReadAsAsync<int>().Result;

            return forumID;
        }

        public int NumOfForums()
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/numOfForums").Result;

            var nums = response.Content.ReadAsAsync<int>().Result;

            return nums;
        }

        public int AddForum(string name, string admin, ForumPolicy forumPolicy)
        {
            List<Object> name_policy = new List<object> { name, forumPolicy};

            StringContent content = new StringContent(JsonConvert.SerializeObject(name_policy), Encoding.UTF8, "application/json");
            
            HttpResponseMessage response = client.PostAsync($"api/forum/AddForum/{admin}", content).Result;

            int id = response.Content.ReadAsAsync<int>().Result;

            return id;
        }

        public bool AddSubForum(int forumID, string name, List<string> moderators, string subject)
        {
            List<List<string>> moderators_name_subject = new List<List<string>>();
            moderators_name_subject.Add(moderators);
            moderators_name_subject.Add(new List<string>() { name});
            moderators_name_subject.Add(new List<string>() { subject });

            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(moderators_name_subject), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/AddSubForum/{forumID}", content).Result;

            bool ans = response.Content.ReadAsAsync<bool>().Result;

            return ans;

        }

        public MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/UserEnterToSubForum/{forumID}/{subForrumID}/{userName}").Result;

            MinimizedUserInSubForum user = response.Content.ReadAsAsync<MinimizedUserInSubForum>().Result;

            return user;
        }

        public bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter)
        {
            List<string> header_content = new List<string>();
            header_content.Add(header);
            header_content.Add(Postcontent);

            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(header_content), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/AddThread/{forumID}/{SubForumName}/{Postwriter}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }


        public ForumPolicy GetForumPolicy(int forumId)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetForumPolicy/{forumId}").Result;

            ForumPolicy fp = response.Content.ReadAsAsync<ForumPolicy>().Result;

            return fp;
        }

        public bool VerifyUser(int forumid, string username, string code)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/VerifyUser/{forumid}/{username}/{code}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;

            
        }

        public bool signUpSuperAdmin(string username, string password, string firstname, string lastname, DateTime birthdate, string email)
        {
            //string encPW = encrypt(password);

            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(birthdate), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/signUpSuperAdmin/{username}/{password}/{firstname}/{lastname}/{email}/", content).Result;
            
            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool signInSuperAdmin(string username, string password)
        {
            //string encPW = encrypt(password);

            HttpResponseMessage response = client.GetAsync($"api/forum/signInSuperAdmin/{username}/{password}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public string getLastSubForumName(int forumId)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/getLastSubForumName/{forumId}").Result;

            string result = response.Content.ReadAsAsync<string>().Result;

            return result;
        }

        public bool EditForumPolicy(int forumID, ForumPolicy policy)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(policy), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/EditForumPolicy/{forumID}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool IsUserAdminOfForum(int iD, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/IsUserAdminOfForum/{iD}/{username}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public int GetSubForumIdByName(string subForumName, int forumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetSubForumIdByName/{subForumName}/{forumID}").Result;

            int id = response.Content.ReadAsAsync<int>().Result;

            return id;
        }

        public List<MinimizedThread> GetAllThreads(int forumId, int subForumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetAllThreads/{forumId}/{subForumID}").Result;

            List<MinimizedThread> lst = response.Content.ReadAsAsync<List<MinimizedThread>>().Result;

            return lst;
        }

        public List<string> getAllUserNames(int forumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/getAllUserNames/{forumID}").Result;

            List<string> lst = response.Content.ReadAsAsync<List<string>>().Result;

            return lst;
        }

        public List<Notification> GetNotifications(string _forum1, string userName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetNotifications/{_forum1}/{userName}").Result;

            List<Notification> lst = response.Content.ReadAsAsync<List<Notification>
                >().Result;

            return lst;
        }

        public bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/PromoteToAdmin/{forumID}/{usernamePromoted}/{usernamePromoter}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public MinimizedThread findThread(int forumID, int subforumid, int threadid)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/findThread/{forumID}/{subforumid}/{threadid}").Result;

            MinimizedThread thread = response.Content.ReadAsAsync<MinimizedThread>().Result;

            return thread;
        }

        public bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent)
        {
            List<string> postpath_content = new List<string>();
            postpath_content.Add(getPath(postId));
            postpath_content.Add(newContent);
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postpath_content), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/EditPost/{_forum}/{_subForum}/{_threadId}/{poster}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool CanGuestStartThread(int ForumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/CanGuestStartThread/{ForumID}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool CanGuestReplyToPost(int ForumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/CanGuestReplyToPost/{ForumID}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public MinimizedThread findThreadByTitle(int forumid, int subforumid, string title)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/findThreadByTitle/{forumid}/{subforumid}/{title}").Result;

            MinimizedThread result = response.Content.ReadAsAsync<MinimizedThread>().Result;

            return result;
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postid), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/findPost/{forumid}/{subforumid}/{threadid}", content).Result;

            MinimizedPost result = response.Content.ReadAsAsync<MinimizedPost>().Result;

            return result;
        }

        public MinimizedPost findPost(int forumid, int subforumid, int threadid, int postid)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/findPost/{forumid}/{subforumid}/{threadid}/{postid}").Result;

            MinimizedPost result = response.Content.ReadAsAsync<MinimizedPost>().Result;

            return result;
        }

        public bool deleteThread(int forumid, int subforumid, int threadid, string deleter)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/deleteThread/{forumid}/{subforumid}/{threadid}/{deleter}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public string GetVerificationCode(int forumID, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetVerificationCode/{forumID}/{username}/").Result;

            string result = response.Content.ReadAsAsync<string>().Result;

            return result;
        }

        public string PromoteModerator(string username, int forumID, int subForumID)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");
            
            HttpResponseMessage response = client.PostAsync($"api/forum/PromoteModerator/{username}/{forumID}/{subForumID}", content).Result;

            string result = response.Content.ReadAsAsync<string>().Result;

            return result;
        }

        public void ResetToDefaultPolicy(int forumID)
        {
            client.GetAsync($"api/forum/ResetToDefaultPolicy/{forumID}");

        }

        public int findThreadId(int forumiD, int subForumiD, string title)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/findThreadId/{forumiD}/{subForumiD}/{title}").Result;

            int result = response.Content.ReadAsAsync<int>().Result;

            return result;
        }

        public List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text)
        {
            List<string> postPath_text = new List<string>();
            postPath_text.Add(getPath(post));
            postPath_text.Add(text);
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postPath_text), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/replyToPost/{forum}/{subForum}/{Thread}/{creator}",content).Result;

            List<int> result = response.Content.ReadAsAsync<List<int>>().Result;

            return result;
        }
        public string replyToPostdebug(int forum, int subForum, int Thread, List<int> post, string creator, string text)
        {
            List<string> postPath_text = new List<string>();
            postPath_text.Add(getPath(post));
            postPath_text.Add(text);
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postPath_text), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/replyToPostDebug/{forum}/{subForum}/{Thread}/{creator}", content).Result;

            string result = response.Content.ReadAsAsync<string>().Result;

            return result;
        }

        private string getPath(List<int> post)
        {
            string path = "";
            for (int i = 0; i < post.Count(); i++)
            {
                if (i == post.Count - 1) // the last i with no comma
                    path += post[i].ToString();
                else
                    path += post[i].ToString() + ",";
            }
            return path;
        }

        public bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(text), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/sendPrivateMessage/{forumID}/{targetUsername}/{senderUserName}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool isUserModeratorOfSubForum(int forumiD, int subForumiD, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/isUserModeratorOfSubForum/{forumiD}/{subForumiD}/{username}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public DateTime? getModTimeLimit(int forumiD, int subForumiD, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/getModTimeLimit/{forumiD}/{subForumiD}/{username}").Result;

            DateTime? result = response.Content.ReadAsAsync<DateTime?>().Result;

            return result;
        }

        public bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(newUntilWhen), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/editModTimeLimit/{forum}/{subforum}/{promoted}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public List<List<string>> GetSubForumModerators(int forumID, int subForumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetSubForumModerators/{forumID}/{subForumID}").Result;

            List<List<string>> result = response.Content.ReadAsAsync<List<List<string>>>().Result;

            return result;
        }

        public bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string username)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postID), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/deletePost/{_forum1Id}/{_subForumId}/{threadId}/{username}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/UnpromoteModerator/{forumId}/{subForumName}/{adminUnpromoting}/{moderatorToUnpromote}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/UnpromoteAdmin/{forumId}/{superAdminUnpromoting}/{adminToUnpromote}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public void deleteForum(string name)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(name), Encoding.UTF8, "application/json");

            client.PostAsync($"api/forum/deleteForum", content);

        }

        public void deleteMember(int forumID, string username)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            client.PostAsync($"api/forum/deleteMember/{forumID}/{username}", content);
        }

        public void deleteSubForum(string forumName, string subForumName)
        {
            List<string> forum_subforum = new List<string> { forumName , subForumName };

            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(forum_subforum), Encoding.UTF8, "application/json");

            client.PostAsync($"api/forum/deleteSubForum", content);
        }

        public List<string> GetAllModerators(int forum)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetAllModerators/{forum}").Result;

            List<string> result = response.Content.ReadAsAsync<List<string>>().Result;

            return result;
        }

        public List<string> GetAllFriends(int forumID, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetAllFriends/{forumID}/{username}").Result;

             List<string> result = response.Content.ReadAsAsync<List<string>>().Result;

            return result;
        }

        public void LogOut(string forumname, string username)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            client.PostAsync($"api/forum/LogOut/{forumname}/{username}", content);
        }

        public bool AddFreind(string senderUserName, string reciverUserName, int forumID)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/AddFreind/{senderUserName}/{reciverUserName}/{forumID}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/ConfirmFriendRequest/{senderUserName}/{reciverUserName}/{forumID}", content).Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public bool IsUserApproved(string memberUserName, int forumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/IsUserApproved/{memberUserName}/{forumID}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetCommentsForUser/{forumID}/{adminUserName}/{username}").Result;

            List<MinimizedPost> result = response.Content.ReadAsAsync<List<MinimizedPost>>().Result;

            return result;
        }

        public int GetNumOfPosts(int forumID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetNumOfPosts/{forumID}").Result;

            int result = response.Content.ReadAsAsync<int>().Result;

            return result;
        }

        public List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetIncomingMessages/{forumID}/{usernameTarget}/{userNameSender}").Result;

            List<MinimizedMessage> result = response.Content.ReadAsAsync<List<MinimizedMessage>>().Result;

            return result;
        }
        public List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/getAllMinimizedUserInSubForum/{forumid}/{subforumid}").Result;

            List<MinimizedUserInSubForum> fp = response.Content.ReadAsAsync<List<MinimizedUserInSubForum>>().Result;

            return fp;
        }

        public List<MinimizedPost> GetAllComments(int forum, int subForum, int threadID, List<int> PostID)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(PostID), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/GetAllComments/{forum}/{subForum}/{threadID}", content).Result;

            List<MinimizedPost> result = response.Content.ReadAsAsync<List<MinimizedPost>>().Result;

            return result;
        }

        public MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetOpenningPost/{forum}/{subForum}/{ThreadID}").Result;

            MinimizedPost post = response.Content.ReadAsAsync<MinimizedPost>().Result;

            return post;
        }

        public List<String> GetUserDetails(int forumID, string userName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetUserDetails/{forumID}/{userName}").Result;

            List<String> result = response.Content.ReadAsAsync<List<String>>().Result;

            return result;
        }

        public void clearDB()
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/clearDB", content).Result;
            
        }

        public void AttachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject
                (growlNotifiactions, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/AttachObserver/{forumID}/{username}", content).Result;
        }

        public void DetachObserver(Observer growlNotifiactions, int forumID, string username)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject
                            (growlNotifiactions, Formatting.Indented,
                                new JsonSerializerSettings
                                {
                                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                                }), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/DetachObserver/{forumID}/{username}", content).Result;
        }

        public List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/GetNotificationsString/{forumName}/{userName}/{getUnreadNotifications}", content).Result;

            List<string[]> result = response.Content.ReadAsAsync<List<string[]>>().Result;

            return result;
        }
        
        public void UnConnectToHub(int forumId, string userName)
        {
        }

        public void ConnectToHub(int forumid, string username, Observer observer)
        {
            this.observer = observer;
            string url = $"http://{host}:64760/";
            var connection = new HubConnection(url);
            _hub = connection.CreateHubProxy("ForumGeneratorHub");

            connection.Start().Wait();

            // here we configure how to react to notifications
            _hub.On("ReceiveNotification", notification =>
            {
                System.Threading.Thread t = new System.Threading.Thread(() =>
                {
                    System.Threading.Thread.CurrentThread.IsBackground = true;
                    string title = notification[0];
                    string message = notification[1];
                    observer.Update(title, message);
                });
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
            }
            
            
            );

            _hub.Invoke("Register", forumid + " " + username).Wait();
        }

        public void Unfriend(int forumid, string username, string username_to_unfriend)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/Unfriend/{forumid}/{username}/{username_to_unfriend}", content).Result;

        }

        public bool changePassword(int forumID, string username, string newPass)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/changePassword/{forumID}/{username}/{newPass}").Result;

            bool result = response.Content.ReadAsAsync<bool>().Result;

            return result;
        }

        public void SendNewCodeToMail(int forumID, string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/SendNewCodeToMail/{forumID}/{username}").Result;
        }

        public List<string> GetAllRequestFriedns(int forumID, string userName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/GetAllRequestFriedns/{forumID}/{userName}").Result;

            List<string> result = response.Content.ReadAsAsync<List<string>>().Result;

            return result;
        }

        public bool IsUserSuperAdmin(string username)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/IsUserSuperAdmin/{username}").Result;

            bool ret = response.Content.ReadAsAsync<bool>().Result;

            return ret;
        }

        public bool isUserSusspend(int forumID, string friendUserName)
        {
            HttpResponseMessage response = client.GetAsync($"api/forum/isUserSusspend/{forumID}/{friendUserName}").Result;

            bool ret = response.Content.ReadAsAsync<bool>().Result;

            return ret;
        }

        public void suspend(int forumID, string friendUserName)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/suspend/{forumID}/{friendUserName}", content).Result;
        }

        public void unSuspend(int forumID, string friendUserName)
        {
            StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync($"api/forum/unSuspend/{forumID}/{friendUserName}", content).Result;
        }
    }
}
