﻿using DomainEntity.MinimizedModels;
using DomainEntity.Models.ForumManagement;
using DomainEntity.Models.UserManagement;
using DomainEntity.ReturnedValues;
using System;
using System.Collections.Generic;
using ClientWPF.NotificationPopUp;
using ObserverPattern;

namespace ClientWPF.Service
{
    public interface Iservice
    {
        // functions for forum statistics
        List<MinimizedPost> GetCommentsForUser(int forumID, string adminUserName, string username);

        List<MinimizedMessage> GetIncomingMessages(int forumID, string usernameTarget, string userNameSender);

        // name, last, user, email
        List<string> GetUserDetails(int forumID, string userName);

        List<MinimizedUserInSubForum> getAllMinimizedUserInSubForum(int forumid, int subforumid);

        List<MinimizedUserInForum> getAllMinimizedUserInForum(int forumid);

        //return error message or String.empty if everything is ok.
        string Register(int forumID, string username, string password,
                string firstName, string lastName, int birthYear, int birthMonth, int birthDay, string eMail, int offlineInteractive);

        int GetForumIdByName(string forumName);

        List<MinimizedForum> GetAllForums();

        int NumOfForums();

        int GetNumOfPosts(int forumID);
        bool isUserSusspend(int forumID, string friendUserName);
        void AttachObserver(ObserverPattern.Observer growlNotifiactions, int forumID, string username);
        List<string> GetAllRequestFriedns(int forumID, string userName);
        void Unfriend(int forumid, string username, string username_to_unfriend);
        bool changePassword(int forumID, string username, string newPass);
        void DetachObserver(ObserverPattern.Observer growlNotifiactions, int forumID, string username);

        //return -1 if not valid 
        int AddForum(string name, string admin,ForumPolicy forumPolicy);

        //if initialized system add only admin to moderator list
        bool AddSubForum(int forumID, string name, List<string> moderators, string subject);

        MinimizedUserInSubForum UserEnterToSubForum(int forumID, int subForrumID, string userName);

        bool AddThread(int forumID, string SubForumName, string header, string Postcontent, string Postwriter);//not tested yet

        bool IsSystemIniaitlized();

        ForumPolicy GetForumPolicy(int forumId);        

        bool VerifyUser(int forumid, string username, string code);

        //SuperAdmin getSuperAdmin(); //what should be returned?

        bool signUpSuperAdmin(String username, String password,
            string firstName, String lastName, DateTime birthDate, String eMail);

        List<MinimizedSubForum> getAllSubForum(int forumId);

        bool signInSuperAdmin(String username, String password);

        List<string[]> GetNotificationsString(string forumName, string userName, bool getUnreadNotifications);

        string getLastSubForumName(int forumId);

        bool EditForumPolicy(int forumID, ForumPolicy policy);

        bool IsUserAdminOfForum(int iD, string username);

        PrimitiveResult Login(int forumID, string username, string password);

        int GetSubForumIdByName(string subForumName, int iD);

        List<MinimizedThread> GetAllThreads(int forumId, int subForumID);

        List<string> getAllUserNames(int forumID);

        List<Notification> GetNotifications(string _forum1, string userName);

        bool PromoteToAdmin(int forumID, string usernamePromoted, string usernamePromoter);

        //SuperAdmin initSuperAdmin(string username, string password); //what should be returned?

        MinimizedThread findThread(int forumid, int subforumid, int threadid);

        bool EditPost(string _forum, string _subForum, int _threadId, List<int> postId, string poster, string newContent);

        bool CanGuestStartThread(int iD);

        bool CanGuestReplyToPost(int iD);

        MinimizedThread findThreadByTitle(int forumid, int subforumid, string title);

        MinimizedPost findPost(int forumid, int subforumid, int threadid, List<int> postid);

        bool deleteThread(int forumid, int subforumid, int threadid, string deleter);

        string GetVerificationCode(int forumID, string username);

        string PromoteModerator(string username, int forumID, int subForumID);
        
        void ResetToDefaultPolicy(int forumID);

        int findThreadId(int forumiD, int subForumiD, string title);

        // -1 for error
        List<int> replyToPost(int forum, int subForum, int Thread, List<int> post, string creator, string text);

        bool sendPrivateMessage(string text, int forumID, string targetUsername, string senderUserName);
        void SendNewCodeToMail(int forumID, string username);
        void suspend(int forumID, string friendUserName);
        bool isUserModeratorOfSubForum(int forumiD, int subForumiD, string username);
        void unSuspend(int forumID, string friendUserName);
        DateTime? getModTimeLimit(int forumiD, int subForumiD, string username);

        bool editModTimeLimit(int forum, int subforum, string promoted, DateTime newUntilWhen);

        // username and time - List<Tuple<string, DateTime?>>
        List<List<string>> GetSubForumModerators(int forumID, int subForumID);

        bool deletePost(int _forum1Id, int _subForumId, int threadId, List<int> postID, string username);

        bool UnpromoteModerator(int forumId, string subForumName, string adminUnpromoting, string moderatorToUnpromote);

        bool UnpromoteAdmin(int forumId, string superAdminUnpromoting, string adminToUnpromote);
        void deleteForum(string name);

        void deleteMember(int forumID, string username);

        void deleteSubForum(string forumName, string subForumName);

        List<string> GetAllModerators(int forum);

        List<MinimizedPost> GetAllComments(int forum,int subForum, int threadID, List<int> PostID);
        bool IsUserSuperAdmin(string text);
        MinimizedPost GetOpenningPost(int forum, int subForum, int ThreadID);

        // username
        List<string> GetAllFriends(int forumID, string username);
        void LogOut(string forumname, string username);
        bool AddFreind(string senderUserName, string reciverUserName, int forumID);
        void ConnectToHub(int forumid, string username, Observer observer);
        bool ConfirmFriendRequest(string senderUserName, string reciverUserName, int forumID);

        bool IsUserApproved(string memberUserName, int forumID);

        void clearDB();
        string replyToPostdebug(int forumId, int subForumId, int threadId, List<int> posts, string memberUserName, string comment);
        void UnConnectToHub(int forumId, string userName);
    }
}
;